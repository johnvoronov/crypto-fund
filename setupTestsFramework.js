const path = require('path');
require('dotenv').config({
  path: path.resolve(__dirname, '.env.test')
});
const models = require('./server/models');
jest.setTimeout(30000);
