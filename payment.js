const rp = require('request-promise');
const qs = require('querystring');
const crypto = require('crypto');

const isLocal = false;
// const endpoint = isLocal ? 'http://localhost:8000' : 'https://fund-local.app-dev.dreamteam.tech/api';
const endpoint = 'https://lk.bit-lab.fund/api';

async function demoPayment(params, uri, amount, address) {
  const body = {
    txn_id: Date.now(),
    fee: 0.01,
    ipn_mode: 'hmac',
    status: 100,
    merchant: params.merchantId,
    address,
    amount
  };

  const hmac = crypto
    .createHmac('sha512', params.ipnSecret)
    .update(qs.stringify(body).replace(/%20/g, '+'))
    .digest('hex');

  return rp({ uri, body, method: 'POST', json: true, headers: { HMAC: hmac } });
}

async function bitbtc(amount, wallet) {
  await demoPayment({
    "publicKey": "a8fb0283d30853cc2f6d24d8acfb87620475b74bf13f8cee94a544a8e770570c",
    "secretKey": "48B9A0bFd743058145f65Ba2e0cD6438340b7f375098acc3b6BE1e4392150904",
    "ipnSecret": "JtL2k2ADEwdSZ9P9",
    "merchantId": "71c88fb095a5cf9a8d61c502473a8259"
  }, `${endpoint}/strategy/2/ipn`, amount, wallet);
}

async function bitusd(amount, wallet) {
  await demoPayment({
    "publicKey": "4258f3dd599dbfae2da4f208a82ad3ca696b618298e0be4460e9614105330230",
    "secretKey": "4679A29dc18135c1635A390822A64d03811FE133a793590d3B8dA29e0aE3EA4a",
    "ipnSecret": "JtL2k2ADEwdSZ9P9",
    "merchantId": "6007b2bf6c2a431e49237b33cedbf8f3"
  }, `${endpoint}/strategy/1/ipn`, amount, wallet);
}

async function deposit(amount, wallet) {
  await demoPayment({
    "publicKey": "1ced9e81ac8311763cd11027e967384788ab47077487ab7570fbc9fb530a79bf",
    "secretKey": "94e560833447782BeCC83b3Ba7e36Cd1Ad3fa6eF38fD67d6f34016145cCEB219",
    "ipnSecret": "JtL2k2ADEwdSZ9P9",
    "merchantId": "6b2740a31dac0fa11da5adcfdeecb7cb"
  }, `${endpoint}/strategy/3/ipn`, amount, wallet);
}

async function main(amount, wallet) {
  await demoPayment({
    "publicKey": "204c9b4bff85efd11679dea6906d8945edc75da14df19a40bd39eb7799bffe22",
    "secretKey": "d0db852799D1f62c18f91b45D3c78f6811972c2b6ab88c413f3dd6410134Ac8A",
    "ipnSecret": "JtL2k2ADEwdSZ9P9",
    "merchantId": "e572ca27fe97537607b453a1b4fb5d4e"
  }, `${endpoint}/ipn`, amount, wallet);
}

// bitbtc(2, '32veA6KpdMR8u3ne757AtUaezese6nPv83');
// bitusd(1, '397EWov4FwSBf7JFGnUWb9APK11oFn4ekz');
// deposit(1, '335V9S9BWmXQbdLm9dZH7fcN3XEgAcTpYA');
// main(0.7504340153363244, '36p59LMNCh2dPea3fmkBQTvPc5hgcceajA');
