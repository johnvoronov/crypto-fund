require('./bootstrap');
if (process.env.LEAD_NODE || false) {
  require('./cron');
}
const app = require('./app');

app.listen(process.env.PORT || 8000, err => {
  if (err) {
    throw err;
  }
  console.log('started');
});
