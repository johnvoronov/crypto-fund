module.exports = {
  'swagger': '2.0',
  'info': {
    'version': '1.0.0',
    'title': 'ic',
    'description': 'hypecorp: invest-cabinet',
    'license': {
      'name': 'MIT',
      'url': 'https://opensource.org/licenses/MIT'
    }
  },
  'host': 'localhost:8000',
  'basePath': '/',
  'schemes': [
    'http',
    'https'
  ],
  'consumes': [
    'application/json'
  ],
  'produces': [
    'application/json'
  ],
  'securityDefinitions': require('./security'),
  'paths': {
    ...require('./routes/balance'),
    ...require('./routes/article'),
    ...require('./routes/faq'),
    ...require('./routes/user'),
    ...require('./routes/strategy'),
    ...require('./routes/payment'),
    ...require('./routes/transaction'),
    ...require('./routes/settings'),
    ...require('./routes/referral'),
    ...require('./routes/wallet'),
    ...require('./routes/twofactor'),
    ...require('./routes/currency'),
    ...require('./routes/report'),
    ...require('./routes/chart'),
    ...require('./routes/admin/currency'),
    ...require('./routes/admin/payment'),
    ...require('./routes/admin/referral'),
    ...require('./routes/admin/user'),
    ...require('./routes/admin/transaction'),
    ...require('./routes/admin/strategy'),
    ...require('./routes/admin/settings'),
    ...require('./routes/admin/checkpoint'),
    ...require('./routes/admin/withdraw')
  },
  'definitions': {
    'Balance': require('./definitions/balance'),
    'Jwt': require('./definitions/jwt'),
    'Report': require('./definitions/report'),
    'Chart': require('./definitions/chart'),
    'Referral': require('./definitions/referral'),
    'Referrals': require('./definitions/referrals'),
    'QuestionsPager': require('./definitions/questions_pager'),
    'Questions': require('./definitions/questions'),
    'Question': require('./definitions/question'),
    'FaqsPager': require('./definitions/faqs_pager'),
    'Faqs': require('./definitions/faqs'),
    'Faq': require('./definitions/faq'),
    'ArticlesPager': require('./definitions/articles_pager'),
    'Articles': require('./definitions/articles'),
    'Article': require('./definitions/article'),
    'Strategies': require('./definitions/strategies'),
    'Strategy': require('./definitions/strategy'),
    'CurrenciesPager': require('./definitions/currencies_pager'),
    'Currencies': require('./definitions/currencies'),
    'Currency': require('./definitions/currency'),
    'TransactionsPager': require('./definitions/transactions_pager'),
    'Transactions': require('./definitions/transactions'),
    'Transaction': require('./definitions/transaction'),
    'PaymentsPager': require('./definitions/payments_pager'),
    'Payments': require('./definitions/payments'),
    'Payment': require('./definitions/payment'),
    'Wallet': require('./definitions/wallet'),
    'Meta': require('./definitions/meta'),
    'User': require('./definitions/user'),
    'Users': require('./definitions/users'),
    'UsersPager': require('./definitions/users_pager'),
    'Checkpoint': require('./definitions/checkpoint'),
    'Checkpoints': require('./definitions/checkpoints'),
    'Withdraw': require('./definitions/withdraw')
  }
};
