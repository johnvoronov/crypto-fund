module.exports = {
  'properties': {
    'objects': {
      '$ref': '#/definitions/Currencies'
    },
    'meta': {
      'type': 'object',
      '$ref': '#/definitions/Meta'
    }
  }
};
