module.exports = {
  'required': [
    'name',
    'currency_id'
  ],
  'properties': {
    'id': {
      'type': 'integer',
      'uniqueItems': true
    },
    'name': {
      'type': 'string'
    },
    'description': {
      'type': 'string'
    },
    'description_short': {
      'type': 'string'
    },
    'position': {
      'type': 'integer'
    },
    'balance': {
      'type': 'object',
      'properties': {
        'balance': {
          'type': 'integer',
          'format': 'double'
        },
        'deposit': {
          'type': 'integer',
          'format': 'double'
        },
        'active': {
          'type': 'integer',
          'format': 'double'
        },
        'inactive': {
          'type': 'integer',
          'format': 'double'
        }
      }
    },
    'currency_id': {
      'type': 'integer'
    },
    'currency': {
      '$ref': '#/definitions/Currency'
    }
  }
};
