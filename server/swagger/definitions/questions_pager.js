module.exports = {
  'properties': {
    'objects': {
      '$ref': '#/definitions/Questions'
    },
    'meta': {
      'type': 'object',
      '$ref': '#/definitions/Meta'
    }
  }
};
