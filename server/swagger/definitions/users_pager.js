module.exports = {
  'properties': {
    'objects': {
      '$ref': '#/definitions/Users'
    },
    'meta': {
      'type': 'object',
      '$ref': '#/definitions/Meta'
    }
  }
};
