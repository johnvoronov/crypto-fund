module.exports = {
  'properties': {
    'id': {
      'type': 'integer',
      'uniqueItems': true
    },
    'amount': {
      'type': 'integer',
      'format': 'decimal'
    },
    'percent': {
      'type': 'integer',
      'format': 'double'
    },
    'start_at': {
      'type': 'string',
      'format': 'date-time'
    },
    'end_at': {
      'type': 'string',
      'format': 'date-time'
    },
    'is_done': {
      'type': 'boolean'
    },
    'created_at': {
      'type': 'string',
      'format': 'date-time'
    },
    'updated_at': {
      'type': 'string',
      'format': 'date-time'
    },
    'strategy_id': {
      'type': 'number'
    }
  }
};
