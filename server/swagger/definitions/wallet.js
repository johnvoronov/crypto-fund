module.exports = {
  'required': [
    'address',
    'user_id',
    'currency_id',
    'id'
  ],
  'properties': {
    'id': {
      'type': 'string',
      'uniqueItems': true
    },
    'address': {
      'type': 'string'
    },
    'user_id': {
      'type': 'integer'
    },
    'user': {
      '$ref': '#/definitions/User'
    },
    'strategy_id': {
      'type': 'integer'
    },
    'strategy': {
      '$ref': '#/definitions/Strategy'
    },
    'currency_id': {
      'type': 'integer'
    },
    'currency': {
      '$ref': '#/definitions/Currency'
    }
  }
};
