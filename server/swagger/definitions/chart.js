module.exports = {
  'type': 'array',
  'items': {
    'properties': {
      'date': {
        'type': 'string'
      },
      'value': {
        'type': 'string'
      }
    }
  }
};
