module.exports = {
  'properties': {
    'id': {
      'type': 'integer',
      'uniqueItems': true
    },
    'subject': {
      'type': 'string'
    },
    'question': {
      'type': 'string'
    },
    'user_id': {
      'type': 'integer'
    },
    'user': {
      '$ref': '#/definitions/User'
    },
    'created_at': {
      'type': 'string',
      'format': 'date-time'
    },
    'updated_at': {
      'type': 'string',
      'format': 'date-time'
    }
  }
};
