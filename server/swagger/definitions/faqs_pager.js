module.exports = {
  'properties': {
    'objects': {
      '$ref': '#/definitions/Faqs'
    },
    'meta': {
      'type': 'object',
      '$ref': '#/definitions/Meta'
    }
  }
};
