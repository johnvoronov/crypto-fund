module.exports = {
  'required': [
    'email',
    'id'
  ],
  'properties': {
    'id': {
      'type': 'integer',
      'uniqueItems': true
    },
    'email': {
      'type': 'string',
      'uniqueItems': true
    },
    'phone': {
      'type': 'string'
    },
    'first_name': {
      'type': 'string'
    },
    'last_name': {
      'type': 'string'
    },
    'country': {
      'type': 'string'
    },
    'city': {
      'type': 'string'
    },
    'btc_address': {
      'type': 'string'
    },
    'refresh_token': {
      'type': 'string'
    },
    'is_active': {
      'type': 'boolean'
    },
    'is_admin': {
      'type': 'boolean'
    },
    'is_superuser': {
      'type': 'boolean'
    },
    'is_two_factor': {
      'type': 'boolean'
    },
    'referral_path': {
      'type': 'string'
    },
    'referral_id': {
      'type': 'integer'
    },
    'level': {
      'type': 'integer'
    },
    'owner_id': {
      'type': 'integer'
    }
  }
};
