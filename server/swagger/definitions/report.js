module.exports = {
  'properties': {
    'deposit': {
      'type': 'integer'
    },
    'income': {
      'type': 'integer'
    },
    'percent': {
      'type': 'integer'
    },
    'product': {
      'type': 'string'
    },
    'status': {
      'type': 'boolean'
    }
  }
};
