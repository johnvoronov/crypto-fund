module.exports = {
  'properties': {
    'objects': {
      '$ref': '#/definitions/Articles'
    },
    'meta': {
      'type': 'object',
      '$ref': '#/definitions/Meta'
    }
  }
};
