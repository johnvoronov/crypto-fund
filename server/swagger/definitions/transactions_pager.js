module.exports = {
  'properties': {
    'objects': {
      '$ref': '#/definitions/Transactions'
    },
    'meta': {
      'type': 'object',
      '$ref': '#/definitions/Meta'
    }
  }
};
