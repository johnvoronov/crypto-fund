module.exports = {
  'properties': {
    'balance': {
      'type': 'integer',
    },
    'deposit': {
      'type': 'integer',
    },
    'partnership': {
      'type': 'integer',
    },
    'active': {
      'type': 'integer',
    },
    'inactive': {
      'type': 'integer',
    },
  }
};
