module.exports = {
  'properties': {
    'id': {
      'type': 'integer',
      'uniqueItems': true
    },
    'name': {
      'type': 'string'
    },
    'user': {
      'type': 'number',
      'format': 'double'
    },
    'team': {
      'type': 'number',
      'format': 'double'
    },
    'levels': {
      'type': 'array',
      'items': {
        'type': 'number',
        'format': 'double'
      }
    }
  }
};
