module.exports = {
  'properties': {
    'user': {
      '$ref': '#/definitions/User'
    },
    'jwt': {
      'type': 'string'
    }
  }
};
