module.exports = {
  'properties': {
    'id': {
      'type': 'integer',
      'uniqueItems': true
    },
    'amount': {
      'type': 'integer',
      'format': 'decimal'
    },
    'btc_address': {
      'type': 'string'
    },
    'transaction_hash': {
      'type': 'string'
    },
    'transaction_id': {
      'type': 'integer'
    },
    'user_id': {
      'type': 'integer'
    },
    'created_at': {
      'type': 'string',
      'format': 'date-time'
    },
    'updated_at': {
      'type': 'string',
      'format': 'date-time'
    }
  }
};
