module.exports = {
  'properties': {
    'id': {
      'type': 'integer',
      'uniqueItems': true
    },
    'answer': {
      'type': 'string'
    },
    'question': {
      'type': 'string'
    },
    'created_at': {
      'type': 'string',
      'format': 'date-time'
    },
    'updated_at': {
      'type': 'string',
      'format': 'date-time'
    }
  }
};
