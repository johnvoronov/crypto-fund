module.exports = {
  'properties': {
    'objects': {
      '$ref': '#/definitions/Payments'
    },
    'meta': {
      'type': 'object',
      '$ref': '#/definitions/Meta'
    }
  }
};
