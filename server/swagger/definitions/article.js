module.exports = {
  'properties': {
    'id': {
      'type': 'integer',
      'uniqueItems': true
    },
    'title': {
      'type': 'string'
    },
    'sub_title': {
      'type': 'string'
    },
    'content': {
      'type': 'string'
    },
    'content_short': {
      'type': 'string'
    },
    'created_at': {
      'type': 'string',
      'format': 'date-time'
    },
    'updated_at': {
      'type': 'string',
      'format': 'date-time'
    }
  }
};
