module.exports = {
  '/report/my': {
    'get': {
      'tags': [
        'Report'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': '',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            'properties': {
              'detail': {
                'type': 'object',
                'properties': {
                  'month': {
                    'type': 'integer'
                  },
                  'total': {
                    'type': 'integer'
                  }
                }
              },
              'transactions': {
                '$ref': '#/definitions/Transactions'
              }
            }
          }
        }
      }
    }
  },
  '/report/team': {
    'get': {
      'tags': [
        'Report'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': '',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            'properties': {
              'detail': {
                'type': 'object',
                'properties': {
                  'month': {
                    'type': 'object',
                    'properties': {
                      'total': {
                        'type': 'integer'
                      },
                      'level_2': {
                        'type': 'integer'
                      },
                      'level_3': {
                        'type': 'integer'
                      }
                    }
                  },
                  'total': {
                    'type': 'object',
                    'properties': {
                      'total': {
                        'type': 'integer'
                      },
                      'level_2': {
                        'type': 'integer'
                      },
                      'level_3': {
                        'type': 'integer'
                      }
                    }
                  }
                }
              },
              'transactions': {
                '$ref': '#/definitions/Transactions'
              }
            }
          }
        }
      }
    }
  },
  '/report/partnership': {
    'get': {
      'tags': [
        'Report'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': '',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            'properties': {
              'detail': {
                'type': 'object',
                'properties': {
                  'month': {
                    'type': 'object',
                    'properties': {
                      'total': {
                        'type': 'integer'
                      },
                      'level_2': {
                        'type': 'integer'
                      },
                      'level_3': {
                        'type': 'integer'
                      }
                    }
                  },
                  'total': {
                    'type': 'object',
                    'properties': {
                      'total': {
                        'type': 'integer'
                      },
                      'level_2': {
                        'type': 'integer'
                      },
                      'level_3': {
                        'type': 'integer'
                      }
                    }
                  }
                }
              },
              'users': {
                'type': 'array',
                'items': {
                  'properties': {
                    'email': {
                      'type': 'string'
                    },
                    'level': {
                      'type': 'integer'
                    },
                    'created_at': {
                      'type': 'string',
                      'format': 'date-time'
                    },
                    'referral_level': {
                      'type': 'string'
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  },
  '/report/productivity': {
    'get': {
      'tags': [
        'Report'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Продутивность',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            'properties': {
              'month': {
                'type': 'object',
                'properties': {
                  'user': {
                    'type': 'integer'
                  },
                  'team': {
                    'type': 'integer'
                  },
                  'partners': {
                    'type': 'integer'
                  },
                  'income': {
                    'type': 'integer'
                  },
                  'withdraw': {
                    'type': 'integer'
                  }
                }
              },
              'total': {
                'type': 'object',
                'properties': {
                  'user': {
                    'type': 'integer'
                  },
                  'team': {
                    'type': 'integer'
                  },
                  'partners': {
                    'type': 'integer'
                  },
                  'income': {
                    'type': 'integer'
                  },
                  'withdraw': {
                    'type': 'integer'
                  }
                }
              }
            }
          }
        }
      }
    }
  },
  '/report/month/{year}/{month}': {
    'get': {
      'tags': [
        'Report'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'parameters': [
        {
          'name': 'year',
          'in': 'path',
          'type': 'integer',
          'format': 'integer',
          'required': true
        },
        {
          'name': 'month',
          'in': 'path',
          'type': 'integer',
          'format': 'integer',
          'required': true
        }
      ],
      'description': 'Отчет по стратегиям за текущий месяц',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/definitions/Report'
          }
        }
      }
    }
  },
  '/report/year/{year}': {
    'get': {
      'tags': [
        'Report'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'parameters': [
        {
          'name': 'year',
          'in': 'path',
          'type': 'integer',
          'format': 'integer',
          'required': true
        }
      ],
      'description': 'Отчет по стратегиям за текущий год',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            'type': 'array',
            'items': {
              '$ref': '#/definitions/Report'
            }
          }
        }
      }
    }
  },
  '/report/year/download': {
    'get': {
      'tags': [
        'Report'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Скачивание отчета в формате xlsx по стратегиям за текущий год',
      'responses': {
        '200': {
          'description': 'Success'
        }
      }
    }
  }
};
