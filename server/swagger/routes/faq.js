module.exports = {
  '/faq': {
    'post': {
      'tags': [
        'Faq'
      ],
      'parameters': [
        {
          'name': 'question',
          'in': 'body',
          'type': 'string',
          'required': true,
          'description': 'Вопрос'
        },
        {
          'name': 'answer',
          'in': 'body',
          'type': 'string',
          'required': true,
          'description': 'Ответ'
        }
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Новый вопрос / ответ',
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    },
    'get': {
      'tags': [
        'Faq'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Получение списка вопросов и ответов',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/definitions/FaqsPager'
          }
        }
      }
    }
  },
  '/faq/{id}': {
    'post': {
      'tags': [
        'Faq'
      ],
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'integer',
          'required': true,
          'example': 1,
          'description': 'ID статьи'
        },
        {
          'name': 'question',
          'in': 'body',
          'type': 'string',
          'required': true,
          'description': 'Вопрос'
        },
        {
          'name': 'answer',
          'in': 'body',
          'type': 'string',
          'required': true,
          'description': 'Ответ'
        },
        {
          'name': 'position',
          'in': 'body',
          'type': 'integer',
          'required': false,
          'example': 1,
          'description': 'Порядок вывода'
        }
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    },
    'delete': {
      'tags': [
        'Faq'
      ],
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'integer',
          'required': true,
          'example': 1,
          'description': 'ID'
        }
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  },
  '/faq/question': {
    'post': {
      'tags': [
        'Faq'
      ],
      'parameters': [
        {
          'name': 'subject',
          'in': 'path',
          'type': 'string',
          'required': true,
          'description': 'Тема вопроса'
        },
        {
          'name': 'question',
          'in': 'path',
          'type': 'string',
          'required': true,
          'description': 'Вопрос'
        }
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Вопрос задаваемый пользователем к администрации проекта',
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  }
};
