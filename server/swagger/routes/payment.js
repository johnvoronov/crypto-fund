module.exports = {
  '/payment/list': {
    'get': {
      'tags': [
        'Payment'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'parameters': require('../parameters/pagination'),
      'description': 'Получение списка платежей текущего пользователя',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/definitions/PaymentsPager'
          }
        }
      }
    }
  }
};
