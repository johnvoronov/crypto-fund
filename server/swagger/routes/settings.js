module.exports = {
  '/settings': {
    'get': {
      'tags': ['Settings'],
      'description': 'Вывод настроек',
      'security': {
        'ApiKeyAuth': []
      },
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/definitions/Settings'
          }
        }
      }
    }
  }
};
