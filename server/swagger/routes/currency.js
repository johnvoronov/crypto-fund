module.exports = {
  '/currency': {
    'get': {
      'tags': [
        'Currency'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Получение списка включенных крипто-валют',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/definitions/Currencies'
          }
        }
      }
    }
  }
};
