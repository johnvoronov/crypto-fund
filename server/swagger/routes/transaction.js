module.exports = {
  '/transaction/list': {
    'get': {
      'tags': [
        'Transaction'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'parameters': require('../parameters/pagination'),
      'description': 'Получение операций по балансу текущего пользователя',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/definitions/TransactionsPager'
          }
        }
      }
    }
  }
};
