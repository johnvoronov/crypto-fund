module.exports = {
  '/article': {
    'post': {
      'tags': [
        'Article'
      ],
      'parameters': [
        {
          'name': 'title',
          'in': 'body',
          'type': 'string',
          'required': true,
          'description': 'Заголовок статьи'
        },
        {
          'name': 'sub_title',
          'in': 'body',
          'type': 'string',
          'required': true,
          'description': 'Подзаголовок статьи'
        },
        {
          'name': 'content',
          'in': 'body',
          'type': 'string',
          'required': true,
          'description': 'Контент'
        },
        {
          'name': 'content_short',
          'in': 'body',
          'type': 'string',
          'required': true,
          'description': 'Краткое содержание'
        }
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Редактирование статьи по ID',
      'responses': {
        '204': {
          'description': 'Success',
        }
      }
    },
    'get': {
      'tags': [
        'Article'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Получение списка статей',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/definitions/ArticlesPager'
          }
        }
      }
    }
  },
  '/article/{id}': {
    'get': {
      'tags': [
        'Article'
      ],
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'integer',
          'required': true,
          'example': 1,
          'description': 'ID статьи'
        }
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Получение статьи по ID',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/definitions/Article'
          }
        }
      }
    },
    'post': {
      'tags': [
        'Article'
      ],
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'integer',
          'required': true,
          'example': 1,
          'description': 'ID статьи'
        },
        {
          'name': 'title',
          'in': 'body',
          'type': 'string',
          'required': false,
          'description': 'Заголовок статьи'
        },
        {
          'name': 'sub_title',
          'in': 'body',
          'type': 'string',
          'required': false,
          'description': 'Подзаголовок статьи'
        },
        {
          'name': 'content',
          'in': 'body',
          'type': 'string',
          'required': false,
          'description': 'Контент'
        },
        {
          'name': 'content_short',
          'in': 'body',
          'type': 'string',
          'required': false,
          'description': 'Краткое содержание'
        }
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Редактирование статьи по ID',
      'responses': {
        '204': {
          'description': 'Success',
        }
      }
    },
    'delete': {
      'tags': [
        'Article'
      ],
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'integer',
          'required': true,
          'example': 1,
          'description': 'ID статьи'
        }
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Удаление статьи по ID',
      'responses': {
        '204': {
          'description': 'Success',
        }
      }
    }
  }
};
