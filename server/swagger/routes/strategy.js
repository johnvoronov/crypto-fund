module.exports = {
  '/strategy': {
    'get': {
      'tags': [
        'Strategy'
      ],
      'description': 'Список стратегий',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/definitions/Strategies'
          }
        }
      }
    },
    'post': {
      'tags': [
        'Strategy'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Создание стратегии',
      'parameters': [
        {
          'name': 'name',
          'in': 'body',
          'type': 'string',
          'required': true,
          'description': 'Название стратегии'
        },
        {
          'name': 'strategy_id',
          'in': 'body',
          'type': 'integer',
          'required': true,
          'description': 'Валюта стратегии'
        },
        {
          'name': 'position',
          'in': 'body',
          'type': 'integer',
          'required': true,
          'description': 'Позиция по порядку'
        },
        {
          'name': 'description',
          'in': 'body',
          'type': 'string',
          'required': true,
          'description': 'Детальное описание стратегии'
        },
        {
          'name': 'description_short',
          'in': 'body',
          'type': 'string',
          'required': true,
          'description': 'Краткое описание стратегии'
        },
        {
          'name': 'icon',
          'in': 'body',
          'type': 'string',
          'required': false,
          'description': 'Иконка на кнопке'
        },
        {
          'name': 'hero_icon',
          'in': 'body',
          'type': 'string',
          'required': false,
          'description': 'Иконка стратегии'
        },
      ],
      'responses': {
        '201': {
          'description': 'Success',
          'schema': {
            'properties': {
              'id': {
                'type': 'integer'
              }
            }
          }
        },
        '400': {
          'description': 'Bad request',
        }
      }
    },
  },
  '/strategy/{id}': {
    'get': {
      'tags': [
        'Strategy'
      ],
      'description': 'Детальная информация о стратегии',
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'string',
          'required': true,
          'example': 1,
          'description': 'ID платежа'
        }
      ],
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/definitions/Strategy'
          }
        },
        '404': {
          'description': 'Not found',
        }
      }
    },
    'post': {
      'tags': [
        'Strategy'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Изменение стратегии',
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'string',
          'required': true,
          'example': 1,
          'description': 'ID платежа'
        },
        {
          'name': 'name',
          'in': 'body',
          'type': 'string',
          'required': true,
          'description': 'Название стратегии'
        },
        {
          'name': 'strategy_id',
          'in': 'body',
          'type': 'integer',
          'required': true,
          'description': 'Валюта стратегии'
        },
        {
          'name': 'position',
          'in': 'body',
          'type': 'integer',
          'required': true,
          'description': 'Позиция по порядку'
        },
        {
          'name': 'description',
          'in': 'body',
          'type': 'string',
          'required': true,
          'description': 'Детальное описание стратегии'
        },
        {
          'name': 'description_short',
          'in': 'body',
          'type': 'string',
          'required': true,
          'description': 'Краткое описание стратегии'
        },
        {
          'name': 'icon',
          'in': 'body',
          'type': 'string',
          'required': false,
          'description': 'Иконка на кнопке'
        },
        {
          'name': 'hero_icon',
          'in': 'body',
          'type': 'string',
          'required': false,
          'description': 'Иконка стратегии'
        },
      ],
      'responses': {
        '204': {
          'description': 'Success'
        },
        '400': {
          'description': 'Bad request',
        }
      }
    },
    'delete': {
      'tags': [
        'Strategy'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Изменение стратегии',
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'string',
          'required': true,
          'example': 1,
          'description': 'ID платежа'
        }
      ],
      'responses': {
        '201': {
          'description': 'Success',
          'schema': {
            'properties': {
              'id': {
                'type': 'integer'
              }
            }
          }
        },
        '400': {
          'description': 'Bad request',
        }
      }
    }
  }
};
