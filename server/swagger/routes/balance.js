module.exports = {
  '/balance': {
    'get': {
      'tags': [
        'Balance'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Баланс пользователя',
      'responses': {
        '200': {
          'description': 'success',
          'schema': {
            '$ref': '#/definitions/Balance'
          },
        }
      }
    }
  }
};
