module.exports = {
  '/user/login': {
    'post': {
      'tags': [
        'Users'
      ],
      'description': 'Авторизация',
      'parameters': [
        {
          'name': 'email',
          'in': 'body',
          'type': 'string',
          'format': 'email',
          'required': true
        },
        {
          'name': 'password',
          'in': 'body',
          'type': 'string',
          'required': true
        },
        {
          'name': 'token',
          'in': 'body',
          'type': 'string',
          'required': false,
          'description': 'Ключ входа для 2fa авторизации'
        }
      ],
      'responses': {
        '204': {
          'description': 'Success'
        },
        '400': {
          'description': 'Invalid credentials',
          'schema': {
            'type': 'object',
            'properties': {
              'email': {
                'type': 'array',
                'example': [
                  'email cannot be blank'
                ]
              }
            }
          }
        },
        '401': {
          'description': '2fa authentication'
        }
      }
    }
  },
  '/user/registration': {
    'post': {
      'tags': [
        'Users'
      ],
      'description': 'Регистрация в системе',
      'parameters': [
        {
          'name': 'email',
          'in': 'body',
          'type': 'string',
          'format': 'email',
          'required': true
        },
        {
          'name': 'password',
          'in': 'body',
          'type': 'string',
          'required': true
        },
        {
          'name': 'password_confirm',
          'in': 'body',
          'type': 'string',
          'required': true
        },
        {
          'name': 'referral_id',
          'in': 'body',
          'type': 'integer',
          'required': true
        },
        {
          'name': 'is_agree',
          'in': 'body',
          'type': 'boolean',
          'required': true
        },
        {
          'name': 'is_confirm',
          'in': 'body',
          'type': 'boolean',
          'required': true
        }
      ],
      'responses': {
        '201': {
          'description': 'Success',
          'schema': {
            'type': 'object',
            'properties': {
              'id': {
                'type': 'integer',
                'example': 4
              }
            }
          }
        },
        '400': {
          'description': 'Ошибки валидации',
          'schema': {
            'type': 'object',
            'properties': {
              'email': {
                'type': 'array',
                'example': [
                  'email cannot be blank'
                ]
              }
            }
          }
        }
      }
    }
  },
  '/user/registration/resend': {
    'post': {
      'tags': [
        'Users'
      ],
      'description': 'Повторная отправка письма после регистрации пользователя',
      'parameters': [
        {
          'name': 'email',
          'in': 'body',
          'type': 'string',
          'format': 'email',
          'required': true
        }
      ],
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  },
  '/user/restore': {
    'post': {
      'tags': [
        'Users'
      ],
      'description': 'Восстановление пароля пользователя',
      'parameters': [
        {
          'name': 'email',
          'in': 'body',
          'format': 'email',
          'type': 'string',
          'required': true
        }
      ],
      'responses': {
        '204': {
          'description': 'Success'
        },
        '400': {
          'description': 'Validation errors',
          'schema': {
            'type': 'object',
            'properties': {
              'email': {
                'type': 'array',
                'example': [
                  'email cannot be blank'
                ]
              }
            }
          }
        }
      }
    }
  },
  '/user/restore/confirm': {
    'post': {
      'tags': [
        'Users'
      ],
      'description': 'Подтверждение электронной почты и изменение пароля',
      'parameters': [
        {
          'name': 'token',
          'in': 'body',
          'format': 'string',
          'required': true
        },
        {
          'name': 'password',
          'in': 'body',
          'format': 'string',
          'required': true
        },
        {
          'name': 'password_confirm',
          'in': 'body',
          'format': 'string',
          'required': true
        }
      ],
      'responses': {
        '204': {
          'description': 'Success'
        },
        '400': {
          'description': 'Ошибки валидации',
          'schema': {
            'type': 'object',
            'properties': {
              'email': {
                'type': 'array',
                'example': [
                  'email cannot be blank'
                ]
              }
            }
          }
        }
      }
    }
  },
  '/user/restore/resend': {
    'post': {
      'tags': [
        'Users'
      ],
      'description': 'Повторная отправка письма после восстановления пароля',
      'parameters': [
        {
          'name': 'email',
          'in': 'body',
          'type': 'string',
          'format': 'email',
          'required': true
        }
      ],
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  },
};
