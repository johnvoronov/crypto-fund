module.exports = {
  '/2fa/qr': {
    'get': {
      'tags': [
        '2fa'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Отображение изображения с QR кодом 2fa авторизации',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            'type': 'object',
            'properties': {
              'image': {
                'type': 'string'
              }
            }
          }
        }
      }
    }
  },
  '/2fa/enable': {
    'post': {
      'tags': [
        '2fa'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Включение 2fa авторизации для текущего пользователя',
      'parameters': [
        {
          'name': 'token',
          'in': 'body',
          'type': 'string',
          'required': true,
          'description': 'Ключ входа для 2fa авторизации'
        }
      ],
      'responses': {
        '204': {
          'description': 'Success'
        },
        '406': {
          'description': 'Not Acceptable'
        },
        '400': {
          'description': 'Invalid token',
          'schema': {
            'type': 'object',
            'properties': {
              'token': {
                'type': 'array',
                'example': [
                  'Invalid token'
                ]
              }
            }
          }
        }
      }
    }
  },
  '/2fa/disable': {
    'post': {
      'tags': [
        '2fa'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Отключение 2fa авторизации для текущего пользователя',
      'parameters': [
        {
          'name': 'token',
          'in': 'body',
          'type': 'string',
          'required': true,
          'description': 'Ключ входа для 2fa авторизации'
        }
      ],
      'responses': {
        '204': {
          'description': 'Success'
        },
        '406': {
          'description': 'Not Acceptable'
        },
        '400': {
          'description': 'Invalid token',
          'schema': {
            'type': 'object',
            'properties': {
              'token': {
                'type': 'array',
                'example': [
                  'Invalid token'
                ]
              }
            }
          }
        }
      }
    }
  }
};
