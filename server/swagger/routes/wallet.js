module.exports = {
  '/wallet': {
    'post': {
      'tags': [
        'Wallet'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Создание кошелька для крипто-валюты',
      'parameters': [
        {
          'name': 'strategy_id',
          'in': 'body',
          'type': 'integer',
          'required': true
        },
        {
          'name': 'token',
          'in': 'body',
          'type': 'string',
          'required': false,
          'description': 'Ключ входа для 2fa авторизации'
        }
      ],
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            'type': 'object',
            'properties': {
              'address': {
                'type': 'string',
                'example': '0xFB897C789DcD53030729FcE3E6fc71EAE8A8183d'
              }
            }
          }
        },
        '406': {
          'description': 'Not Acceptable'
        },
        '400': {
          'description': 'Invalid token',
          'schema': {
            'type': 'object',
            'properties': {
              'token': {
                'type': 'array',
                'example': [
                  'Invalid token'
                ]
              }
            }
          }
        }
      }
    }
  }
};
