module.exports = {
  '/admin/checkpoint': {
    'get': {
      'tags': [
        'Admin',
        'Checkpoint'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Получить список всех чекпойнтов',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/definitions/Checkpoints'
          }
        }
      }
    },
    'post': {
      'tags': [
        'Admin',
        'Checkpoint'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Создание чекпойнта',
      'parameters': [
        {
          'name': 'strategy_id',
          'in': 'body',
          'type': 'integer',
          'required': true
        },
        {
          'name': 'percent',
          'in': 'body',
          'type': 'integer',
          'required': true
        }
      ],
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  },
  '/admin/checkpoint/{id}': {
    'get': {
      'tags': [
        'Admin',
        'Checkpoint'
      ],
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'integer',
          'required': true,
          'example': 1,
          'description': 'ID чекпойнта'
        },
        {
          'name': 'strategy_id',
          'in': 'body',
          'type': 'integer',
          'required': true
        },
        {
          'name': 'percent',
          'in': 'body',
          'type': 'integer',
          'required': true
        }
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Получить чекпойнт по идентификатору',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/definitions/Checkpoint'
          }
        },
        '404': {
          'description': 'NotFound'
        }
      }
    }
  }
};
