module.exports = {
  '/admin/strategy/options': {
    'get': {
      'tags': [
        'Admin',
        'Strategy'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Вывод списка стратегий без постраничной разбивки',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            'type': 'array',
            'items': {
              'properties': {
                'id': {
                  'type': 'integer'
                },
                'name': {
                  'type': 'string',
                }
              }
            }
          }
        }
      }
    }
  }
};
