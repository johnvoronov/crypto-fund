module.exports = {
  '/admin/withdraw': {
    'get': {
      'tags': [
        'Admin',
        'Withdraw'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Получить список всех заявок на вывод',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/definitions/Withdraws'
          }
        }
      }
    }
  },
  '/admin/withdraw/{id}': {
    'post': {
      'tags': [
        'Admin',
        'Withdraw'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Изменение заявки на вывод',
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'string',
          'required': true,
          'example': 1,
          'description': 'ID'
        },
        {
          'name': 'transaction_hash',
          'in': 'body',
          'type': 'string',
          'required': true
        },
      ],
      'responses': {
        '201': {
          'description': 'Success'
        },
        '404': {
          'description': 'NotFound'
        }
      }
    }
  }
};
