module.exports = {
  '/admin/referral': {
    'get': {
      'tags': [
        'Admin',
        'Referral'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Список уровней реферальной системы',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/definitions/Referrals'
          }
        }
      }
    },
    'post': {
      'tags': [
        'Admin',
        'Referral'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Создание уровня реф. системы',
      'parameters': [
        {
          'name': 'name',
          'in': 'body',
          'type': 'string',
          'required': true
        },
        {
          'name': 'user',
          'in': 'body',
          'type': 'integer',
          'required': true
        },
        {
          'name': 'team',
          'in': 'body',
          'type': 'integer',
          'required': true
        },
        {
          'name': 'levels',
          'in': 'body',
          'type': 'array',
          'items': {
            'type': 'integer'
          },
          'required': true
        }
      ],
      'responses': {
        '201': {
          'description': 'Success',
          'schema': {
            'properties': {
              'id': {
                'type': 'integer'
              }
            }
          }
        }
      }
    }
  },
  '/admin/referral/{id}': {
    'get': {
      'tags': [
        'Admin',
        'Referral'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Детальный просмотр уровня реф. системы',
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'string',
          'required': true,
          'example': 1,
          'description': 'ID'
        }
      ],
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/definitions/Referral'
          }
        }
      }
    },
    'post': {
      'tags': [
        'Admin',
        'Referral'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Изменение уровня реф. системы',
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'string',
          'required': true,
          'example': 1,
          'description': 'ID'
        }
      ],
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/definitions/Referral'
          }
        }
      }
    },
    'delete': {
      'tags': [
        'Admin',
        'Referral'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Удаление уровня реф. системы',
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'string',
          'required': true,
          'example': 1,
          'description': 'ID'
        }
      ],
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/definitions/Referral'
          }
        }
      }
    }
  }
};
