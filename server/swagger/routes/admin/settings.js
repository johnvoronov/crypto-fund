module.exports = {
  '/admin/settings': {
    'get': {
      'tags': ['Admin'],
      'description': 'Вывод настроек (public & private)',
      'security': {
        'ApiKeyAuth': []
      },
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/definitions/Settings'
          }
        }
      }
    },
    'post': {
      'tags': ['Admin'],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Сохранение настроек',
      'parameters': [
        ...require('../../parameters/settings/company'),
        ...require('../../parameters/settings/coinpayments'),
        ...require('../../parameters/settings/google'),
        ...require('../../parameters/settings/smtp'),
        ...require('../../parameters/settings/link'),
      ],
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  },
  '/admin/settings/coinpayments': {
    'post': {
      'tags': ['Admin'],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Сохранение настроек (раздел coinpayments)',
      'parameters': require('../../parameters/settings/coinpayments'),
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  },
  '/admin/settings/smtp': {
    'post': {
      'tags': ['Admin'],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Сохранение настроек (раздел smtp)',
      'parameters': require('../../parameters/settings/smtp'),
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  },
  '/admin/settings/google': {
    'post': {
      'tags': ['Admin'],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Сохранение настроек (раздел google)',
      'parameters': require('../../parameters/settings/google'),
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  }
};
