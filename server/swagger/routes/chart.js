module.exports = {
  '/chart/referral': {
    'get': {
      'tags': [
        'Chart'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'График доходности BTC в разделе партнерство',
      'responses': {
        '200': {
          'description': 'success',
          'schema': {
            '$ref': '#/definitions/Chart'
          }
        }
      }
    }
  },
  '/chart/strategy/{id}': {
    'get': {
      'tags': [
        'Chart'
      ],
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'integer',
          'required': true,
          'example': 1,
          'description': 'ID стратегии'
        }
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'График доходности стратегий (по чекпоинтам)',
      'responses': {
        '200': {
          'description': 'success',
          'schema': {
            '$ref': '#/definitions/Chart'
          }
        }
      }
    }
  }
};
