module.exports = [
  {
    'name': 'company_name',
    'in': 'body',
    'type': 'string',
    'required': false,
    'description': 'Название компании'
  },
  {
    'name': 'partnership_description',
    'in': 'body',
    'type': 'string',
    'required': false,
    'description': 'Описание партнерской программмы'
  },
  {
    'name': 'user_registration_invite_only',
    'in': 'body',
    'type': 'integer',
    'required': false,
    'description': 'Регистрация только по приглашению'
  }
];
