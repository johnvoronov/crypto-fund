module.exports = [
  {
    'name': 'google_analytics_id',
    'in': 'body',
    'type': 'string',
    'required': true,
    'description': 'Идентификатор Google Analytics'
  },
  {
    'name': 'google_tag_manager_id',
    'in': 'body',
    'type': 'string',
    'required': true,
    'description': 'Идентификатор Google Tag Manager'
  }
];

