module.exports = [
  {
    'name': 'smtp_from',
    'in': 'body',
    'type': 'string',
    'description': 'Адрес электронной почты с которой будет происходить отправка'
  },
  {
    'name': 'smtp_host',
    'in': 'body',
    'type': 'string',
    'description': 'Хост'
  },
  {
    'name': 'smtp_port',
    'in': 'body',
    'type': 'string',
    'description': 'Порт'
  },
  {
    'name': 'smtp_username',
    'in': 'body',
    'type': 'string',
    'description': 'Имя пользователя или электронная почта. Для AWS используется access key'
  },
  {
    'name': 'smtp_password',
    'in': 'body',
    'type': 'string',
    'description': 'Пароль (или secret key для AWS)'
  },
];
