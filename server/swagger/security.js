module.exports = {
  'ApiKeyAuth': {
    'type': 'apiKey',
    'in': 'header',
    'name': 'Authentication'
  }
};
