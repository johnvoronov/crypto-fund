const jwt = require('../util/jwt');
const models = require('../models');

module.exports = async (req, res, next) => {
  const token = jwt.getTokenFromRequest(req);
  if (null === token) {
    return res.boom.unauthorized('no authorization');
  }

  try {
    const jwtUser = await jwt.verify(token);
    // Если JWT токен корректный, находим пользователя в базе данных
    // и обновляем информацию о нем
    const where = {
      id: jwtUser.id,
      is_active: true
    };

    const count = await models.User.count({ where });
    if (count === 0) {
      return res.boom.forbidden();
    }

    req.user = jwtUser;
    return next();

  } catch (err) {
    res.boom.unauthorized('jwt malformed', { token: err.toString() })
  }
};
