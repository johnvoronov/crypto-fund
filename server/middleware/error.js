const Raven = require('raven');
const UnauthorizedError = require('express-jwt-permissions/error');

function transMock(id) {
  return id;
}

function formatJoi(error, t = null) {
  t = t || transMock;
  let errors = {};

  if (error && error.details) {
    for (let i = 0; i < error.details.length; i++) {
      const err = error.details[i];
      const path = err.path.join('.').replace(/"/g, '');

      if (typeof errors[path] === 'undefined') {
        errors[path] = [];
      }

      const message = err.message
        .replace(path, t(err.context.label))
        .replace(/"/g, '');

      errors[path].push(message);
    }
  }

  return errors;
}

module.exports = (error, req, res, next) => {
  // if (error.isBoom) {
  //   Raven.captureException(error);
  //   return res.status(error.output.statusCode).json(error.output.payload);
  // }

  if (error.isJoi) {
    return res.boom.badRequest(null, { errors: formatJoi(error, req.t) });
  }

  if (error instanceof UnauthorizedError) {
    return res.boom.forbidden(null, {
      error: error.toString(),
      statusCode: 403
    });
  }

  if (error instanceof Error) {
    if (process.env.NODE_ENV !== 'test') {
      console.log(error);
    }

    return res.boom.badImplementation(null, {
      error: error.toString(),
      statusCode: 500
    });
  }

  return next(error);
};
