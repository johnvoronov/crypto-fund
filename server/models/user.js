const passwordUtil = require('../util/password');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      unique: true,
      validate: {
        isEmail: true
      }
    },
    login: {
      type: DataTypes.STRING,
      unique: true,
    },
    password: DataTypes.STRING,
    token: {
      type: DataTypes.STRING,
      allowNull: true
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true
    },
    first_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    last_name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    phone_code: {
      type: DataTypes.STRING,
      allowNull: true
    },
    country: {
      type: DataTypes.STRING,
      allowNull: true
    },
    city: {
      type: DataTypes.STRING,
      allowNull: true
    },
    btc_address: {
      type: DataTypes.STRING,
      allowNull: true
    },
    refresh_token: {
      type: DataTypes.STRING,
      allowNull: true
    },
    is_active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    is_phone_confirmed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    is_admin: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    is_superuser: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    is_two_factor: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    two_factor_secret: {
      type: DataTypes.STRING,
      allowNull: true
    },
    level: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    parents: {
      type: DataTypes.ARRAY(DataTypes.INTEGER),
      allowNull: true
    },
    avatar: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    timestamps: true,
    paranoid: true,
    tableName: 'user',
    hooks: {
      beforeCreate: (user, options) => {
        user.level = (user.parents || []).length;
        user.token = passwordUtil.token();
        user.refresh_token = passwordUtil.token();
      }
    }
  });

  User.associate = models => {
    User.hasMany(models.Wallet, {
      foreignKey: 'user_id',
      as: 'wallets'
    });

    User.hasMany(models.Transaction, {
      foreignKey: 'user_id',
      as: 'transactions'
    });

    User.belongsTo(models.User, {
      foreignKey: 'referral_id',
      as: 'referral',
      allowNull: true
    });

    User.belongsTo(models.Referral, {
      foreignKey: 'referral_level_id',
      as: 'referral_level',
      allowNull: true
    });

    User.belongsTo(models.Referral, {
      foreignKey: 'buy_referral_level_id',
      as: 'buy_referral_level',
      allowNull: true
    });

    User.belongsTo(models.Referral, {
      foreignKey: 'minimal_referral_level_id',
      as: 'minimal_referral_level',
      allowNull: true
    });

    User.belongsTo(models.User, {
      foreignKey: 'owner_id',
      as: 'owner',
      allowNull: true
    });
  };

  return User;
};
