module.exports = (sequelize, DataTypes) => {
  const Finish = sequelize.define('Finish', {

  }, {
    timestamps: true,
    tableName: 'finish'
  });

  Finish.associate = models => {
    Finish.belongsTo(models.Strategy, {
      foreignKey: 'strategy_id',
      as: 'strategy'
    });
    
    Finish.hasMany(models.Checkpoint, {
      foreignKey: 'finish_id',
      as: 'checkpoints'
    });
  };

  return Finish;
};
