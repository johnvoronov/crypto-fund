module.exports = (sequelize, DataTypes) => {
  const Setting = sequelize.define('Setting', {
    path: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    value: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    timestamps: false,
    tableName: 'setting'
  });

  Setting.associate = models => {
  };

  return Setting;
};
