module.exports = (sequelize, DataTypes) => {
  const Article = sequelize.define('Article', {
    title: {
      type: DataTypes.STRING
    },
    sub_title: {
      type: DataTypes.STRING
    },
    content_short: {
      type: DataTypes.TEXT
    },
    content: {
      type: DataTypes.TEXT
    }
  }, {
    timestamps: true,
    tableName: 'article'
  });

  Article.associate = models => {

  };

  return Article;
};
