module.exports = (sequelize, DataTypes) => {
  const Question = sequelize.define('Question', {
    subject: {
      type: DataTypes.STRING,
      allowNull: false
    },
    question: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    timestamps: true,
    tableName: 'question'
  });

  Question.associate = models => {
    Question.belongsTo(models.User, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
      as: 'user'
    });
  };

  return Question;
};
