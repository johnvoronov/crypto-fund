module.exports = (sequelize, DataTypes) => {
  const Checkpoint = sequelize.define('Checkpoint', {
    amount: {
      type: DataTypes.DECIMAL(27, 18)
    },
    percent: {
      type: DataTypes.DOUBLE
    },
    end_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    is_done: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  }, {
    timestamps: true,
    tableName: 'checkpoint'
  });

  Checkpoint.associate = models => {
    Checkpoint.belongsTo(models.Strategy, {
      foreignKey: 'strategy_id',
      onDelete: 'SET NULL',
      as: 'strategy'
    });
    Checkpoint.belongsTo(models.Finish, {
      foreignKey: 'finish_id',
      as: 'finish'
    });
  };

  return Checkpoint;
};
