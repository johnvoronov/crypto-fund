module.exports = (sequelize, DataTypes) => {
  const Wallet = sequelize.define('Wallet', {
    address: {
      type: DataTypes.STRING,
      allowNull: false
    },
    merchant_id: {
      type: DataTypes.STRING,
      allowNull: true
    },
    is_internal: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
  }, {
    timestamps: true,
    tableName: 'wallet'
  });

  Wallet.associate = models => {
    Wallet.belongsTo(models.User, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
      as: 'user'
    });

    Wallet.belongsTo(models.Strategy, {
      foreignKey: 'strategy_id',
      onDelete: 'CASCADE',
      as: 'strategy'
    });

    Wallet.belongsTo(models.Currency, {
      foreignKey: 'currency_id',
      onDelete: 'CASCADE',
      as: 'currency',
      allowNull: true
    });
  };

  return Wallet;
};
