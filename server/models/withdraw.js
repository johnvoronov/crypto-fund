const consts = require('../consts');

module.exports = (sequelize, DataTypes) => {
  const Withdraw = sequelize.define('Withdraw', {
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      validate: {
        isIn: [
          [
            consts.WITHDRAW_PARTNERSHIP,
            consts.WITHDRAW_STRATEGY
          ]
        ]
      }
    },
    amount: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false
    },
    btc_address: {
      type: DataTypes.STRING,
      allowNull: false
    },
    transaction_hash: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    timestamps: true,
    tableName: 'withdraw'
  });

  Withdraw.associate = models => {
    Withdraw.belongsTo(models.User, {
      foreignKey: 'user_id',
      as: 'user'
    });

    Withdraw.belongsTo(models.Transaction, {
      foreignKey: 'transaction_id',
      allowNull: true,
      as: 'transaction'
    });
  };

  return Withdraw;
};
