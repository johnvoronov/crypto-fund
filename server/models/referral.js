module.exports = (sequelize, DataTypes) => {
  const Referral = sequelize.define('Referral', {
    name_en: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name_ru: {
      type: DataTypes.STRING,
      allowNull: false
    },
    color: {
      type: DataTypes.STRING,
      defaultValue: '#cccccc'
    },
    pro: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0
    },
    team: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
    user: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
    user_count: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    // Процент единичной выплаты в зависимости от вложенности
    // реферальных пользователей
    single: {
      type: DataTypes.ARRAY(DataTypes.DOUBLE),
      allowNull: true
    },
    // Процент за активность (за покупку / продажу статусов)
    active: {
      type: DataTypes.ARRAY(DataTypes.DOUBLE),
      allowNull: true
    },
    price: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: true
    },
    levels: {
      type: DataTypes.ARRAY(DataTypes.DOUBLE),
      allowNull: false
    },
    description_ru: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    description_en: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    opportunities_ru: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    opportunities_en: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    caption_ru: {
      type: DataTypes.STRING,
      allowNull: true
    },
    caption_en: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    timestamps: false,
    tableName: 'referral'
  });

  Referral.associate = models => {

  };

  return Referral;
};
