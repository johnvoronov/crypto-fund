module.exports = (sequelize, DataTypes) => {
  const UserReferral = sequelize.define('UserReferral', {
    start_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    end_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    is_active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    }
  }, {
    timestamps: true,
    tableName: 'user_referral'
  });

  UserReferral.associate = models => {
    UserReferral.belongsTo(models.User, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
      as: 'user'
    });
    UserReferral.belongsTo(models.Referral, {
      foreignKey: 'referral_id',
      onDelete: 'CASCADE',
      as: 'referral'
    });
  };

  UserReferral.removeAttribute('id');

  return UserReferral;
};
