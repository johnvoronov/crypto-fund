module.exports = (sequelize, DataTypes) => {
  const Faq = sequelize.define('Faq', {
    question_en: {
      type: DataTypes.TEXT
    },
    question_ru: {
      type: DataTypes.TEXT
    },
    answer_ru: {
      type: DataTypes.TEXT
    },
    answer_en: {
      type: DataTypes.TEXT
    },
    position: {
      type: DataTypes.INTEGER
    }
  }, {
    timestamps: true,
    tableName: 'faq'
  });

  Faq.associate = models => {

  };

  return Faq;
};
