module.exports = (sequelize, DataTypes) => {
  const Franchise = sequelize.define('Franchise', {
    // Флаг зачисления бонусов рефералам первого уровня
    is_done: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    is_active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    price: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    },
  }, {
    timestamps: true,
    tableName: 'franchise'
  });

  Franchise.associate = models => {
    Franchise.belongsTo(models.User, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
      as: 'user'
    });

    Franchise.belongsTo(models.Currency, {
      foreignKey: 'currency_id',
      onDelete: 'CASCADE',
      as: 'currency'
    });
  };

  return Franchise;
};
