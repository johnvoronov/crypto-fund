module.exports = (sequelize, DataTypes) => {
  const Strategy = sequelize.define('Strategy', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    limit: {
      type: DataTypes.JSON,
      allowNull: true
    },
    items: {
      type: DataTypes.JSON,
      allowNull: true
    },
    coinpayments: {
      type: DataTypes.JSON,
      allowNull: true
    },
    fee: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0
    },
    caption: {
      type: DataTypes.STRING,
      allowNull: true
    },
    features: {
      type: DataTypes.ARRAY(DataTypes.STRING),
      allowNull: true
    },
    position: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    tax: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    help_text: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    icon: {
      type: DataTypes.STRING,
      allowNull: true
    },
    hero_icon: {
      type: DataTypes.STRING,
      allowNull: true
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    description_short: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    deduct_coinpayments_tax: {
      // Вычитать из суммы к зачислению комиссию coinpayments
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  }, {
    timestamps: false,
    tableName: 'strategy',
  });

  Strategy.associate = models => {
    Strategy.belongsTo(models.Currency, {
      foreignKey: 'currency_id',
      onDelete: 'CASCADE',
      as: 'currency'
    });
    Strategy.belongsTo(models.Currency, {
      foreignKey: 'convert_id',
      onDelete: 'CASCADE',
      as: 'convert'
    });
  };

  return Strategy;
};
