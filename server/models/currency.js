module.exports = (sequelize, DataTypes) => {
  const Currency = sequelize.define('Currency', {
    code: {
      type: DataTypes.STRING,
      unique: true
    },
    name: DataTypes.STRING,
    to_usd: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    },
    to_btc: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    },
    to_rub: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    },
    to_eth: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    }
  }, {
    timestamps: true,
    tableName: 'currency'
  });

  Currency.associate = models => {

  };

  return Currency;
};
