const consts = require('../consts');

module.exports = (sequelize, DataTypes) => {
  const Payment = sequelize.define('Payment', {
    amount: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
    transaction_id: {
      type: DataTypes.STRING,
      allowNull: true
    },
    fee: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0
    },
    usd: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    },
    rub: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    },
    btc: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    },
    eth: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    },
    to_usd: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    },
    to_btc: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    },
    to_rub: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    },
    to_eth: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    },
    status: {
      type: DataTypes.INTEGER,
      validate: {
        isIn: [
          [
            consts.PAYMENT_PENDING,
            consts.PAYMENT_COMPLETE,
            consts.PAYMENT_ERROR
          ]
        ]
      }
    }
  }, {
    timestamps: true,
    paranoid: true,
    tableName: 'payment',
    hooks: {
      beforeValidate: (payment, options) => {
        if (!payment.usd) {
          payment.usd = payment.amount * payment.to_usd;
        }
      }
    }
  });

  Payment.associate = models => {
    Payment.belongsTo(models.User, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
      as: 'user'
    });

    Payment.belongsTo(models.Currency, {
      foreignKey: 'currency_id',
      onDelete: 'CASCADE',
      as: 'currency'
    });

    Payment.belongsTo(models.Strategy, {
      foreignKey: 'strategy_id',
      onDelete: 'CASCADE',
      as: 'strategy'
    });

    Payment.belongsTo(models.Wallet, {
      foreignKey: 'wallet_id',
      onDelete: 'CASCADE',
      as: 'wallet'
    });
  };

  return Payment;
};
