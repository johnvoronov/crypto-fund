module.exports = (sequelize, DataTypes) => {
  const UserCheckpoint = sequelize.define('UserCheckpoint', {
    before: {
      type: DataTypes.DECIMAL(27, 18)
    },
    after: {
      type: DataTypes.DECIMAL(27, 18)
    },
    base: {
      type: DataTypes.DECIMAL(27, 18)
    },
    diff: {
      type: DataTypes.DECIMAL(27, 18)
    },
    percent: {
      type: DataTypes.DOUBLE
    },
    is_loss: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    level: {
      type: DataTypes.STRING
    },
    fee: {
      type: DataTypes.DECIMAL(27, 18)
    },
    usd: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false
    },
  }, {
    timestamps: true,
    tableName: 'user_checkpoint'
  });

  UserCheckpoint.associate = models => {
    UserCheckpoint.belongsTo(models.User, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
      as: 'user'
    });

    UserCheckpoint.belongsTo(models.Checkpoint, {
      foreignKey: 'checkpoint_id',
      onDelete: 'SET NULL',
      as: 'checkpoint'
    });

    UserCheckpoint.belongsTo(models.Strategy, {
      foreignKey: 'strategy_id',
      onDelete: 'CASCADE',
      as: 'strategy'
    });
  };

  return UserCheckpoint;
};
