module.exports = (sequelize, DataTypes) => {
  const StrategyTax = sequelize.define('StrategyTax', {

  }, {
    timestamps: true,
    paranoid: false,
    underscored: true,
    freezeTableName: true,
    tableName: 'strategy_tax',
    version: false
  });

  StrategyTax.associate = models => {
    StrategyTax.belongsTo(models.Strategy, {
      foreignKey: 'strategy_id',
      as: 'strategy'
    });
    StrategyTax.belongsTo(models.User, {
      foreignKey: 'user_id',
      as: 'user'
    });
  };

  return StrategyTax;
};
