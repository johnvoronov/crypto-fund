const consts = require('../consts');

module.exports = (sequelize, DataTypes) => {
  const Transaction = sequelize.define('Transaction', {
    amount: DataTypes.DECIMAL(27, 18),
    purchase_amount: DataTypes.DECIMAL(27, 18),
    comment: {
      type: DataTypes.STRING,
      allowNull: true
    },
    account_type: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    is_deposit: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    is_freeze: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    is_income: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    percent: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0
    },
    usd: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    },
    rub: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    },
    btc: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    },
    eth: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    },
    fee: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: true
    },
    fund_amount: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: true
    },
    status: {
      type: DataTypes.INTEGER,
      validate: {
        isIn: [
          [
            consts.TRANSACTION_PAYMENT,
            consts.TRANSACTION_INCOME,
            consts.TRANSACTION_WITHDRAW,
            consts.TRANSACTION_ONE_TIME_BONUS,
            consts.TRANSACTION_DEPOSIT,
            consts.TRANSACTION_FUND_INCOME,
            consts.TRANSACTION_FUND_INCOME_FEE,
            consts.TRANSACTION_WRITE_OFF,
            consts.TRANSACTION_FREEZE,
            consts.TRANSACTION_PAY_STATUS,
            consts.TRANSACTION_PRO_BONUS,
            consts.TRANSACTION_PAY_STATUS_BONUS,
            consts.TRANSACTION_PARTNER_INCOME
          ]
        ]
      }
    },
    referral_percent: {
      type: DataTypes.DOUBLE,
      allowNull: true
    }
  }, {
    timestamps: true,
    tableName: 'transaction'
  });

  Transaction.associate = models => {
    Transaction.belongsTo(models.User, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
      as: 'user'
    });

    Transaction.belongsTo(models.User, {
      foreignKey: 'owner_id',
      onDelete: 'CASCADE',
      as: 'owner',
      allowNull: true
    });

    Transaction.belongsTo(models.Payment, {
      foreignKey: 'payment_id',
      onDelete: 'CASCADE',
      as: 'payment',
      allowNull: true
    });

    Transaction.belongsTo(models.Franchise, {
      foreignKey: 'franchise_id',
      onDelete: 'CASCADE',
      as: 'franchise',
      allowNull: true
    });

    Transaction.belongsTo(models.Strategy, {
      foreignKey: 'strategy_id',
      onDelete: 'SET NULL',
      as: 'strategy'
    });

    Transaction.belongsTo(models.UserCheckpoint, {
      foreignKey: 'user_checkpoint_id',
      onDelete: 'SET NULL',
      as: 'user_checkpoint',
      allowNull: true
    });

    Transaction.belongsTo(models.Checkpoint, {
      foreignKey: 'checkpoint_id',
      as: 'checkpoint',
      allowNull: true
    });
  };

  return Transaction;
};
