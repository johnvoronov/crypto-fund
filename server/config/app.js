const path = require('path');

module.exports = {
  name: process.env.APP_NAME || 'fund-local',
  public_url: '/public/media',
  upload_directory: path.resolve(__dirname, '../../public/media'),
  frontend_url: process.env.FRONTEND_URL,
  backend_url: process.env.BACKEND_URL,
  backup_path: path.resolve(__dirname, '../../backup'),
  bitcoin_explorer_transaction_url: 'https://www.blockchain.com/btc/tx',
  sms_key: process.env.SMS_API
};
