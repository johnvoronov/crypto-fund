const express = require('express');
const router = express.Router();
const Joi = require('joi');
const auth = require('../middleware/auth');
const validation = require('../middleware/validation');
const models = require('../models');
const consts = require('../consts');
const { Op } = require('sequelize');
const referralService = require('../service/referral');

/**
 * Вывод баланса по реферальной системе в ETH
 *
 * @param req
 * @param res
 */
router.get('/level', auth, async (req, res) => {
  const rows = await models.Referral.findAll({
    order: [['price', 'ASC']]
  });
  res.json(rows);
});

/**
 * Вывод баланса по реферальной системе в ETH
 *
 * @param req
 * @param res
 */
router.get('/status', auth, async (req, res) => {
  const user = await models.User.findById(req.user.id);
  res.json(await referralService.getStats(user));
});

/**
 * Получить сетку статусов пользователя для дальнейшей покупки
 *
 * @param req
 * @param res
 */
router.get('/grid', auth, async (req, res) => {
  const user = await models.User.findById(req.user.id);
  res.json(await referralService.getGridByUser(user));
});

/**
 * Вывод пользователей по уровню реферальной системы
 *
 * @param req
 * @param res
 */
router.get('', auth, async (req, res) => {
  const user = await models.User.findById(req.user.id);
  const rows = await models.User.findAll({
    where: {
      parents: {
        [Op.contains]: [req.user.id]
      },
      level: {
        [Op.and]: [
          { [Op.lte]: user.level + 3 },
          { [Op.gte]: user.level }
        ]
      }
    },
    order: [
      ['parents', 'ASC']
    ]
  });

  const users = rows.map(row => {
    const item = row.toJSON();
    delete item.password;
    delete item.refresh_token;
    return item;
  });
  const isRoot = node => node.referral_id === req.user.id;

  const incomeItem = item => ({
    where: {
      account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
      owner_id: item.id,
      user_id: user.id,
      status: {
        [Op.not]: consts.TRANSACTION_WITHDRAW
      }
    }
  });

  const tree = await referralService.buildTree(users, 'referral_id', async item => ({
    ...item,
    income: {
      btc: await models.Transaction.sum('btc', incomeItem(item)) || 0,
      usd: await models.Transaction.sum('usd', incomeItem(item)) || 0,
      rub: await models.Transaction.sum('rub', incomeItem(item)) || 0,
      eth: await models.Transaction.sum('eth', incomeItem(item)) || 0
    }
  }), isRoot);

  res.json(tree);
});

router.get('/balance', auth, async (req, res) => {
  const balance = await models.Referral.sum('amount', {
    where: { user_id: req.user.id }
  }) || 0;
  res.json({ balance });
});

router.get('/:id', auth, validation({
  params: {
    id: Joi.number().integer().positive().required()
  }
}), async (req, res) => {
  const referral = await models.Referral.findById(req.params.id);
  if (null === referral) {
    return res.boom.notFound();
  }

  res.json(referral);
});

module.exports = router;
