const express = require('express');
const swaggerUi = require('swagger-ui-express');
const swaggerSchema = require('../swagger/schema');
const router = express.Router();

router.use(
  swaggerUi.serve,
  swaggerUi.setup(swaggerSchema)
);

module.exports = router;
