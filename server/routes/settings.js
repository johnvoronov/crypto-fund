const express = require('express');
const router = express.Router();
const settings = require('../util/settings');

router.get('', async (req, res) => {
  res.json(await settings.fetch(true));
});

module.exports = router;
