const express = require('express');
const auth = require('../middleware/auth');
const validation = require('../middleware/validation');
const twoFactorService = require('../service/twoFactor');
const Joi = require('joi');
const models = require('../models');
const router = express.Router();

/**
 * Отображение пользователю изображения с QR кодом
 * для активации 2fa авторизации
 *
 * @param req
 * @param res
 */
router.get('/qr', auth, async (req, res) => {
  const user = await models.User.findById(req.user.id);
  if (null === user.two_factor_secret) {
    const secret = twoFactorService.generate();
    await user.update({ two_factor_secret: secret.base32 });
  }
  const url = `otpauth://totp/SecretKey?secret=${user.two_factor_secret}`;

  return res.json({ url });
});

/**
 * Включение пользователю 2fa авторизации с подтверждением
 *
 * @param req
 * @param res
 * @returns {*}
 */
router.post('/enable', auth, validation({
  body: {
    token: Joi.string().required()
  }
}), async (req, res) => {
  const { token } = req.body;

  const user = await models.User.findById(req.user.id);

  const verified = twoFactorService.verify(user, token);
  if (!verified) {
    return res.boom.badRequest('invalid query', {
      errors: {
        token: [
          req.t('controller.user.login.invalid_2fa_token')
        ]
      }
    });
  }

  await user.update({ is_two_factor: true });

  res.status(204).end();
});

/**
 * Отключение 2fa авторизации без подтверждения
 *
 * @param req
 * @param res
 */
router.post('/disable', auth, validation({
  body: {
    token: Joi.string().required()
  }
}), async (req, res) => {
  const { token } = req.body;
  const user = await models.User.findById(req.user.id);

  const verified = twoFactorService.verify(user, token);
  if (!verified) {
    return res.boom.badRequest('invalid query', {
      errors: {
        token: [
          req.t('controller.user.login.invalid_2fa_token')
        ]
      }
    });
  }

  await user.update(
    {
      is_two_factor: false
    },
    { where: { id: req.user.id } }
  );

  res.status(204).end();
});

module.exports = router;
