const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validation = require('../middleware/validation');
const auth = require('../middleware/auth');
const report = require('../service/report');
const productivity = require('../service/productivity');

router.get('/my', auth, async (req, res) => {
  res.json(await report.getMy(req.user.id));
});

router.get('/team', auth, async (req, res) => {
  res.json(await report.getTeam(req.user.id));
});

router.get('/activity', auth, async (req, res) => {
  res.json(await report.getActivity(req.user.id));
});

router.get('/activity/pro', auth, async (req, res) => {
  res.json(await report.getActivityPro(req.user.id));
});

router.get('/onetime', auth, async (req, res) => {
  res.json(await report.getOneTime(req.user.id));
});

router.get('/income', auth, async (req, res) => {
  res.json(await report.getIncome(req.user.id));
});

router.get('/partnership', auth, async (req, res) => {
  res.json(await report.getForPartnership(req.user.id));
});

router.get('/month/:year/:month', auth, validation({
  params: {
    year: Joi.number().integer().positive().required(),
    month: Joi.number().integer().positive().required()
  }
}), async (req, res) => {
  const date = new Date();
  date.setMonth(req.params.month);
  date.setFullYear(req.params.year);

  res.json(await report.getMonthReport(req.user, date));
});

router.get('/year/download', auth, async (req, res) => {
  const reportFile = await report.downloadYearReport(req.user);

  reportFile.write('report.xlsx', res);
});

router.get('/year/:year', auth, validation({
  params: {
    year: Joi.number().integer().positive().required()
  }
}), async (req, res) => {
  const date = new Date();
  date.setFullYear(req.params.year);

  res.json(await report.getYearReport(req.user, date));
});

router.get('/productivity', auth, async (req, res) => {
  res.json({
    month: await productivity.getMonthReport(req.user.id),
    total: await productivity.getTotalReport(req.user.id)
  });
});

module.exports = router;
