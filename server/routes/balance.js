const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const balanceService = require('../service/balance');

router.get('', auth, async (req, res) => {
  res.json(await balanceService.getResponse({ user_id: req.user.id }));
});

module.exports = router;
