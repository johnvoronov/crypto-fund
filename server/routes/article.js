const express = require('express');
const router = express.Router();
const guard = require('express-jwt-permissions')();
const auth = require('../middleware/auth');
const validation = require('../middleware/validation');
const models = require('../models');
const Joi = require('joi');
const filterValues = require('../util/filterValues');

router.get(
  '',
  auth,
  validation({
    query: {
      page_size: Joi.number().integer().min(1).max(300).default(100),
      page: Joi.number().integer().min(1).default(1)
    }
  }),
  async (req, res) => {
    const result = await models.Article.findAndCountAll({
      limit: req.query.page_size,
      offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
      order: [
        ['id', 'DESC']
      ]
    });

    res.json({
      meta: {
        page: req.query.page,
        page_size: req.query.page_size,
        page_count: Math.ceil(result.count / req.query.page_size),
        total: result.count
      },
      objects: result.rows
    });
  }
);

router.post(
  '',
  auth,
  guard.check(['admin']),
  validation({
    body: {
      title: Joi.string().required(),
      sub_title: Joi.string().required(),
      content: Joi.string().required(),
      content_short: Joi.string().required()
    }
  }),
  async (req, res) => {
    const article = await models.Article.create(filterValues(req.body));

    res.status(201).json({
      id: article.id
    });
  }
);

router.get(
  '/:id',
  auth,
  validation({
    params: {
      id: Joi.number().integer().required()
    }
  }),
  async (req, res) => {
    const article = await models.Article.findById(req.params.id);

    if (!article) {
      return res.boom.notFound();
    }

    res.json(article);
  }
);

router.post(
  '/:id',
  auth,
  guard.check(['admin']),
  validation({
    params: {
      id: Joi.number().integer().required()
    },
    body: {
      title: Joi.string(),
      sub_title: Joi.string(),
      content: Joi.string(),
      content_short: Joi.string()
    }
  }),
  async (req, res) => {
    const article = await models.Article.findById(req.params.id);

    if (!article) {
      return res.boom.notFound();
    }

    await article.update(filterValues(req.body));

    res.status(204).end();
  }
);

router.delete(
  '/:id',
  auth,
  guard.check(['admin']),
  validation({
    params: {
      id: Joi.number().integer().required()
    }
  }),
  async (req, res) => {
    const article = await models.Article.findById(req.params.id);

    if (!article) {
      return res.boom.notFound();
    }

    await article.destroy();

    res.status(204).end();
  }
);

module.exports = router;
