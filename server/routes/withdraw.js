const express = require('express');
const router = express.Router();
const guard = require('express-jwt-permissions')();
const auth = require('../middleware/auth');
const models = require('../models');
const transactionService = require('../service/transaction');
const twoFactorService = require('../service/twoFactor');
const balanceService = require('../service/balance');
const filterValues = require('../util/filterValues');
const validation = require('../middleware/validation');
const consts = require('../consts');
const Joi = require('joi');
const mail = require('../service/mail');
const referralService = require('../service/referral');

router.post('', auth, validation({
  body: {
    strategy_id: Joi.number().positive().required(),
    btc_address: Joi.string().required(),
    amount: Joi.number().positive().required(),
    token: Joi.number().label('validation.token')
  }
}), async (req, res) => {
  const {
    strategy_id,
    amount,
    token
  } = req.body;

  const strategy = await models.Strategy.findById(req.body.strategy_id);
  if (null === strategy) {
    return res.boom.badRequest(null, {
      errors: {
        strategy_id: [
          req.t('controller.withdraw.withdraw.strategy_not_found')
        ]
      }
    });
  }

  const userBalance = await models.Transaction.sum('amount', {
    where: {
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      user_id: req.user.id,
      strategy_id: strategy.id
    }
  }) || 0;
  if (req.body.amount > userBalance) {
    return res.boom.badRequest(null, {
      errors: {
        amount: [
          req.t('controller.withdraw.withdraw.insufficient_funds')
        ]
      }
    });
  }

  const user = await models.User.findById(req.user.id);
  if (user.is_two_factor) {
    if (!token) {
      return res.boom.notAcceptable();
    }

    const verified = twoFactorService.verify(user, token);
    if (!verified) {
      return res.boom.badRequest(null, {
        errors: {
          token: [
            req.t('controller.user.login.invalid_2fa_token')
          ]
        }
      });
    }
  }

  const tx = await transactionService.createWithdraw(
    req.user.id,
    amount,
    strategy_id
  );

  const withdraw = await models.Withdraw.create(filterValues({
    ...req.body,
    user_id: req.user.id,
    transaction_id: tx.id,
    type: consts.WITHDRAW_STRATEGY
  }));

  referralService.updateUsers();

  mail.send(req.user.email, 'withdraw_create', {
    name: req.user.first_name,
    strategy: strategy.name,
    amount: withdraw.amount
  });

  res.status(201).json({ id: withdraw.id });
});

router.post('/partner', auth, validation({
  body: {
    btc_address: Joi.string().required(),
    amount: Joi.number().positive().required(),
    token: Joi.number().label('validation.token')
  }
}), async (req, res) => {
  const { amount, token } = req.body;

  const userBalance = await balanceService.getPartnershipBalance({
    user_id: req.user.id
  });
  if (req.body.amount > userBalance) {
    return res.boom.badRequest(null, {
      errors: {
        amount: [
          req.t('controller.withdraw.withdraw.insufficient_funds')
        ]
      }
    });
  }

  const user = await models.User.findById(req.user.id);
  if (user.is_two_factor) {
    if (!token) {
      return res.boom.notAcceptable();
    }

    const verified = twoFactorService.verify(user, token);
    if (!verified) {
      return res.boom.badRequest(null, {
        errors: {
          token: [
            req.t('controller.user.login.invalid_2fa_token')
          ]
        }
      });
    }
  }

  const tx = await transactionService.createWithdraw(
    req.user.id,
    amount
  );
  await tx.update({
    comment: 'partner withdraw'
  });

  const withdraw = await models.Withdraw.create(filterValues({
    ...req.body,
    user_id: req.user.id,
    transaction_id: tx.id,
    type: consts.WITHDRAW_PARTNERSHIP
  }));

  referralService.updateUsers();

  mail.send(req.user.email, 'withdraw_partner_create', {
    name: req.user.first_name,
    amount: withdraw.amount
  });

  res.status(201).json({
    id: withdraw.id
  });
});

router.post('/confirm/:id', auth, guard.check(['admin']), validation({
  params: {
    id: Joi.number().positive().required()
  },
  body: {
    transaction_hash: Joi.string().required()
  }
}), async (req, res) => {
  const withdraw = await models.Withdraw.findById(req.params.id);
  if (null === withdraw) {
    return res.boom.notFound();
  }

  await withdraw.update(filterValues(req.body));

  referralService.updateUsers();

  res.status(204).end();
});

module.exports = router;
