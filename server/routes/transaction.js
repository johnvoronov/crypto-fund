const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const validation = require('../middleware/validation');
const Joi = require('joi');
const models = require('../models');
const { Op } = require('sequelize');
const consts  = require('../consts');

const include = [
  {
    model: models.Payment,
    as: 'payment',
    include: [
      {
        model: models.Currency,
        as: 'currency'
      }
    ]
  },
  {
    model: models.Strategy,
    as: 'strategy',
    include: [
      {
        model: models.Currency,
        as: 'currency'
      }
    ]
  }
];

async function getTransactionStats(where) {
  const first = await models.Transaction.findOne({
    where: where,
    order: [
      ['id', 'ASC']
    ]
  });
  const last = await models.Transaction.findOne({
    where: where,
    order: [
      ['id', 'DESC']
    ]
  });

  return {
    amount: await models.Transaction.sum('amount', { where }) || 0,
    count: await models.Transaction.count({ where }) || 0,
    first: first ? first.created_at : null,
    last: last ? last.created_at : null
  };
}

router.get(
  '/stats',
  auth,
  async (req, res) => {
    res.json({
      withdraw: await getTransactionStats({
        user_id: req.user.id,
        status: consts.TRANSACTION_WITHDRAW
      }),
      income: await getTransactionStats({
        user_id: req.user.id,
        status: consts.TRANSACTION_PAYMENT
      })
    });
  });

router.get(
  '',
  auth,
  validation({
    query: {
      date_from: Joi.date().iso().default(null),
      date_to: Joi.date().iso().default(null),
      status: Joi.number().integer().default(null),
      page_size: Joi.number().integer().min(1).max(300).default(10),
      page: Joi.number().integer().min(1).default(1)
    }
  }),
  /**
   * Получение списка транзакций (операций по балансу пользователя).
   *
   * В данном контексте транзакция по конкретной операции - это зачисление
   * бонуса, бонус по реф системе и прочее.
   *
   * @param req
   * @param res
   * @returns {Promise<T>}
   */
  async (req, res) => {
    let where = {
      user_id: req.user.id
    };

    if (req.query.status) {
      where = {
        ...where,
        status: req.query.status
      };
    }

    if (req.query.date_from && req.query.date_to) {
      where = {
        ...where,
        created_at: {
          [Op.between]: [
            req.query.date_from,
            req.query.date_to
          ]
        }
      };
    } else if (req.query.date_from) {
      where = {
        ...where,
        created_at: {
          [Op.gte]: req.query.date_from
        }
      };
    } else if (req.query.date_to) {
      where = {
        ...where,
        created_at: {
          [Op.lte]: req.query.date_to
        }
      };
    }

    const result = await models.Transaction.findAndCountAll({
      where,
      include,
      limit: req.query.page_size,
      offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
      order: [
        ['id', 'DESC']
      ]
    });

    res.json({
      meta: {
        page: req.query.page,
        page_size: req.query.page_size,
        page_count: Math.ceil(result.count / req.query.page_size),
        total: result.count
      },
      objects: result.rows
    });
  }
);

router.get(
  '/all',
  auth,
  validation({
    query: {
      date_from: Joi.date().iso().default(null),
      date_to: Joi.date().iso().default(null),
      status: Joi.number().integer().default(null),
    }
  }),
  async (req, res) => {
    let where = {
      user_id: req.user.id
    };

    if (req.query.status) {
      where = {
        ...where,
        status: req.query.status
      };
    }

    if (req.query.date_from && req.query.date_to) {
      where = {
        ...where,
        created_at: {
          [Op.between]: [
            req.query.date_from,
            req.query.date_to
          ]
        }
      };
    } else if (req.query.date_from) {
      where = {
        ...where,
        created_at: {
          [Op.gte]: req.query.date_from
        }
      };
    } else if (req.query.date_to) {
      where = {
        ...where,
        created_at: {
          [Op.lte]: req.query.date_to
        }
      };
    }

    const result = await models.Transaction.findAll({
      where,
      include,
      order: [
        ['id', 'DESC']
      ]
    });

    res.json(result);
  }
);

router.post(
  '/unfreeze/:id',
  auth,
  validation({
    params: {
      id: Joi.number().integer().positive().required()
    }
  }),
  /**
   * Получение списка транзакций (операций по балансу пользователя).
   *
   * В данном контексте транзакция по конкретной операции - это зачисление
   * бонуса, бонус по реф системе и прочее.
   *
   * @param req
   * @param res
   * @returns {Promise<T>}
   */
  async (req, res) => {
    const tx = await models.Transaction.findById(req.params.id);
    if (null === tx) {
      return res.boom.notFound();
    }

    await tx.update({
      is_freeze: false
    });

    res.status(204).end();
  }
);

module.exports = router;
