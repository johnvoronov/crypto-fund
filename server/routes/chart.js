const express = require('express');
const router = express.Router();
const Joi = require('joi');
const auth = require('../middleware/auth');
const models = require('../models');
const validation = require('../middleware/validation');
const chart = require('../service/chart');
const consts = require('../consts');

router.get('/strategy/:strategy_id', auth, validation({
  params: {
    strategy_id: Joi.number().integer().positive().required()
  }
}), async (req, res) => {
  const { strategy_id } = req.params;

  res.json(await chart.getStrategyChartData(
    req.user.id,
    strategy_id
  ));
});

router.get('/strategy', auth, async (req, res) => {
  const strategies = await models.Strategy.findAll();
  let data = {};

  for (let i = 0; i < strategies.length; i++) {
    data[strategies[i].id] = await chart.getStrategyChartData(
      req.user.id,
      strategies[i].id
    );
  }
  res.json(data);
});

router.get('/fund-income/:period?/:interval?', auth, validation({
  params: {
    interval: Joi.string().allow('week', 'month', 'year').default('week'),
    period: Joi.string().allow('month', 'year').default('year')
  }
}), async (req, res) => {
  const { interval, period } = req.params;

  res.json(await chart.getFundIncomeChartData(
    consts.TRANSACTION_FUND_INCOME,
    period,
    interval
  ));
});

router.get('/data/:status?/:period?/:interval?', auth, validation({
  params: {
    interval: Joi.string().allow('week', 'month', 'year').default('week'),
    period: Joi.string().allow('month', 'year').default('year'),
    status: Joi.string().allow(...Object.values(consts)).default(consts.TRANSACTION_PAYMENT)
  }
}), async (req, res) => {
  const { interval, period, status } = req.params;

  res.json(await chart.getChartDataByStatus(
    consts[status],
    period,
    interval,
    req.user.id
  ));
});

router.get('/partnership/:period?/:interval?', auth, validation({
  params: {
    interval: Joi.string().allow('week', 'month', 'year').default('week'),
    period: Joi.string().allow('month', 'year').default('year')
  }
}), async (req, res) => {
  const { interval, period } = req.params;

  res.json(await chart.getPartnershipChartData(
    period,
    interval,
    req.user.id
  ));
});

router.get('/strategies-pie', auth, async (req, res) => {
  res.json(await chart.getStrategiesPieChartData());
});

module.exports = router;
