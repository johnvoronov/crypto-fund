const express = require('express');
const { Op } = require('sequelize');
const router = express.Router();
const Joi = require('joi');
const auth = require('../middleware/auth');
const validation = require('../middleware/validation');
const models = require('../models');
const wallet = require('../service/wallet');
const consts = require('../consts');
const settingsService = require('../util/settings');

/**
 * Создание кошелька для пользователя. Кошельки под
 * конкретную стратегию создаются через coinpayments.
 *
 * @param req
 * @param res
 */
router.post('', auth, validation({
  body: {
    referral_id: Joi.number().required()
  }
}), async (req, res) => {
  const {
    referral_id
  } = req.body;

  const currency = await models.Currency.findOne({
    where: { code: 'BTC' }
  });
  if (null === currency) {
    return res.boom.notFound();
  }

  const referral = await models.Referral.findById(referral_id);
  if (referral === null) {
    return res.boom.notFound();
  }

  const userWallet = await wallet.getInternalWallet(req.user, currency);
  const coinpaymentPercent = await settingsService.getBool('deduct_coinpayments_tax', false) ? 0.5 : 0;

  const btc = await models.Currency.findOne({
    where: { code: 'BTC' }
  });

  const currentAmount = await models.Transaction.sum('usd', {
    where: {
      account_type: consts.ACCOUNT_TYPE_STATUS,
      user_id: req.user.id
    }
  }) || 0;
  const price = (referral.price || 0) - currentAmount;
  const btcAmount = price / btc.to_usd;

  res.json({
    amount: {
      btc: coinpaymentPercent > 0
        ? btcAmount - ((btcAmount / 100) * coinpaymentPercent)
        : btcAmount
    },
    address: userWallet.address
  });
});

module.exports = router;
