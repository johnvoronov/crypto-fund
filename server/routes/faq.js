const express = require('express');
const router = express.Router();
const guard = require('express-jwt-permissions')();
const auth = require('../middleware/auth');
const validation = require('../middleware/validation');
const models = require('../models');
const Joi = require('joi');
const filterValues = require('../util/filterValues');
const mail = require('../service/mail');

router.get('', auth, async (req, res) => {
  const rows = await models.Faq.findAll({
    order: [
      ['position', 'DESC']
    ]
  });

  res.json(rows);
});

router.post('', auth, guard.check(['admin']), validation({
  body: {
    question_en: Joi.string().required(),
    question_ru: Joi.string().required(),
    answer_ru: Joi.string().required(),
    answer_en: Joi.string().required(),
    position: Joi.number().integer().default(0)
  }
}), async (req, res) => {
  const faq = await models.Faq.create(filterValues(req.body));

  res.status(201).json({
    id: faq.id
  });
});

router.post('/question', auth, validation({
  body: {
    question: Joi.string().required(),
    subject: Joi.string().required()
  }
}), async (req, res) => {
  const question = await models.Question.create(filterValues({
    ...req.body,
    user_id: req.user.id
  }));

  const admins = await models.User.findAll({
    where: { is_admin: true }
  });
  for (let i = 0; i < admins.length; i++) {
    await mail.send(admins[i].email, 'faq_question', {
      question
    });
  }

  res.status(204).json();
});

router.post('/:id', auth, guard.check(['admin']), validation({
  params: {
    id: Joi.number().integer().required()
  },
  body: {
    question_en: Joi.string().allow(null, ''),
    question_ru: Joi.string().allow(null, ''),
    answer_ru: Joi.string().allow(null, ''),
    answer_en: Joi.string().allow(null, ''),
    position: Joi.number().integer().allow(null, '')
  }
}), async (req, res) => {
  const faq = await models.Faq.findById(req.params.id);
  if (null === faq) {
    return res.boom.notFound();
  }

  await faq.update(filterValues(req.body));

  res.status(204).end();
});

router.delete('/:id', auth, guard.check(['admin']), validation({
  params: {
    id: Joi.number().integer().required()
  }
}), async (req, res) => {
  const faq = await models.Faq.findById(req.params.id);
  if (null === faq) {
    return res.boom.notFound();
  }

  await faq.destroy();

  res.status(204).end();
});

module.exports = router;
