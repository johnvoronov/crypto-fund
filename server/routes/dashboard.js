const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

router.get(
  '',
  auth,
  /**
   * @param req
   * @param res
   */
  async (req, res) => {
    res.json({});
  }
);

module.exports = router;
