const express = require('express');
const router = express.Router();

router.use('/dashboard', require('./dashboard'));
router.use('/transaction', require('./transaction'));
router.use('/user', require('./user'));
router.use('/2fa', require('./two_factor'));
router.use('/payment', require('./payment'));
router.use('/article', require('./article'));
router.use('/balance', require('./balance'));
router.use('/strategy', require('./strategy'));
router.use('/franchise', require('./franchise'));
router.use('/report', require('./report'));
router.use('/chart', require('./chart'));
router.use('/faq', require('./faq'));
router.use('/currency', require('./currency'));
router.use('/referral', require('./referral'));
router.use('/wallet', require('./wallet'));
router.use('/withdraw', require('./withdraw'));
router.use('/settings', require('./settings'));
router.use('/ipn', require('./ipn'));
router.use('/doc', require('./doc'));

if (process.env.NODE_ENV !== 'production') {
  router.use('/system', require('./system'));
}

router.use('/admin', require('./admin'));

module.exports = router;
