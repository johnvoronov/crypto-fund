const express = require('express');
const Joi = require('joi');
const zxcvbn = require('zxcvbn');
const phoneService = require('../service/phone');
const smsService = require('../service/sms');
const fileUpload = require('express-fileupload');
const router = express.Router();
const validation = require('../middleware/validation');
const auth = require('../middleware/auth');
const settings = require('../util/settings');
const models = require('../models');
const jwt = require('../util/jwt');
const referralService = require('../service/referral');
const twoFactorSevice = require('../service/twoFactor');
const uploader = require('../util/uploader');
const filterValues = require('../util/filterValues');
const passwordUtil = require('../util/password');
const mail = require('../service/mail');

/**
 * Авторизация пользователя с JWT токеном
 *
 * @param req
 * @param res
 */
router.post('/login', validation({
  body: {
    login: Joi.string().required().label('validation.login'),
    password: Joi.string().min(6).max(50).required().label('validation.password'),
    token: Joi.number().label('validation.token')
  }
}), async (req, res) => {
  const { login, password, token } = req.body;

  const user = await models.User.findOne({
    where: login.indexOf('@') > -1
      ? { email: login.toLowerCase() }
      : { login: login.toLowerCase() }
  });

  if (!user) {
    return res.boom.badRequest(null, {
      errors: {
        password: [req.t('controller.user.login.invalid_username_or_password')]
      }
    });
  }

  if (!user.is_active) {
    return res.boom.badRequest(null, {
      errors: {
        email: [req.t('controller.user.login.inactive_account')]
      }
    });
  }

  const valid = await passwordUtil.compare(password, user.password);
  if (false === valid) {
    return res.boom.badRequest(null, {
      errors: {
        password: [req.t('controller.user.login.invalid_username_or_password')]
      }
    });
  }

  if (user.is_two_factor) {
    if (!token) {
      return res.boom.notAcceptable();
    }

    const valid = twoFactorSevice.verify(user, token);
    if (false === valid) {
      return res.boom.badRequest(null, {
        errors: {
          token: [
            req.t('controller.user.login.invalid_2fa_token')
          ]
        }
      });
    }
  }

  await user.update({
    refresh_token: passwordUtil.token()
  });

  res.json(jwt.response(await user.reload()));
});

router.post('/phone/confirm', auth, validation({
  body: {
    phone_code: [
      Joi.number().label('validation.code'),
      Joi.string().label('validation.code')
    ]
  }
}), async (req, res) => {
  const { phone_code } = req.body;

  const user = await models.User.findById(req.user.id);
  if (user === null) {
    return req.boom.notFound();
  }

  if (user.phone_code !== String(phone_code)) {
    return res.boom.badRequest(null, {
      errors: {
        phone_code: [
          req.t('validation.invalid_phone_code')
        ]
      }
    });
  }

  await user.update({
    phone_code: null,
    is_phone_confirmed: true
  });

  return res.json(jwt.getPublicFields(user));
});

router.post('/phone/resend', auth, async (req, res) => {
  const user = await models.User.findById(req.user.id);
  if (user === null) {
    return req.boom.notFound();
  }

  if (!user.phone_code) {
    await user.update({
      phone_code: passwordUtil.randomToken()
    });
  }
  await smsService.send(user.phone, `${req.t('confirmation_code')}: ${user.phone_code}`);

  return res.status(204).end();
});

router.post('/phone', auth, validation({
  body: {
    phone: [
      Joi.string().required().label('validation.phone'),
      Joi.number().required().label('validation.phone')
    ]
  }
}), async (req, res) => {
  const { phone } = req.body;

  const validPhone = phoneService.validate(phone);
  if (false === validPhone) {
    return res.boom.badRequest(null, {
      errors: {
        phone: [
          req.t('validation.phone_error')
        ]
      }
    });
  }

  const user = await models.User.findById(req.user.id);
  if (user === null) {
    return req.boom.notFound();
  }

  await user.update({
    phone: validPhone
  });
  if (!user.phone_code) {
    await user.update({
      phone_code: passwordUtil.randomToken()
    });
  }
  await smsService.send(user.phone, `${req.t('confirmation_code')}: ${user.phone_code}`);

  return res.status(204).end();
});

/**
 * Регистрация пользователя
 *
 * @param req
 * @param res
 */
router.post('/registration', validation({
  body: {
    is_agree: Joi.boolean().required().label('validation.is_agree'),
    login: Joi.string().required().label('validation.login'),
    email: Joi.string().email().required().label('validation.email'),
    password: Joi.string().min(6).max(50).required().label('validation.password'),
    password_confirm: Joi.string()
      .min(6)
      .max(50)
      .required()
      .valid(Joi.ref('password'))
      .options({
        language: {
          any: {
            allowOnly: '!!Passwords do not match'
          }
        }
      })
      .label('validation.password_confirm'),
    referral_id: Joi.any().allow(null, '').default(null)
  }
}), async (req, res) => {
  const {
    login,
    email,
    password,
    referral_id
  } = req.body;

  const inviteOnly = await settings.getBool('user_registration_invite_only', false);
  if (inviteOnly) {
    if (!referral_id) {
      return res.boom.badRequest(null, {
        errors: {
          referral_id: [req.t('controller.user.registration.invite_only')]
        }
      });
    } else if (referral_id) {
      // Проверяем существование пользователя
      const refUser = await referralService.findUser(referral_id);
      if (null === refUser) {
        // Если пользователя нет, то запрещаем регистрацию
        return res.boom.badRequest(null, {
          errors: {
            referral_id: [
              req.t('controller.user.registration.invite_only_no_user')
            ]
          }
        });
      }
    }
  }

  if (zxcvbn(password).score < 3) {
    return res.boom.badRequest(null, {
      errors: {
        password: [req.t('controller.user.registration.weak_password')]
      }
    });
  }

  if ((await models.User.count({ where: { email: email.toLowerCase() } })) > 0) {
    return res.boom.badRequest(null, {
      errors: {
        email: [req.t('controller.user.registration.email_already_taken')]
      }
    });
  }

  if ((await models.User.count({ where: { login: login.toLowerCase() } })) > 0) {
    return res.boom.badRequest(null, {
      errors: {
        login: [req.t('controller.user.registration.login_already_taken')]
      }
    });
  }

  const extra = await referralService.findReferral(referral_id);

  const user = await models.User.create({
    login: login.toLowerCase(),
    email: email.toLowerCase(),
    token: passwordUtil.token(),
    password: await passwordUtil.hash(password),
    phone_code: passwordUtil.randomToken(),
    ...extra
  });

  await referralService.updateUser(user);

  await mail.send(email, 'user_registration', { user });

  res.status(201).json({ id: user.id });
});

/**
 * Регистрация пользователя
 *
 * @param req
 * @param res
 */
router.post('/profile', auth, fileUpload({
  limits: 2 * 1024 * 1024
}), validation({
  body: {
    btc_address: Joi.number().allow(null, '').label('validation.btc_address'),
    first_name: Joi.string().allow(null, '').label('validation.email'),
    last_name: Joi.string().allow(null, '').label('validation.email'),
    country: Joi.string().allow(null, '').label('validation.email'),
    city: Joi.string().allow(null, '').label('validation.email'),
    token: Joi.number().label('validation.token')
  }
}), async (req, res) => {
  const {
    btc_address,
    first_name,
    last_name,
    country,
    city,
    token
  } = req.body;

  const user = await models.User.findById(req.user.id);

  if (user.is_two_factor && btc_address) {
    if (!token) {
      return res.boom.notAcceptable();
    }

    const valid = twoFactorSevice.verify(user, token);
    if (false === valid) {
      return res.boom.badRequest(null, {
        errors: {
          token: [
            req.t('controller.user.login.invalid_2fa_token')
          ]
        }
      });
    }
  }

  let avatar = '___undefined';
  if (req.files && req.files.avatar) {
    avatar = await uploader.uploadThumb(req.files.avatar, 80, 80);
  }

  if (null === user) {
    return res.boom.notFound();
  }

  if (btc_address !== user.btc_address) {
    await mail.send(req.user.email, 'wallet_confirm', {
      name: first_name
    });
  }

  await user.update(filterValues({
    btc_address,
    first_name,
    last_name,
    country,
    city,
    avatar
  }));

  res.json(jwt.getPublicFields(user));
});

/**
 * Восстановление доступа к учетной записи пользователя
 *
 * @param req
 * @param res
 */
router.post('/restore', validation({
  body: {
    email: Joi.string().email().required().label('validation.email')
  }
}), async (req, res) => {
  const { email } = req.body;

  const user = await models.User.findOne({
    where: { email }
  });
  if (!user) {
    return res.boom.badRequest(null, {
      errors: {
        email: [req.t('controller.user.invalid_email_or_user_not_found')]
      }
    });
  }

  await user.update({ token: passwordUtil.token() });

  await mail.send(user.email, 'user_restore', { user });

  res.status(204).end();
});

/**
 * Подтверждение пользователем восстановления пароля
 * Метод почти полностью дублирует изменение пароля пользователем, но
 * с дополнительной проверкой по корректности полученного токена из письма
 *
 * @param req
 * @param res
 */
router.post('/restore/confirm', validation({
  body: {
    token: Joi.string().required().label('validation.token'),
    password: Joi.string().required().min(6).max(50).label('validation.password'),
    password_confirm: Joi.string()
      .required()
      .min(6)
      .max(50)
      .valid(Joi.ref('password'))
      .options({
        language: {
          any: {
            allowOnly: '!!Passwords do not match'
          }
        }
      })
      .label('validation.password_confirm')
  }
}), async (req, res) => {
  const { token, password, password_confirm } = req.body;

  const user = await models.User.findOne({
    where: { token }
  });

  if (!user) {
    return res.boom.badRequest(null, {
      errors: {
        password: [req.t('controller.invalid_token')]
      }
    });
  }

  if (password !== password_confirm) {
    return res.boom.badRequest(null, {
      errors: {
        password: [req.t('controller.user.password_not_match')]
      }
    });
  }

  await user.update({
    // Активируем учетную запись пользователя, так как
    // восстановление учетки подразумевает отправку письма на
    // ящик пользователя, что само сабой подразумевает подтверждение
    // ящика электронной почты пользователя
    is_active: true,
    // Обнуляем токен воизбежание переиспользования
    token: null,
    password: await passwordUtil.hash(password)
  });

  res.json(jwt.response(user));
});

/**
 * Проверка токена полученного пользователем по эл. почте перед установкой пароля.
 * Пользователь заказывает восстановление пароля ->
 * Вводит токен с эл. почты ->
 * Проверяем токен на корректность ->
 * Если верный, позволяем задать новый пароль с тем же токеном. Иначе валим ошибку.
 *
 * @param req
 * @param res
 */
router.post('/restore/check', validation({
  body: {
    token: Joi.string().required().label('validation.token')
  }
}), async (req, res) => {
  const { token } = req.body;

  const user = await models.User.findOne({
    where: { token }
  });

  if (!user) {
    return res.boom.badRequest(null, {
      errors: {
        token: [req.t('controller.invalid_token')]
      }
    });
  }

  res.status(204).end();
});

/**
 * Повторная отправка письма с активацией учетной записи.
 * Если пользователь был ранее уже активирован, то возвращается
 * 400 ответ от сервера
 *
 * @param req
 * @param res
 */
router.post('/restore/resend', validation({
  body: {
    email: Joi.string().email().required().label('validation.email')
  }
}), async (req, res) => {
  const { email } = req.body;

  const user = await models.User.findOne({
    where: { email }
  });

  if (!user) {
    return res.boom.badRequest(null, {
      errors: {
        email: [req.t('controller.user.invalid_email_or_user_not_found')]
      }
    });
  }

  const newUser = await user.update({
    token: passwordUtil.token()
  });

  await mail.send(newUser.email, 'user_restore', {
    user: newUser
  });

  return res.status(204).end();
});

/**
 * Изменение пароля текущего пользователя
 *
 * @param req
 * @param res
 */
router.post('/change_password', auth, validation({
  body: {
    password_current: Joi.string().required().min(6).max(50).label('validation.password_current'),
    password: Joi.string().required().min(6).max(50).label('validation.password'),
    password_confirm: Joi.string()
      .required()
      .min(6)
      .max(50)
      .valid(Joi.ref('password'))
      .options({
        language: {
          any: {
            allowOnly: '!!Passwords do not match'
          }
        }
      })
      .label('validation.password_confirm')
  }
}), async (req, res) => {
  const { password_current, password, password_confirm } = req.body;

  // Не обязательная проверка, так как факт существования пользователя
  // с данным ID проверяется в auth middleware
  const user = await models.User.findById(req.user.id);

  const valid = await passwordUtil.compare(password_current, user.password);
  if (!valid) {
    return res.boom.badRequest(null, {
      errors: {
        password_current: [req.t('controller.user.change_password.invalid_password')]
      }
    });
  }

  if (password !== password_confirm) {
    return res.boom.badRequest(null, {
      errors: {
        password_confirm: [req.t('controller.user.password_not_match')]
      }
    });
  }

  await user.update({
    password: await passwordUtil.hash(password)
  });

  res.status(204).send();
});

/**
 * Повторная отправка письма с активацией учетной записи.
 * Если пользователь был ранее уже активирован, то возвращается
 * 400 ответ от сервера
 *
 * @param req
 * @param res
 */
router.post('/registration/resend', validation({
  body: {
    email: Joi.string().email().required().label('validation.email')
  }
}), async (req, res) => {
  const { email } = req.body;

  const user = await models.User.findOne({
    where: { email, is_active: false }
  });

  if (!user) {
    return res.boom.badRequest(null, {
      errors: {
        email: [req.t('controller.user.invalid_email_or_user_not_found')]
      }
    });
  }

  const newUser = await user.update({
    token: passwordUtil.token()
  });

  await mail.send(email, 'user_registration', {
    user: newUser,
    token: newUser.token
  });

  res.status(204).end();
});

router.post('/refresh_token', validation({
  body: {
    email: Joi.string().email().required().label('validation.email'),
    refresh_token: Joi.string().required().label('validation.refresh_token')
  }
}), async (req, res) => {
  const { email, refresh_token } = req.body;

  const user = await models.User.findOne({
    where: { email }
  });

  if (!user) {
    return res.boom.notFound('user not found');
  }

  if (user.refresh_token !== refresh_token) {
    return res.boom.unauthorized(null, {
      error: 'invalid refresh token'
    });
  }

  return res.json(jwt.response(user));
});

/**
 * Подтверждение электронного адреса пользователя
 *
 * @param req
 * @param res
 */
router.post('/registration/confirm', validation({
  body: {
    token: Joi.string().required().label('validation.token')
  }
}), async (req, res) => {
  const { token } = req.body;

  const user = await models.User.findOne({
    where: { token }
  });

  if (!user) {
    return res.boom.badRequest(null, {
      errors: {
        token: [req.t('controller.invalid_token')]
      }
    });
  }

  await user.update({
    is_active: true,
    token: null
  });

  res.json(jwt.response(user));
});

module.exports = router;
