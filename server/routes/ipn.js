const express = require('express');
const router = express.Router();
const mail = require('../service/mail');
const referralService = require('../service/referral');
const ipn = require('../middleware/ipn');
const transactionService = require('../service/transaction');
const { callback } = require('../util/coinpayment_system');
const consts = require('../consts');

/**
 * IPN запрос от coinpayments, в документацию swagger добавлять не нужно.
 * callback вынесен в отдельный метод для простоты тестирования бизнес логики.
 *
 * @param req
 * @param res
 * @returns {*}
 */
router.post('', ipn, async (req, res) => {
  const {
    status
  } = req.body;

  const statusInt = parseInt(status, 10);

  let state = null;
  if (statusInt < 0) {
    state = consts.PAYMENT_ERROR;
  }
  if (statusInt < 100) {
    state = consts.PAYMENT_PENDING;
  }
  if (statusInt === 100) {
    state = consts.PAYMENT_COMPLETE;
  }

  const {
    payment,
    wallet
  } = await callback(state, req.body);

  const user = await wallet.getUser();
  if (!user) {
    return res.boom.notFound(null, {
      error: 'User not found'
    });
  }

  const emailData = { payment, wallet, user };

  // Только для успешно проведенного платежа создаем транзакцию и
  // отправляем пользователю уведомление на почту
  if (payment.status === consts.PAYMENT_COMPLETE) {
    const tx = await transactionService.createPaymentForStatus(payment, user);
    mail.send(user.email, 'payment_success', {
      ...emailData,
      transaction: tx
    });

    // Если транзакция прошла успешно, то добавляем статус пользователю
    const referral = await referralService.buyStatus(user);
    if (referral) {
      // TODO send user notification
      // TODO get mail template for new email notification
      // mail.send(user.email, 'new_referral_level', {
      //   ...emailData,
      //   referral,
      //   transaction: tx
      // });
    }

    res.status(201).json({ id: tx.id });
  } else {
    res.status(200).end();
  }
});

module.exports = router;
