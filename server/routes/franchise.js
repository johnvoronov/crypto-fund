const express = require('express');
const router = express.Router();
const models = require('../models');
const auth = require('../middleware/auth');

router.post('/join', auth, async (req, res) => {
  const franchise = await models.Franchise.create({
    user_id: req.user.id
  });
  res.status(201).json({ id: franchise.id });
});

module.exports = router;
