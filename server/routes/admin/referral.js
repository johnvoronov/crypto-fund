const express = require('express');
const router = express.Router();
const { Op } = require('sequelize');
const auth = require('../../middleware/auth');
const filterValues = require('../../util/filterValues');
const jwt = require('../../util/jwt');
const models = require('../../models');
const consts = require('../../consts');
const referral = require('../../service/referral');
const validation = require('../../middleware/validation');
const sequelizeUtils = require('../../util/sequelizeUtils');
const Joi = require('joi');
const referralService = require('../../service/referral');

router.get('', auth, async (req, res) => {
  res.json({
    objects: await models.Referral.findAll({
      order: sequelizeUtils.getOrderBy(models.Referral, req.query.order, [
        ['id', 'DESC']
      ])
    })
  });
});

router.post('/user', auth, validation({
  body: {
    user_id: Joi.number().required(),
    minimal_referral_level_id: Joi.number().required()
  }
}), async (req, res) => {
  const {
    user_id,
    minimal_referral_level_id
  } = req.body;

  const user = await models.User.findById(user_id);
  if (null === user) {
    return res.boom.notFound();
  }

  const level = await models.Referral.findById(minimal_referral_level_id);
  if (null === level) {
    return res.boom.notFound();
  }

  await referralService.setMinimalLevel(user, level);
  res.status(204).end();
});

router.get('/options', auth, async (req, res) => {
  const rows = await models.Referral.findAll();

  res.json(rows.map(i => ({
    value: i.id,
    label_ru: i.name_ru,
    label_en: i.name_en
  })));
});

router.post('', auth, validation({
  body: {
    name_en: Joi.string().required(),
    name_ru: Joi.string().required(),
    user: Joi.number().required(),
    team: Joi.number().required(),
    price: Joi.number().required(),
    user_count: Joi.number().required(),
    pro: Joi.number().required(),
    single: Joi.array().items(Joi.number().positive()).required(),
    active: Joi.array().items(Joi.number().positive()).required(),
    levels: Joi.array().items(Joi.number().positive()).required()
  }
}), async (req, res) => {
  const referral = await models.Referral.create(filterValues(req.body));

  await referralService.updateUsers();

  res.status(201).json({ id: referral.id });
});

router.post('/:id', auth, validation({
  params: {
    id: Joi.number().required()
  },
  body: {
    name_en: Joi.string().allow('', null),
    name_ru: Joi.string().allow('', null),
    user: Joi.number().allow('', null),
    team: Joi.number().allow('', null),
    price: Joi.number().allow('', null),
    user_count: Joi.number().allow('', null),
    single: Joi.array().items(Joi.number().positive()).allow('', null, []),
    active: Joi.array().items(Joi.number().positive()).allow('', null, []),
    levels: Joi.array().items(Joi.number().positive()).allow('', null, [])
  }
}), async (req, res) => {
  const referral = await models.Referral.findById(req.params.id);
  if (null === referral) {
    return res.boom.notFound();
  }

  await referral.update(filterValues(req.body));

  if (req.body.user || req.body.team || req.body.user_count) {
    await referralService.updateUsers();
  }

  res.status(204).end();
});

router.get('/:id', validation({
  params: {
    id: Joi.number().positive().required()
  }
}), async (req, res) => {
  const referralLevel = await models.Referral.findById(req.params.id);
  if (null === referralLevel) {
    return res.boom.notFound();
  }

  res.json(referralLevel);
});

/**
 * Вывод пользователей по уровню реферальной системы
 *
 * @param req
 * @param res
 */
router.get('/tree/all', async (req, res) => {
  const rows = await models.User.findAll({
    order: [
      ['parents', 'ASC']
    ],
    include: [
      { model: models.Referral, as: 'referral_level' },
      { model: models.Referral, as: 'minimal_referral_level' }
    ],
    attributes: jwt.getPublicAttributes()
  });

  const users = rows.map(row => row.toJSON());
  const tree = await referral.buildTree(users, 'referral_id', async item => ({
    ...item,
    income: {
      btc: await models.Transaction.sum('amount', {
        where: {
          user_id: item.id,
          status: consts.TRANSACTION_ONE_TIME_BONUS
        }
      }) || 0,
      usd: await models.Transaction.sum('usd', {
        where: {
          user_id: item.id,
          status: consts.TRANSACTION_ONE_TIME_BONUS
        }
      }) || 0
    }
  }));

  res.json(tree);
});

/**
 * Вывод пользователей по уровню реферальной системы
 *
 * @param req
 * @param res
 */
router.get('/tree/:id', auth, validation({
  params: {
    id: Joi.number().integer().positive().required()
  }
}), async (req, res) => {
  const user = await models.User.findById(req.params.id);
  if (null === user) {
    return res.boom.notFound();
  }

  const rows = await models.User.findAll({
    where: {
      parents: {
        [Op.contains]: [user.id]
      }
    },
    include: [
      { model: models.Referral, as: 'referral_level' },
      { model: models.Referral, as: 'minimal_referral_level' }
    ],
    order: [
      ['parents', 'ASC']
    ],
    attributes: jwt.getPublicAttributes()
  });

  const users = rows.map(row => {
    const item = row.toJSON();
    delete item.password;
    delete item.refresh_token;
    return item;
  });
  const isRoot = node => node.referral_id === user.id;
  const tree = await referral.buildTree([user, ...users], 'referral_id', async item => ({
    ...item,
    income: {
      btc: await models.Transaction.sum('amount', {
        where: {
          user_id: item.id,
          status: consts.TRANSACTION_ONE_TIME_BONUS
        }
      }) || 0,
      usd: await models.Transaction.sum('usd', {
        where: {
          user_id: item.id,
          status: consts.TRANSACTION_ONE_TIME_BONUS
        }
      }) || 0
    }
  }), isRoot);

  res.json(tree);
});

router.delete('/:id', auth, validation({
  params: {
    id: Joi.number().required()
  }
}), async (req, res) => {
  const referral = await models.Referral.findById(req.params.id);
  if (null === referral) {
    return res.boom.notFound();
  }

  await referral.destroy();

  await referralService.updateUsers();

  res.status(204).end();
});

module.exports = router;
