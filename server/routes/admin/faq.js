const router = require('express').Router();
const Joi = require('joi');
const models = require('../../models');
const validation = require('../../middleware/validation');
const filterValues = require('../../util/filterValues');
const sequelizeUtils = require('../../util/sequelizeUtils');

router.get('', validation({
  query: {
    order: Joi.string().allow('', null),
    page_size: Joi.number().integer().min(1).max(300).default(100),
    page: Joi.number().integer().min(1).default(1)
  }
}), async (req, res) => {
  const result = await models.Faq.findAndCountAll({
    order: sequelizeUtils.getOrderBy(models.Faq, req.query.order, [
      ['id', 'DESC']
    ])
  });

  res.json({
    meta: {
      page: req.query.page,
      page_size: req.query.page_size,
      page_count: Math.ceil(result.count / req.query.page_size),
      total: result.count
    },
    objects: result.rows
  });
});

router.get('/:id', validation({
  params: {
    id: Joi.number().positive().required()
  }
}), async (req, res) => {
  const faq = await models.Faq.findById(req.params.id);
  if (null === faq) {
    return res.boom.notFound();
  }

  res.json(faq);
});

router.post('/:id', validation({
  params: {
    id: Joi.number().positive().required()
  },
  body: {
    question_en: Joi.string().allow('', null),
    question_ru: Joi.string().allow('', null),
    answer_ru: Joi.string().allow('', null),
    answer_en: Joi.string().allow('', null),
    position: Joi.number().allow('', null),
  }
}), async (req, res) => {
  const faq = await models.Faq.findById(req.params.id);
  if (null === faq) {
    return res.boom.notFound();
  }
  await faq.update(filterValues(req.body));

  res.status(204).end();
});

router.post('', validation({
  body: {
    question_en: Joi.string().required(),
    question_ru: Joi.string().required(),
    answer_ru: Joi.string().required(),
    answer_en: Joi.string().required(),
    position: Joi.number().required(),
  }
}), async (req, res) => {
  const faq = await models.Faq.create(req.body);
  res.status(201).json({ id: faq.id });
});

router.delete('/:id', validation({
  params: {
    id: Joi.number().positive().required()
  }
}), async (req, res) => {
  const faq = await models.Faq.findById(req.params.id);
  if (null === faq) {
    return res.boom.notFound();
  }

  await faq.destroy();

  res.status(204).end();
});

module.exports = router;
