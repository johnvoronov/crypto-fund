const router = require('express').Router();
const models = require('../../models');
const jwt = require('../../util/jwt');
const Joi = require('joi');
const consts = require('../../consts');
const validation = require('../../middleware/validation');
const sequelizeUtils = require('../../util/sequelizeUtils');
const transactionService = require('../../service/transaction');
const { Op } = require('sequelize');

const include = [
  {
    model: models.Strategy,
    as: 'strategy',
    include: [
      {
        model: models.Currency,
        as: 'convert'
      }
    ]
  },
  {
    model: models.Payment,
    as: 'payment',
    include: [
      {
        model: models.Currency,
        as: 'currency'
      },
      {
        model: models.Strategy,
        as: 'strategy'
      }
    ]
  },
  {
    model: models.User,
    as: 'user',
    attributes: jwt.getPublicAttributes()
  }
];

router.get('/list', validation({
  query: {
    q: Joi.string().allow('', null),
    is_freeze: Joi.boolean().default(null),
    is_income: Joi.boolean().default(null),
    is_deposit: Joi.boolean().default(null),
    user_id: Joi.number().default(null),
    strategy_id: Joi.number().default(null),
    order: Joi.string().allow('', null),
    page_size: Joi.number().integer().min(1).max(300).default(100),
    page: Joi.number().integer().min(1).default(1)
  }
}), async (req, res) => {
  let where = {};

  if (req.query.q) {
    where = {
      // amount: { [Op.like]: `%${String(req.query.q)}` }
    };
  }

  if (req.query.is_freeze) {
    where = { ...where, is_freeze: req.query.is_freeze };
  }

  if (req.query.is_income) {
    where = { ...where, is_income: req.query.is_income };
  }

  if (req.query.is_deposit) {
    where = { ...where, is_deposit: req.query.is_deposit };
  }

  if (req.query.strategy_id) {
    where = { ...where, strategy_id: req.query.strategy_id };
  }

  if (req.query.user_id) {
    where = { ...where, user_id: req.query.user_id };
  }

  const result = await models.Transaction.findAndCountAll({
    where,
    include: [
      {
        model: models.Strategy,
        as: 'strategy',
        include: [
          {
            model: models.Currency,
            as: 'convert'
          },
          {
            model: models.Currency,
            as: 'currency'
          }
        ]
      },
      {
        model: models.Payment,
        as: 'payment',
        ...(req.query.currency_id
          ? {
            where: { currency_id: req.query.currency_id }
          }
          : {}),
        include: [
          {
            model: models.Currency,
            as: 'currency'
          },
          {
            model: models.Strategy,
            as: 'strategy'
          }
        ]
      },
      {
        model: models.User,
        as: 'user',
        attributes: jwt.getPublicAttributes()
      }
    ],
    limit: req.query.page_size,
    offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
    order: sequelizeUtils.getOrderBy(models.Transaction, req.query.order, [['id', 'DESC']])
  });

  res.json({
    meta: {
      page: req.query.page,
      page_size: req.query.page_size,
      page_count: Math.ceil(result.count / req.query.page_size),
      total: result.count
    },
    objects: result.rows
  });
});

router.post('/activate/:id', validation({
  params: { id: Joi.number().integer().positive().required() }
}), async (req, res) => {
  const tx = await models.Transaction.findById(req.params.id);
  await transactionService.createDeposit(tx);
  res.status(204).end();
});

router.get('/pending', validation({
  query: {
    user_id: Joi.number().default(null),
    strategy_id: Joi.number().default(null),
    order: Joi.string().allow('', null),
    page_size: Joi.number().integer().min(1).max(300).default(100),
    page: Joi.number().integer().min(1).default(1)
  }
}), async (req, res) => {
  let where = {
    account_type: consts.ACCOUNT_TYPE_STRATEGY,
    is_freeze: true
  };

  if (req.query.strategy_id) {
    where = { ...where, strategy_id: req.query.strategy_id };
  }

  if (req.query.user_id) {
    where = { ...where, user_id: req.query.user_id };
  }

  const result = await models.Transaction.findAndCountAll({
    where,
    include: [
      {
        model: models.Strategy,
        as: 'strategy',
        include: [
          {
            model: models.Currency,
            as: 'convert'
          },
          {
            model: models.Currency,
            as: 'currency'
          }
        ]
      },
      {
        model: models.Payment,
        as: 'payment',
        include: [
          {
            model: models.Currency,
            as: 'currency'
          },
          {
            model: models.Strategy,
            as: 'strategy'
          }
        ]
      },
      {
        model: models.User,
        as: 'user',
        attributes: jwt.getPublicAttributes()
      }
    ],
    limit: req.query.page_size,
    offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
    order: sequelizeUtils.getOrderBy(models.Transaction, req.query.order, [['id', 'DESC']])
  });

  res.json({
    meta: {
      page: req.query.page,
      page_size: req.query.page_size,
      page_count: Math.ceil(result.count / req.query.page_size),
      total: result.count
    },
    objects: result.rows
  });
});

router.get('/:id', validation({
  params: {
    id: Joi.number().required()
  }
}), async (req, res) => {
  const transaction = await models.Transaction.findById(req.params.id, {
    include
  });

  if (!transaction) {
    return res.boom.notFound();
  }

  res.json(transaction);
});

router.get('/accept/:id', validation({
  params: {
    id: Joi.number().required()
  }
}), async (req, res) => {
  const transaction = await models.Transaction.findOne({
    where: {
      id: req.params.id,
      status: consts.TRANSACTION_FREEZE
    }
  });

  if (!transaction) {
    return res.boom.notFound();
  }

  await transactionService.accept(transaction);

  res.status(204).end();
});

module.exports = router;
