const router = require('express').Router();
const Joi = require('joi');
const models = require('../../models');
const validation = require('../../middleware/validation');
const filterValues = require('../../util/filterValues');
const sequelizeUtils = require('../../util/sequelizeUtils');

router.get('/options', async (req, res) => {
  const rows = await models.Strategy.findAll({
    order: [
      ['name', 'ASC']
    ]
  });

  res.json(rows);
});

router.get('', validation({
  query: {
    page_size: Joi.number().integer().min(1).max(300).default(100),
    page: Joi.number().integer().min(1).default(1)
  }
}), async (req, res) => {
  const result = await models.Strategy.findAndCountAll({
    include: [
      { model: models.Currency, as: 'currency' },
      { model: models.Currency, as: 'convert' }
    ],
    limit: req.query.page_size,
    offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
    order: sequelizeUtils.getOrderBy(models.Strategy, req.query.order, [
      ['id', 'DESC']
    ])
  });

  res.json({
    meta: {
      page: req.query.page,
      page_size: req.query.page_size,
      page_count: Math.ceil(result.count / req.query.page_size),
      total: result.count
    },
    objects: result.rows
  });
});

router.get('/:id', validation({
  params: {
    id: Joi.number().positive().required()
  }
}), async (req, res) => {
  const strategy = await models.Strategy.findById(req.params.id);

  if (null === strategy) {
    return res.boom.notFound();
  }

  res.json(strategy);
});

router.post('/:id', validation({
  params: {
    id: Joi.number().positive().required()
  },
  body: {
    name: Joi.string().required(),
    description: Joi.string().required(),
    description_short: Joi.string().required(),
    limit: Joi.array().items(Joi.object().keys({
      limit: Joi.number().positive().min(0).required(),
      percent: Joi.number().positive().min(0).max(100).required()
    })),
    items: Joi.array().items(Joi.object().keys({
      label: Joi.string().required(),
      description: Joi.string().required()
    })),
    caption: Joi.string(),
    features: Joi.array().items(Joi.string().required()),
    coinpayments: Joi.object().keys({
      merchantId: Joi.string().required(),
      publicKey: Joi.string().required(),
      secretKey: Joi.string().required(),
      ipnSecret: Joi.string().required()
    })
  }
}), async (req, res) => {
  const strategy = await models.Strategy.findById(req.params.id);

  if (null === strategy) {
    return res.boom.notFound();
  }

  await strategy.update(filterValues(req.body));

  res.status(204).end();
});

module.exports = router;
