const express = require('express');
const router = express.Router();
const checkpoint = require('../../service/checkpoint');
const balanceService = require('../../service/balance');
const models = require('../../models');
const validation = require('../../middleware/validation');
const Joi = require('joi');

router.get('', validation({
  query: {
    strategy_id: Joi.number().integer().allow('', null),
    order: Joi.string().allow('', null),
    page_size: Joi.number().integer().min(1).max(300).default(100),
    page: Joi.number().integer().min(1).default(1)
  }
}), async (req, res) => {
  let where = {};

  if (req.query.strategy_id) {
    where = { ...where, strategy_id: req.query.strategy_id };
  }

  const result = await models.Checkpoint.findAndCountAll({
    where,
    include: [
      { model: models.Strategy, as: 'strategy' }
    ],
    limit: req.query.page_size,
    offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
    order: [
      ['is_done', 'ASC'],
      ['end_at', 'DESC']
    ]
  });

  res.json({
    meta: {
      page: req.query.page,
      page_size: req.query.page_size,
      page_count: Math.ceil(result.count / req.query.page_size),
      total: result.count
    },
    objects: result.rows
  });
});

router.post('/confirm/:id', validation({
  params: {
    id: Joi.number().integer().required()
  }
}), async (req, res) => {
  await checkpoint.completeIteration(req.params.id);
  res.status(204).end();
});

router.get('/:id', validation({
  params: {
    id: Joi.number().positive().required()
  }
}), async (req, res) => {
  const checkpoint = await models.Checkpoint.findById(req.params.id);
  if (null === checkpoint) {
    return res.boom.notFound();
  }

  res.json(checkpoint)
});

router.post('', validation({
  body: {
    end_at: Joi.date().iso().required(),
    strategy_id: Joi.number().positive().required(),
    percent: Joi.number().required()
  }
}), async (req, res) => {
  const {
    strategy_id,
    percent,
    end_at
  } = req.body;

  const balance = await balanceService.getActiveDepositBalance({ strategy_id });
  if (balance <= 0) {
    return res.boom.badRequest(null, {
      errors: {
        percent: [
          req.t("controller.withdraw.withdraw.insufficient_funds")
        ]
      }
    });
  }

  await checkpoint.checkpoint(strategy_id, percent, end_at);

  res.status(204).end();
});

module.exports = router;
