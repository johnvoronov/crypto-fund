const router = require('express').Router();
const models = require('../../models');
const Joi = require('joi');
const validation = require('../../middleware/validation');
const sequelizeUtils = require('../../util/sequelizeUtils');
const consts = require('../../consts');
const { Op } = require('sequelize');

const fluidBoolean = Joi.boolean() // Жидкий бульон
  .truthy('yes')
  .truthy('y')
  .truthy('1')
  .truthy(1)
  .falsy('no')
  .falsy('n')
  .falsy('0')
  .falsy(0)
  .insensitive(true);

router.get('/list', validation({
  query: {
    q: Joi.string().allow('', null),
    is_active: Joi.boolean().allow('', null),
    is_admin: Joi.boolean().allow('', null),
    minimal_referral_level_id: Joi.number().allow('', null),
    referral_level_id: Joi.number().allow('', null),
    order: Joi.string().allow('', null),
    page_size: Joi.number().integer().min(1).max(300).default(100),
    page: Joi.number().integer().min(1).default(1)
  }
}), async (req, res) => {
  let where = {
    is_superuser: false
  };
  if (req.query.q) {
    where = {
      [Op.or]: [
        {
          email: { [Op.like]: `%${req.query.q}%` }
        },
        {
          login: { [Op.like]: `%${req.query.q}%` }
        }
      ]
    };
  }

  if (req.query.is_active) {
    where = { ...where, is_active: req.query.is_active };
  }

  if (req.query.is_admin) {
    where = { ...where, is_admin: req.query.is_admin };
  }

  if (req.query.minimal_referral_level_id) {
    where = { ...where, minimal_referral_level_id: req.query.minimal_referral_level_id };
  }

  if (req.query.referral_level_id) {
    where = { ...where, referral_level_id: req.query.referral_level_id };
  }

  const result = await models.User.findAndCountAll({
    where,
    limit: req.query.page_size,
    offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
    order: sequelizeUtils.getOrderBy(models.User, req.query.order, [
      ['id', 'DESC']
    ]),
    include: [
      { model: models.User, as: 'owner' },
      { model: models.Referral, as: 'referral_level' },
      { model: models.Referral, as: 'minimal_referral_level' },
      { model: models.Referral, as: 'buy_referral_level' },
    ]
  });

  res.json({
    meta: {
      page: req.query.page,
      page_size: req.query.page_size,
      page_count: Math.ceil(result.count / req.query.page_size),
      total: result.count
    },
    objects: result.rows
  });
});

router.get('/options', async (req, res) => {
  const users = await models.User.findAll({
    where: { is_superuser: false },
    attributes: ['id', 'email']
  });

  res.json(users.map(u => ({
    value: u.id,
    label: u.email
  })));
});

router.get('/autocomplete', validation({
  query: {
    q: Joi.string().required()
  }
}), async (req, res) => {
  const users = await models.User.findAll({
    where: {
      is_superuser: false,
      [Op.or]: [
        {
          email: {
            [Op.like]: `${req.query.q}%`
          }
        },
        {
          login: {
            [Op.like]: `${req.query.q}%`
          }
        }
      ]
    },
    attributes: ['id', 'email']
  });

  res.json(users.map(u => ({
    value: u.id,
    label: u.email
  })));
});

router.get('/:id', validation({
  params: {
    id: Joi.number().required()
  }
}), async (req, res) => {
  const user = await models.User.findById(req.params.id, {
    include: [
      { model: models.User, as: 'owner' },
      { model: models.Referral, as: 'referral_level' },
      { model: models.Referral, as: 'minimal_referral_level' },
      { model: models.Referral, as: 'buy_referral_level' }
    ]
  });

  if (!user) {
    return res.boom.notFound();
  }

  const publicUser = user.toJSON();
  delete publicUser.password;
  delete publicUser.refresh_token;

  const payments = await models.Transaction.sum('amount', {
    where: {
      user_id: user.id,
      status: consts.TRANSACTION_PAYMENT
    }
  }) || 0;
  const fundValue = await models.Transaction.sum('amount', {
    where: {
      user_id: user.id,
      status: consts.TRANSACTION_FUND_INCOME
    }
  }) || 0;
  const userValue = await models.Transaction.sum('amount', {
    where: {
      user_id: user.id,
      status: consts.TRANSACTION_INCOME
    }
  }) || 0;
  const stats = {
    fund: {
      value: fundValue,
      percent: fundValue > 0 ? payments / fundValue : 0
    },
    user: {
      value: userValue,
      percent: userValue > 0 ? payments / userValue : 0
    }
  };

  res.json({
    ...publicUser,
    stats
  });
});

router.post('/admin/:id', validation({
  params: {
    id: Joi.number().required()
  },
  body: {
    is_admin: fluidBoolean.required()
  }
}), async (req, res) => {
  const user = await models.User.findById(req.params.id);

  if (!user) {
    return res.boom.notFound();
  }

  await user.update({
    is_admin: req.body.is_admin
  });

  res.status(204).end();
});

router.get('/referral/options', validation({
  query: {
    user_id: Joi.number().integer().positive().required()
  }
}), async (req, res) => {
  const referrals = await models.User.findAll({
    where: {
      parents: {
        [Op.contains]: [req.query.user_id]
      }
    },
    attributes: ['id', 'email']
  });
  let where = {
    id: {
      [Op.not]: req.query.user_id
    }
  };
  if (referrals.length > 0) {
    where = {
      id: {
        [Op.notIn]: [
          ...referrals.map(ref => ref.id),
          req.query.user_id
        ]
      }
    };
  }

  const users = await models.User.findAll({ where });

  res.json(users.map(u => ({
    value: u.id,
    label: u.email
  })));
});

/*
TODO https://blockchaincorp.atlassian.net/browse/CL-259
router.post('/referral/add', validation({
  body: {
    user_id: Joi.number().integer().positive().required(),
    children_id: Joi.number().integer().positive().required()
  }
}), async (req, res) => {
  const parentUser = await models.User.findById(req.body.user_id);
  if (!parentUser) {
    return res.boom.notFound();
  }

  const childrUser = await models.User.findById(req.body.children_id);
  if (!childrUser) {
    return res.boom.notFound();
  }
  await referralService.moveToNewParent(parentUser, childrUser);

  res.status(204).end();
});
*/

module.exports = router;
