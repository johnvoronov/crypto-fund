const express = require('express');
const router = express.Router();
const models = require('../../models');
const validation = require('../../middleware/validation');
const sequelizeUtils = require('../../util/sequelizeUtils');
const referralService = require('../../service/referral');
const transactionService = require('../../service/transaction');
const filterValues = require('../../util/filterValues');
const Joi = require('joi');

router.get('', validation({
  query: {
    order: Joi.string().allow('', null),
    page_size: Joi.number().integer().min(1).max(300).default(100),
    page: Joi.number().integer().min(1).default(1)
  }
}), async (req, res) => {
  const result = await models.Franchise.findAndCountAll({
    limit: req.query.page_size,
    offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
    order: sequelizeUtils.getOrderBy(models.Franchise, req.query.order, [
      ['id', 'DESC']
    ]),
    include: [
      { model: models.User, as: 'user' },
      { model: models.Currency, as: 'currency' }
    ]
  });

  res.json({
    meta: {
      page: req.query.page,
      page_size: req.query.page_size,
      page_count: Math.ceil(result.count / req.query.page_size),
      total: result.count
    },
    objects: result.rows
  });
});

router.get('/:id', validation({
  params: {
    id: Joi.number().positive().required()
  }
}), async (req, res) => {
  const franchise = await models.Franchise.findById(req.params.id, {
    include: [
      { model: models.User, as: 'user' },
      { model: models.Currency, as: 'currency' }
    ]
  });
  if (null === franchise) {
    return res.boom.notFound();
  }

  res.json(franchise);
});

router.post('/price/:id', validation({
  params: { id: Joi.number().positive().required() },
  body: { price: Joi.number().required() }
}), async (req, res) => {
  const franchise = await models.Franchise.findById(req.params.id);
  if (null === franchise) {
    return res.boom.notFound();
  }

  await franchise.update(filterValues(req.body));

  res.status(204).end();
});

router.post('/activate/:id', validation({
  params: {
    id: Joi.number().positive().required()
  }
}), async (req, res) => {
  const franchise = await models.Franchise.findById(req.params.id);
  if (null === franchise) {
    return res.boom.notFound();
  }

  if (franchise.is_active) {
    return res.status(204).end();
  }

  if (!franchise.price || !franchise.currency_id) {
    return res.boom.badRequest(null, {
      errors: {
        price: [req.t('price_not_set')]
      }
    });
  }

  await franchise.update({
    is_active: true,
    is_done: true
  });
  await transactionService.createPro(franchise);
  res.status(204).end();
});

module.exports = router;
