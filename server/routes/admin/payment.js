const router = require('express').Router();
const models = require('../../models');
const Joi = require('joi');
const validation = require('../../middleware/validation');
const mail = require('../../service/mail');
const paymentService = require('../../service/payment');
const transactionService = require('../../service/transaction');
const sequelizeUtils = require('../../util/sequelizeUtils');
const jwt = require('../../util/jwt');
const Op = models.sequelize.Op;
const referralService = require('../../service/referral');

const include = [
  {
    model: models.Currency,
    as: 'currency'
  },
  {
    model: models.Strategy,
    as: 'strategy'
  },
  {
    model: models.Wallet,
    as: 'wallet'
  },
  {
    model: models.User,
    as: 'user',
    attributes: jwt.getPublicAttributes()
  }
];

router.get('/list', validation({
  query: {
    q: Joi.string().allow('', null),
    user_id: Joi.number().default(null),
    order: Joi.string().allow('', null),
    page_size: Joi.number().integer().min(1).max(300).default(100),
    page: Joi.number().integer().min(1).default(1)
  }
}), async (req, res) => {
  let where = {};
  if (req.query.q) {
    where = {
      [Op.or]: [
        {
          amount: { [Op.like]: `%${req.query.q}%` }
        },
        {
          tokens_amount: { [Op.like]: `%${req.query.q}%` }
        }
      ]
    };
  }

  if (req.query.strategy_id) {
    where = { ...where, strategy_id: req.query.strategy_id };
  }

  if (req.query.user_id) {
    where = { ...where, user_id: req.query.user_id };
  }

  const result = await models.Payment.findAndCountAll({
    where,
    include,
    limit: req.query.page_size,
    offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
    order: sequelizeUtils.getOrderBy(models.Payment, req.query.order, [
      ['id', 'DESC']
    ])
  });

  res.json({
    meta: {
      page: req.query.page,
      page_size: req.query.page_size,
      page_count: Math.ceil(result.count / req.query.page_size),
      total: result.count
    },
    objects: result.rows
  });
});

router.post('/create', validation({
  body: {
    currency_id: Joi.number().integer().required(),
    user_id: Joi.number().integer().required(),
    strategy_id: Joi.number().integer().required(),
    amount: Joi.number().required()
  }
}), async (req, res) => {
  const {
    currency_id,
    user_id,
    strategy_id,
    amount
  } = req.body;

  // Находим пользователя
  const user = await models.User.findById(user_id);
  if (null === user) {
    return res.boom.badRequest(null, {
      errors: {
        user_id: [req.t('user_not_found')]
      }
    });
  }

  // Находим валюту
  const currency = await models.Currency.findById(currency_id);
  if (null === currency) {
    return res.boom.badRequest(null, {
      errors: {
        currency_id: [req.t('currency_not_found')]
      }
    });
  }

  // Находим валюту
  const strategy = await models.Strategy.findById(strategy_id);
  if (null === strategy) {
    return res.boom.badRequest(null, {
      errors: {
        strategy_id: [req.t('strategy_not_found')]
      }
    });
  }

  const payment = await paymentService.createPayment(user, strategy, currency, amount, 'admin payment');
  const tx = await transactionService.createPayment(payment);
  await transactionService.createDeposit(tx);

  await referralService.updateReferralLevel(user);

  mail.send(user.email, 'payment_success', {
    payment,
    user,
    transaction: tx
  });

  res.status(201).json({
    id: payment.id
  });
});

router.get('/:id', validation({
  params: {
    id: Joi.number().required()
  }
}), async (req, res) => {
  const payment = await models.Payment.findById(req.params.id, {
    include
  });

  if (!payment) {
    return res.boom.notFound();
  }

  res.json(payment);
});

module.exports = router;
