const settings = require('../../util/settings');
const Joi = require('joi');
const router = require('express').Router();
const validate = require('../../middleware/validation');
const auth = require('../../middleware/auth');
const guard = require('express-jwt-permissions')();

const companySettings = {
  // Название компании
  company_name: Joi.string().default(null).allow([null, '']),
  // Вычет комиссии coinpayments
  deduct_coinpayments_tax: Joi.boolean().default(null).allow([null, '']),
  // Текст в настройках
  settings_info_ru: Joi.string().default(null).allow([null, '']),
  settings_info_en: Joi.string().default(null).allow([null, '']),
  // Правила сервиса
  terms_and_conditions_ru: Joi.string().default(null).allow([null, '']),
  terms_and_conditions_en: Joi.string().default(null).allow([null, '']),
  // CL-130
  partnership_description_ru: Joi.string().default(null).allow([null, '']),
  partnership_description_en: Joi.string().default(null).allow([null, '']),
  // Регистрация только по приглашению
  user_registration_invite_only: Joi.number().integer().default(0).allow([null, '']),
  // Обязательный сбор телефона с пользователей и подтверждение по смс
  phone_required: Joi.number().integer().default(0).allow([null, ''])
};

const linkSettings = {
  // Social media links
  bitcoinstalk_link: Joi.string().default(null).allow([null, '']),
  facebook_link: Joi.string().default(null).allow([null, '']),
  twitter_link: Joi.string().default(null).allow([null, '']),
  youtube_link: Joi.string().default(null).allow([null, '']),
  telegram_link: Joi.string().default(null).allow([null, ''])
};

const coinpaymentsSettings = {
  // Coinpayments
  coinpayments_public_key: Joi.string().default(null).allow([null, '']),
  coinpayments_secret_key: Joi.string().default(null).allow([null, '']),
  coinpayments_ipn_secret: Joi.string().default(null).allow([null, '']),
  coinpayments_merchant_id: Joi.string().default(null).allow([null, ''])
};

const googleSettings = {
  // Google Analytics
  google_analytics_id: Joi.string().default(null).allow([null, '']),
  // Google Tag Manager
  google_tag_manager_id: Joi.string().default(null).allow([null, ''])
};

const smtpSettings = {
  // SMTP settings
  smtp_from: Joi.string().email().default(null).allow([null, '']),
  smtp_host: Joi.string().default(null).allow([null, '']),
  smtp_port: Joi.number().integer().default(null).allow([null, '']),
  smtp_username: Joi.string().default(null).allow([null, '']),
  smtp_password: Joi.string().default(null).allow([null, ''])
};

const saveSettings = async (req, res) => {
  for (let path in req.body) {
    await settings.setValue(path, req.body[path]);
  }

  res.status(204).end();
};

router.post('', auth, guard.check(['admin']), validate({
  body: {
    ...companySettings,
    ...coinpaymentsSettings,
    ...googleSettings,
    ...smtpSettings,
    ...linkSettings
  }
}, { allowUnknown: false }), saveSettings);

router.post(
  '/smtp',
  auth, guard.check(['admin']),
  validate({ body: smtpSettings }),
  saveSettings);

router.post(
  '/company',
  auth, guard.check(['admin']),
  validate({
    body: companySettings
  }, { allowUnknown: false }),
  saveSettings);

router.post(
  '/google',
  auth, guard.check(['admin']),
  validate({
    body: googleSettings
  }, { allowUnknown: false }),
  saveSettings);

router.post(
  '/coinpayments',
  auth, guard.check(['admin']),
  validate({
    body: coinpaymentsSettings
  }, { allowUnknown: false }),
  saveSettings);

router.post(
  '/link',
  auth, guard.check(['admin']),
  validate({
    body: linkSettings
  }, { allowUnknown: false }),
  saveSettings);

router.get('', async (req, res) => {
  res.json(await settings.fetch(false))
});

module.exports = router;
