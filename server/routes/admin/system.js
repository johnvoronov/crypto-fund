const fs = require('fs');
const router = require('express').Router();
const auth = require('../../middleware/auth');
const backup = require('../../service/backup');
const guard = require('express-jwt-permissions')();

router.post('/backup', auth, guard.check(['admin']), async (req, res) => {
  const file = await backup.create();
  res.download(file, 'dump.sql.gz', err => {
    if (err) {
      console.log(err);
    }
    fs.unlinkSync(file);
  });
});

module.exports = router;
