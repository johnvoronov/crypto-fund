const router = require('express').Router();
const Joi = require('joi');
const models = require('../../models');
const validation = require('../../middleware/validation');
const sequelizeUtils = require('../../util/sequelizeUtils');

router.get('', validation({
  query: {
    order: Joi.string().allow('', null),
    page_size: Joi.number().integer().min(1).max(300).default(100),
    page: Joi.number().integer().min(1).default(1)
  }
}), async (req, res) => {
  const result = await models.Question.findAndCountAll({
    include: [
      { model: models.User, as: 'user' }
    ],
    limit: req.query.page_size,
    offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
    order: sequelizeUtils.getOrderBy(models.Question, req.query.order, [
      ['id', 'DESC']
    ])
  });

  res.json({
    meta: {
      page: req.query.page,
      page_size: req.query.page_size,
      page_count: Math.ceil(result.count / req.query.page_size),
      total: result.count
    },
    objects: result.rows
  });
});

router.get('/:id', validation({
  params: {
    id: Joi.number().positive().required()
  }
}), async (req, res) => {
  const question = await models.Question.findById(req.params.id);
  if (null === question) {
    return res.boom.notFound();
  }

  res.json(question);
});

module.exports = router;
