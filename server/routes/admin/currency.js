const router = require('express').Router();
const models = require('../../models');
const Joi = require('joi');
const currencyUtil = require('../../util/currency');
const sequelizeUtils = require('../../util/sequelizeUtils');
const validation = require('../../middleware/validation');
const Op = models.sequelize.Op;

router.get('/list', validation({
  query: {
    user_id: Joi.number().default(null),
    strategy_id: Joi.number().default(null),
    page_size: Joi.number().integer().min(1).max(300).default(100),
    page: Joi.number().integer().min(1).default(1)
  }
}), async (req, res) => {
  let where = {};
  if (req.query.q) {
    where = {
      [Op.or]: [
        {
          code: { [Op.like]: `%${req.query.q}%` }
        },
        {
          name: { [Op.like]: `%${req.query.q}%` }
        }
      ]
    };
  }

  const result = await models.Currency.findAndCountAll({
    where,
    limit: req.query.page_size,
    offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
    order: sequelizeUtils.getOrderBy(models.Currency, req.query.order, [
      ['id', 'DESC']
    ])
  });

  res.json({
    meta: {
      page: req.query.page,
      page_size: req.query.page_size,
      page_count: Math.ceil(result.count / req.query.page_size),
      total: result.count
    },
    objects: result.rows
  });
});

router.get('/options', async (req, res) => {
  const rows = await models.Currency.findAll({
    order: [
      ['name', 'ASC']
    ]
  });

  res.json(rows);
});

router.post('/exchange', async (req, res) => {
  try {
    await currencyUtil.update();
    res.status(204).end();
  } catch (e) {
    console.log(e);
  }
});

module.exports = router;
