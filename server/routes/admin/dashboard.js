const router = require('express').Router();
const chart = require('../../service/chart');
const consts = require('../../consts');
const models = require('../../models');

router.get('', async (req, res) => {
  res.json({
    strategies: await chart.getStrategiesPieChartData(),
    investments: {
      user_income: await models.Transaction.sum('amount', {
        where: {
          status: consts.TRANSACTION_INCOME
        }
      }) || 0,
      fund_income: await models.Transaction.sum('amount', {
        where: {
          status: consts.TRANSACTION_FUND_INCOME
        }
      }) || 0,
      withdraw: await models.Transaction.sum('amount', {
        where: {
          status: consts.TRANSACTION_WITHDRAW
        }
      }) || 0
    },
    users: await chart.users('month'),
    fund: {
      history: {
        fund: await chart.getChartDataByStatus(consts.TRANSACTION_FUND_INCOME),
        user: await chart.getChartDataByStatus(consts.TRANSACTION_INCOME),
        withdraw: await chart.getChartDataByStatus(consts.TRANSACTION_WITHDRAW),
      },
      checkpoint: await chart.checkpoint('month')
    }
  });
});

module.exports = router;
