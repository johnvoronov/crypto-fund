const router = require('express').Router();
const Joi = require('joi');
const path = require('path');
const backup = require('../../service/backup');
const validation = require('../../middleware/validation');

router.get('', async (req, res) => {
  res.json(await backup.getList());
});

router.post('/create', async (req, res) => {
  const filePath = await backup.create();
  return res.download(filePath, path.basename(filePath));
});

router.get('/download', validation({
  query: {
    name: Joi.string().required()
  }
}), async (req, res) => {
  const filePath = backup.getFile(req.query.name);

  if (!filePath) {
    return res.boom.notFound(null, {
      errors: {
        name: [
          req.t('backup.file_not_found')
        ]
      }
    });
  }

  return res.download(filePath, path.basename(filePath));
});

module.exports = router;
