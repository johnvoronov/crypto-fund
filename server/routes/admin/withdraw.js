const express = require('express');
const router = express.Router();
const { Op } = require('sequelize');
const mail = require('../../service/mail');
const auth = require('../../middleware/auth');
const models = require('../../models');
const validation = require('../../middleware/validation');
const sequelizeUtils = require('../../util/sequelizeUtils');
const Joi = require('joi');
const consts = require('../../consts');

router.get('', auth, validation({
  query: {
    type: Joi.number().integer().default(0).allow(consts.WITHDRAW_STRATEGY, consts.WITHDRAW_PARTNERSHIP, '', null)
  }
}), async (req, res) => {
  let where = {};
  let strategyWhere = {};

  const type = parseInt(req.query.type);
  switch (type) {
    case consts.WITHDRAW_PARTNERSHIP:
      where = { ...where, type: consts.WITHDRAW_PARTNERSHIP };
      break;

    case consts.WITHDRAW_STRATEGY:
      where = { ...where, type: consts.WITHDRAW_STRATEGY };
      break;

    default:
      break;
  }

  if (req.query.user_id) {
    where = { ...where, user_id: req.query.user_id };
  }

  if (req.query.strategy_id) {
    strategyWhere = { strategy_id: req.query.strategy_id };
  }

  const result = await models.Withdraw.findAndCountAll({
    where,
    include: [
      {
        model: models.User,
        as: 'user'
      },
      {
        model: models.Transaction,
        as: 'transaction',
        where: strategyWhere,
        include: [
          {
            model: models.Strategy,
            as: 'strategy',
            include: [
              {
                model: models.Currency,
                as: 'convert'
              }
            ]
          }
        ]
      }
    ],
    limit: req.query.page_size,
    offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
    order: sequelizeUtils.getOrderBy(models.Transaction, req.query.order, [
      ['id', 'DESC']
    ])
  });

  res.json({
    meta: {
      page: req.query.page,
      page_size: req.query.page_size,
      page_count: Math.ceil(result.count / req.query.page_size),
      total: result.count
    },
    objects: result.rows
  });
});

router.post('/:id', auth, validation({
  params: {
    id: Joi.number().positive().required()
  },
  body: {
    transaction_hash: Joi.string().required()
  }
}), async (req, res) => {
  const {
    transaction_hash
  } = req.body;

  const withdraw = await models.Withdraw.findById(req.params.id);
  if (null === withdraw) {
    return res.boom.notFound();
  }

  await withdraw.update({ transaction_hash });

  const user = await models.User.findById(withdraw.user_id);
  if (null === user) {
    return res.boom.notFound();
  }

  mail.send(req.user.email, 'withdraw_confirm', {
    name: user.first_name,
    amount: withdraw.amount
  });

  res.status(204).end();
});

router.get('/:id', auth, validation({
  params: {
    id: Joi.number().positive().required()
  }
}), async (req, res) => {
  const withdraw = await models.Withdraw.findById(req.params.id);
  if (null === withdraw) {
    return res.boom.notFound();
  }

  res.json(withdraw);
});

module.exports = router;
