const express = require('express');
const Joi = require('joi');
const router = express.Router();
const auth = require('../middleware/auth');
const guard = require('express-jwt-permissions')();
const validation = require('../middleware/validation');
const ipnStrategy = require('../middleware/ipnStrategy');
const balance = require('../service/balance');
const wallet = require('../service/wallet');
const taxService = require('../service/tax');
const transactionService = require('../service/transaction');
const mail = require('../service/mail');
const { callback } = require('../util/coinpayment_events');
const filterValues = require('../util/filterValues');
const models = require('../models');
const consts = require('../consts');

router.get('', auth, validation({
  body: {
    token: Joi.number().label('validation.token')
  }
}), async (req, res) => {
  const rows = await models.Strategy.findAll({
    order: [
      ['position', 'ASC']
    ],
    include: [
      {
        model: models.Currency,
        as: 'currency'
      },
      {
        model: models.Currency,
        as: 'convert'
      }
    ]
  });

  const objects = [];
  for (let i = 0; i < rows.length; i++) {
    const where = {
      user_id: req.user.id,
      strategy_id: rows[i].id
    };
    objects.push({
      ...rows[i].toJSON(),
      balance: await balance.getResponse(where),
      checkpoints: {
        percent: await models.UserCheckpoint.sum('percent', { where }) || 0,
        count: await models.UserCheckpoint.count({ where }) || 0
      }
    })
  }

  res.json(objects);
});

router.get('/:id', auth, validation({
  params: {
    id: Joi.number().positive().integer().required()
  }
}), async (req, res) => {
  const strategy = await models.Strategy.findById(req.params.id, {
    include: [
      { model: models.Currency, as: 'currency' },
      { model: models.Currency, as: 'convert' }
    ]
  });

  const where = {
    user_id: req.user.id,
    strategy_id: strategy.id
  };

  res.json({
    ...strategy.toJSON(),
    balance: await balance.getResponse(where),
    checkpoints: {
      percent: await models.UserCheckpoint.sum('percent', { where }) || 0,
      count: await models.UserCheckpoint.count({ where }) || 0
    }
  });
});

/**
 * Создание кошелька для пользователя. Кошельки под
 * конкретную стратегию создаются через coinpayments.
 *
 * @param req
 * @param res
 */
router.post('/:id/wallet', auth, validation({
  params: {
    id: Joi.number().required()
  }
}), async (req, res) => {
  const strategy = await models.Strategy.findById(req.params.id);
  if (null === strategy) {
    return res.boom.notFound('currency not found');
  }

  const userWallet = await wallet.getWallet(
    req.user,
    strategy,
    await strategy.getConvert()
  );

  res.json({ address: userWallet.address });
});

/*
router.post('', auth, guard.check(['admin']), validation({
  body: {
    name: Joi.string().required(),
    description: Joi.string().required(),
    description_short: Joi.string().required(),
    icon: Joi.string(),
    hero_icon: Joi.string(),
    currency_id: Joi.number().positive().integer().required(),
    position: Joi.number().integer().default(0),
  }
}), async (req, res) => {
  const strategy = await models.Strategy.create(filterValues(req.body));

  res.status(201).json({
    id: strategy.id
  });
});
*/

router.post('/:id/ipn', validation({
  params: {
    id: Joi.number().positive().required()
  }
}), async (req, res, next) => {
  const strategy = await models.Strategy.findById(req.params.id);
  if (null === strategy) {
    return res.boom.notFound();
  }

  req.coinpayments = strategy.coinpayments;
  next();
}, ipnStrategy, async (req, res) => {
  const { status } = req.body;

  const statusInt = parseInt(status, 10);

  let state = null;

  if (statusInt < 0) {
    state = consts.PAYMENT_ERROR;
  }
  if (statusInt < 100) {
    state = consts.PAYMENT_PENDING;
  }
  if (statusInt === 100) {
    state = consts.PAYMENT_COMPLETE;
  }

  const { payment, wallet } = await callback(state, req.body);

  const user = await wallet.getUser();
  if (!user) {
    return res.boom.notFound(null, {
      error: 'User not found'
    });
  }

  const strategy = await wallet.getStrategy();

  const emailData = { strategy, payment, wallet, user };

  // Только для успешно проведенного платежа создаем транзакцию и
  // отправляем пользователю уведомление на почту
  if (payment.status === consts.PAYMENT_COMPLETE) {
    const tx = await transactionService.createPayment(payment);
    if (strategy.tax > 0) {
      await taxService.isTax(user.id, wallet.strategy_id, strategy.tax);
      //  https://blockchaincorp.atlassian.net/browse/CL-304
      // } else {
      //   await transactionService.createDeposit(tx);
    }

    mail.send(user.email, 'payment_success', {
      ...emailData,
      transaction: tx
    });

    res.status(201).json({ id: tx.id });
  } else {
    res.status(200).end();
  }
});

router.post('/:id', auth, guard.check(['admin']), validation({
  params: {
    id: Joi.number().integer().positive().required()
  },
  body: {
    name: Joi.string(),
    description: Joi.string(),
    description_short: Joi.string(),
    help_text: Joi.string(),
    icon: Joi.string(),
    hero_icon: Joi.string(),
    currency_id: Joi.number().positive().integer(),
    position: Joi.number().integer()
  }
}), async (req, res) => {
  const strategy = await models.Strategy.findById(req.params.id);

  if (null === strategy) {
    return res.boom.notFound();
  }

  await strategy.update(filterValues(req.body));

  res.status(204).end();
});

router.delete('/:id', auth, guard.check(['admin']), validation({
  params: {
    id: Joi.number().integer().positive().required()
  }
}), async (req, res) => {
  const strategy = await models.Strategy.findById(req.params.id);

  if (null === strategy) {
    return res.boom.notFound();
  }

  await strategy.destroy();

  res.status(204).end();
});

module.exports = router;
