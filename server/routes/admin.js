const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const guard = require('express-jwt-permissions')();

router.use('/dashboard', auth, guard.check(['admin']), require('./admin/dashboard'));
router.use('/currency', auth, guard.check(['admin']), require('./admin/currency'));
router.use('/franchise', auth, guard.check(['admin']), require('./admin/franchise'));
router.use('/user', auth, guard.check(['admin']), require('./admin/user'));
router.use('/transaction', auth, guard.check(['admin']), require('./admin/transaction'));
router.use('/payment', auth, guard.check(['admin']), require('./admin/payment'));
router.use('/strategy', auth, guard.check(['admin']), require('./admin/strategy'));
router.use('/faq', auth, guard.check(['admin']), require('./admin/faq'));
router.use('/question', auth, guard.check(['admin']), require('./admin/question'));
router.use('/referral', auth, guard.check(['admin']), require('./admin/referral'));
router.use('/settings', require('./admin/settings'));
router.use('/backup', auth, guard.check(['admin']), require('./admin/backup'));
router.use('/checkpoint', auth, require('./admin/checkpoint'));
router.use('/withdraw', auth, guard.check(['admin']), require('./admin/withdraw'));

module.exports = router;
