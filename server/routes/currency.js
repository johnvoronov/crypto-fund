const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const models = require('../models');

router.get(
  '/list',
  auth,
  async (req, res) => {
    const rows = await models.Currency.findAll();

    res.json(rows);
  });

module.exports = router;
