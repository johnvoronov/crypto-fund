const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const validation = require('../middleware/validation');
const coinpayments = require('../util/coinpayments');
const models = require('../models');
const appConfig = require('../config/app');
const Joi = require('joi');

router.get(
  '/list',
  auth,
  validation({
    query: {
      page_size: Joi.number().integer().min(1).max(300).default(100),
      page: Joi.number().integer().min(1).default(1)
    }
  }),
  /**
   * Получение списка платежей
   *
   * @param req
   * @param res
   * @returns {Promise<T>}
   */
  async (req, res) => {
    const result = await models.Payment.findAndCountAll({
      where: {
        user_id: req.user.id
      },
      include: [
        {
          model: models.Currency,
          as: 'currency'
        }
      ],
      limit: req.query.page_size,
      offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
      order: [
        ['id', 'DESC']
      ]
    });

    res.json({
      meta: {
        page: req.query.page,
        page_size: req.query.page_size,
        page_count: Math.ceil(result.count / req.query.page_size),
        total: result.count
      },
      objects: result.rows
    });
  }
);

router.post(
  '/debug',
  auth,
  validation({
    body: {
      address: Joi.string().required(),
      amount: Joi.number().required()
    }
  }),
  async (req, res) => {
    const {
      amount,
      address
    } = req.body;

    const schema = process.env.NODE_ENV === 'production' ? 'https' : 'http';
    const backend_url = appConfig.backend_url || req.headers.host;

    res.status(201).json({
      id: await coinpayments.fakePayment(
        `${schema}://${backend_url}/ipn`,
        amount,
        address
      )
    });
  }
);

module.exports = router;
