module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn('user_checkpoint', 'before', {
        type: Sequelize.DECIMAL(27, 18),
        allowNull: true
      }, { transaction });
      await queryInterface.renameColumn('user_checkpoint', 'amount', 'after', { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('user_checkpoint', 'before', { transaction });
      await queryInterface.renameColumn('user_checkpoint', 'after', 'amount', { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
