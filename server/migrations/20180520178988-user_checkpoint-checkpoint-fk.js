module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('user_checkpoint', 'checkpoint_id', {
      type: Sequelize.INTEGER,
      allowNull: true,
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
      references: {
        model: 'checkpoint',
        key: 'id',
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('user_checkpoint', 'checkpoint_id');
  }
};
