module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn('currency', 'to_rub', {
        type: Sequelize.DECIMAL(27, 18),
        allowNull: false,
        defaultValue: 0
      }, { transaction });
      await queryInterface.addColumn('currency', 'to_eth', {
        type: Sequelize.DECIMAL(27, 18),
        allowNull: false,
        defaultValue: 0
      }, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('currency', 'to_rub', { transaction });
      await queryInterface.removeColumn('currency', 'to_eth', { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
