module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('wallet', 'merchant_id', {
      type: Sequelize.STRING
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('wallet', 'merchant_id', {
      type: Sequelize.INTEGER
    });
  }
};
