module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.changeColumn('faq', 'question_en', {
        type: Sequelize.TEXT,
        allowNull: true
      }, { transaction });
      await queryInterface.changeColumn('faq', 'question_ru', {
        type: Sequelize.TEXT,
        allowNull: true
      }, { transaction });
      await queryInterface.changeColumn('faq', 'answer_en', {
        type: Sequelize.TEXT,
        allowNull: true
      }, { transaction });
      await queryInterface.changeColumn('faq', 'answer_ru', {
        type: Sequelize.TEXT,
        allowNull: true
      }, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.changeColumn('faq', 'question_en', {
        type: Sequelize.STRING,
        allowNull: true
      }, { transaction });
      await queryInterface.changeColumn('faq', 'question_ru', {
        type: Sequelize.STRING,
        allowNull: true
      }, { transaction });
      await queryInterface.changeColumn('faq', 'answer_en', {
        type: Sequelize.STRING,
        allowNull: true
      }, { transaction });
      await queryInterface.changeColumn('faq', 'answer_ru', {
        type: Sequelize.STRING,
        allowNull: true
      }, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
