const models = require('../models');

module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      const limit = [
        { limit: 50.01, percent: 20 },
        { limit: 10.01, percent: 30 },
        { limit: 1.01, percent: 40 }
      ];
      await models.Strategy.update({ limit }, { where: {} });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  async down(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await models.Strategy.update({ limit: [] }, { where: {} });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
