const models = require('../models');
const referralService = require('../service/referral');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await referralService.updateUsers();

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    await models.User.update({
      referral_level_id: null
    }, {
      where: {}
    });
  }
};
