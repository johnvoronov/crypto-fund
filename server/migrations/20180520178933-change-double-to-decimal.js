const decimal = types => ({
  type: types.DECIMAL(27, 18),
  allowNull: false
});

const double = type => ({
  type: types.DOUBLE,
  allowNull: false
});

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('currency', 'to_usd', decimal(Sequelize));
    await queryInterface.changeColumn('currency', 'to_eth', decimal(Sequelize));
    await queryInterface.changeColumn('payment', 'amount', decimal(Sequelize));
    await queryInterface.changeColumn('payment', 'token_price', decimal(Sequelize));
    await queryInterface.changeColumn('payment', 'tokens_amount', decimal(Sequelize));
    await queryInterface.changeColumn('payment', 'to_usd', decimal(Sequelize));
    await queryInterface.changeColumn('payment', 'to_eth', decimal(Sequelize));
    await queryInterface.changeColumn('transaction', 'amount', decimal(Sequelize));
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('currency', 'to_usd', double(Sequelize));
    await queryInterface.changeColumn('currency', 'to_eth', double(Sequelize));
    await queryInterface.changeColumn('payment', 'amount', double(Sequelize));
    await queryInterface.changeColumn('payment', 'token_price', double(Sequelize));
    await queryInterface.changeColumn('payment', 'tokens_amount', double(Sequelize));
    await queryInterface.changeColumn('payment', 'to_usd', double(Sequelize));
    await queryInterface.changeColumn('payment', 'to_eth', double(Sequelize));
    await queryInterface.changeColumn('transaction', 'amount', double(Sequelize));
  }
};
