module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('user_checkpoint', 'fee', {
      type: Sequelize.DECIMAL(27, 18),
    });
    await queryInterface.addColumn('user_checkpoint', 'level', {
      type: Sequelize.STRING,
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('user_checkpoint', 'fee');
    await queryInterface.removeColumn('user_checkpoint', 'level');
  }
};
