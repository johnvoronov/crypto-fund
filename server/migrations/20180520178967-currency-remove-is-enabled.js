module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('currency', 'is_enabled');
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('currency', 'is_enabled', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    });
  }
};
