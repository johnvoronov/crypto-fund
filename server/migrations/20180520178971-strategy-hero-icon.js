module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('strategy', 'hero_icon', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('strategy', 'hero_icon');
  }
};
