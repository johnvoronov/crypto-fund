module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn('user_checkpoint', 'usd', {
        type: Sequelize.DECIMAL(27, 18),
        allowNull: true
      }, { transaction });
      await queryInterface.addColumn('transaction', 'usd', {
        type: Sequelize.DECIMAL(27, 18),
        allowNull: true
      }, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },
  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('user_checkpoint', 'usd', { transaction });
      await queryInterface.removeColumn('transaction', 'usd', { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
