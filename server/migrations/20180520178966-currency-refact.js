module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('currency', 'to_usd', {
      type: Sequelize.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    });
    await queryInterface.addColumn('currency', 'to_btc', {
      type: Sequelize.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('currency', 'to_usd', {
      type: Sequelize.DECIMAL(27, 18),
      allowNull: false
    });
    await queryInterface.removeColumn('currency', 'to_btc');
  }
};
