module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('setting', 'value', {
      type: Sequelize.TEXT,
      allowNull: true
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('setting', 'value', {
      type: Sequelize.STRING,
      allowNull: true
    });
  }
};
