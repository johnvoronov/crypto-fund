module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('referral', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      team: {
        type: Sequelize.DOUBLE,
        allowNull: false
      },
      user: {
        type: Sequelize.DOUBLE,
        allowNull: false
      },
      levels: {
        type: Sequelize.ARRAY(Sequelize.DOUBLE),
        allowNull: true
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('referral');
  }
};
