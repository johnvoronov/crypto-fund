const consts = require('../consts');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn('transaction', 'account_type', {
        type: Sequelize.INTEGER,
        allowNull: true
      }, { transaction });
      await queryInterface.sequelize.query(`
      UPDATE transaction SET account_type = :account_type WHERE status NOT IN (:status)
      `, {
        type: queryInterface.sequelize.QueryTypes.UPDATE,
        replacements: {
          account_type: consts.ACCOUNT_TYPE_STRATEGY,
          status: [
            consts.TRANSACTION_PRO_BONUS,
            consts.TRANSACTION_PAY_STATUS_BONUS,
            consts.TRANSACTION_ONE_TIME_BONUS
          ]
        },
        transaction
      });
      await queryInterface.sequelize.query(`
      UPDATE transaction SET account_type = :account_type WHERE status IN (:status)
      `, {
        type: queryInterface.sequelize.QueryTypes.UPDATE,
        replacements: {
          account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
          status: [
            consts.TRANSACTION_PRO_BONUS,
            consts.TRANSACTION_PAY_STATUS_BONUS,
            consts.TRANSACTION_ONE_TIME_BONUS
          ]
        },
        transaction
      });
      await queryInterface.changeColumn('transaction', 'account_type', {
        type: Sequelize.INTEGER,
        allowNull: false
      }, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('transaction', 'account_type', { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
