module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('user_checkpoint', 'percent', {
      type: Sequelize.DOUBLE,
      allowNull: false,
      defaultValue: 0
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('user_checkpoint', 'percent');
  }
};
