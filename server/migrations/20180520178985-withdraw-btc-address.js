module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('withdraw', 'btc_address', {
      type: Sequelize.STRING,
      allowNull: false
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('withdraw', 'btc_address');
  }
};
