module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn('strategy', 'caption', {
        type: Sequelize.STRING,
        allowNull: true
      }, { transaction });
      await queryInterface.addColumn('strategy', 'features', {
        type: Sequelize.ARRAY(Sequelize.STRING),
        allowNull: true
      }, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('strategy', 'caption', { transaction });
      await queryInterface.removeColumn('strategy', 'features', { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
