module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('payment', 'deleted_at', {
      allowNull: true,
      type: Sequelize.DATE
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('payment', 'deleted_at');
  }
};
