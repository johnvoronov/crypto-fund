module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn('referral', 'description_ru', {
        type: Sequelize.TEXT,
        allowNull: true
      }, { transaction });
      await queryInterface.addColumn('referral', 'description_en', {
        type: Sequelize.TEXT,
        allowNull: true
      }, { transaction });
      await queryInterface.addColumn('referral', 'opportunities_ru', {
        type: Sequelize.TEXT,
        allowNull: true
      }, { transaction });
      await queryInterface.addColumn('referral', 'opportunities_en', {
        type: Sequelize.TEXT,
        allowNull: true
      }, { transaction });
      await queryInterface.addColumn('referral', 'caption_ru', {
        type: Sequelize.STRING,
        allowNull: true
      }, { transaction });
      await queryInterface.addColumn('referral', 'caption_en', {
        type: Sequelize.STRING,
        allowNull: true
      }, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.dropColumn('referral', 'description_ru', { transaction });
      await queryInterface.dropColumn('referral', 'description_en', { transaction });
      await queryInterface.dropColumn('referral', 'opportunities_ru', { transaction });
      await queryInterface.dropColumn('referral', 'opportunities_en', { transaction });
      await queryInterface.dropColumn('referral', 'caption_ru', { transaction });
      await queryInterface.dropColumn('referral', 'caption_en', { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
