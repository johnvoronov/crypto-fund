module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('strategy', 'position', {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('strategy', 'position');
  }
};
