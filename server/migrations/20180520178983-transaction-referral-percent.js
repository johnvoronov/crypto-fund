module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('transaction', 'referral_percent', {
      type: Sequelize.DOUBLE,
      allowNull: true
    });
    await queryInterface.addColumn('transaction', 'user_checkpoint_id', {
      type: Sequelize.INTEGER,
      allowNull: true,
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
      references: {
        model: 'user_checkpoint',
        key: 'id',
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('transaction', 'referral_percent');
    await queryInterface.removeColumn('transaction', 'user_checkpoint_id');
  }
};
