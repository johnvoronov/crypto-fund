module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('user_checkpoint', 'strategy_id', {
      type: Sequelize.INTEGER,
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
      references: {
        model: 'strategy',
        key: 'id',
        as: 'strategy_id'
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('user_checkpoint', 'strategy_id');
  }
};
