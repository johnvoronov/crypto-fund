module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    const field = {
      type: Sequelize.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    };

    try {
      await queryInterface.addColumn('payment', 'to_rub', field, { transaction });
      await queryInterface.addColumn('payment', 'to_eth', field, { transaction });
      await queryInterface.addColumn('payment', 'eth', field, { transaction });
      await queryInterface.addColumn('payment', 'rub', field, { transaction });
      await queryInterface.addColumn('payment', 'btc', field, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('payment', 'to_rub', { transaction });
      await queryInterface.removeColumn('payment', 'to_eth', { transaction });
      await queryInterface.removeColumn('payment', 'eth', { transaction });
      await queryInterface.removeColumn('payment', 'rub', { transaction });
      await queryInterface.removeColumn('payment', 'btc', { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
