module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('transaction', 'status');
    await queryInterface.removeColumn('transaction', 'is_return');
    await queryInterface.removeColumn('transaction', 'comment');
  },

  down: async (queryInterface, Sequelize) => {
  }
};
