module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn('strategy', 'fee', {
        type: Sequelize.DOUBLE,
        allowNull: false,
        defaultValue: 0
      }, { transaction });
      await queryInterface.addColumn('payment', 'fund_fee', {
        type: Sequelize.DECIMAL(27, 18),
        allowNull: false,
        defaultValue: 0
      }, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('strategy', 'fee', { transaction });
      await queryInterface.removeColumn('payment', 'fund_fee', { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
