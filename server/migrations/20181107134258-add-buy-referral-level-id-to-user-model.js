module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn('user', 'buy_referral_level_id', {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'referral',
          key: 'id'
        }
      }, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('user', 'buy_referral_level_id', { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
    }
  }
};
