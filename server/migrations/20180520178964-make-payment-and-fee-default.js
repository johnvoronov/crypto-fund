module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('payment', 'transaction_id', {
      type: Sequelize.STRING,
      allowNull: true
    });
    await queryInterface.changeColumn('payment', 'fee', {
      type: Sequelize.DOUBLE,
      allowNull: false,
      defaultValue: 0
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('payment', 'transaction_id', {
      type: Sequelize.STRING,
      allowNull: false
    });
    await queryInterface.changeColumn('payment', 'fee', {
      type: Sequelize.DOUBLE,
      allowNull: false
    });
  }
};
