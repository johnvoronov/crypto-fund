module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('payment', 'conversion');
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('payment', 'conversion', {
      type: DataTypes.STRING,
      allowNull: true
    });
  }
};
