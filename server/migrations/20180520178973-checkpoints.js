module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('checkpoint', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      amount: {
        type: Sequelize.DECIMAL(27, 18)
      },
      percent: {
        type: Sequelize.DOUBLE
      },
      start_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      end_at: {
        allowNull: true,
        type: Sequelize.DATE
      },
      is_done: {
        allowNull: false,
        defaultValue: false,
        type: Sequelize.BOOLEAN
      },
      strategy_id: {
        type: Sequelize.INTEGER,
        onDelete: 'SET NULL',
        references: {
          model: 'strategy',
          key: 'id',
          as: 'strategy_id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
    await queryInterface.createTable('user_checkpoint', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      amount: {
        type: Sequelize.DECIMAL(27, 18)
      },
      diff: {
        type: Sequelize.DECIMAL(27, 18)
      },
      user_id: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'user',
          key: 'id',
          as: 'user_id'
        }
      },
      is_done: {
        allowNull: false,
        defaultValue: false,
        type: Sequelize.BOOLEAN
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('checkpoint');
    await queryInterface.dropTable('user_checkpoint');
  }
};
