module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.renameColumn('user', 'eth_address', 'btc_address');
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.renameColumn('user', 'btc_address', 'eth_address');
  }
};
