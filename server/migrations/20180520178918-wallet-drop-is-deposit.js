module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'wallet',
      'is_deposit'
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'wallet',
      'is_deposit',
      {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      }
    );
  }
};
