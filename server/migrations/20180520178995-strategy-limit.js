module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn('strategy', 'limit', {
        type: Sequelize.JSON,
        allowNull: true
      }, {
        transaction
      });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  async down(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('strategy', 'limit', {
        transaction
      });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
