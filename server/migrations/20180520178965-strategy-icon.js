module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('strategy', 'icon', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('strategy', 'icon');
  }
};
