module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn('referral', 'name_ru', {
        type: Sequelize.STRING,
        allowNull: true
      }, { transaction });
      await queryInterface.renameColumn('referral', 'name', 'name_en', { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('referral', 'name_ru', { transaction });
      await queryInterface.renameColumn('referral', 'name_en', 'name', { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
