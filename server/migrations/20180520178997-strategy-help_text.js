module.exports = {
  async up(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn('strategy', 'help_text', {
        type: Sequelize.TEXT,
        allowNull: true
      }, {
        transaction
      });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  async down(queryInterface, Sequelize) {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('strategy', 'help_text', {
        transaction
      });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
