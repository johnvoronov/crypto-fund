module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('payment', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'user',
          key: 'id',
          as: 'user_id'
        }
      },
      currency_id: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'currency',
          key: 'id',
          as: 'currency_id'
        }
      },
      amount: {
        type: Sequelize.DOUBLE,
        allowNull: false
      },
      discount: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      wallet_id: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'wallet',
          key: 'id',
          as: 'wallet_id'
        }
      },
      transaction_id: {
        type: Sequelize.STRING,
        allowNull: false
      },
      fee: {
        type: Sequelize.DOUBLE,
        allowNull: false
      },
      token_price: {
        type: Sequelize.DOUBLE,
        allowNull: false
      },
      conversion: {
        type: Sequelize.STRING,
        allowNull: true
      },
      is_complete: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      tokens_amount: {
        type: Sequelize.DOUBLE,
        allowNull: false
      },
      to_usd: {
        type: Sequelize.DOUBLE,
        allowNull: false
      },
      to_eth: {
        type: Sequelize.DOUBLE,
        allowNull: false
      },
      is_cancelled: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      is_pending: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('payment');
  }
};
