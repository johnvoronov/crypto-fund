module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('wallet', 'merchant_id', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('wallet', 'merchant_id');
  }
};
