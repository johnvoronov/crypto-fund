module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('social', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'user',
          key: 'id',
          as: 'user_id'
        },
        allowNull: true
      },
      owner_id: {
        type: Sequelize.STRING
      },
      provider: {
        type: Sequelize.STRING
      },
      parameters: {
        type: Sequelize.JSON
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('social');
  }
};
