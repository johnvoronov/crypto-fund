module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('payment', 'usd', {
      type: Sequelize.DECIMAL(27, 18),
      allowNull: true
    });
    await queryInterface.addColumn('transaction', 'comment', {
      type: Sequelize.STRING,
      allowNull: true
    });
    await queryInterface.removeColumn('currency', 'to_eth');
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('currency', 'to_eth', {
      type: Sequelize.DECIMAL(27, 18),
      allowNull: false
    });
    await queryInterface.removeColumn('transaction', 'comment');
    await queryInterface.removeColumn('payment', 'usd');
  }
};
