module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('user', 'phone', {
      type: Sequelize.STRING,
      allowNull: true
    });
    await queryInterface.addColumn('user', 'first_name', {
      type: Sequelize.STRING,
      allowNull: true
    });
    await queryInterface.addColumn('user', 'last_name', {
      type: Sequelize.STRING,
      allowNull: true
    });
    await queryInterface.addColumn('user', 'country', {
      type: Sequelize.STRING,
      allowNull: true
    });
    await queryInterface.addColumn('user', 'city', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('user', 'phone');
    await queryInterface.removeColumn('user', 'first_name');
    await queryInterface.removeColumn('user', 'last_name');
    await queryInterface.removeColumn('user', 'country');
    await queryInterface.removeColumn('user', 'city');
  }
};
