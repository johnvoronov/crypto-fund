module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('payment', 'to_btc', {
      type: Sequelize.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('payment', 'to_btc');
  }
};
