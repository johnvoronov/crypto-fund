module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn('faq', 'question_en', {
        type: Sequelize.STRING,
        allowNull: true
      }, { transaction });
      await queryInterface.renameColumn('faq', 'question', 'question_ru', { transaction });

      await queryInterface.addColumn('faq', 'answer_en', {
        type: Sequelize.STRING,
        allowNull: true
      }, { transaction });
      await queryInterface.renameColumn('faq', 'answer', 'answer_ru', { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('faq', 'question_en', { transaction });
      await queryInterface.renameColumn('faq', 'question_ru', 'question', { transaction });

      await queryInterface.removeColumn('faq', 'answer_en', { transaction });
      await queryInterface.renameColumn('faq', 'answer_ru', 'answer', { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
