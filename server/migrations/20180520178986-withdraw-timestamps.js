module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('withdraw', 'created_at', {
      allowNull: false,
      type: Sequelize.DATE
    });
    await queryInterface.addColumn('withdraw', 'updated_at', {
      allowNull: false,
      type: Sequelize.DATE
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('withdraw', 'created_at');
    await queryInterface.removeColumn('withdraw', 'updated_at');
  }
};
