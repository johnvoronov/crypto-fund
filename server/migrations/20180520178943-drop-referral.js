module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('referral', { cascade: true });
    await queryInterface.dropTable('referral_withdraw', { cascade: true });
  },

  down: async (queryInterface, Sequelize) => {
  }
};
