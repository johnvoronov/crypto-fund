module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('transaction', 'is_freeze', {
      allowNull: false,
      defaultValue: false,
      type: Sequelize.BOOLEAN
    });
    await queryInterface.addColumn('transaction', 'is_deposit', {
      allowNull: false,
      defaultValue: false,
      type: Sequelize.BOOLEAN
    });
    await queryInterface.addColumn('transaction', 'strategy_id', {
      type: Sequelize.INTEGER,
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
      references: {
        model: 'strategy',
        key: 'id',
        as: 'strategy_id'
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('transaction', 'is_freeze');
    await queryInterface.removeColumn('transaction', 'is_deposit');
    await queryInterface.removeColumn('transaction', 'strategy_id');
  }
};
