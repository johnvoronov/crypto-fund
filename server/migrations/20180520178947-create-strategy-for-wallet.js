module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('wallet', 'strategy_id', {
      type: Sequelize.INTEGER,
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
      references: {
        model: 'strategy',
        key: 'id',
        as: 'strategy_id'
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('wallet', 'strategy_id');
  }
};
