module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('strategy', 'description', {
      type: Sequelize.TEXT,
      allowNull: true
    });
    await queryInterface.addColumn('strategy', 'description_short', {
      type: Sequelize.TEXT,
      allowNull: true
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('strategy', 'description');
    await queryInterface.removeColumn('strategy', 'description_short');
  }
};
