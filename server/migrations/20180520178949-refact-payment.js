module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('payment', 'tokens_amount');
    await queryInterface.removeColumn('payment', 'discount');
    await queryInterface.removeColumn('payment', 'token_price');
    await queryInterface.addColumn('payment', 'strategy_id', {
      type: Sequelize.INTEGER,
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
      references: {
        model: 'strategy',
        key: 'id',
        as: 'strategy_id'
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
  }
};
