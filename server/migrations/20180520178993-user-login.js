module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn('user', 'login', {
        type: Sequelize.STRING,
        allowNull: true
      }, { transaction });

      const users = await queryInterface.sequelize.query('SELECT * FROM "user"', {
        type: queryInterface.sequelize.QueryTypes.SELECT,
        transaction
      });
      for (let i = 0; i < users.length; i++) {
        const user = users[i];
        const [
          login,
          domain
        ] = user.email.split('@');
        await queryInterface.sequelize.query('UPDATE "user" SET login = :login WHERE id = :id', {
          replacements: {
            login: login,
            id: user.id
          },
          transaction
        });
      }
      await queryInterface.changeColumn('user', 'login', {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      }, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('user', 'login', { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
