module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    const field = {
      type: Sequelize.DECIMAL(27, 18),
      allowNull: false,
      defaultValue: 0
    };
    try {
      await queryInterface.addColumn('transaction', 'rub', field, { transaction });
      await queryInterface.addColumn('transaction', 'eth', field, { transaction });
      await queryInterface.addColumn('transaction', 'btc', field, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('transaction', 'rub', { transaction });
      await queryInterface.removeColumn('transaction', 'eth', { transaction });
      await queryInterface.removeColumn('transaction', 'btc', { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
