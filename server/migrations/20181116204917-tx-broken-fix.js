module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn('transaction', 'checkpoint_id', {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'checkpoint',
          key: 'id'
        }
      }, { transaction });
      await queryInterface.removeColumn('checkpoint', 'start_at', { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('transaction', 'checkpoint_id', { transaction });
      await queryInterface.addColumn('checkpoint', 'start_at', {
        allowNull: true,
        type: Sequelize.DATE
      }, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
