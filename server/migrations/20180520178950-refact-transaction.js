module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('transaction', 'is_referrer');
    await queryInterface.removeColumn('transaction', 'is_bonus');
    await queryInterface.removeColumn('transaction', 'is_withdraw');
    await queryInterface.addColumn('transaction', 'status', {
      type: Sequelize.INTEGER,
      defaultValue: 0,
      allowNull: false
    });
    await queryInterface.addColumn('transaction', 'is_done', {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      allowNull: false
    });
  },
  down: async (queryInterface, Sequelize) => {
  }
};
