const paymentService = require('../service/payment');
const transactionService = require('../service/transaction');

const debugPaymentTx = async (user, strategy, currency, amount, createdAt = null) => {
  return await transactionService.createPayment(
    await paymentService.createPayment(user, strategy, currency, amount, 'debug', createdAt)
  );
};

const debugTx = async (user, strategy, currency, amount, createdAt) => {
  return await transactionService.createDeposit(
    await debugPaymentTx(user, strategy, currency, amount, createdAt)
  );
};

module.exports = {
  debugPaymentTx,
  debugTx
};
