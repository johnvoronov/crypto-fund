const models = require('../../models');
const checkpointService = require('../../service/checkpoint');
const balance = require('../../service/balance');
const referralService = require('../../service/referral');
const transactionService = require('../../service/transaction');
const consts = require('../../consts');
const { debugPaymentTx, debugTx } = require('../utils');

describe('service/checkpoint', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });

    const level = {
      name: 'newbie',
      team: 5,
      user: 5,
      levels: [5]
    };
    referralService.getGrid = jest.fn(() => Promise.resolve([level]));
  });

  afterEach(async () => {
    process.env.APP_NAME = null;
    jest.resetAllMocks();
  });

  test('getUserPercent / ctplatform', async () => {
    process.env.APP_NAME = 'ctplatform';
    const currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 100
    });
    const strategy = await models.Strategy.create({
      name: 'test',
      currency_id: currency.id,
      convert_id: currency.id,
      tax: 50,
      limit: [
        { limit: 10, percent: 5 },
        { limit: 80, percent: 10 }
      ]
    });

    // Отдельно получаем сумму бонуса по данному платежу, так как часть дохода, если это
    // конечно доход, а не убыток переходит в фонд.
    const user = await models.User.create({
      email: 'user@example.com'
    });

    const startAt = new Date();
    startAt.setDate(startAt.getDate() - 1);
    const endAt = new Date();
    endAt.setDate(endAt.getDate() + 1);

    const point = await models.Checkpoint.create({
      amount: 100,
      percent: 10,
      start_at: startAt,
      end_at: endAt,
      is_done: true
    });

    await debugTx(user, strategy, currency, 0.01);
    expect(await checkpointService.getUserPercent(strategy, user.id, point)).toEqual(0);
    await debugTx(user, strategy, currency, 0.5);
    expect(await checkpointService.getUserPercent(strategy, user.id, point)).toEqual(5);
    await debugTx(user, strategy, currency, 10);
    expect(await checkpointService.getUserPercent(strategy, user.id, point)).toEqual(10);
  });

  test('getUserPercent / bitlab', async () => {
    const currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 100
    });
    const strategy = await models.Strategy.create({
      name: 'test',
      currency_id: currency.id,
      convert_id: currency.id,
      limit: [
        { limit: 0.005, percent: 60 },
        { limit: 35, percent: 20 }
      ]
    });

    // Отдельно получаем сумму бонуса по данному платежу, так как часть дохода, если это
    // конечно доход, а не убыток переходит в фонд.
    const user = await models.User.create({
      email: 'user@example.com'
    });

    const startAt = new Date();
    startAt.setDate(startAt.getDate() - 1);
    const endAt = new Date();
    endAt.setDate(endAt.getDate() + 1);

    const point = await models.Checkpoint.create({
      amount: 100,
      percent: 10,
      start_at: startAt,
      end_at: endAt,
      is_done: true
    });

    await debugTx(user, strategy, currency, 0.125);
    expect(await checkpointService.getUserPercent(strategy, user.id, point)).toEqual(40);

    await debugTx(user, strategy, currency, 40);
    expect(await checkpointService.getUserPercent(strategy, user.id, point)).toEqual(80);
  });

  test('getIncomeAmount', async () => {
    const currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 100
    });
    const strategy = await models.Strategy.create({
      name: 'test',
      currency_id: currency.id,
      convert_id: currency.id
    });

    // Отдельно получаем сумму бонуса по данному платежу, так как часть дохода, если это
    // конечно доход, а не убыток переходит в фонд.
    const user = await models.User.create({
      email: 'user@example.com'
    });

    const startAt = new Date();
    startAt.setDate(startAt.getDate() - 1);

    await debugTx(user, strategy, currency, 10);
    const point = await models.Checkpoint.create({
      amount: 100,
      percent: 10,
      start_at: startAt,
      end_at: new Date,
      is_done: true
    });

    const percents = await checkpointService.getIncomePercent(strategy.id, user.id, point);
    expect(percents.userPercent).toEqual(50);
    expect(percents.fundPercent).toEqual(50);

    const amounts = checkpointService.getIncomeAmount(percents.userPercent, percents.fundPercent, 100, point);
    expect(amounts.userAmount.toNumber()).toEqual(50);
    expect(amounts.fundAmount.toNumber()).toEqual(50);
  });

  test('checkpoint', async () => {
    const currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 10
    });
    const strategy = await models.Strategy.create({
      currency_id: currency.id,
      convert_id: currency.id,
      name: 'test'
    });
    expect(await models.Checkpoint.count()).toEqual(0);
    await checkpointService.checkpoint(strategy.id, 10, new Date);
    expect(await models.Checkpoint.count()).toEqual(1);
  });

  test('completeIteration', async () => {
    const currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 100
    });
    const strategy = await models.Strategy.create({
      name: 'test',
      currency_id: currency.id,
      convert_id: currency.id,
      limit: [
        { limit: 100, percent: 20 }
      ]
    });

    const user = await models.User.create({
      email: 'user@example.com',
      password: '123456',
      is_active: true
    });

    // Создаем платеж + транзакцию + сразу отправляем в депозит в тратегию
    await debugTx(user, strategy, currency, 100);
    // Создаем платеж + транзакцию, но не отправляем ее "в работу", она по умолчанию заморожена
    await debugPaymentTx(user, strategy, currency, 50);

    const where = {
      user_id: user.id
    };

    expect(await balance.getUserBalance(where)).toEqual(50);
    expect(await balance.getDepositBalance(where)).toEqual(100);

    expect(await models.Checkpoint.count()).toEqual(0);
    const startAt = new Date;
    startAt.setMonth(startAt.getMonth() - 1);
    const endAt = new Date;
    endAt.setMonth(endAt.getMonth() + 1);
    await checkpointService.checkpoint(strategy.id, 10, endAt);
    expect(await models.Checkpoint.count()).toEqual(1);

    await checkpointService.completeIteration(strategy.id);

    expect(await balance.getUserBalance(where)).toEqual(50);
    expect(await balance.getIncomeBalance(where)).toEqual(8);
    expect(await balance.getDepositBalance(where)).toEqual(108);

    expect(await balance.getFundBalance()).toEqual(2);
  });

  test('completeIteration strategy fee', async () => {
    const currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 100
    });
    const strategy = await models.Strategy.create({
      name: 'test',
      currency_id: currency.id,
      convert_id: currency.id,
      fee: 10,
      limit: [
        { limit: 100, percent: 20 }
      ]
    });

    const user = await models.User.create({
      email: 'user@example.com',
      is_active: true
    });

    // Создаем платеж + транзакцию + сразу отправляем в депозит в тратегию
    await debugTx(user, strategy, currency, 100);
    // Создаем платеж + транзакцию, но не отправляем ее "в работу", она по умолчанию заморожена
    await debugPaymentTx(user, strategy, currency, 50);

    const where = {
      user_id: user.id
    };

    expect(await balance.getUserBalance(where)).toEqual(50);
    expect(await balance.getDepositBalance(where)).toEqual(100);

    expect(await models.Checkpoint.count()).toEqual(0);
    const startAt = new Date;
    startAt.setMonth(startAt.getMonth() - 1);
    const endAt = new Date;
    endAt.setMonth(endAt.getMonth() + 1);
    await checkpointService.checkpoint(strategy.id, 10, endAt);
    expect(await models.Checkpoint.count()).toEqual(1);

    await checkpointService.completeIteration(strategy.id);

    expect(await balance.getUserBalance(where)).toEqual(50);
    expect(await balance.getIncomeBalance(where)).toEqual(7.2);
    expect(await balance.getDepositBalance(where)).toEqual(107.2);

    expect(await balance.getFundBalance()).toEqual(2.8);
  });
});
