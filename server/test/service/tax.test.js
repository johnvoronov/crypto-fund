const models = require('../../models');
const tax = require('../../service/tax');
const { debugPaymentTx } = require('../utils');

describe('service/tax', () => {
  beforeEach(async () => {
    await models.sequelize.sync({
      force: true
    });
  });

  it('tax', async () => {
    const user = await models.User.create({
      email: 'user@example.com'
    });

    const currency = await models.Currency.create({
      code: 'BTC',
      name: 'Bitcoin',
      to_usd: 100,
      to_btc: 1,
      to_rub: 1,
    });

    const strategy = await models.Strategy.create({
      name: 'Test',
      currency_id: currency.id,
      convert_id: currency.id
    });

    const taxAmount = 50;
    expect(await tax.isTax(null, strategy.id, taxAmount)).toEqual(false);
    expect(await tax.isTax(0, strategy.id, taxAmount)).toEqual(false);
    expect(await tax.isTax(user.id, strategy.id, taxAmount)).toEqual(false);

    await debugPaymentTx(user, strategy, currency, 1);
    expect(await tax.isTax(user.id, strategy.id, taxAmount)).toEqual(true);

    const value = await models.Transaction.sum('amount', {
      where: { user_id: user.id }
    });
    expect(value).toEqual(.5);
  });
});
