const models = require('../../models');
const userCheckpointService = require('../../service/userCheckpoint');

describe('service/userCheckpoint', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  test('findValidPreviousFinish', async () => {
    const currency = await models.Currency.create({
      name: 'Test',
      code: 'TST'
    });
    const strategy = await models.Strategy.create({
      name: 'test',
      currency_id: currency.id,
      convert_id: currency.id,
    });
    const finish1 = await models.Finish.create({ strategy_id: strategy.id });
    const finish2 = await models.Finish.create({ strategy_id: strategy.id });
    const finish3 = await models.Finish.create({ strategy_id: strategy.id });

    const valid = await userCheckpointService.findValidPreviousFinish(finish3.id, strategy.id);
    expect(valid.id).toEqual(finish2.id);
  });

  test('checkpoint', async () => {
    const user = await models.User.create({
      email: 'user@example.com'
    });
    const currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 10,
      to_btc: 1,
      to_eth: 2,
      to_rub: 100
    });
    const strategy = await models.Strategy.create({
      name: 'user@example.com',
      currency_id: currency.id,
      convert_id: currency.id
    });
    const lastSums = await userCheckpointService.getHistory(user.id, strategy.id);
    await models.UserCheckpoint.create({
      user_id: user.id,
      strategy_id: strategy.id,
      before: 100,
      after: 50,
      usd: 50,
      diff: 50,
    });
    expect(await userCheckpointService.findBaseBalanceByCheckpoints(lastSums)).toEqual(0);
  });
});
