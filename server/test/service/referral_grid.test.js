const referral = require('../../service/referral');
const models = require('../../models');

describe('service/referral_grid', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
    referral.getUserAmount = jest.fn(() => Promise.resolve(0));
    referral.getTeamAmount = jest.fn(() => Promise.resolve(0));
    referral.getReferralCount = jest.fn();
    referral.getGrid = jest.fn(() => Promise.resolve([
      {
        name_en: 'vip',
        name_ru: 'vip',
        team: 50000,
        user: 5000,
        user_count: 15,
        levels: [10, 5, 3, 1, 0.5, 0.3, 0.3]
      },
      {
        name_en: 'profi',
        name_ru: 'profi',
        team: 10000,
        user: 1000,
        user_count: 10,
        levels: [9, 4, 1, 0.5, 0.5]
      },
      {
        name_en: 'partner',
        name_ru: 'partner',
        team: 5000,
        user: 500,
        user_count: 5,
        levels: [7, 2, 1]
      },
      {
        name_en: 'newbie',
        name_ru: 'newbie',
        team: 0,
        user: 100,
        user_count: 2,
        levels: [5]
      },
      {
        name_en: 'no status',
        name_ru: 'no status',
        team: 0,
        user: 0,
        user_count: 0,
        levels: []
      }
    ]));
  });

  afterEach(async () => {
    jest.resetAllMocks();
  });

  test('testGrid: no status', async () => {
    referral.getUserAmount.mockReturnValue(Promise.resolve(0));
    referral.getTeamAmount.mockReturnValue(Promise.resolve(0));
    referral.getReferralCount.mockReturnValue(Promise.resolve(0));
    const levels = await referral.getGrid();
    const level = await referral.getReferralLevel(null, levels);
    expect(level.name_en).toEqual('no status');
  });

  test('testGrid: newbie', async () => {
    referral.getUserAmount.mockReturnValue(Promise.resolve(100));
    referral.getTeamAmount.mockReturnValue(Promise.resolve(0));
    referral.getReferralCount.mockReturnValue(Promise.resolve(2));
    const levels = await referral.getGrid();
    const level = await referral.getReferralLevel(null, levels);
    expect(level.name_en).toEqual('newbie');
  });

  test('testGrid: partner', async () => {
    referral.getUserAmount.mockReturnValue(Promise.resolve(500));
    referral.getTeamAmount.mockReturnValue(Promise.resolve(5000));
    referral.getReferralCount.mockReturnValue(Promise.resolve(5));
    const levels = await referral.getGrid();
    const level = await referral.getReferralLevel(null, levels);
    expect(level.name_en).toEqual('partner');
  });

  test('testGrid: profi', async () => {
    referral.getUserAmount.mockReturnValue(Promise.resolve(1000));
    referral.getTeamAmount.mockReturnValue(Promise.resolve(10000));
    referral.getReferralCount.mockReturnValue(Promise.resolve(10));
    const levels = await referral.getGrid();
    const level = await referral.getReferralLevel(null, levels);
    expect(level.name_en).toEqual('profi');
  });

  test('testGrid: vip', async () => {
    referral.getUserAmount.mockReturnValue(Promise.resolve(5000));
    referral.getTeamAmount.mockReturnValue(Promise.resolve(50000));
    referral.getReferralCount.mockReturnValue(Promise.resolve(15));
    const levels = await referral.getGrid();
    const level = await referral.getReferralLevel(null, levels);
    expect(level.name_en).toEqual('vip');
  });

  test('getReferralLevel', async () => {
    let level;
    const levels = await referral.getGrid();

    // Если пользователь внес 100$ но его команда внесла 0
    // то выводить ему статус за 100$ и реф система будет
    // зачислять ему бонусы в размере 5% (newbie)
    referral.getUserAmount.mockReturnValue(Promise.resolve(100));
    referral.getTeamAmount.mockReturnValue(Promise.resolve(0));
    referral.getReferralCount.mockReturnValue(Promise.resolve(2));
    level = await referral.getReferralLevel(null, levels);
    expect(level.levels[0]).toEqual(5);

    // Если пользователь внес 500$ а его команда внесла 5000
    // то выводить ему статус за 500$ и реф система будет
    // зачислять ему бонусы
    referral.getUserAmount.mockReturnValue(Promise.resolve(500));
    referral.getTeamAmount.mockReturnValue(Promise.resolve(5000));
    referral.getReferralCount.mockReturnValue(Promise.resolve(5));
    level = await referral.getReferralLevel(null, levels);
    expect(level.levels[0]).toEqual(7);
    expect(level.levels[1]).toEqual(2);

    // Если пользователь внес 500$ но его команда внесла 0
    // то выводить ему статус за 500$ и реф система будет
    // зачислять ему бонусы в размере 5% (newbie)
    referral.getUserAmount.mockReturnValue(Promise.resolve(500));
    referral.getTeamAmount.mockReturnValue(Promise.resolve(0));
    referral.getReferralCount.mockReturnValue(Promise.resolve(5));
    level = await referral.getReferralLevel(null, levels);
    expect(level.levels[0]).toEqual(5);
  });

  test('getNextLevel', async () => {
    let level;
    const levels = await referral.getGrid();

    // Если пользователь внес 100$ но его команда внесла 0
    // то выводить ему статус за 100$ (newbie) и следющий уровень
    // со статусом за 500$ (partner)
    referral.getUserAmount.mockReturnValue(Promise.resolve(100));
    referral.getTeamAmount.mockReturnValue(Promise.resolve(0));
    referral.getReferralCount.mockReturnValue(Promise.resolve(2));
    level = await referral.getNextLevel(null, levels);
    expect(level.levels[0]).toEqual(7);

    // Если пользователь внес 500$ а его команда внесла 5000
    // то выводить ему статус за 500$ (partner) и следющий уровень
    // со статусом за 1000$ (profi)
    referral.getUserAmount.mockReturnValue(Promise.resolve(500));
    referral.getTeamAmount.mockReturnValue(Promise.resolve(5000));
    referral.getReferralCount.mockReturnValue(Promise.resolve(5));
    level = await referral.getNextLevel(null, levels);
    expect(level.levels[0]).toEqual(9);
    expect(level.levels[1]).toEqual(4);

    // Если пользователь внес 500$ но его команда внесла 0
    // то выводить ему статус за 500$ (newbie) и следющий уровень
    // со статусом за 500$ (partner)
    referral.getUserAmount.mockReturnValue(Promise.resolve(500));
    referral.getTeamAmount.mockReturnValue(Promise.resolve(0));
    referral.getReferralCount.mockReturnValue(Promise.resolve(5));
    level = await referral.getReferralLevel(null, levels);
    expect(level.levels[0]).toEqual(5);
  });

  test('createTransaction', async () => {
    referral.findUser = jest.fn();

    referral.findUser.mockReturnValue(Promise.resolve(null));
    expect(await referral.findReferral(1)).toEqual({
      level: 0,
      owner_id: null,
      referral_id: null,
      parents: null
    });

    referral.findUser.mockReturnValue(Promise.resolve({
      id: 1
    }));
    expect(await referral.findReferral(1)).toEqual({
      level: 1,
      owner_id: 1,
      referral_id: 1,
      parents: [1]
    });
  });
});
