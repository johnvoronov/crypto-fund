const coinpayments = require('coinpayments');
jest.mock('coinpayments');
const models = require('../../models');
const settings = require('../../util/settings');
const walletService = require('../../service/wallet');

describe('service/wallet', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });

    await settings.setValue('coinpayments_public_key', '1234');
    await settings.setValue('coinpayments_secret_key', '1234');
    await settings.setValue('coinpayments_ipn_secret', '1234');
    await settings.setValue('coinpayments_merchant_id', '1234');

    coinpayments.mockImplementation(() => ({
      getCallbackAddress: jest.fn(() => ({
        address: 'test'
      }))
    }));
  });

  afterEach(async () => {
    jest.resetAllMocks();
  });

  afterAll(async () => {
    jest.clearAllMocks();
  });

  test('createNewWallet', async () => {
    expect(await walletService.createNewWallet({ code: 'test' })).toEqual({
      address: 'test'
    });
  });

  test('getWallet', async () => {
    const coinpayments = {
      publicKey: 123,
      secretKey: 123,
      merchantId: 123,
      ipnSecret: 123
    };

    const user = await models.User.create({
      email: 'user@user.com'
    });
    const currency = await models.Currency.create({
      to_usd: 1,
      code: 'TST',
      name: 'Test'
    });
    const strategy = await models.Strategy.create({
      name: 'test',
      currency_id: currency.id,
      convert_id: currency.id,
      coinpayments
    });
    const wallet = await models.Wallet.create({
      strategy_id: strategy.id,
      user_id: user.id,
      merchant_id: coinpayments.merchantId,
      address: 'test'
    });
    const coinpaymentsWallet = await walletService.getWallet(user, strategy, currency);
    expect(coinpaymentsWallet.address).toEqual(wallet.address);
    expect(await models.Wallet.count()).toEqual(1);

    const newCurrency = await models.Currency.create({
      to_eth: 1,
      to_usd: 1,
      code: 'Foo',
      name: 'Bar'
    });
    const newStrategy = await models.Strategy.create({
      name: 'test123',
      currency_id: newCurrency.id,
      convert_id: currency.id,
      coinpayments
    });
    const coinpaymentsNewWallet = await walletService.getWallet(user, newStrategy, newCurrency);
    expect(coinpaymentsNewWallet.address).toEqual(wallet.address);
    expect(await models.Wallet.count()).toEqual(2);
  });

  test('getInternalWallet', async () => {
    const user = await models.User.create({
      email: 'user@user.com'
    });
    const currency = await models.Currency.create({
      to_usd: 1,
      code: 'TST',
      name: 'Test'
    });
    const wallet = await models.Wallet.create({
      is_internal: true,
      user_id: user.id,
      currency_id: currency.id,
      merchant_id: '1234',
      address: 'test'
    });
    const coinpaymentsWallet = await walletService.getInternalWallet(user, currency);
    expect(coinpaymentsWallet.address).toEqual(wallet.address);
    expect(await models.Wallet.count()).toEqual(1);

    const newCurrency = await models.Currency.create({
      to_eth: 1,
      to_usd: 1,
      code: 'Foo',
      name: 'Bar'
    });
    const coinpaymentsNewWallet = await walletService.getInternalWallet(user, newCurrency);
    expect(coinpaymentsNewWallet.address).toEqual(wallet.address);
    expect(await models.Wallet.count()).toEqual(2);
  });
});
