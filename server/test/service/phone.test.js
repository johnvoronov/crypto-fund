const phoneService = require('../../service/phone');

describe('service/phone', () => {
  test('clean', async () => {
    expect(phoneService.clean('900-526-77-77')).toEqual('9005267777');
    expect(phoneService.clean('+7-900-526-77-77')).toEqual('+79005267777');
    expect(phoneService.clean('+7 900-526 77-77')).toEqual('+79005267777');
  });

  test('validate', async () => {
    expect(phoneService.validate('900-526-77-77')).toEqual('+79005267777');
    expect(phoneService.validate('+7-900-526-77-77')).toEqual('+79005267777');
    expect(phoneService.validate('+7 900-526 77-77')).toEqual('+79005267777');
  });
});
