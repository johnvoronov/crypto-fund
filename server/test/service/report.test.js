const models = require('../../models');
const report = require('../../service/report');
const checkpoint = require('../../service/checkpoint');
const consts = require('../../consts');

describe('service/report', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  test('getMonthReport / getYearReport', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '123456',
      is_active: true
    });
    const currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 100,
      to_btc: 1
    });
    const strategy = await models.Strategy.create({
      name: 'test',
      currency_id: currency.id,
      convert_id: currency.id
    });
    await models.Transaction.create({
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      amount: 150,
      strategy_id: strategy.id,
      usd: 150 * currency.to_usd,
      user_id: user.id,
      status: consts.TRANSACTION_PAYMENT
    });
    await models.Transaction.create({
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      amount: 100,
      strategy_id: strategy.id,
      usd: 100 * currency.to_usd,
      user_id: user.id,
      is_deposit: true,
      status: consts.TRANSACTION_DEPOSIT
    });
    await models.Transaction.create({
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      amount: 20,
      usd: 20 * currency.to_usd,
      strategy_id: strategy.id,
      user_id: user.id,
      is_deposit: true,
      is_freeze: true,
      status: consts.TRANSACTION_INCOME
    });
    await checkpoint.checkpoint(strategy.id, 10, new Date);

    const monthReport = await report.getMonthReport(user);
    expect(monthReport).toEqual(expect.any(Array));
    const yearReport = await report.getYearReport(user);
    expect(yearReport).toEqual(expect.any(Array));
    expect(yearReport.length).toEqual(12);
  });
});
