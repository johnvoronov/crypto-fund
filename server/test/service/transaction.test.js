const coinpayments = require('coinpayments');
jest.mock('coinpayments');
const models = require('../../models');
const balanceService = require('../../service/balance');
const paymentService = require('../../service/payment');
const transactionService = require('../../service/transaction');
const referralService = require('../../service/referral');
const referralHelper = require('../../service/referralHelper');
const consts = require('../../consts');

describe('service/transaction', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });

    const level = {
      name: 'newbie',
      team: 5,
      user: 5,
      levels: [5],
      single: [10]
    };
    referralService.getGrid = jest.fn(() => Promise.resolve([level]));
    referralService.getCurrentLevel = jest.fn(() => Promise.resolve(level));
    referralHelper.getCurrentLevel = jest.fn(() => Promise.resolve(level));

    coinpayments.mockImplementation(() => ({
      getCallbackAddress: jest.fn(() => ({
        address: 'test'
      }))
    }));
  });

  afterEach(async () => {
    jest.resetAllMocks();
  });

  test('createReferral', async () => {
    const currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 10
    });
    const strategy = await models.Strategy.create({
      currency_id: currency.id,
      convert_id: currency.id,
      name: 'test'
    });

    const user = await models.User.create({
      email: 'user@example.com'
    });
    const child = await models.User.create({
      email: 'child@example.com',
      owner_id: user.id,
      referral_id: user.id,
      parents: [user.id]
    });

    await models.Payment.create({
      amount: 10,
      status: consts.PAYMENT_COMPLETE,
      user_id: user.id,
      to_usd: 10
    });
    await models.Payment.create({
      amount: 10,
      status: consts.PAYMENT_COMPLETE,
      user_id: child.id,
      to_usd: 10
    });

    const data = await transactionService.createReferral(100, strategy, child);

    expect(data.decreaseAmount).toEqual(10);
    expect(data.transactions.length).toEqual(1);
  });

  test('createPayment / createDeposit', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      is_active: true
    });
    const currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 100,
      to_btc: 1
    });
    const strategy = await models.Strategy.create({
      name: 'test',
      currency_id: currency.id,
      convert_id: currency.id
    });

    const payment = await paymentService.createPayment(user, strategy, currency, 100);
    const tx = await transactionService.createPayment(payment);
    expect(await models.Transaction.count()).toEqual(1);

    expect(await balanceService.getUserBalance({ user_id: user.id })).toEqual(100);

    expect(await transactionService.createDeposit(tx));
    expect(await balanceService.getUserBalance({ user_id: user.id })).toEqual(0);
    expect(await balanceService.getDepositBalance({ user_id: user.id })).toEqual(100);
  });

  test('one time referral payment', async () => {
    const parent = await models.User.create({
      email: 'parent@example.com',
      is_active: true
    });
    const child = await models.User.create({
      email: 'child@example.com',
      is_active: true,
      referral_id: parent.id,
      owner_id: parent.id,
      parents: [parent.id],
      level: 1
    });
    const currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 100,
      to_btc: 1
    });
    const strategy = await models.Strategy.create({
      name: 'test',
      currency_id: currency.id,
      convert_id: currency.id
    });

    const payment = await paymentService.createPayment(child, strategy, currency, 100);
    const tx = await transactionService.createPayment(payment);
    expect(await models.Transaction.count()).toEqual(2);
  });
});
