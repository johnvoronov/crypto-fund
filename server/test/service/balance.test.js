const models = require('../../models');
const balance = require('../../service/balance');
const transactionService = require('../../service/transaction');
const paymentService = require('../../service/payment');

describe('service/balance', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  test('userBalance / depositBalance', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '123456',
      is_active: true
    });
    const currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 100
    });
    const strategy = await models.Strategy.create({
      name: 'test',
      currency_id: currency.id,
      convert_id: currency.id
    });

    const payment = await paymentService.createPayment(user, strategy, currency, 150);
    const tx = await transactionService.createPayment(payment);

    const where = { user_id: user.id };
    expect(await balance.getUserBalance(where)).toEqual(150);
    expect(await balance.getDepositBalance(where)).toEqual(0);

    await transactionService.createDeposit(tx);

    expect(await balance.getUserBalance(where)).toEqual(0);
    expect(await balance.getDepositBalance(where)).toEqual(150);

    // await tx.update({
    //   is_freeze: false
    // });
    // expect(await balance.getDepositBalance(where)).toEqual(100);
    // expect(await balance.getActiveDepositBalance(where)).toEqual(100);
    // expect(await balance.getInactiveDepositBalance(where)).toEqual(0);
  });
});
