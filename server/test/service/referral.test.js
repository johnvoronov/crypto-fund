const referralService = require('../../service/referral');
const models = require('../../models');
const consts = require('../../consts');

describe('service/referral', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  test('getUserAmount', async () => {
    const user = await models.User.create({
      email: 'user@user.com'
    });
    const team = await models.User.create({
      email: 'user1@user.com',
      password: '',
      owner_id: user.id,
      parents: [user.id]
    });
    const currency = await models.Currency.create({
      code: 'ETH',
      name: 'Ethereum',
      to_usd: 10,
      to_btc: 0
    });
    await models.Transaction.create({
      user_id: user.id,
      amount: 100,
      account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
      usd: 100 * currency.to_usd,
      status: consts.TRANSACTION_FREEZE,
      created_at: new Date,
      updated_at: new Date
    });
    await models.Transaction.create({
      user_id: team.id,
      amount: 100,
      account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
      usd: 100 * currency.to_usd,
      status: consts.TRANSACTION_FREEZE,
      created_at: new Date,
      updated_at: new Date
    });
    await models.Transaction.create({
      user_id: team.id,
      amount: 100,
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      usd: 100 * currency.to_usd,
      status: consts.TRANSACTION_FREEZE,
      created_at: new Date,
      updated_at: new Date
    });

    // Рефералы у user инвестировали сумму в размере 1000$
    // Когда сам user имеет сумму инвестиций 0
    expect(await referralService.getUserAmount(user)).toEqual(100);
    expect(await referralService.getTeamAmount(user)).toEqual(200);
    expect(await referralService.getReferralCount(user)).toEqual(1);

    // Рефералы у user инвестировали сумму в размере 1000$
    // Когда проверка сборов конкретного реферала составляет 0
    // так как его рефы ничего не инвестировали или их не существует
    expect(await referralService.getUserAmount(team)).toEqual(200);
    expect(await referralService.getTeamAmount(team)).toEqual(0);
    expect(await referralService.getReferralCount(team)).toEqual(0);
  });

  test('updateUsers', async () => {
    const level1 = await models.Referral.create({
      name_en: 'no status',
      name_ru: 'no status',
      team: 0,
      user: 0,
      price: 0,
      user_count: 0,
      levels: []
    });
    const level2 = await models.Referral.create({
      name_en: 'newbie',
      name_ru: 'newbie',
      team: 100,
      user: 100,
      price: 100,
      user_count: 1,
      levels: [5]
    });

    const user = await models.User.create({
      email: 'user@user.com',
      minimal_referral_level_id: level2.id
    });
    await referralService.updateUser(user);
    await user.reload();
    expect(user.referral_level_id).toEqual(level2.id);

    for (let i = 0; i < 10; i++) {
      const testUser = await models.User.create({
        login: `user${i}`,
        email: `user${i}@example.com`
      });
      await referralService.updateUser(testUser);
      await testUser.reload();
      expect(testUser.referral_level_id).toEqual(level1.id);
    }
  });

  test('updateUser', async () => {
    const level1 = await models.Referral.create({
      name_en: 'first',
      name_ru: 'first',
      team: 0,
      user: 0,
      price: 0,
      user_count: 0,
      levels: []
    });
    const level2 = await models.Referral.create({
      name_en: 'middle',
      name_ru: 'middle',
      team: 0,
      user: 100,
      price: 100,
      user_count: 0,
      levels: [5]
    });
    const level3 = await models.Referral.create({
      name_en: 'last',
      name_ru: 'last',
      team: 0,
      user: 200,
      price: 200,
      user_count: 0,
      levels: [5]
    });
    const user = await models.User.create({ email: 'user@user.com' });

    await user.update({ minimal_referral_level_id: level3.id, buy_referral_level_id: level2.id });
    await referralService.updateUser(user);
    await user.reload();
    expect(user.referral_level_id).toEqual(level3.id);

    await user.update({ minimal_referral_level_id: null, buy_referral_level_id: level2.id });
    await referralService.updateUser(user);
    await user.reload();
    expect(user.referral_level_id).toEqual(level2.id);

    await user.update({
      referral_level_id: null,
      minimal_referral_level_id: null,
      buy_referral_level_id: null
    });
    const currency = await models.Currency.create({
      code: 'ETH',
      name: 'Ethereum',
      to_usd: 10,
      to_btc: 0
    });
    await models.Transaction.create({
      user_id: user.id,
      amount: 100,
      account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
      usd: 100 * currency.to_usd,
      status: consts.TRANSACTION_FREEZE,
      created_at: new Date,
      updated_at: new Date
    });
    await referralService.updateUser(user);
    await user.reload();
    expect(user.referral_level_id).toEqual(level2.id);
  });

  test('updateReferralLevel', async () => {
    const user = await models.User.create({
      email: 'user@user.com'
    });
    const team = await models.User.create({
      email: 'user1@user.com',
      password: '',
      owner_id: user.id,
      parents: [user.id]
    });
    const currency = await models.Currency.create({
      code: 'ETH',
      name: 'Ethereum',
      to_usd: 10,
      to_btc: 0
    });
    await models.Transaction.create({
      user_id: user.id,
      amount: 100,
      account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
      usd: 100 * currency.to_usd,
      status: consts.TRANSACTION_FREEZE,
      created_at: new Date,
      updated_at: new Date
    });
    await models.Transaction.create({
      user_id: team.id,
      amount: 100,
      account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
      usd: 100 * currency.to_usd,
      status: consts.TRANSACTION_FREEZE,
      created_at: new Date,
      updated_at: new Date
    });

    const level1 = await models.Referral.create({
      name_en: 'no status',
      name_ru: 'no status',
      team: 0,
      user: 0,
      price: 0,
      user_count: 0,
      levels: []
    });
    const level2 = await models.Referral.create({
      name_en: 'newbie',
      name_ru: 'newbie',
      team: 100,
      user: 100,
      price: 100,
      user_count: 1,
      levels: [5]
    });

    await referralService.updateReferralLevel(user);
    await user.reload();
    expect(user.referral_level_id).toEqual(2);
  });

  /**
   * Обновить реф. уровень пользователя при создании, удалении, редактировании уровня
   */
  test('createUpdateDeleteReferralLevel', async () => {
    const user = await models.User.create({
      email: 'user@user.com'
    });
    const team = await models.User.create({
      email: 'user1@user.com',
      password: '',
      owner_id: user.id,
      parents: [user.id]
    });
    const currency = await models.Currency.create({
      code: 'ETH',
      name: 'Ethereum',
      to_usd: 10,
      to_btc: 0
    });
    await models.Transaction.create({
      user_id: user.id,
      amount: 100,
      account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
      usd: 100 * currency.to_usd,
      status: consts.TRANSACTION_FREEZE,
      created_at: new Date,
      updated_at: new Date
    });
    await models.Transaction.create({
      user_id: team.id,
      amount: 100,
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      usd: 100 * currency.to_usd,
      status: consts.TRANSACTION_FREEZE,
      created_at: new Date,
      updated_at: new Date
    });

    const level1 = await models.Referral.create({
      name_en: 'no status',
      name_ru: 'no status',
      team: 0,
      user: 0,
      price: 0,
      user_count: 0,
      levels: []
    });
    const level2 = await models.Referral.create({
      name_en: 'newbie',
      name_ru: 'newbie',
      team: 100,
      user: 100,
      price: 100,
      user_count: 1,
      levels: [5]
    });

    // Обновить уровень пользователей
    await referralService.updateUsers();
    await user.reload();
    // Уровень (referral_level_id) изменится на 2, изначально null
    expect(user.referral_level_id).toEqual(level2.id);

    /**
     * УДАЛИТЬ teammate реферальный уровень
     */
    await level2.destroy();

    // Обновить уровень пользователей
    await referralService.updateUsers();
    await user.reload();
    // Уровень (referral_level_id) изменится на 1
    expect(user.referral_level_id).toEqual(level1.id);
  });

  /**
   * Создать платеж
   */
  test('createPayment', async () => {
    const user = await models.User.create({
      email: 'user@user.com'
    });
    const team = await models.User.create({
      email: 'user1@user.com',
      password: '',
      owner_id: user.id,
      parents: [user.id]
    });
    const currency = await models.Currency.create({
      code: 'ETH',
      name: 'Ethereum',
      to_usd: 10,
      to_btc: 0
    });
    await models.Transaction.create({
      user_id: user.id,
      amount: 100,
      account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
      usd: 100 * currency.to_usd,
      status: consts.TRANSACTION_FREEZE,
      created_at: new Date,
      updated_at: new Date
    });
    await models.Transaction.create({
      user_id: team.id,
      amount: 100,
      account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
      usd: 100 * currency.to_usd,
      status: consts.TRANSACTION_FREEZE,
      created_at: new Date,
      updated_at: new Date
    });

    const level1 = await models.Referral.create({
      name_en: 'no status',
      name_ru: 'no status',
      team: 0,
      user: 0,
      price: 0,
      user_count: 0,
      levels: []
    });
    const level2 = await models.Referral.create({
      name_en: 'newbie',
      name_ru: 'newbie',
      team: 0,
      user: 100,
      price: 100,
      user_count: 1,
      levels: [5]
    });

    // Обновляем реф. уровень пользователя до текущего, так как сейчас он является null
    await user.update({ referral_level_id: level1.id });

    // Обновляем реф. уровень пользователя
    await referralService.updateReferralLevel(user);
    await user.reload();
    // Уровень (referral_level_id) равен 2
    expect(user.referral_level_id).toEqual(level2.id);
  });

  test('setMinimalLevel', async () => {
    const user = await models.User.create({
      email: 'user@user.com'
    });
    const team = await models.User.create({
      email: 'user1@user.com',
      password: '',
      owner_id: user.id,
      parents: [user.id]
    });
    const currency = await models.Currency.create({
      code: 'ETH',
      name: 'Ethereum',
      to_usd: 10,
      to_btc: 0
    });
    await models.Transaction.create({
      user_id: user.id,
      amount: 100,
      account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
      usd: 100 * currency.to_usd,
      status: consts.TRANSACTION_FREEZE,
      created_at: new Date,
      updated_at: new Date
    });
    await models.Transaction.create({
      user_id: team.id,
      amount: 100,
      account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
      usd: 100 * currency.to_usd,
      status: consts.TRANSACTION_FREEZE,
      created_at: new Date,
      updated_at: new Date
    });

    await models.Transaction.destroy({ truncate: true, cascade: true });
    const level1 = await models.Referral.create({
      name_en: 'no status',
      name_ru: 'no status',
      team: 0,
      user: 0,
      price: 0,
      user_count: 0,
      levels: []
    });
    const level2 = await models.Referral.create({
      name_en: 'newbie',
      name_ru: 'newbie',
      team: 100,
      user: 100,
      price: 100,
      user_count: 1,
      levels: [5]
    });

    expect(user.referral_level_id).toBeFalsy();

    await referralService.setMinimalLevel(user, level1);
    await user.reload();
    expect(user.referral_level_id).toEqual(level1.id);
    expect(user.minimal_referral_level_id).toEqual(level1.id);

    await referralService.setMinimalLevel(user, level2);
    await user.reload();
    expect(user.referral_level_id).toEqual(level2.id);
    expect(user.minimal_referral_level_id).toEqual(level2.id);

    await referralService.setMinimalLevel(user, level1);
    await user.reload();
    expect(user.referral_level_id).toEqual(level1.id);
    expect(user.minimal_referral_level_id).toEqual(level1.id);
  });

  test('buyStatus', async () => {
    const user = await models.User.create({
      email: 'user@user.com'
    });

    await models.Referral.create({
      name_en: 'no status',
      name_ru: 'no status',
      team: 0,
      user: 0,
      price: 0,
      user_count: 0,
      levels: []
    });
    await models.Referral.create({
      name_en: 'newbie',
      name_ru: 'newbie',
      team: 100,
      user: 100,
      price: 100,
      user_count: 1,
      levels: [5]
    });

    let referral;

    referral = await referralService.buyStatus(user);
    expect(referral).toEqual(null);

    const btc = await models.Currency.create({
      code: 'BTC',
      name: 'Bitcoin',
      to_usd: 1,
      to_btc: 1,
      to_eth: 1,
      to_rub: 1
    });

    await models.Transaction.create({
      user_id: user.id,
      account_type: consts.ACCOUNT_TYPE_STATUS,
      status: consts.TRANSACTION_PAY_STATUS,
      amount: 99,
      usd: 99 * btc.to_usd,
      eth: 99 * btc.to_eth,
      rub: 99 * btc.to_rub,
      btc: 99 * btc.to_btc,
      to_usd: btc.to_usd,
      to_eth: btc.to_eth,
      to_rub: btc.to_rub,
      to_btc: btc.to_btc
    });
    referral = await referralService.buyStatus(user);
    expect(referral).toEqual(null);

    await models.Transaction.create({
      user_id: user.id,
      account_type: consts.ACCOUNT_TYPE_STATUS,
      status: consts.TRANSACTION_PAY_STATUS,
      amount: 1,
      usd: 1 * btc.to_usd,
      eth: 1 * btc.to_eth,
      rub: 1 * btc.to_rub,
      btc: 1 * btc.to_btc,
      to_usd: 1 * btc.to_usd,
      to_eth: 1 * btc.to_eth,
      to_rub: 1 * btc.to_rub,
      to_btc: 1 * btc.to_btc
    });
    referral = await referralService.buyStatus(user);
    expect(parseFloat(referral.price)).toEqual(100);

    referral = await referralService.buyStatus(user);
    expect(referral).toEqual(null);
  });

  /*
  TODO https://blockchaincorp.atlassian.net/browse/CL-259
  test('moveToNewParent', async () => {
    const parent = await models.User.create({
      email: 'parent@parent.com'
    });
    const level1 = await models.User.create({
      owner_id: parent.id,
      parents: [parent.id],
      level: 1,
      referral_id: parent.id
    });
    const level2 = await models.User.create({
      owner_id: parent.id,
      parents: [parent.id, level1.id],
      level: 2,
      referral_id: level1.id
    });
    expect(await models.User.count()).toEqual(3);

    expect((await referral.findAllChildren(parent)).length).toEqual(2);
    expect((await referral.findAllChildren(level1)).length).toEqual(1);
    expect((await referral.findAllChildren(level2)).length).toEqual(0);

    await referral.moveToNewParent(parent, level2);
    expect((await referral.findAllChildren(parent)).length).toEqual(2);
    expect((await referral.findAllChildren(level1)).length).toEqual(0);
    expect((await referral.findAllChildren(level2)).length).toEqual(0);

    await referral.moveToNewParent(level2, parent);
    expect((await referral.findAllChildren(level2)).length).toEqual(2);
    expect((await referral.findAllChildren(level1)).length).toEqual(0);
    expect((await referral.findAllChildren(parent)).length).toEqual(0);
  });
  */

  test('getStats', async () => {
    const user = await models.User.create({
      email: 'user@user.com'
    });
    const team = await models.User.create({
      email: 'user1@user.com',
      password: '',
      owner_id: user.id,
      parents: [user.id]
    });
    const currency = await models.Currency.create({
      code: 'ETH',
      name: 'Ethereum',
      to_usd: 10,
      to_btc: 0
    });
    await models.Transaction.create({
      user_id: user.id,
      amount: 100,
      account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
      usd: 100 * currency.to_usd,
      status: consts.TRANSACTION_FREEZE,
      created_at: new Date,
      updated_at: new Date
    });
    await models.Transaction.create({
      user_id: team.id,
      amount: 100,
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      usd: 100 * currency.to_usd,
      status: consts.TRANSACTION_FREEZE,
      created_at: new Date,
      updated_at: new Date
    });

    const start = await models.Referral.create({
      name_en: 'no status',
      name_ru: 'no status',
      team: 0,
      user: 0,
      price: 0,
      user_count: 0,
      levels: []
    });
    const next = await models.Referral.create({
      name_en: 'newbie',
      name_ru: 'newbie',
      team: 100,
      user: 100,
      price: 100,
      user_count: 1,
      levels: [5]
    });
    await referralService.updateUsers(user);
    await user.reload();
    expect(user.referral_level_id).toEqual(next.id);

    expect(await referralService.getStats(user)).toEqual({
      raised: {
        team: 100,
        user: 100,
        user_count: 1
      },
      current: next.toJSON(),
      next: next.toJSON()
    });
  });

  test('findExpiredStatuses', async () => {
    const user = await models.User.create({
      email: 'user@user.com'
    });
    const team = await models.User.create({
      email: 'user1@user.com',
      password: '',
      owner_id: user.id,
      parents: [user.id]
    });
    const currency = await models.Currency.create({
      code: 'ETH',
      name: 'Ethereum',
      to_usd: 10,
      to_btc: 0
    });
    await models.Transaction.create({
      user_id: user.id,
      amount: 100,
      account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
      usd: 100 * currency.to_usd,
      status: consts.TRANSACTION_FREEZE,
      created_at: new Date,
      updated_at: new Date
    });
    await models.Transaction.create({
      user_id: team.id,
      amount: 100,
      account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
      usd: 100 * currency.to_usd,
      status: consts.TRANSACTION_FREEZE,
      created_at: new Date,
      updated_at: new Date
    });
    await models.Currency.create({
      code: 'BTC',
      name: 'Bitcoin',
      to_usd: 1,
      to_btc: 1,
      to_eth: 1,
      to_rub: 1
    });
    const level1 = await models.Referral.create({
      name_en: 'no status',
      name_ru: 'no status',
      team: 0,
      user: 0,
      price: 10,
      user_count: 0,
      levels: []
    });

    const start = new Date();
    start.setYear(start.getYear() - 2);

    await models.UserReferral.create({
      user_id: user.id,
      start_at: start,
      referral_id: level1.id
    });

    const getAmount = async () => await models.Transaction.sum('amount', {
      where: {
        user_id: user.id,
        account_type: consts.ACCOUNT_TYPE_STATUS
      }
    }) || 0;

    expect(await getAmount()).toEqual(0);
    await referralService.findExpiredStatuses();
    expect(await getAmount()).toEqual(-10);
  });
});
