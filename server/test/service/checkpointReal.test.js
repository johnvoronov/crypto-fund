const models = require('../../models');
const consts = require('../../consts');
const { Op } = require('sequelize');
const checkpointService = require('../../service/checkpoint');
const { debugTx } = require('../utils');

const findCheckpoint = async (checkpoint_id, strategy_id, user_id) => await models.UserCheckpoint.findOne({
  where: { checkpoint_id, strategy_id, user_id }
});

const findAmount = async (ids, strategy_id, user_id) => await models.Transaction.sum('amount', {
  where: {
    account_type: consts.ACCOUNT_TYPE_STRATEGY,
    strategy_id,
    user_id,
    [Op.or]: [
      { checkpoint_id: { [Op.in]: ids } },
      { payment_id: { [Op.not]: null } }
    ]
  }
});

describe('service/checkpoint real case', () => {
  let currency;
  let user;
  let strategy;

  beforeEach(async () => {
    await models.sequelize.sync({ force: true });

    currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 1,
      to_btc: 1,
      to_eth: 1,
      to_rub: 1
    });
    strategy = await models.Strategy.create({
      name: 'test',
      currency_id: currency.id,
      convert_id: currency.id,
      fee: 0,
      limit: [{ limit: 50.01, percent: 20 }, { limit: 10.01, percent: 30 }, { limit: 1.01, percent: 40 }]
    });
    user = await models.User.create({
      email: 'user@example.com'
    });
  });

  test('-50% +10% +40% +80%', async () => {
    /*
    Платеж:
    {
      "user_id": 8,
      "amount": 1.000000000000000000,
    }
     */
    await debugTx(user, strategy, currency, 1, new Date('2018-11-11 23:59:59.999000 +00:00'));
    expect(await models.Transaction.sum('amount', {
      where: {
        is_deposit: true,
        is_freeze: false,
        user_id: user.id,
        strategy_id: strategy.id
      }
    })).toBe(1);
    expect(await models.Transaction.count()).toBe(1);
    expect(await models.Transaction.sum('amount', {
      where: { strategy_id: strategy.id, user_id: user.id }
    })).toBe(1);

    // =============================== Чекпоинт 1
    const checkpoint1 = await checkpointService.checkpoint(strategy.id, -50, new Date('2018-11-11 23:59:59.999999 +00:00'));
    const checkpoint2 = await checkpointService.checkpoint(strategy.id, 10, new Date('2018-11-12 23:59:59.999999 +00:00'));
    const checkpoint3 = await checkpointService.checkpoint(strategy.id, 40, new Date('2018-11-13 23:59:59.999999 +00:00'));
    const checkpoint4 = await checkpointService.checkpoint(strategy.id, 80, new Date('2018-11-14 23:59:59.999999 +00:00'));

    // =============================== Старт отсечки
    expect(await models.Checkpoint.count({
      where: { is_done: false }
    })).toEqual(4);
    await checkpointService.completeIteration(strategy.id);
    expect(await models.Checkpoint.count({
      where: { is_done: true }
    })).toEqual(4);

    // Проверка пользовательского чекпоинта = 1
    /*
    ====================== 1 чекпоинт -50%
    { isProfit: true, // статус доходности / убыточности тут не играет никакой роли, так как это первый чекпоинт пользователя
      currentUserBalance: 1, // текущий базовый баланс 1 BTC
      incomingAmount: 0.5, // Новый входящий баланс 0.5 BTC, действие плюс или минус зависит от доходности или убыточности за исключением первого платежа
      newUserBalance: 0.5, // Новый баланс пользователя по состоянию на чекпоинт
      baseBalance: 0 } // базовый баланс еще не определен, чекпоинт первый, поэтому isProfit = true, его не берем в алгоритме в расчет
    completeLossCheckpoint // зачисляем по убыточному алгоритму

    Транзакции:
    {
      "user_id": 8,
      "amount": -0.500000000000000000,
      "comment": "Фонд сработал в минус = -0.5",
    }
     */
    expect(await findCheckpoint(checkpoint1.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint1.id,
      before: "1.000000000000000000",
      after: "0.500000000000000000",
      diff: "-0.500000000000000000"
    }));
    expect(await findAmount([checkpoint1.id], strategy.id, user.id)).toBe(0.5);

    // Проверка пользовательского чекпоинта = 2
    /*
    ====================== 2 чекпоинт +10%
    { isProfit: false, // пользователь восстанавливает убытки из 1 чекпоинта
      currentUserBalance: 0.5, // Текущий баланс пользователя на момент чекпоинта
      incomingAmount: 0.05, // Входящий баланс
      newUserBalance: 0.55, // Новый баланс пользователя
      baseBalance: '1.000000000000000000' } // Базовый баланс
    completeNonProfitCheckpoint // Зачисляем по алгоритму восстановления убытков пользователя

    Транзакции:
    {
      "user_id": 8,
      "amount": 0.050000000000000000,
      "comment": "Возврат средств после убыточности = 0.05"
    }
     */
    expect(await findCheckpoint(checkpoint2.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint2.id,
      before: "0.500000000000000000",
      after: "0.550000000000000000",
      diff: "0.050000000000000000"
    }));
    expect(await findAmount([checkpoint1.id, checkpoint2.id], strategy.id, user.id)).toBe(0.55);

    // Проверка пользовательского чекпоинта = 3
    /*
    ====================== 3 чекпоинт +40%
    { isProfit: false, // пользователь все еще восстанавливает убытки из 1 чекпоинта
      currentUserBalance: 0.55, // текущий баланс пользователя
      incomingAmount: 0.22, // Новое пополнение на 40% = 0.22 от 0.55
      newUserBalance: 0.77, // Новый баланс пользователя после расчета
      baseBalance: '1.000000000000000000' } // Базовый баланс все еще 1 BTC
    completeNonProfitCheckpoint // Расчитываем по алгоритму возмещения убытков

    Транзакции:
    {
      "user_id": 8,
      "amount": 0.220000000000000000,
      "comment": "Возврат средств после убыточности = 0.22",
    }
     */
    expect(await findCheckpoint(checkpoint3.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint3.id,
      before: "0.550000000000000000",
      after: "0.770000000000000000",
      diff: "0.220000000000000000"
    }));
    expect(await findAmount([checkpoint1.id, checkpoint2.id, checkpoint3.id], strategy.id, user.id)).toBe(0.77);

    // Проверка пользовательского чекпоинта = 4
    /*
    ====================== 4 чекпоинт +80%
    { isProfit: true, // Уже получаем доход
      currentUserBalance: 0.77, // Текущий баланс пользователя
      incomingAmount: 0.616, // Входящая сумма 80% от 0,77 = 0,616
      newUserBalance: 1.386, // Баланс пользователя после расчетом = 1,078
      baseBalance: '1.000000000000000000' } // Базовый баланс все еще 1 BTC
    completeProfitAndNonProfitCheckpoint // Расчитываем по смешанному алгоритму возмещения убыточности и начисления доходности от разницы в балансах. А именно: 1.078 - базовый баланс в 1 BTC = 0.078 BTC доходности.

    Транзакции:
    {
      "user_id": 8,
      "amount": 0.230000000000000000,
      "comment": "Возврат средств после убыточности = 0.23"
    }

    С 0.78 считаем доходность:
    Тот же пользователь:
    {
      "user_id": 8,
      "amount": 0.039000000000000000,
      "comment": "Доход c инвестиций в стратегию. userAmount = 0.039, userPercent = 50, strategyFee = 0",
      "fund_amount": 0.039000000000000000
    }

    Выше стоящие рефы и фонд:
    {
      "user_id": 7,
      "amount": 0.007800000000000000,
      "comment": "Реф. уровень = 3, Процент = 20",
      "owner_id": 8,
      "referral_percent": 20,
      "fund_amount": 0.039000000000000000
    },
    {
      "user_id": 1,
      "amount": 0.002730000000000000,
      "comment": "Реф. уровень = 4, Процент = 7",
      "owner_id": 8,
      "referral_percent": 7,
      "fund_amount": 0.039000000000000000
    },
    {
      "user_id": null,
      "amount": 0.028470000000000000,
      "comment": "Доход фонда",
    }
     */
    expect(await findCheckpoint(checkpoint4.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint4.id,
      before: "0.770000000000000000",
      after: "1.193000000000000000",
      diff: "0.423000000000000000"
    }));
    expect(await findAmount([checkpoint1.id, checkpoint2.id, checkpoint3.id, checkpoint4.id], strategy.id, user.id)).toBe(1.193);
  });

  test('+50% -50%', async () => {
    // Платеж: { "user_id": 8, "amount": 1.000000000000000000 }
    await debugTx(user, strategy, currency, 1, new Date('2018-11-11 23:59:59.999000 +00:00'));
    expect(await models.Transaction.sum('amount', {
      where: {
        is_deposit: true,
        is_freeze: false,
        user_id: user.id,
        strategy_id: strategy.id
      }
    })).toBe(1);
    expect(await models.Transaction.count()).toBe(1);
    expect(await models.Transaction.sum('amount', {
      where: { strategy_id: strategy.id, user_id: user.id }
    })).toBe(1);

    // =============================== Чекпоинт 1 = 1.25 BTC
    const checkpoint1 = await checkpointService.checkpoint(strategy.id, 50, new Date('2018-11-11 23:59:59.999999 +00:00'));

    // =============================== Чекпоинт 2 = 0.625 BTC
    const checkpoint2 = await checkpointService.checkpoint(strategy.id, -50, new Date('2018-11-12 23:59:59.999999 +00:00'));

    // =============================== Старт отсечки
    expect(await models.Checkpoint.count({
      where: { is_done: false }
    })).toEqual(2);
    await checkpointService.completeIteration(strategy.id);
    expect(await models.Checkpoint.count({
      where: { is_done: true }
    })).toEqual(2);

    /*
    ====================== 1 чекпоинт +50%
    { isProfit: true, // Фиксируем доходность
      percent: 50, // +50% от 1 BTC = 0.5 BTC
      currentUserBalance: 1, // Текущий баланс 1 BTC
      incomingAmount: 0.5, // Доход = 0.5 BTC
      newUserBalance: 1.5, // Новый баланс = 1.5 BTC
      baseBalance: 1 } // Базовый баланс 1 BTC
    completeProfitAndNonProfitCheckpoint
     */
    expect(await findCheckpoint(checkpoint1.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      base: "1.000000000000000000",
      before: "1.000000000000000000",
      after: "1.250000000000000000",
      diff: "0.250000000000000000",
      percent: 50
    }));
    expect(await findAmount([checkpoint1.id], strategy.id, user.id)).toBe(1.25);

    /*
    ======================  2 чекпоинт -50%
    { isProfit: false, // Убыточный чекпоинт
      percent: -50, // Убыточность в 50%
      currentUserBalance: 1.25, // Текущий баланс = 1.25 BTC
      incomingAmount: 0.625, // Сумма к вычету 50% от 1.25 BTC = 0.625 BTC
      newUserBalance: 0.625, // Новый баланс = 0.625 BTC
      baseBalance: 1 } // Базовый баланс 1 BTC
    completeLossCheckpoint
    */
    expect(await findCheckpoint(checkpoint2.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint2.id,
      before: "1.250000000000000000",
      after: "0.625000000000000000",
      diff: "-0.625000000000000000",
      percent: -50
    }));
    expect(await findAmount([checkpoint1.id, checkpoint2.id], strategy.id, user.id)).toBe(0.625);
  });

  test('-100% / -100%', async () => {
    // Платеж: { "user_id": 8, "amount": 1.000000000000000000 }
    await debugTx(user, strategy, currency, 1, new Date('2018-11-11 23:59:59.999000 +00:00'));
    expect(await models.Transaction.sum('amount', {
      where: {
        is_deposit: true,
        is_freeze: false,
        user_id: user.id,
        strategy_id: strategy.id
      }
    })).toBe(1);
    expect(await models.Transaction.count()).toBe(1);
    expect(await models.Transaction.sum('amount', {
      where: { strategy_id: strategy.id, user_id: user.id }
    })).toBe(1);

    // =============================== Чекпоинт 1 = 0 BTC
    const checkpoint1 = await checkpointService.checkpoint(strategy.id, -100, new Date('2018-11-11 23:59:59.999999 +00:00'));
    await checkpointService.completeIteration(strategy.id);

    expect(await findCheckpoint(checkpoint1.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      base: "1.000000000000000000",
      before: "1.000000000000000000",
      after: "0.000000000000000000",
      diff: "-1.000000000000000000",
      percent: -100
    }));
    expect(await findAmount([checkpoint1.id], strategy.id, user.id)).toBe(0);
    expect(await models.Transaction.count()).toBe(2);

    // =============================== Чекпоинт 1 = 0 BTC
    const checkpoint2 = await checkpointService.checkpoint(strategy.id, -100, new Date('2018-11-11 23:59:59.999999 +00:00'));
    await checkpointService.completeIteration(strategy.id);

    // Смотри checkpoint.js#completeIterationForCheckpoint
    expect(await findCheckpoint(checkpoint2.id, strategy.id, user.id)).toEqual(null);
    expect(await findAmount([checkpoint1.id, checkpoint2.id], strategy.id, user.id)).toBe(0);
    expect(await models.Transaction.count()).toBe(2);
  });

  test('-50% / +100%', async () => {
    /*
    Платеж:
    {
      "user_id": 8,
      "amount": 1.000000000000000000,
    }
     */
    await debugTx(user, strategy, currency, 2, new Date('2018-11-11 23:59:59.999000 +00:00'));
    expect(await models.Transaction.sum('amount', {
      where: {
        is_deposit: true,
        is_freeze: false,
        user_id: user.id,
        strategy_id: strategy.id
      }
    })).toBe(2);
    expect(await models.Transaction.count()).toBe(1);
    expect(await models.Transaction.sum('amount', {
      where: { strategy_id: strategy.id, user_id: user.id }
    })).toBe(2);

    // =============================== Чекпоинт 1
    const checkpoint1 = await checkpointService.checkpoint(strategy.id, -50, new Date('2018-11-11 23:59:59.999999 +00:00'));
    await checkpointService.completeIteration(strategy.id);

    // =============================== Чекпоинт 2
    const checkpoint2 = await checkpointService.checkpoint(strategy.id, 100, new Date('2018-11-12 23:59:59.999999 +00:00'));
    await checkpointService.completeIteration(strategy.id);

    expect(await models.UserCheckpoint.findOne({
      where: {
        checkpoint_id: checkpoint1.id,
        user_id: user.id,
        strategy_id: strategy.id
      }
    })).toEqual(expect.objectContaining({
      before: "2.000000000000000000",
      after: "1.000000000000000000",
      diff: "-1.000000000000000000",
      percent: -50
    }));
    expect(await models.Transaction.sum('amount', {
      where: {
        strategy_id: strategy.id,
        user_id: user.id,
        [Op.or]: [
          {
            user_checkpoint_id: {
              [Op.in]: [checkpoint1.id]
            }
          },
          {
            payment_id: { [Op.not]: null }
          }
        ]
      }
    })).toBe(1);

    expect(await models.UserCheckpoint.findOne({
      where: {
        checkpoint_id: checkpoint2.id,
        user_id: user.id,
        strategy_id: strategy.id
      }
    })).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint2.id,
      before: "1.000000000000000000",
      after: "2.000000000000000000",
      diff: "1.000000000000000000",
      percent: 100
    }));
    expect(await models.Transaction.sum('amount', {
      where: {
        strategy_id: strategy.id,
        user_id: user.id,
        [Op.or]: [
          {
            user_checkpoint_id: {
              [Op.in]: [checkpoint1.id, checkpoint2.id]
            }
          },
          {
            payment_id: { [Op.not]: null }
          }
        ]
      }
    })).toBe(2);
  });

  test('broken -50% / +100% / +50% = 2 BTC', async () => {
    await debugTx(user, strategy, currency, 2, new Date('2018-12-02 16:12:22.200000 +00:00'));
    expect(await models.Transaction.count()).toBe(1);
    expect(await models.Transaction.sum('amount', {
      where: {
        is_deposit: true,
        is_freeze: false,
        user_id: user.id,
        strategy_id: strategy.id
      }
    })).toBe(2);

    // =============================== Чекпоинт 1
    const checkpoint1 = await checkpointService.checkpoint(strategy.id, -50, new Date('2018-12-12 20:59:59.999000 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    expect(await findCheckpoint(checkpoint1.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint1.id,
      before: "2.000000000000000000",
      after: "1.000000000000000000",
      diff: "-1.000000000000000000"
    }));
    expect(await findAmount([checkpoint1.id], strategy.id, user.id)).toBe(1);

    // =============================== Чекпоинт 2
    const checkpoint2 = await checkpointService.checkpoint(strategy.id, 100, new Date('2018-12-30 20:59:59.999000 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    expect(await findCheckpoint(checkpoint2.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint2.id,
      before: "1.000000000000000000",
      after: "2.000000000000000000",
      diff: "1.000000000000000000"
    }));
    expect(await findAmount([checkpoint1.id, checkpoint2.id], strategy.id, user.id)).toBe(2);

    // =============================== Чекпоинт 3
    const checkpoint3 = await checkpointService.checkpoint(strategy.id, 50, new Date('2018-12-31 20:59:59.999000 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    expect(await findCheckpoint(checkpoint3.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint3.id,
      before: "2.000000000000000000",
      after: "2.600000000000000000",
      diff: "0.600000000000000000"
    }));
    expect(await findAmount([checkpoint1.id, checkpoint2.id, checkpoint3.id], strategy.id, user.id)).toBe(2.6);
  });

  test('broken -50% / +100% / +50% = 1 BTC', async () => {
    await debugTx(user, strategy, currency, 1, new Date('2018-12-02 16:12:22.200000 +00:00'));
    expect(await models.Transaction.count()).toBe(1);
    expect(await models.Transaction.sum('amount', {
      where: {
        is_deposit: true,
        is_freeze: false,
        user_id: user.id,
        strategy_id: strategy.id
      }
    })).toBe(1);

    // =============================== Чекпоинт 1
    const checkpoint1 = await checkpointService.checkpoint(strategy.id, -50, new Date('2018-12-12 20:59:59.999000 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    expect(await findCheckpoint(checkpoint1.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint1.id,
      before: "1.000000000000000000",
      after: "0.500000000000000000",
      diff: "-0.500000000000000000"
    }));
    expect(await findAmount([checkpoint1.id], strategy.id, user.id)).toBe(0.5);

    // =============================== Чекпоинт 2
    const checkpoint2 = await checkpointService.checkpoint(strategy.id, 100, new Date('2018-12-30 20:59:59.999000 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    expect(await findCheckpoint(checkpoint2.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint2.id,
      before: "0.500000000000000000",
      after: "1.000000000000000000",
      diff: "0.500000000000000000"
    }));
    expect(await findAmount([checkpoint1.id, checkpoint2.id], strategy.id, user.id)).toBe(1);

    // =============================== Чекпоинт 3
    const checkpoint3 = await checkpointService.checkpoint(strategy.id, 50, new Date('2018-12-31 20:59:59.999000 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    // =============================== Пользователь 1
    expect(await findCheckpoint(checkpoint3.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint3.id,
      before: "1.000000000000000000",
      after: "1.250000000000000000",
      diff: "0.250000000000000000"
    }));
    expect(await findAmount([checkpoint1.id, checkpoint2.id, checkpoint3.id], strategy.id, user.id)).toBe(1.25);
  });

  test('broken -50% / +100% / +50% = 2+1 BTC multiple users', async () => {
    const user2 = await models.User.create({ email: 'user2@example.com' });

    await debugTx(user, strategy, currency, 1, new Date('2018-12-02 16:12:22.200000 +00:00'));
    await debugTx(user2, strategy, currency, 2, new Date('2018-12-02 16:12:22.200000 +00:00'));
    expect(await models.Transaction.count()).toBe(2);
    const defaultWhere = {
      is_deposit: true,
      is_freeze: false,
      strategy_id: strategy.id
    };
    expect(await models.Transaction.sum('amount', {
      where: { ...defaultWhere, user_id: user.id }
    })).toBe(1);
    expect(await models.Transaction.sum('amount', {
      where: { ...defaultWhere, user_id: user2.id }
    })).toBe(2);

    // =============================== Чекпоинт 1
    const checkpoint1 = await checkpointService.checkpoint(strategy.id, -50, new Date('2018-12-12 20:59:59.999000 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    // #1 - 1 BTC
    expect(await findCheckpoint(checkpoint1.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint1.id,
      before: "1.000000000000000000",
      after: "0.500000000000000000",
      diff: "-0.500000000000000000"
    }));
    expect(await findAmount([checkpoint1.id], strategy.id, user.id)).toBe(0.5);
    // #2 - 2 BTC
    expect(await findCheckpoint(checkpoint1.id, strategy.id, user2.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint1.id,
      before: "2.000000000000000000",
      after: "1.000000000000000000",
      diff: "-1.000000000000000000"
    }));
    expect(await findAmount([checkpoint1.id], strategy.id, user2.id)).toBe(1);

    // =============================== Чекпоинт 2
    const checkpoint2 = await checkpointService.checkpoint(strategy.id, 100, new Date('2018-12-30 20:59:59.999000 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    // #1 - 1 BTC
    expect(await findCheckpoint(checkpoint2.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint2.id,
      before: "0.500000000000000000",
      after: "1.000000000000000000",
      diff: "0.500000000000000000"
    }));
    expect(await findAmount([checkpoint1.id, checkpoint2.id], strategy.id, user.id)).toBe(1);
    // #2 - 2 BTC
    expect(await findCheckpoint(checkpoint2.id, strategy.id, user2.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint2.id,
      before: "1.000000000000000000",
      after: "2.000000000000000000",
      diff: "1.000000000000000000"
    }));
    expect(await findAmount([checkpoint1.id, checkpoint2.id], strategy.id, user2.id)).toBe(2);

    // =============================== Чекпоинт 3
    const checkpoint3 = await checkpointService.checkpoint(strategy.id, 50, new Date('2018-12-31 20:59:59.999000 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    // #1 - 1 BTC
    expect(await findCheckpoint(checkpoint3.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint3.id,
      before: "1.000000000000000000",
      after: "1.250000000000000000",
      diff: "0.250000000000000000"
    }));
    expect(await findAmount([checkpoint1.id, checkpoint2.id, checkpoint3.id], strategy.id, user.id)).toBe(1.25);
    // #2 - 2 BTC
    expect(await findCheckpoint(checkpoint3.id, strategy.id, user2.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint3.id,
      before: "2.000000000000000000",
      after: "2.600000000000000000",
      diff: "0.600000000000000000"
    }));
    expect(await findAmount([checkpoint1.id, checkpoint2.id, checkpoint3.id], strategy.id, user2.id)).toBe(2.6);
  });

  test('+50% / -50% / +100%', async () => {
    await debugTx(user, strategy, currency, 1, new Date('2018-11-11 23:59:59.999000 +00:00'));
    expect(await models.Transaction.sum('amount', {
      where: {
        is_deposit: true,
        is_freeze: false,
        user_id: user.id,
        strategy_id: strategy.id
      }
    })).toBe(1);
    expect(await models.Transaction.count()).toBe(1);
    expect(await models.Transaction.sum('amount', {
      where: { strategy_id: strategy.id, user_id: user.id }
    })).toBe(1);

    // =============================== Чекпоинт 1
    const checkpoint1 = await checkpointService.checkpoint(strategy.id, 50, new Date('2018-11-11 23:59:59.999999 +00:00'));
    await checkpointService.completeIteration(strategy.id);

    // =============================== Чекпоинт 2
    const checkpoint2 = await checkpointService.checkpoint(strategy.id, -50, new Date('2018-11-11 23:59:59.999999 +00:00'));
    await checkpointService.completeIteration(strategy.id);

    // =============================== Чекпоинт 3
    const checkpoint3 = await checkpointService.checkpoint(strategy.id, 100, new Date('2018-11-12 23:59:59.999999 +00:00'));
    await checkpointService.completeIteration(strategy.id);

    expect(await findCheckpoint(checkpoint1.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint1.id,
      base: "1.000000000000000000",
      before: "1.000000000000000000",
      after: "1.250000000000000000",
      diff: "0.250000000000000000",
      percent: 50
    }));
    expect(await findAmount([checkpoint1.id], strategy.id, user.id)).toBe(1.25);

    expect(await findCheckpoint(checkpoint2.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint2.id,
      base: "1.000000000000000000",
      before: "1.250000000000000000",
      after: "0.625000000000000000",
      diff: "-0.625000000000000000",
      percent: -50
    }));
    expect(await findAmount([checkpoint1.id, checkpoint2.id], strategy.id, user.id)).toBe(0.625);

    expect(await findCheckpoint(checkpoint3.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      checkpoint_id: checkpoint3.id,
      base: "0.625000000000000000",
      before: "0.625000000000000000",
      after: "0.937500000000000000",
      diff: "0.312500000000000000",
      percent: 100
    }));
    expect(await findAmount([checkpoint1.id, checkpoint2.id, checkpoint3.id], strategy.id, user.id)).toBe(0.9375);
  });

  test('+10% +10% +10% +10% +10% +10% / -50% +30% / +20% +10%', async () => {
    // Платеж: { "user_id": 8, "amount": 1.000000000000000000 }
    await debugTx(user, strategy, currency, 1, new Date('2018-11-11 23:59:59.999000 +00:00'));

    expect(await models.Transaction.sum('amount', {
      where: {
        is_deposit: true,
        is_freeze: false,
        user_id: user.id,
        strategy_id: strategy.id
      }
    })).toBe(1);
    expect(await models.Transaction.count()).toBe(1);
    expect(await models.Transaction.sum('amount', {
      where: { strategy_id: strategy.id, user_id: user.id }
    })).toBe(1);

    // =============================== Чекпоинт 1-6 + Отсечка 1
    const checkpoint1 = await checkpointService.checkpoint(strategy.id, 100, new Date('2018-11-11 23:59:59.999999 +00:00'));
    const checkpoint2 = await checkpointService.checkpoint(strategy.id, 100, new Date('2018-11-12 23:59:59.999999 +00:00'));
    const checkpoint3 = await checkpointService.checkpoint(strategy.id, 1, new Date('2018-11-13 23:59:59.999999 +00:00'));
    const checkpoint4 = await checkpointService.checkpoint(strategy.id, 1, new Date('2018-11-14 23:59:59.999999 +00:00'));
    const checkpoint5 = await checkpointService.checkpoint(strategy.id, 10, new Date('2018-11-15 23:59:59.999999 +00:00'));
    const checkpoint6 = await checkpointService.checkpoint(strategy.id, 100, new Date('2018-11-16 23:59:59.999999 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    expect(await findCheckpoint(checkpoint1.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      before: "1.000000000000000000",
      after: "1.500000000000000000",
      diff: "0.500000000000000000"
    }));
    // 60% / 40%
    expect(await findCheckpoint(checkpoint2.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      before: "1.500000000000000000",
      after: "2.400000000000000000",
      diff: "0.900000000000000000"
    }));
    expect(await findCheckpoint(checkpoint3.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      before: "2.400000000000000000",
      after: "2.414400000000000000",
      diff: "0.014400000000000000"
    }));
    expect(await findCheckpoint(checkpoint4.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      before: "2.414400000000000000",
      after: "2.428886400000000000",
      diff: "0.014486400000000000"
    }));
    expect(await findCheckpoint(checkpoint5.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      before: "2.428886400000000000",
      after: "2.574619584000000000",
      diff: "0.145733184000000000"
    }));
    expect(await findCheckpoint(checkpoint6.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      before: "2.574619584000000000",
      after: "4.119391334400000000",
      diff: "1.544771750400000000"
    }));
    expect(await findAmount([
      checkpoint1.id,
      checkpoint2.id,
      checkpoint3.id,
      checkpoint4.id,
      checkpoint5.id,
      checkpoint6.id
    ], strategy.id, user.id)).toEqual(4.1193913344);

    // =============================== Чекпоинт 6-10 + Отсечка 2
    const checkpoint7 = await checkpointService.checkpoint(strategy.id, 100, new Date('2018-11-17 23:59:59.999999 +00:00'));
    const checkpoint8 = await checkpointService.checkpoint(strategy.id, -50, new Date('2018-11-18 23:59:59.999999 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    expect(await findCheckpoint(checkpoint7.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      base: "1.000000000000000000",
      before: "4.119391334400000000",
      after: "6.591026135040000000",
      diff: "2.471634800640000000"
    }));
    expect(await findCheckpoint(checkpoint8.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      base: "1.000000000000000000",
      before: "6.591026135040000000",
      after: "3.295513067520000000",
      diff: "-3.295513067520000000"
    }));

    const checkpoint9 = await checkpointService.checkpoint(strategy.id, 10, new Date('2018-11-19 23:59:59.999999 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    // Не берем комиссию после убыточности
    expect(await findCheckpoint(checkpoint9.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      base: "3.295513067520000000",
      before: "3.295513067520000000",
      after: "3.493243851571200000",
      diff: "0.197730784051200000"
    }));
    expect(await findAmount([
      checkpoint1.id,
      checkpoint2.id,
      checkpoint3.id,
      checkpoint4.id,
      checkpoint5.id,
      checkpoint6.id,
      checkpoint7.id,
      checkpoint8.id,
      checkpoint9.id
    ], strategy.id, user.id)).toEqual(3.4932438515712);

    const checkpoint10 = await checkpointService.checkpoint(strategy.id, 10, new Date('2018-11-20 23:59:59.999999 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    expect(await findCheckpoint(checkpoint10.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      base: "3.295513067520000000",
      before: "3.493243851571200000",
      after: "3.702838482665472000",
      diff: "0.209594631094272000"
    }));

    const checkpoint11 = await checkpointService.checkpoint(strategy.id, 10, new Date('2018-11-21 23:59:59.999999 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    expect(await findCheckpoint(checkpoint11.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      "after": "3.925008791625400400",
      "base": "3.702838482665472000",
      "before": "3.702838482665472000",
      "diff": "0.222170308959928310"
    }));
  });

  test('base balance', async () => {
    await debugTx(user, strategy, currency, 1, new Date('2018-11-11 23:59:59.999000 +00:00'));
    expect(await models.Transaction.sum('amount', {
      where: {
        is_deposit: true,
        is_freeze: false,
        user_id: user.id,
        strategy_id: strategy.id
      }
    })).toBe(1);
    expect(await models.Transaction.count()).toBe(1);
    expect(await models.Transaction.sum('amount', {
      where: { strategy_id: strategy.id, user_id: user.id }
    })).toBe(1);

    // 1 месяц, фонд сработал в -50% = 0.5 BTC (базовый 1), отсечка
    const checkpoint1 = await checkpointService.checkpoint(strategy.id, -50, new Date('2018-11-11 23:59:59.999999 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    expect(await findCheckpoint(checkpoint1.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      base: "1.000000000000000000"
    }));

    // 2 месяц, фонд сработал в -20% = 0.4 BTC (базовый 1), отсечка
    const checkpoint2 = await checkpointService.checkpoint(strategy.id, -20, new Date('2018-11-12 23:59:59.999999 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    expect(await findCheckpoint(checkpoint2.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      base: "1.000000000000000000"
    }));

    // 3 месяц, фонд сработал в +10% = 0.42 BTC (базовый 0.4), отсечка
    const checkpoint3 = await checkpointService.checkpoint(strategy.id, 10, new Date('2018-11-12 23:59:59.999999 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    expect(await findCheckpoint(checkpoint3.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      base: "0.400000000000000000"
    }));

    // 4 месяц, фонд сработал в +100% = 0.63 BTC (базовый 0.4), отсечка
    const checkpoint4 = await checkpointService.checkpoint(strategy.id, 100, new Date('2018-11-12 23:59:59.999999 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    expect(await findCheckpoint(checkpoint4.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      base: "0.400000000000000000"
    }));

    // 5 месяц, фонд сработал в +100% = 0,945 (базовый 0.63)
    const checkpoint5 = await checkpointService.checkpoint(strategy.id, 100, new Date('2018-11-12 23:59:59.999999 +00:00'));
    await checkpointService.completeIteration(strategy.id);
    expect(await findCheckpoint(checkpoint5.id, strategy.id, user.id)).toEqual(expect.objectContaining({
      base: "0.630000000000000000"
    }));
  });
});
