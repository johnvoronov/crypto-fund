const request = require('supertest');
const app = require('../../../app');
const models = require('../../models');
const jwt = require('../../util/jwt');
const balance = require('../../service/balance');
const consts = require('../../consts');
const { debugPaymentTx, debugTx } = require('../utils');

describe('routes/withdraw', function () {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  test('withdraw / confirm', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true
    });

    const userHeaders = {
      Accept: 'application/json',
      Authorization: jwt.signUser(user)
    };

    const currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 100
    });
    const strategy = await models.Strategy.create({
      name: 'test',
      currency_id: currency.id,
      convert_id: currency.id
    });

    await debugPaymentTx(user, strategy, currency, 100);
    expect(await balance.getUserBalance({ user_id: user.id })).toEqual(100);

    await request(app)
      .post('/withdraw')
      .send({ amount: 80, strategy_id: strategy.id })
      .set(userHeaders)
      .expect(400);

    await request(app)
      .post('/withdraw')
      .send({
        amount: 80,
        strategy_id: strategy.id,
        btc_address: '1BoatSLRHtKNngkdXEeobR76b53LETtpyT',
        user_id: user.id
      })
      .set(userHeaders)
      .expect(201);

    expect(await models.Transaction.count()).toEqual(2); // 1 payment, 1 withdraw
    expect(await balance.getUserBalance({ user_id: user.id })).toEqual(20);
    const withdraw = await models.Withdraw.findById(1);
    expect(parseFloat(withdraw.amount)).toEqual(80);

    await request(app)
      .post('/withdraw')
      .send({ amount: 80, btc_address: '1BoatSLRHtKNngkdXEeobR76b53LETtpyT' })
      .set(userHeaders)
      .expect(400);
  });
});
