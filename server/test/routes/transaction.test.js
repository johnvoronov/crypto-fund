const request = require('supertest');
const app = require('../../../app');
const models = require('../../models');
const jwt = require('../../util/jwt');
const consts = require('../../consts');

describe('routes/transaction', () => {
  beforeEach(() => {
    return models.sequelize.sync({
      force: true
    });
  });

  afterAll(() => models.sequelize.close());

  it('list', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      is_active: true
    });

    const headers = {
      Accept: 'application/json',
      Authorization: jwt.signUser(user)
    };

    const currency = await models.Currency.create({
      to_eth: 1,
      to_usd: 1,
      code: 'TST',
      name: 'Test'
    });
    const wallet = await models.Wallet.create({
      currency_id: currency.id,
      user_id: user.id,
      merchant_id: '1234',
      address: 'test'
    });
    const payment = await models.Payment.create({
      amount: 1,
      wallet_id: wallet.id,
      currency_id: currency.id,
      user_id: user.id,
      to_eth: currency.to_eth,
      to_usd: currency.to_usd,
      transaction_id: 'test'
    });

    const startAt = new Date();
    startAt.setDate(startAt.getDate() - 1);
    const endAt = new Date();
    endAt.setDate(endAt.getDate() + 1);

    const tx = await models.Transaction.create({
      amount: 1,
      payment_id: payment.id,
      user_id: user.id,
      usd: payment.amount * currency.to_usd,
      status: consts.TRANSACTION_DEPOSIT,
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      created_at: startAt
    });

    await request(app)
      .get('/transaction')
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body).toEqual(
          expect.objectContaining({
            objects: expect.any(Array),
            meta: expect.any(Object)
          })
        );
      });

    await request(app)
      .get('/transaction?status=' + consts.TRANSACTION_DEPOSIT)
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.objects.length).toEqual(1);
      });

    await request(app)
      .get('/transaction?status=' + consts.TRANSACTION_WRITE_OFF)
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.objects.length).toEqual(0);
      });

    await tx.update({
      status: consts.TRANSACTION_DEPOSIT
    });
    await request(app)
      .get('/transaction?status=' + consts.TRANSACTION_DEPOSIT)
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.objects.length).toEqual(1);
      });

    await request(app)
      .get('/transaction?date_from=' + startAt.toISOString() + '&date_to=' + endAt.toISOString())
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.objects.length).toEqual(1);
      });

    await request(app)
      .get('/transaction?date_from=' + startAt.toISOString())
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.objects.length).toEqual(1);
      });

    await request(app)
      .get('/transaction?date_to=' + endAt.toISOString())
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.objects.length).toEqual(1);
      });

    await request(app)
      .get('/transaction?date_to=' + startAt.toISOString() + '&date_from=' + endAt.toISOString())
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.objects.length).toEqual(0);
      });
  });
});
