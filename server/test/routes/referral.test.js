const request = require('supertest');
const app = require('../../../app');
const models = require('../../models');
const jwt = require('../../util/jwt');

describe('routes/referral', () => {
  let user;

  beforeEach(async () => {
    await models.sequelize.sync({ force: true });

    user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true,
      is_superuser: true,
      is_admin: true
    });

    await models.Referral.create({
      name_en: 'no status',
      name_ru: 'no status',
      team: 0,
      user: 0,
      user_count: 0,
      levels: []
    });
  });

  /**
   * Получить сетку статусов пользователя для дальнейшей покупки
   */
  test('grid', async () => {
    const headers = {
      Accept: 'application/json',
      Authorization: jwt.signUser(user)
    };

    await request(app)
      .get('/referral/grid')
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200);
  });
});
