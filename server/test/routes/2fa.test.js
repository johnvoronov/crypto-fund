const request = require('supertest');
const speakeasy = require('speakeasy');
const app = require('../../../app');
const models = require('../../models');
const jwt = require('../../util/jwt');
jest.mock('speakeasy', () => ({
  generateSecret: () => ({
    base32: '123456'
  }),
  totp: { verify: jest.fn() }
}));

describe('routes/two_factor', function () {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });

    speakeasy.totp.verify.mockReturnValue(true);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('show qr code', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      is_active: true
    });

    expect(user.two_factor_secret).toBeFalsy();

    await request(app)
      .get('/2fa/qr')
      .set({ 'Authorization': jwt.sign({ id: user.id }) })
      .expect(200);

    await user.reload();
    expect(user.two_factor_secret).toBeTruthy();
  });

  test('enable two factor with incorrect token', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      is_active: true
    });

    request(app)
      .post('/2fa/enable')
      .send({ token: '123' })
      .set({ 'Authorization': jwt.sign({ id: user.id }) })
      .expect(204);
  });

  test('disable two factor', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_two_factor: true,
      is_active: true
    });

    await request(app)
      .post('/2fa/disable')
      .send({ token: '123' })
      .set({ 'Authorization': jwt.sign({ id: user.id }) })
      .expect(204);

    await user.reload();
    expect(user.is_two_factor).toBeFalsy();
  });
});
