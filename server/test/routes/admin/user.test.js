const request = require('supertest');
const app = require('../../../../app');
const models = require('../../../models');
const jwt = require('../../../util/jwt');

describe('routes/admin/user', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true })
  });

  test('list', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true,
      is_superuser: true,
      is_admin: true
    });

    await request(app)
      .get('/admin/user/list')
      .set({
        'Accept': 'application/json',
        'Authorization': jwt.signUser(user)
      })
      .expect('Content-Type', /json/)
      .expect(200);
  });

  test('options', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true,
      is_superuser: true,
      is_admin: true
    });

    await request(app)
      .get('/admin/user/options')
      .set({
        'Accept': 'application/json',
        'Authorization': jwt.signUser(user)
      })
      .expect('Content-Type', /json/)
      .expect(200);
  });

  test('view', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true,
      is_superuser: true,
      is_admin: true
    });

    await request(app)
      .get('/admin/user/' + user.id)
      .set({
        'Accept': 'application/json',
        'Authorization': jwt.signUser(user)
      })
      .expect('Content-Type', /json/)
      .expect(200);
  });
});
