const request = require('supertest');
const app = require('../../../../app');
const models = require('../../../models');
const jwt = require('../../../util/jwt');

describe('routes/admin/currency', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true })
  });

  test('list', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true,
      is_superuser: true,
      is_admin: true
    });

    await request(app)
      .get('/admin/currency/list')
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect('Content-Type', /json/)
      .expect(200);
  });

  test.skip('exchange', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true,
      is_admin: true
    });

    expect(await models.Currency.count()).toEqual(0);

    await request(app)
      .post('/admin/currency/exchange')
      .set('Authorization', jwt.signUser(user))
      .expect(204);

    expect(await models.Currency.count()).toEqual(4);
  });
});
