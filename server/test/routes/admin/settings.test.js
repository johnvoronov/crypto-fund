const request = require('supertest');
const app = require('../../../../app');
const models = require('../../../models');
const jwt = require('../../../util/jwt');

describe('routes/admin/settings', function () {
  let user;
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });

    user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_two_factor: false,
      is_active: true,
      is_admin: true
    });
  });

  it('save', async () => {
    const headers = {
      Accept: 'application/json',
      Authorization: jwt.signUser(user)
    };

    await request(app)
      .post('/admin/settings')
      .send({ foo: 'bar' })
      .set(headers)
      .expect(400)
      .expect({
        statusCode: 400,
        error: 'Bad Request',
        errors: { foo: ['foo is not allowed'] }
      });

    await request(app)
      .post('/admin/settings')
      .send({ company_name: '10' })
      .set(headers)
      .expect(204);

    await request(app)
      .get('/admin/settings')
      .set(headers)
      .expect(200)
      .then(resp => {
        expect(resp.body).toEqual({
          company_name: '10',
          user_registration_invite_only: '0',
          coinpayments_public_key: null,
          coinpayments_secret_key: null,
          coinpayments_ipn_secret: null,
          coinpayments_merchant_id: null,
          google_analytics_id: null,
          google_tag_manager_id: null,
          partnership_description_en: null,
          partnership_description_ru: null,
          phone_required: '0',
          settings_info_ru: null,
          settings_info_en: null,
          terms_and_conditions_en: null,
          terms_and_conditions_ru: null,
          deduct_coinpayments_tax: null,
          smtp_from: null,
          smtp_host: null,
          smtp_port: null,
          smtp_username: null,
          smtp_password: null,
          bitcoinstalk_link: null,
          facebook_link: null,
          twitter_link: null,
          youtube_link: null,
          telegram_link: null,
        })
      });
  });

  it('save / smtp', async () => {
    await request(app)
      .post('/admin/settings/smtp')
      .send()
      .set({ Accept: 'application/json', Authorization: jwt.signUser(user) })
      .expect(204);
    expect(await models.Setting.count()).toEqual(5);
  });

  it('save / link', async () => {
    await request(app)
      .post('/admin/settings/link')
      .send()
      .set({ Accept: 'application/json', Authorization: jwt.signUser(user) })
      .expect(204);
    expect(await models.Setting.count()).toEqual(5);
  });

  it('save / company', async () => {
    await request(app)
      .post('/admin/settings/company')
      .send()
      .set({ Accept: 'application/json', Authorization: jwt.signUser(user) })
      .expect(204);
    expect(await models.Setting.count()).toEqual(10);
  });
});
