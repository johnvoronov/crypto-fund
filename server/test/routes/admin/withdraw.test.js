const request = require('supertest');
const app = require('../../../../app');
const models = require('../../../models');
const jwt = require('../../../util/jwt');
const consts = require('../../../consts');

describe('routes/admin/withdraw', function () {
  let headers;
  let user;
  let withdraw;

  beforeEach(async () => {
    await models.sequelize.sync({ force: true });

    user = await models.User.create({
      email: 'user@example.com',
      is_active: true,
      is_superuser: true,
      is_admin: true
    });

    headers = { Authorization: jwt.signUser(user) };

    const currency = await models.Currency.create({
      code: 'TST',
      name: 'test',
      to_usd: 0,
    });

    const wallet = await models.Wallet.create({
      user_id: user.id,
      currency_id: currency.id,
      address: 'test'
    });

    const payment = await models.Payment.create({
      amount: 0.0005,
      transaction_id: '0xeac693d64d94152631f7d81fcef4c089bc1452c7c556900812874e9f6635e49c',
      fee: 0,
      to_usd: 793.519,
      status: consts.PAYMENT_COMPLETE,
      created_at: new Date,
      updated_at: new Date,
      wallet_id: wallet.id,
      user_id: user.id,
      currency_id: currency.id
    });

    const transaction = await models.Transaction.create({
      amount: 35.16,
      comment: 'Bonus',
      is_referrer: false,
      is_bonus: true,
      user_id: user.id,
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      usd: payment.amount * currency.to_usd,
      payment_id: payment.id,
      created_at: new Date,
      updated_at: new Date
    });

    withdraw = await models.Withdraw.create({
      amount: 35.16,
      transaction_hash: '9a2c425ab033f42d420e97fdc9a32efbf2663a13288d8092538c4ef6d4a1121b',
      transaction_id: transaction.id,
      user_id: user.id,
      btc_address: '1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX',
      created_at: new Date,
      updated_at: new Date
    });
  });

  it('list', async () => {
    await request(app)
      .get('/admin/withdraw')
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.objects.length).toEqual(1);
      });
  });

  it('update', async () => {
    await request(app)
      .post('/admin/withdraw/100')
      .send({ transaction_hash: 'e183b487d2bc7b2372541ddba3a2809953f63e0fa0fd111966266d5373b9149a' })
      .set(headers)
      .expect(404)
      .expect({
        statusCode: 404,
        error: 'Not Found'
      });

    await request(app)
      .post('/admin/withdraw/' + withdraw.id)
      .send({ transaction_hash: 'e183b487d2bc7b2372541ddba3a2809953f63e0fa0fd111966266d5373b9149a' })
      .set(headers)
      .expect(204);
  });
});
