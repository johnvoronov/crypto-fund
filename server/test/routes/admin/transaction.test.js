const request = require('supertest');
const app = require('../../../../app');
const models = require('../../../models');
const jwt = require('../../../util/jwt');
const consts = require('../../../consts');

describe('routes/admin/transaction', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  test('list', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      is_active: true,
      is_superuser: true,
      is_admin: true
    });

    request(app)
      .get('/admin/transaction/list')
      .set('Authorization', jwt.signUser(user))
      .expect('Content-Type', /json/)
      .expect(200);
  });

  test('view / accept', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      is_active: true,
      is_superuser: true,
      is_admin: true
    });

    const currency = await models.Currency.create({
      code: 'TST',
      name: 'test',
      to_usd: 0
    });

    const strategy = await models.Strategy.create({
      name: 'test',
      currency_id: currency.id
    });

    const wallet = await models.Wallet.create({
      user_id: user.id,
      currency_id: currency.id,
      address: 'test'
    });

    const payment = await models.Payment.create({
      amount: 0.0005,
      transaction_id: '0xeac693d64d94152631f7d81fcef4c089bc1452c7c556900812874e9f6635e49c',
      fee: 0,
      to_usd: 793.519,
      status: consts.PAYMENT_COMPLETE,
      created_at: new Date(),
      updated_at: new Date(),
      wallet_id: wallet.id,
      user_id: user.id,
      currency_id: currency.id
    });

    const tx = await models.Transaction.create({
      amount: 35.16,
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      comment: 'Bonus',
      is_referrer: false,
      is_bonus: true,
      user_id: user.id,
      usd: payment.amount * currency.to_usd,
      payment_id: payment.id,
      strategy_id: strategy.id,
      created_at: new Date(),
      updated_at: new Date(),
      status: consts.TRANSACTION_FREEZE
    });

    await request(app)
      .get('/admin/transaction/1')
      .set({
        Accept: 'application/json',
        Authorization: jwt.signUser(user)
      })
      .expect('Content-Type', /json/)
      .expect(200);

    await request(app)
      .get('/admin/transaction/accept/1')
      .set({
        Accept: 'application/json',
        Authorization: jwt.signUser(user)
      })
      .expect(204);

    await tx.reload();
    expect(tx.status).toEqual(consts.TRANSACTION_DEPOSIT);

    expect(await models.Transaction.count()).toEqual(1);
  });
});
