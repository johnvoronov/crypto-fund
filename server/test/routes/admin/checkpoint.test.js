const request = require('supertest');
const app = require('../../../../app');
const models = require('../../../models');
const balanceService = require('../../../service/balance');
const jwt = require('../../../util/jwt');

describe('routes/admin/checkpoint', function () {
  let user;
  let strategy;
  let checkpoint;

  beforeEach(async () => {
    await models.sequelize.sync({ force: true });

    user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true,
      is_superuser: true,
      is_admin: true
    });

    const startAt = new Date();
    startAt.setDate(startAt.getDate() - 1);
    const endAt = new Date();
    endAt.setDate(endAt.getDate() + 1);

    const currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 100
    });

    strategy = await models.Strategy.create({
      name: 'test',
      currency_id: currency.id,
      convert_id: currency.id
    });

    checkpoint = await models.Checkpoint.create({
      amount: 100,
      strategy_id: strategy.id,
      percent: 10,
      start_at: startAt,
      end_at: endAt,
      is_done: true
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('list', async () => {
    const headers = {
      Accept: 'application/json',
      Authorization: jwt.signUser(user)
    };

    await request(app)
      .get('/admin/checkpoint')
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.objects.length).toEqual(1);
      });
  });

  test('view', async () => {
    const headers = {
      Accept: 'application/json',
      Authorization: jwt.signUser(user)
    };

    await request(app)
      .get('/admin/checkpoint/' + checkpoint.id)
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body).toEqual(expect.any(Object));
      });
  });

  test('create', async () => {
    const headers = {
      Accept: 'application/json',
      Authorization: jwt.signUser(user)
    };

    balanceService.getActiveDepositBalance = jest.fn(() => Promise.resolve(1));

    const startAt = new Date;
    startAt.setMonth(startAt.getMonth() - 1);
    const endAt = new Date;
    await request(app)
      .post('/admin/checkpoint')
      .send({
        strategy_id: strategy.id,
        percent: 20,
        start_at: startAt,
        end_at: endAt
      })
      .set(headers)
      .expect(204);
  });
});
