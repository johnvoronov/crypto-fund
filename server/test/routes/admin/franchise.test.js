const request = require('supertest');
const app = require('../../../../app');
const models = require('../../../models');
const jwt = require('../../../util/jwt');

describe('routes/admin/franchise', function () {
  let user;
  let owner;
  let referral;
  let currency;
  let headers;

  beforeEach(async () => {
    await models.sequelize.sync({ force: true });

    currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 1,
      to_btc: 1,
      to_rub: 1,
      to_eth: 1
    });

    const level = await models.Referral.create({
      name_en: 'newbie',
      name_ru: 'newbie',
      team: 5,
      user: 5,
      user_count: 10,
      pro: 10,
      levels: [5]
    });

    user = await models.User.create({
      email: 'user@example.com',
      is_active: true,
      is_admin: true
    });
    owner = await models.User.create({
      email: 'owner@example.com',
      is_active: true,
      referral_level_id: level.id
    });
    referral = await models.User.create({
      email: 'referral@example.com',
      is_active: true,
      referral_id: owner.id
    });

    headers = { Authorization: jwt.signUser(user) };
  });

  test('list / view', async () => {
    const headers = {
      Authorization: jwt.signUser(user)
    };

    const fr = await models.Franchise.create({
      user_id: referral.id
    });

    await request(app)
      .get('/admin/franchise')
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.objects.length).toEqual(1);
      });

    await request(app)
      .get(`/admin/franchise/${fr.id}`)
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200);

    await request(app)
      .post(`/admin/franchise/activate/${fr.id}`)
      .send({ is_active: true })
      .set(headers)
      .expect(400);

    await request(app)
      .post(`/admin/franchise/price/${fr.id}`)
      .send({ price: 100, currency_id: currency.id })
      .set(headers)
      .expect('')
      .expect(204);

    await request(app)
      .post(`/admin/franchise/activate/${fr.id}`)
      .send({ is_active: true })
      .set(headers)
      .expect(204);

    await fr.reload();
    expect(parseFloat(fr.price)).toEqual(100);
    expect(fr.is_active).toEqual(true);

    expect(await models.Transaction.count()).toEqual(1);
  });
});
