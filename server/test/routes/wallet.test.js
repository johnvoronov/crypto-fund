const request = require('supertest');
const coinpayments = require('coinpayments');
const app = require('../../../app');
const models = require('../../models');
const transactionService = require('../../service/transaction');
const consts = require('../../consts');
const passwordUtil = require('../../util/password');
const jwt = require('../../util/jwt');
const settings = require('../../util/settings');
jest.mock('coinpayments');

describe('routes/wallet', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });

    await settings.setValue('coinpayments_public_key', '66daee661f062a17810c7ab9190e1ee99d9a81777a376d6055588ccd657e203f');
    await settings.setValue('coinpayments_secret_key', '3fDcd6d738baAd51c10CbB9374D345970128872BC367111d22F91945689a3b77');
    await settings.setValue('coinpayments_ipn_secret', '3c9b1ba13753a91b78f0428803410dc3');
    await settings.setValue('coinpayments_merchant_id', '9mh5tqnVqr6y86VN2YunZTupvKG6Pv3m');

    let i = 0;
    coinpayments.mockImplementation(() => ({
      getCallbackAddress: jest.fn(() => {
        i++;
        return {
          address: `test${i}`
        }
      })
    }));
  });

  afterAll(async () => {
    await models.sequelize.close();
  });

  test('create strategy wallet', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: await passwordUtil.hash('123456'),
      is_active: true
    });

    await request(app)
      .post('/strategy/1/wallet')
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect('Content-Type', /json/)
      .expect(404);

    const currency = await models.Currency.create({
      code: 'BTC',
      name: 'Bitcoin',
      to_usd: 0,
      is_enabled: true
    });

    const strategy = await models.Strategy.create({
      name: 'Купи слона',
      currency_id: currency.id,
      convert_id: currency.id,
      coinpayments: {
        publicKey: 123,
        secretKey: 123,
        merchantId: 123,
        ipnSecret: 123
      }
    });

    const strategyNew = await models.Strategy.create({
      name: 'Купи купи купи',
      currency_id: currency.id,
      convert_id: currency.id,
      coinpayments: {
        publicKey: 123,
        secretKey: 123,
        merchantId: 123,
        ipnSecret: 123
      }
    });

    const resp = await request(app)
      .post(`/strategy/${strategy.id}/wallet`)
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect('Content-Type', /json/)
      .expect(200);
    expect(resp.body.address).toBeTruthy();

    const againResp = await request(app)
      .post(`/strategy/${strategy.id}/wallet`)
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect('Content-Type', /json/)
      .expect(200);
    expect(resp.body.address).toEqual(againResp.body.address);

    const newResp = await request(app)
      .post(`/strategy/${strategyNew.id}/wallet`)
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect('Content-Type', /json/)
      .expect(200);
    expect(newResp.body.address).not.toEqual(againResp.body.address);

    expect(await models.Wallet.count()).toEqual(2);
  });

  test('create internal wallet', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      is_active: true
    });

    await request(app)
      .post('/wallet')
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect('Content-Type', /json/)
      .expect(400);

    const currency = await models.Currency.create({
      code: 'BTC',
      name: 'Bitcoin',
      to_btc: 1,
      to_eth: 1,
      to_rub: 1,
      to_usd: 1000,
      is_enabled: true
    });

    const ref = await models.Referral.create({
      name_en: 'en',
      name_ru: 'ru',
      user: 1,
      team: 1,
      user_count: 1,
      levels: [],
      price: 10
    });

    await request(app)
      .post('/wallet')
      .send({ referral_id: ref.id })
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect('Content-Type', /json/)
      .expect(200)
      .expect({
        amount: { btc: 0.01 },
        address: 'test1'
      });

    await transactionService.createTx({
      amount: ref.price,
      user_id: user.id,
      account_type: consts.ACCOUNT_TYPE_STATUS,
      status: consts.TRANSACTION_PAY_STATUS
    }, currency);

    await request(app)
      .post('/wallet')
      .send({ referral_id: ref.id })
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect('Content-Type', /json/)
      .expect(200)
      .expect({
        amount: { btc: -9.99 },
        address: 'test1'
      });

    expect(await models.Wallet.count()).toEqual(1);
  });
});
