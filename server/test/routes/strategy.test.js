const request = require('supertest');
const app = require('../../../app');
const models = require('../../models');
const jwt = require('../../util/jwt');
const balance = require('../../service/balance');
const transactionService = require('../../service/transaction');
const paymentService = require('../../service/payment');
const consts = require('../../consts');

const debugPaymentTx = async (user, strategy, currency, amount) => {
  return await transactionService.createPayment(
    await paymentService.createPayment(user, strategy, currency, amount)
  )
};

const debugTx = async (user, strategy, currency, amount) => {
  await transactionService.createDeposit(
    await debugPaymentTx(user, strategy, currency, amount)
  );
};

describe('routes/strategy', function () {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  test.skip('DEPRECATED deposit', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true
    });

    const userHeaders = {
      Accept: 'application/json',
      Authorization: jwt.signUser(user)
    };

    await request(app)
      .post('/strategy/deposit')
      .send({ amount: 100, strategy_id: 1 })
      .set(userHeaders)
      .expect(404);

    const currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 100
    });

    const strategy = await models.Strategy.create({
      name: 'test',
      currency_id: currency.id,
      convert_id: currency.id
    });

    expect(await balance.getUserBalance({ user_id: user.id })).toEqual(0);

    await request(app)
      .post('/strategy/deposit')
      .send({ amount: 100, strategy_id: strategy.id })
      .set(userHeaders)
      .expect(400);

    await models.Transaction.create({
      amount: 107,
      strategy_id: strategy.id,
      user_id: user.id,
      status: consts.TRANSACTION_PAYMENT
    });
    expect(await balance.getUserBalance(user.id)).toEqual(107);

    await request(app)
      .post('/strategy/deposit')
      .send({ amount: 100, strategy_id: strategy.id })
      .set(userHeaders)
      .expect(204);

    expect(await balance.getUserBalance(user.id)).toEqual(7);
    expect(await balance.getDepositBalance(user.id)).toEqual(100);
  });

  test.skip('DEPRECATED return', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true
    });

    const userHeaders = {
      Accept: 'application/json',
      Authorization: jwt.signUser(user)
    };

    await request(app)
      .post('/strategy/return')
      .send({ amount: 100, strategy_id: 1 })
      .set(userHeaders)
      .expect(404);

    const currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 100
    });

    const strategy = await models.Strategy.create({
      name: 'test',
      currency_id: currency.id,
      convert_id: currency.id
    });

    await models.Transaction.create({
      amount: 100,
      strategy_id: strategy.id,
      user_id: user.id,
      status: consts.TRANSACTION_PAYMENT
    });

    expect(await balance.getUserBalance(user.id)).toEqual(100);
    expect(await balance.getDepositBalance(user.id)).toEqual(0);

    await models.Transaction.create({
      amount: -80,
      strategy_id: strategy.id,
      user_id: user.id,
      status: consts.TRANSACTION_DEPOSIT
    });
    await models.Transaction.create({
      amount: 80,
      strategy_id: strategy.id,
      user_id: user.id,
      is_deposit: true,
      status: consts.TRANSACTION_DEPOSIT
    });

    expect(await balance.getUserBalance(user.id)).toEqual(20);
    expect(await balance.getDepositBalance(user.id)).toEqual(80);

    await request(app)
      .post('/strategy/return')
      .send({ amount: 80, strategy_id: strategy.id })
      .set(userHeaders)
      .expect(204);

    expect(await balance.getUserBalance(user.id)).toEqual(100);
    expect(await balance.getDepositBalance(user.id)).toEqual(0);
  });

  test('detail view', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true
    });

    const userHeaders = {
      Accept: 'application/json',
      Authorization: jwt.signUser(user)
    };

    const currency = await models.Currency.create({
      name: 'Bitcoin',
      code: 'BTC',
      to_usd: 100
    });

    const strategy = await models.Strategy.create({
      name: 'test',
      currency_id: currency.id,
      convert_id: currency.id
    });

    await request(app)
      .get(`/strategy/${strategy.id}`)
      .set(userHeaders)
      .expect(200);
  });
});
