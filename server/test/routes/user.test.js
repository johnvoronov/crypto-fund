const request = require('supertest');
const speakeasy = require('speakeasy');
const app = require('../../../app');
const models = require('../../models');
const passwordUtil = require('../../util/password');
const smsService = require('../../service/sms');
jest.mock('../../service/sms');
const jwt = require('../../util/jwt');
const settings = require('../../util/settings');
jest.mock('speakeasy', () => ({
  generateSecret: () => '123456',
  totp: { verify: jest.fn() }
}));

describe('routes/user', function () {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });

    smsService.send = jest.fn(() => Promise.resolve(true));
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('login invalid credentials', async () => {
    const resp = await request(app)
      .post('/user/login')
      .send({ name: 'john' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400);

    expect(resp.body.errors).toEqual({
      login: ['Login is required'],
      password: ['Password is required']
    });
  });

  test('login no user', async () => {
    await request(app)
      .post('/user/login')
      .send({ login: 'user@example.com', password: '123456' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400);
  });

  test('login', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: await passwordUtil.hash('123456'),
      is_active: true
    });
    const resp = await request(app)
      .post('/user/login')
      .send({ login: 'user@example.com', password: '123456' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200);
    expect(resp.body.user.id).toEqual(user.id);
    expect(resp.body.jwt).toBeTruthy();
  });

  test('login with 2fa', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: await passwordUtil.hash('123456'),
      is_active: true,
      is_two_factor: true
    });

    await request(app)
      .post('/user/login')
      .send({ login: 'user@example.com', password: '123456' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(406);

    const resp = await request(app)
      .post('/user/login')
      .send({ login: 'user@example.com', password: '123456', token: '123' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200);
    expect(resp.body.user.id).toEqual(user.id);
    expect(resp.body.jwt).toBeTruthy();
  });

  test('registration invite only', async () => {
    await settings.setValue('user_registration_invite_only', true);

    await request(app)
      .post('/user/registration')
      .send({
        phone: 79005554433,
        is_agree: true,
        is_confirm: true,
        login: 'foo',
        email: 'foo@bar.com',
        password: 'qwe!@#eq@w!@#',
        password_confirm: 'qwe!@#eq@w!@#'
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400);

    await request(app)
      .post('/user/registration')
      .send({
        phone: 79005554433,
        is_agree: true,
        is_confirm: true,
        login: 'foo',
        email: 'foo@bar.com',
        password: 'qwe!@#eq@w!@#',
        password_confirm: 'qwe!@#eq@w!@#'
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400);

    const user = await models.User.create({
      login: 'user',
      email: 'user@example.com',
      is_active: true
    });

    await request(app)
      .post('/user/registration')
      .send({
        phone: 79005554433,
        is_agree: true,
        is_confirm: true,
        login: 'foo',
        email: 'foo@bar.com',
        password: 'qwe!@#eq@w!@#',
        password_confirm: 'qwe!@#eq@w!@#',
        referral_id: user.id
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201);
  });

  test('update profile', async () => {
    const user = await models.User.create({
      login: 'user',
      email: 'user@example.com',
      password: await passwordUtil.hash('123456'),
      is_active: true
    });

    await request(app)
      .post('/user/profile')
      .send({ first_name: 'foo', last_name: 'bar' })
      .set('Authorization', jwt.signUser(user))
      .expect(200);

    await user.reload();

    expect(user.first_name).toEqual('foo');
    expect(user.last_name).toEqual('bar');

    await user.update({
      two_factor_secret: 123,
      is_two_factor: true
    });

    await request(app)
      .post('/user/profile')
      .send({ first_name: 'hello world' })
      .set('Authorization', jwt.signUser(user))
      .expect(200);

    await request(app)
      .post('/user/profile')
      .send({ btc_address: '12345' })
      .set('Authorization', jwt.signUser(user))
      .expect(406);

    speakeasy.totp.verify.mockReturnValue(false);
    await request(app)
      .post('/user/profile')
      .send({ btc_address: '12345', token: '12345' })
      .set('Authorization', jwt.signUser(user))
      .expect(400);

    speakeasy.totp.verify.mockReturnValue(true);
    await request(app)
      .post('/user/profile')
      .send({ btc_address: '12345', token: '12345' })
      .set('Authorization', jwt.signUser(user))
      .expect(200);
  });

  test('login with inactive user', async () => {
    await models.User.create({
      email: 'user@example.com',
      password: await passwordUtil.hash('123456')
    });
    await request(app)
      .post('/user/login')
      .send({ email: 'user@example.com', password: '123456' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400);
  });

  test('registration with same email', async () => {
    await models.User.create({
      login: 'user',
      email: 'user@example.com',
      password: ''
    });
    await request(app)
      .post('/user/registration')
      .send({ login: 'user', email: 'user@example.com', password: '123456' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400);
  });

  test('registration valid', async () => {
    await request(app)
      .post('/user/registration')
      .send({
        phone: 79005554433,
        login: 'foo',
        is_agree: true,
        email: 'foo@bar.com',
        password: 'qwe!@#eq@w!@#',
        password_confirm: 'qwe!@#eq@w!@#'
      })
      .expect(201)
      .then(async response => {
        expect(response.body.id).toEqual(1);
      });
  });

  test('registration valid ref level CL-299', async () => {
    const level = await models.Referral.create({
      name_en: 'no status',
      name_ru: 'no status',
      team: 0,
      user: 0,
      price: 0,
      user_count: 0,
      levels: []
    });
    const resp = await request(app)
      .post('/user/registration')
      .send({
        login: 'foo',
        phone: 79005554433,
        is_agree: true,
        email: 'foo@bar.com',
        password: 'qwe!@#eq@w!@#',
        password_confirm: 'qwe!@#eq@w!@#'
      })
      .expect(201);
    expect(resp.body.id).toEqual(1);

    const user = await models.User.findById(resp.body.id);
    expect(user.referral_level_id).toEqual(level.id);
  });

  test('registration invalid zxcvbn', async () => {
    const resp = await request(app)
      .post('/user/registration')
      .send({
        phone: 79005554433,
        is_agree: true,
        login: 'user',
        email: 'user@user.com',
        password: '123456',
        password_confirm: '123456'
      })
      .set('Cookie', ['lang=en'])
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400);

    await request(app)
      .post('/user/registration')
      .send({
        phone: 79005554433,
        is_agree: true,
        login: 'user',
        email: 'user@user.com',
        password: '123456',
        password_confirm: '123456'
      })
      .set('Cookie', ['lang=ru'])
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400);
  });

  test('registration invalid - empty', async () => {
    await request(app)
      .post('/user/registration')
      .send({ is_agree: true })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400);
  });

  test('registration referral', async () => {
    const user = await models.User.create({
      login: 'owner',
      email: 'owner@owner.com',
      password: '',
      is_active: true
    });

    await request(app)
      .post('/user/registration')
      .send({
        phone: 79005554433,
        is_agree: true,
        is_confirm: true,
        login: 'foo',
        email: 'foo@bar.com',
        password: 'qwe!@#eq@w!@#',
        password_confirm: 'qwe!@#eq@w!@#',
        referral_id: user.id
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
      .then(async response => {
        expect(response.body.id).toEqual(2);
        expect(await models.User.count()).toEqual(2);
      });

    await request(app)
      .post('/user/registration')
      .send({
        phone: 79005554433,
        is_agree: true,
        is_confirm: true,
        login: 'foo1',
        email: 'foo1@bar.com',
        password: 'qwe!@#eq@w!@#',
        password_confirm: 'qwe!@#eq@w!@#',
        referral_id: 'foo'
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
      .then(async response => {
        expect(response.body.id).toEqual(3);
        expect(await models.User.count()).toEqual(3);
      });

    const target = await models.User.findById(3);
    expect(target.parents).toEqual([1, 2]);
    expect(target.owner_id).toEqual(1);
    expect(target.level).toEqual(2);

    await request(app)
      .post('/user/registration')
      .send({
        phone: 79005554433,
        is_agree: true,
        is_confirm: true,
        login: 'foo2',
        email: 'foo2@bar.com',
        password: 'qwe!@#eq@w!@#',
        password_confirm: 'qwe!@#eq@w!@#',
        referral_id: 2
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
      .then(async response => {
        expect(response.body.id).toEqual(4);
        expect(await models.User.count()).toEqual(4);
      });

    const last = await models.User.findById(4);
    expect(last.parents).toEqual([1, 2]);
    expect(last.owner_id).toEqual(1);
    expect(last.level).toEqual(2);
  });

  test('restore', async () => {
    await models.User.create({ email: 'user@example.com' });
    await request(app)
      .post('/user/restore')
      .send({ email: 'user@example.com' })
      .expect(204);
  });

  test('phone confirm', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      is_active: true,
      is_phone_confirmed: false,
      phone_code: '1234'
    });
    await request(app)
      .post('/user/phone/confirm')
      .send({ phone_code: 1234 })
      .set({ 'Authorization': jwt.signUser(user) })
      .expect(200);
    await user.reload();
    expect(user.phone_code).toEqual(null);
    expect(user.is_phone_confirmed).toEqual(true);
  });

  test('phone set', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      is_active: true
    });

    await request(app)
      .post('/user/phone')
      .send({ phone: '900-526-77-77' })
      .set({ 'Authorization': jwt.signUser(user) })
      .expect(204);
    await request(app)
      .post('/user/phone')
      .send({ phone: '7900-526-77-77' })
      .set({ 'Authorization': jwt.signUser(user) })
      .expect(204);
    await request(app)
      .post('/user/phone')
      .send({ phone: '+12133734253' })
      .set({ 'Authorization': jwt.signUser(user) })
      .expect(204);
    await request(app)
      .post('/user/phone')
      .send({ phone: 79005002121 })
      .set({ 'Authorization': jwt.signUser(user) })
      .expect(204);

    await user.reload();
    expect(user.phone).toBeTruthy();
    expect(user.phone_code).toBeTruthy();
  });

  test('phone resend', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      is_active: true,
      is_phone_confirmed: false,
      phone_code: '1234'
    });
    await request(app)
      .post('/user/phone/resend')
      .set({ 'Authorization': jwt.signUser(user) })
      .expect(204);
    expect(smsService.send.mock.calls.length).toBe(1);
  });

  test('change_password', async () => {
    const user = await models.User.create({
      login: 'user',
      email: 'user@example.com',
      password: await passwordUtil.hash('123456'),
      is_active: true
    });
    await request(app)
      .post('/user/change_password')
      .send({ password_current: '123456', password: '654321', password_confirm: '654321' })
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect(204);
  });

  test('change_password invalid #2', done => {
    passwordUtil.hash('123456').then(hash => {
      models.User.create({
        email: 'user@example.com',
        password: hash,
        is_active: true
      }).then(user => {
        request(app)
          .post('/user/change_password')
          .send({ password_current: '654321', password: '654321', password_confirm: '654321' })
          .set({ 'Accept': 'application/json', 'Authorization': jwt.sign({ id: user.id }) })
          .expect(400, done);
      });
    });
  });

  test('change_password invalid', done => {
    passwordUtil.hash('123456').then(hash => {
      models.User.create({
        email: 'user@example.com',
        password: hash,
        is_active: true
      }).then(user => {
        request(app)
          .post('/user/change_password')
          .send({ password_current: '123456', password: '654321', password_confirm: '555666' })
          .set({ 'Accept': 'application/json', 'Authorization': jwt.sign({ id: user.id }) })
          .expect(400, done);
      });
    });
  });

  test('registration/resend', done => {
    passwordUtil.hash('123456').then(hash => {
      models.User.create({ email: 'user@example.com', password: hash }).then(user => {
        request(app)
          .post('/user/registration/resend')
          .send({ email: 'user@example.com' })
          .set({ 'Accept': 'application/json', 'Authorization': jwt.sign({ id: user.id }) })
          .expect(204, done);
      });
    });
  });

  test('restore/confirm', done => {
    passwordUtil.hash('123456').then(hash => {
      models.User.create({ email: 'user@example.com', password: hash }).then(user => {
        request(app)
          .post('/user/restore/confirm')
          .send({ token: user.token, password: '654321', password_confirm: '654321' })
          .set({ 'Accept': 'application/json' })
          .expect(200)
          .then(response => {
            expect(response.body.user.id).toEqual(user.id);
            expect(response.body.jwt).toBeTruthy();
            done();
          });
      });
    });
  });

  test('registration/confirm', done => {
    passwordUtil.hash('123456').then(hash => {
      models.User.create({ email: 'user@example.com', password: hash }).then(user => {
        request(app)
          .post('/user/registration/confirm')
          .send({ token: user.token })
          .set({ 'Accept': 'application/json' })
          .expect(200)
          .then(response => {
            expect(response.body.user.id).toEqual(user.id);
            expect(response.body.jwt).toBeTruthy();
            done();
          });
      });
    });
  });
});
