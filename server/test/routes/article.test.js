const request = require('supertest');
const app = require('../../../app');
const models = require('../../models');
const jwt = require('../../util/jwt');

describe('routes/article', function () {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  it('crud & permissions', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true
    });

    const userHeaders = {
      Accept: 'application/json',
      Authorization: jwt.signUser(user)
    };

    expect(await models.Article.count()).toEqual(0);

    const demo = {
      title: 'hello world',
      sub_title: 'hello world',
      content: 'hello world',
      content_short: 'hello world'
    };

    await request(app)
      .post('/article')
      .send(demo)
      .set(userHeaders)
      .expect(403);

    await user.update({ is_admin: true });

    const adminHeaders = {
      ...userHeaders,
      Authorization: jwt.signUser(user)
    };

    await request(app)
      .post('/article')
      .send(demo)
      .set(adminHeaders)
      .expect(201);

    expect(await models.Article.count()).toEqual(1);

    await request(app)
      .get('/article')
      .set(adminHeaders)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.objects.length).toEqual(1);
        expect(response.body.objects[0].title).toEqual('hello world');
        expect(response.body.meta).toEqual({
          page: 1,
          page_count: 1,
          page_size: 100,
          total: 1
        });
      });

    await request(app)
      .post('/article/1')
      .send({
        title: 'foo bar'
      })
      .set(adminHeaders)
      .expect(204);

    expect(await models.Article.count()).toEqual(1);

    const article = await models.Article.findById(1);
    expect(article.title).toEqual('foo bar');

    await request(app)
      .delete('/article/1')
      .set(adminHeaders)
      .expect(204);
    expect(await models.Article.count()).toEqual(0);
  });
});
