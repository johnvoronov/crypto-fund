const request = require('supertest');
const app = require('../../../app');
const models = require('../../models');
const jwt = require('../../util/jwt');

describe('routes/faq', function () {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  it('crud & permissions', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true
    });

    const userHeaders = {
      Accept: 'application/json',
      Authorization: jwt.signUser(user)
    };

    expect(await models.Faq.count()).toEqual(0);

    const demo = {
      question_en: 'hello world',
      question_ru: 'hello world',
      answer_en: 'hello world',
      answer_ru: 'hello world',
      position: 0,
    };

    await request(app)
      .post('/faq')
      .send(demo)
      .set(userHeaders)
      .expect(403);

    await user.update({ is_admin: true });

    const adminHeaders = {
      ...userHeaders,
      Authorization: jwt.signUser(user)
    };

    await request(app)
      .post('/faq')
      .send(demo)
      .set(adminHeaders)
      .expect(201);

    expect(await models.Faq.count()).toEqual(1);

    await request(app)
      .get('/faq')
      .set(adminHeaders)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.length).toEqual(1);
        expect(response.body[0].question_en).toEqual('hello world');
      });

    await request(app)
      .post('/faq/1')
      .send({
        question_en: 'foo bar'
      })
      .set(adminHeaders)
      .expect(204);

    expect(await models.Faq.count()).toEqual(1);

    const article = await models.Faq.findById(1);
    expect(article.question_en).toEqual('foo bar');

    await request(app)
      .delete('/faq/1')
      .set(adminHeaders)
      .expect(204);
    expect(await models.Faq.count()).toEqual(0);
  });

  it('question', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true
    });

    const headers = {
      Accept: 'application/json',
      Authorization: jwt.signUser(user)
    };

    expect(await models.Question.count()).toEqual(0);

    await request(app)
      .post('/faq/question')
      .send({
        question: 'hello world',
        subject: 'hello world',
      })
      .set(headers)
      .expect(204);

    expect(await models.Question.count()).toEqual(1);
  });
});
