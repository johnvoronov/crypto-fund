const sinon = require('sinon');
const authTest = require('../../middleware/auth');
const jwt = require('../../util/jwt');

describe('Test Auth Middleware', function () {
  let request;
  let response;
  let next;

  beforeEach(function () {
    request = {
      query: {},
      body: {},
      headers: {}
    };
    response = {
      status: sinon.stub().returnsThis(),
      json: sinon.spy(),
      boom: {
        unauthorized: sinon.spy()
      }
    };
    next = sinon.spy();
  });

  it('next should not be called if no token provided', function () {
    authTest(request, response, next);
    expect(next.called).toEqual(false);
  });

  it('should return 401 status code if no token provided', function () {
    authTest(request, response, next);
    expect(response.boom.unauthorized.called).toEqual(true);
  });

  it('next should not be called if bad token was provided', function () {
    request.headers.authorization = 'some authorization header';
    authTest(request, response, next);
    expect(next.called).toEqual(false);
  });

  it('request should contain user info if good token was provided', function () {
    request.headers.authorization = jwt.sign({ id: 1 });
    authTest(request, response, function next() {
      expect(request).to.have.property('user');
      expect(request.user).to.have.property('id');
      expect(request.user.id).toEqual(1);
    });
  });
});
