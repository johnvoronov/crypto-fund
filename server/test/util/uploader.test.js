const fs = require('fs');
const path = require('path');
const uploader = require('../../util/uploader');

describe('util/uploader', () => {
  it('upload', async () => {
    const file = {
      name: 'cat.jpeg',
      data: fs.readFileSync(path.resolve(__dirname, 'cat.png')),
      mv: () => Promise.resolve(null)
    };

    expect(await uploader.uploadFile(file)).toBeTruthy();
  });

  it('thumb', async () => {
    const file = {
      name: 'cat.jpeg',
      data: fs.readFileSync(path.resolve(__dirname, 'cat.png')),
      mv: () => Promise.resolve(null)
    };

    expect(await uploader.uploadThumb(file, 80, 80)).toBeTruthy();
  });
});
