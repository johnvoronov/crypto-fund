const models = require('../../models');
const calc = require('../../util/calc');

describe('util/calc', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  test('calculate', async () => {
    const btc = await models.Currency.create({
      code: 'BTC',
      name: 'Bitcoin',
      to_btc: 1,
      to_usd: 100,
      to_eth: 10,
      to_rub: 1000
    });
    const eth = await models.Currency.create({
      code: 'ETH',
      name: 'Ethereum',
      to_btc: 0.1,
      to_usd: 10,
      to_eth: 1,
      to_rub: 100
    });
    expect(calc.calculate(1, eth, btc)).toEqual(0.1);
    expect(calc.calculate(1, btc, eth)).toEqual(10);
  });
});
