const currency = require('../../util/currency');
const models = require('../../models');

describe('util/currency', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  it('update - create usd if not exists', async () => {
    expect(await models.Currency.count()).toEqual(0);

    await models.Currency.create({ name: 'Bitcoin', code: 'BTC' });
    await models.Currency.create({ name: 'Ethereum', code: 'ETH' });
    await currency.update();

    expect(await models.Currency.count()).toEqual(4);

    const btc = await models.Currency.findOne({
      where: { code: 'BTC' }
    });
    expect(btc.to_usd).toBeTruthy();
    expect(btc.to_rub).toBeTruthy();
    expect(btc.to_eth).toBeTruthy();
    expect(btc.to_btc).toBeTruthy();
  });
});
