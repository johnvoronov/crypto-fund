const treeUtils = require('../../util/tree');

describe('util/tree', () => {
  const callback = async item => ({ ...item, foo: 'bar' });

  test('generate valid tree', async () => {
    const items = [
      { id: 1, referral_id: null },
      { id: 2, referral_id: 1 },
      { id: 3, referral_id: 2 },
      { id: 4, referral_id: 1 }
    ];

    expect(await treeUtils.generate(items, 'referral_id', callback)).toEqual([
      {
        id: 1,
        referral_id: null,
        foo: 'bar',
        children: [
          {
            id: 2,
            referral_id: 1,
            foo: 'bar',
            children: [
              {
                id: 3,
                referral_id: 2,
                foo: 'bar',
                children: []
              }
            ]
          },
          {
            id: 4,
            referral_id: 1,
            foo: 'bar',
            children: []
          }
        ]
      }
    ]);
  });

  test('generate custom root', async () => {
    const customRootItems = [
      { id: 2, referral_id: 1 },
      { id: 3, referral_id: 2 },
      { id: 4, referral_id: 1 }
    ];
    expect(await treeUtils.generate(customRootItems, 'referral_id', callback, node => node.referral_id === 1)).toEqual([
      {
        id: 2,
        referral_id: 1,
        foo: 'bar',
        children: [
          {
            id: 3,
            referral_id: 2,
            foo: 'bar',
            children: []
          }
        ]
      },
      {
        id: 4,
        referral_id: 1,
        foo: 'bar',
        children: []
      }
    ]);
  });

  test('generate real case', async () => {
    const newItems = [
      {
        'id': 5,
        'email': 'user5@demo.com',
        'referral_id': 2,
        'is_active': true,
        'owner_id': 1,
        'parents': [1, 2],
        'level': 2,
        'avatar': null,
        'login': 'user5',
        'minimal_referral_level_id': null,
        'referral_level_id': 5
      },
      {
        'id': 3,
        'email': 'user3@demo.com',
        'referral_id': 2,
        'is_active': true,
        'owner_id': 1,
        'parents': [1, 2],
        'level': 2,
        'avatar': null,
        'login': 'user3',
        'minimal_referral_level_id': null,
        'referral_level_id': 5
      },
      {
        'id': 4,
        'email': 'user4@demo.com',
        'referral_id': 3,
        'is_active': true,
        'owner_id': 1,
        'parents': [1, 2, 3],
        'level': 3,
        'avatar': null,
        'login': 'user4',
        'minimal_referral_level_id': null,
        'referral_level_id': 5
      },
      {
        'id': 2,
        'email': 'user2@demo.com',
        'referral_id': 1,
        'is_active': true,
        'owner_id': 1,
        'parents': [1],
        'level': 1,
        'avatar': null,
        'login': 'user2',
        'minimal_referral_level_id': null,
        'referral_level_id': 5
      },
      {
        'id': 1,
        'email': 'user@demo.com',
        'referral_id': null,
        'is_active': true,
        'owner_id': null,
        'parents': null,
        'level': 0,
        'avatar': null,
        'login': 'user',
        'minimal_referral_level_id': null,
        'referral_level_id': 5
      }
    ];
    expect(await treeUtils.generate(newItems, 'referral_id', callback)).toEqual([
      {
        'avatar': null,
        'children': [{
          'avatar': null,
          'children': [{
            'avatar': null,
            'children': [],
            'email': 'user5@demo.com',
            'foo': 'bar',
            'id': 5,
            'is_active': true,
            'level': 2,
            'login': 'user5',
            'minimal_referral_level_id': null,
            'owner_id': 1,
            'parents': [1, 2],
            'referral_id': 2,
            'referral_level_id': 5
          }, {
            'avatar': null,
            'children': [{
              'avatar': null,
              'children': [],
              'email': 'user4@demo.com',
              'foo': 'bar',
              'id': 4,
              'is_active': true,
              'level': 3,
              'login': 'user4',
              'minimal_referral_level_id': null,
              'owner_id': 1,
              'parents': [1, 2, 3],
              'referral_id': 3,
              'referral_level_id': 5
            }],
            'email': 'user3@demo.com',
            'foo': 'bar',
            'id': 3,
            'is_active': true,
            'level': 2,
            'login': 'user3',
            'minimal_referral_level_id': null,
            'owner_id': 1,
            'parents': [1, 2],
            'referral_id': 2,
            'referral_level_id': 5
          }],
          'email': 'user2@demo.com',
          'foo': 'bar',
          'id': 2,
          'is_active': true,
          'level': 1,
          'login': 'user2',
          'minimal_referral_level_id': null,
          'owner_id': 1,
          'parents': [1],
          'referral_id': 1,
          'referral_level_id': 5
        }],
        'email': 'user@demo.com',
        'foo': 'bar',
        'id': 1,
        'is_active': true,
        'level': 0,
        'login': 'user',
        'minimal_referral_level_id': null,
        'owner_id': null,
        'parents': null,
        'referral_id': null,
        'referral_level_id': 5
      }
    ]);
  });
});
