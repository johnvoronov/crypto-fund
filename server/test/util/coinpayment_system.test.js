const { callback } = require('../../util/coinpayment_system');
const settingService = require('../../util/settings');
const consts = require('../../consts');
const models = require('../../models');

describe('util/coinpayment_events', () => {
  let user;
  let currency;
  let wallet;

  async function prepare(data = {}, deduct_coinpayments_tax = false) {
    user = await models.User.create({
      email: 'oleg@gazmanov.com',
      password: '12-chasov-nochi-blyad'
    });

    currency = await models.Currency.create({
      code: 'BTC',
      ...data
    });

    wallet = await models.Wallet.create({
      address: 'test',
      user_id: user.id,
      is_internal: true,
      currency_id: currency.id
    });
  }

  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  afterEach(async () => {
    jest.resetAllMocks();
  });

  const defaultPayment = {
    txn_id: '1',
    fee: 0,
    to_usd: 0,
    conversion: 'USD',
    address: 'test'
  };

  test('pending', async () => {
    await prepare({ to_usd: 123 });

    const { payment } = await callback(consts.PAYMENT_PENDING, {
      amount: 1,
      currency: 'test',
      ...defaultPayment
    });
    expect(payment.status).toEqual(consts.PAYMENT_PENDING);
  });

  test('complete', async () => {
    await prepare({ to_usd: 123 });

    const { payment } = await callback(consts.PAYMENT_COMPLETE, {
      amount: 1,
      currency: 'test',
      ...defaultPayment
    });
    expect(payment.status).toEqual(consts.PAYMENT_COMPLETE);
  });

  test('fail', async () => {
    await prepare({ to_usd: 123 });

    const { payment } = await callback(consts.PAYMENT_ERROR, {
      amount: 1,
      currency: 'test',
      ...defaultPayment
    });
    expect(payment.status).toEqual(consts.PAYMENT_ERROR);
  });

  test('real#1', async () => {
    await prepare({ to_usd: 123 });

    const { payment } = await callback(consts.PAYMENT_PENDING, {
      amount: 1,
      currency: 'test',
      ...defaultPayment
    });
    expect(payment.status).toEqual(consts.PAYMENT_PENDING);
    expect(payment.amount).toEqual(1);
  });

  test('coinpayments fee', async () => {
    await settingService.setValue('deduct_coinpayments_tax', true);
    await prepare({ to_usd: 466 }, true);

    const { payment } = await callback(consts.PAYMENT_PENDING, {
      ...defaultPayment,
      amount: 1,
      fee: 0.1,
      currency: 'test'
    });

    expect(payment.status).toEqual(consts.PAYMENT_PENDING);
    expect(payment.amount).toEqual(0.9);
  });

  test('coinpayments deduct fee', async () => {
    await prepare({ to_usd: 466 }, false);

    const { payment } = await callback(consts.PAYMENT_PENDING, {
      ...defaultPayment,
      amount: 1,
      fee: 0.1,
      currency: 'test'
    });

    expect(payment.status).toEqual(consts.PAYMENT_PENDING);
    expect(payment.amount).toEqual(1);
  });
});
