const settings = require('../../util/settings');
const models = require('../../models');

describe('util/settings', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  it('fetch', async () => {
    await models.Setting.create({
      path: 'support_email',
      value: 'user@admin.com'
    });

    expect(await models.Setting.count()).toEqual(1);

    const params = await settings.fetch();
    expect(params.support_email).toEqual('user@admin.com');
  });

  it('setValue', async () => {
    expect(await models.Setting.count()).toEqual(0);

    await settings.setValue('token_price', 1);
    expect(await settings.getValue('token_price')).toEqual("1");
    expect(await models.Setting.count()).toEqual(1);
    await settings.setValue('token_price', 2);
    expect(await settings.getValue('token_price')).toEqual("2");
    expect(await models.Setting.count()).toEqual(1);

    expect(await settings.fetch(false)).toEqual({
      token_price: "2"
    });
  });

  it('getValue', async () => {
    const setting = await models.Setting.create({
      path: 'token_price',
      value: 1
    });

    expect(await settings.getValue('token_price')).toEqual("1");
    expect(await settings.getInt('token_price')).toEqual(1);
    expect(await settings.getFloat('token_price')).toEqual(1.0);
    await setting.update({ value: null });

    expect(await settings.getValue('token_price')).toEqual(null);
    expect(await settings.getValue('token_price', 1)).toEqual(1);
    await setting.update({ value: 0 });

    expect(await settings.getValue('token_price', 2)).toEqual("0");
  });

  it('getFloat', async () => {
    expect(await settings.getFloat('token_price')).toEqual(0.0);
    expect(await settings.getFloat('token_price', 1)).toEqual(1.0);
  });

  it('getBool', async () => {
    expect(await settings.getBool('token_price')).toEqual(false);
    expect(await settings.getBool('token_price', true)).toEqual(true);
  });

  it('getInt', async () => {
    expect(await settings.getInt('token_price')).toEqual(0);
    expect(await settings.getInt('token_price', 1)).toEqual(1);
  });

  it('getValue', async () => {
    expect(await settings.getValue('token_price')).toEqual(null);
    expect(await settings.getValue('token_price', 1)).toEqual(1);
  });
});
