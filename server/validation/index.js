const Joi = require('joi');

const objectBase = Joi.object().options({ abortEarly: false });

module.exports = {
  objectBase,
};
