const jwt = require('jsonwebtoken');
const config = require('../config/jwt');

module.exports = {
  /**
   * @returns {string[]}
   */
  getPublicAttributes() {
    return [
      'id',
      'login',
      'email',
      'avatar',
      'is_active',
      'is_admin',
      'refresh_token',
      'is_phone_confirmed',
      'referral_id',
      'minimal_referral_level_id',
      'buy_referral_level_id',
      'referral_level_id',
      'owner_id',
      'parents',
      'created_at',
      'phone',
      'first_name',
      'level',
      'last_name',
      'country',
      'city',
      'btc_address'
    ];
  },

  /**
   * @param user
   * @returns {{user: *|{permissions}, jwt: *}}
   */
  response(user) {
    return {
      user: this.getPublicFields(user),
      jwt: this.signUser(user)
    };
  },

  /**
   * Возвращаем в виде объекта только доступные для публичного просмотра поля из модели User
   *
   * @param user
   * @returns object
   */
  getPublicFields(user) {
    const attributes = this.getPublicAttributes();

    let params = {};

    for (let i = 0; i < attributes.length; i++) {
      const attribute = attributes[i];
      params[attribute] = user[attribute];
    }

    let permissions = [];
    if (user.is_admin) {
      permissions.push('admin');
    }
    if (user.is_superuser) {
      permissions.push('superuser');
    }

    return { ...params, permissions };
  },

  getTokenFromRequest(req) {
    const queryToken = (req.query || {}).token || '';
    const auth = (req.headers || {}).authorization || '';
    if (queryToken.length === 0 && auth.length === 0) {
      return null;
    }

    // Поддерживаем два вида авторизации: Bearer и вариант в лоб
    const headerToken = auth.split(' ')[0] === 'Bearer' ? auth.split(' ')[1] : auth;
    return headerToken || queryToken;
  },

  verify(token) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, config.secret, (err, decodedToken) => {
        if (err || !decodedToken) {
          return reject(err)
        }

        resolve(decodedToken)
      })
    })
  },

  signUser(user) {
    return this.sign(this.getPublicFields(user));
  },

  sign(data = {}) {
    const options = {
      expiresIn: Math.floor(Date.now() / 1000) + (60 * 60) // 1 hour
    };

    return jwt.sign(data, config.secret, options);
  }
};
