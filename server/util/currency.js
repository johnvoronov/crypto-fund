const rp = require('request-promise');
const models = require('../models');

module.exports = {
  url(name, convert) {
    return `https://api.coinmarketcap.com/v1/ticker/${name.toLowerCase()}/?convert=${convert}`;
  },

  async fetch(name) {
    const rub = await rp({ uri: this.url(name.toLowerCase(), 'RUB'), json: true });
    const eth = await rp({ uri: this.url(name.toLowerCase(), 'ETH'), json: true });
    const result = {
      ...eth.pop(),
      ...rub.pop()
    };

    return {
      to_usd: result.price_usd,
      to_eth: result.price_eth,
      to_btc: result.price_btc,
      to_rub: result.price_rub
    };
  },

  /**
   * Do not use findOrCreate & findOrUpdate
   * TODO https://blockchaincorp.atlassian.net/browse/CL-338
   * TODO https://blockchaincorp.atlassian.net/browse/CL-340
   *
   * @returns {Promise<void>}
   */
  async findOrCreateFix(defaults) {
    let entity = await models.Currency.findOne({ where: defaults });
    if (null === entity) {
      entity = await models.Currency.create(defaults);
    }
    return await entity.update(await this.fetch(entity.name));
  },

  async update() {
    const btc = await this.findOrCreateFix({ name: 'Bitcoin', code: 'BTC' });
    const eth = await this.findOrCreateFix({ name: 'Ethereum', code: 'ETH' });
    await this.fiatCurrencies(btc, eth);
  },

  async updateRub(btc, eth, tether) {
    let rub = await models.Currency.findOne({
      where: { code: 'RUB' }
    });
    if (null === rub) {
      rub = await models.Currency.create({ name: 'Russian Rouble', code: 'RUB' });
    }
    await rub.update({
      to_rub: 1,
      to_eth: 1 / eth.to_rub,
      to_btc: 1 / btc.to_rub,
      to_usd: 1 / tether.to_rub
    });
  },

  async updateUsd(btc, eth, tether) {
    let usd = await models.Currency.findOne({
      where: { code: 'USD' }
    });
    if (null === usd) {
      usd = await models.Currency.create({ name: 'American Dollar', code: 'USD' });
    }
    await usd.update({
      to_usd: 1,
      to_eth: 1 / eth.to_usd,
      to_btc: 1 / btc.to_usd,
      to_rub: tether.to_rub
    });
  },

  async fiatCurrencies(btc, eth) {
    const tether = await this.fetch('tether');
    await this.updateRub(btc, eth, tether);
    await this.updateUsd(btc, eth, tether);
  }
};
