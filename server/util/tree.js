module.exports = {
  async generate(rows, field, itemCallback = item => item, isRoot = null) {
    const items = [];
    for (let i = 0; i < rows.length; i++) {
      items.push(await itemCallback(rows[i]));
    }

    const process = item => ({ ...item, children: traverse(item.id) });

    const traverse = parentId => {
      return items
        .filter(each => each[field] === parentId)
        .map(process);
    };

    const itIsRoot = node => isRoot ? isRoot(node) : !node[field];

    return items.filter(itIsRoot).map(process);
  }
};
