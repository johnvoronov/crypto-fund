const Coinpayments = require('coinpayments');
const rp = require('request-promise');
const qs = require('querystring');
const crypto = require('crypto');
const settings = require('./settings');

module.exports = {
  async client() {
    const params = await this.getParameters();

    return new Coinpayments({
      key: params.publicKey,
      secret: params.secretKey
    });
  },

  async getParameters() {
    const params = await settings.fetch(false);

    return {
      publicKey: params.coinpayments_public_key,
      secretKey: params.coinpayments_secret_key,
      ipnSecret: params.coinpayments_ipn_secret,
      merchantId: params.coinpayments_merchant_id
    }
  },

  async fakePayment(uri, amount, address) {
    const params = await this.getParameters();

    const parameters = {
      txn_id: Date.now(),
      fee: 0.01,
      ipn_mode: 'hmac',
      status: 100,
      merchant: params.merchantId,
      address,
      amount
    };

    const hmac = crypto
      .createHmac('sha512', params.ipnSecret)
      .update(qs.stringify(parameters).replace(/%20/g, '+'))
      .digest('hex');

    return rp({
      method: 'POST',
      uri,
      body: parameters,
      json: true,
      headers: {
        HMAC: hmac
      }
    });
  }
};
