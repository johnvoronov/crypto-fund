module.exports = (amount, entity) => ({
  usd: amount * entity.to_usd,
  rub: amount * entity.to_rub,
  eth: amount * entity.to_eth,
  btc: amount * entity.to_btc,

  to_rub: entity.to_rub,
  to_eth: entity.to_eth,
  to_usd: entity.to_usd,
  to_btc: entity.to_btc
});
