module.exports = {
  calculate(amount, sourceCurrency, targetCurrency) {
    const code = targetCurrency.code.toLowerCase();
    return amount * sourceCurrency[`to_${code}`];
  }
};
