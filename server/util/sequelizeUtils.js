module.exports = {
  getOrderBy(definition, orderBy, defaultOrder = []) {
    const allowed = Object.keys(definition.rawAttributes);

    if (false === Array.isArray(orderBy)) {
      orderBy = [orderBy];
    }

    if (orderBy.length === 0) {
      return defaultOrder;
    }

    const result = [];
    for (let i = 0; i < orderBy.length; i++) {
      const value = orderBy[i];
      if (value) {
        const isDesc = value.charAt(0) === '-';
        const field = isDesc ? value.slice(1) : value;
        if (allowed.indexOf(field) > -1) {
          result.push([field, isDesc ? 'DESC' : 'ASC']);
        }
      }
    }

    return result;
  }
};
