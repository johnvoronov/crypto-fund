const config = require('../config/app');
const uuidv4 = require('uuid/v4');
const sharp = require('sharp');

module.exports = {
  getPublicUrl(path) {
    return [config.public_url, path].join('/');
  },

  generatePath() {
    return [uuidv4(), 'png'].join('.');
  },

  /**
   * req.files.foo.name: "car.jpg"
   * req.files.foo.mv: A function to move the file elsewhere on your server
   * req.files.foo.mimetype: The mimetype of your file
   * req.files.foo.data: A buffer representation of your file
   * req.files.foo.truncated: A boolean that represents if the file is over the size limit
   *
   * @param file
   *
   * @returns {Promise<any>}
   */
  uploadFile(file) {
    const filepath = this.generatePath();

    const absPath = [
      config.upload_directory,
      filepath
    ].join('/');

    return file.mv(absPath).then(() => this.getPublicUrl(filepath));
  },

  uploadThumb(file, width = 640, height = 640) {
    const filepath = this.generatePath();

    const absPath = [
      config.upload_directory,
      filepath
    ].join('/');

    return sharp(file.data)
      .resize({
        width,
        height,
        fit: 'cover',
        position: sharp.strategy.entropy
      })
      .png()
      .toFile(absPath)
      .then(() => this.getPublicUrl(filepath));
  }
};
