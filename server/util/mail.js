const path = require('path');
const nunjucks = require('../service/template');
const htmlToText = require('nodemailer-html-to-text').htmlToText;
const nodemailer = require('nodemailer');
const config = require('../config/mail');
const mailgunConfig = require('../config/mailgun');
const appConfig = require('../config/app');
const mailgun = require('mailgun-js');

module.exports = {
  createTransport(config) {
    const transport = nodemailer.createTransport(config);

    // https://www.npmjs.com/package/html-to-text
    transport.use('compile', htmlToText({ wordwrap: 130 }));

    return transport;
  },

  render(template, data) {
    const subject = String(nunjucks.render(path.join(template, 'subject.txt'), data)).trim();
    const html = String(nunjucks.render(path.join(template, 'body.html'), { ...data, subject })).trim();
    const text = String(nunjucks.render(path.join(template, 'body.txt'), { ...data, subject })).trim();

    return {
      subject,
      html,
      text
    };
  },

  async send(to, template, data, transport = null, from = null) {
    const { subject, html, text } = this.render(template, {
      ...data,
      app_name: appConfig.name,
      backend_url: appConfig.backend_url,
      frontend_url: appConfig.frontend_url
    });

    const options = {
      to,
      text,
      html,
      subject
    };

    if (null === from) {
      from = config.from;
    }

    if (mailgunConfig.apiKey && mailgunConfig.domain) {
      return new Promise((resolve, reject) => {
        mailgun(mailgunConfig).messages().send({
          ...options,
          from
        }, (error, body) => {
          if (error) {
            reject(error);
          } else {
            resolve(body);
          }
        });
      });
    } else {
      if (null === transport) {
        transport = this.createTransport(config.transport);
      }

      return transport.sendMail({
        ...options,
        from
      });
    }
  }
};
