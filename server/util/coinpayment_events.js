const models = require('../models');
const getRate = require('./paymentHelper');

module.exports = {
  /**
   * @param status
   * @param data
   * @returns {Promise<any>}
   */
  async callback(status, data) {
    const wallet = await models.Wallet.findOne({
      where: { address: data.address }
    });

    if (!wallet) {
      throw new Error('Wallet not found');
    }

    const payment = await models.Payment.findOne({
      where: {
        transaction_id: String(data.txn_id),
        user_id: wallet.user_id
      }
    });

    if (payment) {
      return {
        wallet,
        payment: await payment.update({ status })
      };

    } else {
      const strategy = await wallet.getStrategy();
      const currency = await strategy.getConvert();

      // Вычитать из суммы к зачислению комиссию coinpayments
      const amount = strategy.deduct_coinpayments_tax ? data.amount - data.fee : data.amount;

      const payment = await models.Payment.create({
        currency_id: currency.id,
        strategy_id: wallet.strategy_id,
        wallet_id: wallet.id,
        user_id: wallet.user_id,
        transaction_id: data.txn_id,
        fee: data.fee,
        amount,
        status,
        ...getRate(data.amount, currency)
      });

      return {
        wallet,
        payment
      };
    }
  }
};
