const Setting = require('../models').Setting;

const secretSettings = [
  'coinpayments_public_key',
  'coinpayments_secret_key',
  'coinpayments_ipn_secret',
  'coinpayments_merchant_id',

  'smtp_from',
  'smtp_host',
  'smtp_port',
  'smtp_password',
  'smtp_username',

  'social_facebook_client_secret',
  'social_google_client_secret',
  'social_twitter_client_secret'
];

module.exports = {
  /**
   * Фильтруем только публичные настройки
   *
   * @param rows
   * @param onlyPublic
   * @returns {Object}
   * @private
   */
  _filterSettings(rows, onlyPublic = true) {
    let parameters = {};

    for (let i = 0; i < rows.length; i++) {
      const {
        path,
        value
      } = rows[i];

      if (onlyPublic && secretSettings.indexOf(path) > -1) {
        continue;
      }

      parameters[path] = value;
    }

    return parameters;
  },

  /**
   * Получение полного списка настроек из бд
   *
   * @returns {Promise<Object>}
   */
  async fetch(onlyPublic = true) {
    return this._filterSettings(await Setting.findAll(), onlyPublic);
  },

  /**
   * @param path
   * @param value
   * @returns {Promise<*>}
   */
  async setValue(path, value) {
    const setting = await Setting.findById(path);
    if (setting) {
      return await setting.update({ value });
    } else {
      return await Setting.create({ path, value })
    }
  },

  /**
   * @param path
   * @param defaultValue
   * @returns {Promise<*>}
   */
  async getValue(path, defaultValue = null) {
    const param = await Setting.findOne({
      where: { path }
    });

    if (
      null === param
      || param && null === param.value
    ) {
      return defaultValue;
    }

    return param.value;
  },

  /**
   * @param path
   * @param defaultValue
   * @returns {Promise<*>}
   */
  async getFloat(path, defaultValue = 0.0) {
    return parseFloat(await this.getValue(path, defaultValue));
  },

  /**
   * @param path
   * @param defaultValue
   * @returns {Promise<*>}
   */
  async getInt(path, defaultValue = 0) {
    return parseInt(await this.getValue(path, defaultValue), 10);
  },

  /**
   * @param path
   * @param defaultValue
   * @returns {Promise<*>}
   */
  async getBool(path, defaultValue = false) {
    return Boolean(await this.getValue(path, defaultValue));
  }
};
