const models = require('../models');
const Op = models.sequelize.Op;
const consts = require('../consts');

module.exports = {
  async _fetch(column, where, extra = {}) {
    return await models.Transaction.sum(column, { where, ...extra }) || 0;
  },

  getIncomeBalance(where) {
    return this._fetch('amount', {
      ...where,
      status: consts.TRANSACTION_INCOME
    });
  },

  /**
   * @param where
   * @returns {Promise<float>}
   */
  async getWithdrawBalance(where) {
    return await this._fetch('amount', {
      ...where,
      status: consts.TRANSACTION_WITHDRAW
    });
  },

  async getUserBalance(where) {
    return await this._fetch('amount', {
      ...where,
      status: {
        [Op.notIn]: [
          consts.TRANSACTION_FUND_INCOME,
          consts.TRANSACTION_ONE_TIME_BONUS
        ]
      },
      is_freeze: true
    });
  },

  async getDepositBalance(where) {
    return await this._fetch('amount', {
      ...where,
      is_deposit: true,
      is_freeze: false
    });
  },

  async getFreezeDepositBalance(where) {
    return await this._fetch('amount', {
      ...where,
      is_deposit: true,
      is_freeze: true
    });
  },

  /**
   * @DEPRECATED
   * @param where
   * @returns {Promise<*>}
   */
  async getActiveDepositBalance(where) {
    return await this._fetch('amount', {
      ...where,
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      is_deposit: true,
      is_freeze: false
    });
  },

  /**
   * @DEPRECATED
   * @param where
   * @returns {Promise<*>}
   */
  async getInactiveDepositBalance(where) {
    return await this._fetch('amount', {
      ...where,
      status: {
        [Op.in]: [
          consts.TRANSACTION_INCOME,
          consts.TRANSACTION_DEPOSIT
        ]
      },
      is_deposit: true,
      is_freeze: true
    });
  },

  async getFundBalance() {
    return await this._fetch('amount', {
      is_income: true
    });
  },

  async getPartnershipBalance(where) {
    return await this._fetch('amount', {
      ...where,
      account_type: consts.ACCOUNT_TYPE_PARTNERSHIP
    });
  },

  async multiCurrency(where, extra = {}) {
    return {
      btc: await this._fetch('btc', where, extra),
      usd: await this._fetch('usd', where, extra),
      rub: await this._fetch('rub', where, extra),
      eth: await this._fetch('eth', where, extra)
    }
  },

  async getResponse(where) {
    return {
      total: await this.multiCurrency({
        ...where,
        account_type: consts.ACCOUNT_TYPE_STRATEGY
      }),
      freeze: await this.multiCurrency({
        ...where,
        account_type: consts.ACCOUNT_TYPE_STRATEGY,
        status: consts.TRANSACTION_FREEZE
      }),
      deposit: await this.multiCurrency({
        ...where,
        account_type: consts.ACCOUNT_TYPE_STRATEGY
      }),
      partnership: await this.multiCurrency({
        ...where,
        status: {
          [Op.in]: [
            consts.TRANSACTION_ONE_TIME_BONUS,
            consts.TRANSACTION_PRO_BONUS,
            consts.TRANSACTION_PAY_STATUS_BONUS,
            consts.TRANSACTION_WITHDRAW,
            consts.TRANSACTION_PARTNER_INCOME
          ]
        },
        account_type: consts.ACCOUNT_TYPE_PARTNERSHIP
      }),
      balance: await this.multiCurrency({
        ...where,
        account_type: consts.ACCOUNT_TYPE_STRATEGY,
        is_freeze: true
      }),
      income: await this.multiCurrency({
        ...where,
        account_type: consts.ACCOUNT_TYPE_STRATEGY,
        status: {
          [Op.in]: [
            consts.TRANSACTION_INCOME
          ]
        }
      })
    };
  }
};
