const path = require('path');
const i18n = require('i18n');

i18n.configure({
  defaultLocale: 'en',
  locales: [
    'en',
    'ru'
  ],
  cookie: 'lang',
  queryParameter: 'lang',
  directory: path.resolve(__dirname, '../locales'),
  api: {
    '__': 't',
    '__n': 'tn'
  }
});

module.exports = i18n;
