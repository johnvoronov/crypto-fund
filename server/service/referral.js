const { Op } = require('sequelize');
const models = require('../models');
const consts = require('../consts');
const referralHelper = require('./referralHelper');
const transactionService = require('./transaction');
const treeUtil = require('../util/tree');
const settingsService = require('../util/settings');

module.exports = {
  /**
   * Сетка с уровнями реферальной системы
   *
   * @returns {*[]}
   */
  getGrid() {
    return models.Referral.findAll({
      order: [
        ['price', 'DESC']
      ]
    });
  },

  /**
   * Валидация соответствия уровню
   *
   * @param level
   * @param userAmount
   * @param teamAmount
   * @param userCount
   * @returns {*|boolean}
   */
  validateLevel(level, userAmount, teamAmount, userCount) {
    return this.validateUserAmount(level, userAmount)
      && this.validateTeamAmount(level, teamAmount)
      && this.validateUserCount(level, userCount);
  },

  /**
   * Валидация суммы персональных инвестиций
   *
   * @param level
   * @param userAmount
   * @returns {boolean}
   */
  validateUserAmount(level, userAmount) {
    return userAmount >= (level.user || 0);
  },

  /**
   * Валидация суммы коммандных инвестиций
   *
   * @param level
   * @param teamAmount
   * @returns {boolean}
   */
  validateTeamAmount(level, teamAmount) {
    return teamAmount >= (level.team || 0);
  },

  /**
   *  Валидация количества приглашенных пользователей
   *
   * @param level
   * @param userCount
   * @returns {boolean}
   */
  validateUserCount(level, userCount) {
    return userCount >= (level.user_count || 0);
  },

  /**
   * Получить количество рефералов пользователя
   *
   * @param user
   * @returns {Promise<*|number>}
   */
  async getReferralCount(user) {
    return await models.User.count({
      where: {
        parents: { [Op.contains]: [user.id] },
        level: { [Op.gt]: user.level }
      }
    }) || 0;
  },

  /**
   * Получение процента
   *
   * @param level
   * @param index
   * @returns float
   */
  getPercent(level, index = 0) {
    if (!level) {
      return 0;
    }

    return (level.levels || [])[index] || 0;
  },

  /**
   * Получить текущий уровень пользователя
   *
   * @param user
   * @returns {Promise<*>}
   */
  async getReferralLevel(user) {
    const levels = await this.getGrid();
    const userAmount = await this.getUserAmount(user);
    const teamAmount = await this.getTeamAmount(user);
    const userCount = await this.getReferralCount(user);

    for (let z = 0; z < levels.length; z++) {
      if (
        z === levels.length - 1 ||
        this.validateLevel(levels[z], userAmount, teamAmount, userCount)
      ) {
        return levels[z];
      }
    }

    return null;
  },

  /**
   * Получить следующий уровень пользователя
   *
   * @param user
   * @param levels
   * @returns {Promise<*>}
   */
  async getNextLevel(user, levels) {
    const userAmount = await this.getUserAmount(user);
    const teamAmount = await this.getTeamAmount(user);
    const userCount = await this.getReferralCount(user);

    for (let i = 0; i < levels.length; i++) {
      const level = levels[i];

      if (this.validateLevel(level, userAmount, teamAmount, userCount)) {
        if (typeof levels[i - 1] === 'undefined') {
          return level;
        } else {
          return levels[i - 1];
        }
      }
    }

    return levels.length > 0 ? levels[levels.length - 1] : null;
  },

  /**
   * Сравнить два уровня между собой
   *
   * @param currentLevel
   * @param newLevel
   * @returns {boolean}
   */
  isBiggerThan(currentLevel, newLevel) {
    return currentLevel && newLevel ? currentLevel.price >= newLevel.price : false;
  },

  /**
   * Обновление всего дерева реферальных пользователей сверху вниз.
   * От конечных узлов к родительским.
   *
   * @returns {Promise<int>}
   */
  async updateUsers() {
    const users = await this.findUsersTree();

    let changed = 0;
    for (let i = 0; i < users.length; i++) {
      changed += await this.updateUser(users[i]);
    }

    return changed;
  },

  /**
   * Обновление текущего реф уровня
   * @param {models.User} user
   */
  async updateUser(user) {
    let changed = 0;

    const calculatedLevel = await this.getReferralLevel(user);

    const ids = [user.buy_referral_level_id, user.minimal_referral_level_id].filter(value => value);
    let currentLevel = null;
    if (ids.length > 0) {
      const levels = await models.Referral.findAll({
        where: { id: { [Op.in]: ids } }
      });
      currentLevel = levels.reduce((prev, current) => prev.price > current.price ? prev : current);
    }

    if (null === currentLevel && calculatedLevel) {
      await user.update({ referral_level_id: calculatedLevel.id });
      changed += 1;
    } else if (currentLevel) {
      await user.update({ referral_level_id: currentLevel.id });
      changed += 1;
      // } else if (calculatedLevel) {
      //   await user.update({ referral_level_id: calculatedLevel.id });
      //   changed += 1;
    }

    return changed;
  },

  /**
   * Проверка на возможность перейти на выбранный реф уровень.
   * Переход осуществляется только если проходят проверки по условиям реф уровня.
   *
   * @param user
   * @param level
   * @returns {Promise<boolean>}
   */
  async canChangeLevel(user, level) {
    const userAmount = await this.getUserAmount(user);
    const teamAmount = await this.getTeamAmount(user);
    const userCount = await this.getReferralCount(user);

    return this.validateLevel(level, userAmount, teamAmount, userCount);
  },

  /**
   * Поиск пользователей сверху вниз от конечных узлов к родительским.
   *
   * @returns {Promise<Array<Model>>}
   */
  findUsersTree() {
    return models.User.findAll({
      order: [['level', 'DESC']]
    });
  },

  /**
   * Обновить уровень пользователя
   *
   * @param user
   * @returns {Promise<*>}
   */
  async updateReferralLevel(user) {
    const levels = await this.getGrid();
    for (let z = 0; z < levels.length; z++) {
      const level = levels[z];
      if (await this.canChangeLevel(user, level)) {
        await user.update({ referral_level_id: level.id });
        break;
      }
    }
  },

  buildTree(rows, field, callback, isRoot) {
    return treeUtil.generate(rows, field, callback, isRoot);
  },

  async getStats(user) {
    const current = await this.getCurrentLevel(user);
    let next = await models.Referral.findOne({
      where: current ? {
        price: { [Op.gt]: current.price }
      } : {},
      order: [
        ['price', 'ASC']
      ]
    });
    if (null === next) {
      next = await models.Referral.findOne({
        order: [
          ['price', 'DESC']
        ]
      });
    }

    const userRaised = await this.getUserAmount(user);
    const teamRaised = await this.getTeamAmount(user);

    return {
      raised: {
        user: userRaised,
        team: teamRaised,
        user_count: await models.User.count({
          where: {
            parents: {
              [Op.contains]: [user.id]
            },
            level: {
              [Op.lte]: user.level + 3
            }
          }
        }) || 0
      },
      current: current ? current.toJSON() : null,
      next: next ? next.toJSON() : null
    };
  },

  async buyStatus(user) {
    // Находим сумму платежей пользователя
    const amount = await models.Transaction.sum('usd', {
      where: {
        account_type: consts.ACCOUNT_TYPE_STATUS,
        user_id: user.id,
        status: consts.TRANSACTION_PAY_STATUS
      }
    }) || 0;

    const currentLevel = await this.getCurrentLevel(user);
    const grid = await models.Referral.findAll({
      order: [
        ['price', 'DESC']
      ]
    });

    // Находим требуемый реф уровень в соответствии со стоимостью
    // в обратном порядке. Выбор происходит на основе той суммы, которая
    // есть у пользователя и которой хватает на приобретение статуса.
    let shouldByLevel = null;
    for (let i = 0; i < grid.length; i++) {
      const level = grid[i];
      const price = parseFloat(level.price);
      if (price > 0 && amount >= price) {
        shouldByLevel = level;
        break;
      }
    }

    if (shouldByLevel === null) {
      // Нечего покупать, нехватает средств.
      // Причин может быть несколько:
      // * пользователь закинул недостаточное кол-во криптовалюты
      // * возникла проблема с недостатком средств при конвертации и комиссии coinpayments
      // это кейс когда пользователь закинул валюту в притык и не учел 0.5% комиссии coinpayments
      return null;
    }

    if (currentLevel && parseFloat(currentLevel.price) >= parseFloat(shouldByLevel.price)) {
      // Если у пользователя текущий статус выше чем статус на
      // который хватает средств
      return null;
    }

    // Выключаем предыдущие статусы
    await models.UserReferral.update({ is_active: false }, {
      where: { user_id: user.id }
    });

    await models.UserReferral.create({
      user_id: user.id,
      referral_id: shouldByLevel.id,
      start_at: new Date,
      end_at: new Date().setFullYear(new Date().getFullYear() + 1)
    });

    // Пересчитать текущий уровень пользователя и обновить его
    await user.update({
      buy_referral_level_id: shouldByLevel.id,
      referral_level_id: shouldByLevel.id
    });

    return shouldByLevel;
  },

  async setMinimalLevel(user, level) {
    // Find current user (dont fetch referral level from database)
    const current = await this.getReferralLevel(user);

    if (false === this.isBiggerThan(current, level)) {
      // if current level < new minimal level
      await user.update({
        minimal_referral_level_id: level.id,
        referral_level_id: level.id
      });
    } else {
      // if user has current level and current level biggest than new minimal level
      await user.update({
        minimal_referral_level_id: level.id,
        referral_level_id: current ? current.id : level.id
      });
    }
  },

  /**
   * Поиск просроченных статусов.
   *
   * @returns {Promise<void>}
   */
  async findExpiredStatuses() {
    // Выбираем все активные статусы
    const levels = await models.UserReferral.findAll({
      where: { is_active: true }
    });

    const now = new Date;
    for (let i = 0; i < levels.length; i++) {
      const level = levels[i];
      const end = new Date(level.end_at);

      // Проверяем, что текущий статус просрочен или не просрочен
      if (now.getTime() >= end.getTime()) {
        await level.update({ is_active: false });
        const referralLevel = await level.getReferral();
        const currency = await models.Currency.findOne({
          where: { code: 'BTC' }
        });
        await transactionService.createTx({
          user_id: level.user_id,
          account_type: consts.ACCOUNT_TYPE_STATUS,
          amount: -referralLevel.price,
          status: consts.TRANSACTION_PAY_STATUS,
          comment: 'Вычет по окончанию срока действия статуса'
        }, currency);

        // TODO send user notification
        // Отправляем пользователю уведомление
        // mail.send()
      }
    }
  },

  async getGridByUser(user) {
    const coinpaymentPercent = await settingsService.getBool('deduct_coinpayments_tax', false) ? 0.5 : 0;

    const currentAmount = await models.Transaction.sum('usd', {
      where: {
        account_type: consts.ACCOUNT_TYPE_STATUS,
        user_id: user.id
      }
    }) || 0;
    const amount = coinpaymentPercent > 0
      ? currentAmount - ((currentAmount / 100) * coinpaymentPercent)
      : currentAmount;

    const levels = await models.Referral.findAll({
      where: {
        price: { [Op.gt]: 0 }
      },
      order: [['price', 'ASC']]
    });
    const current = await this.getCurrentLevel(user);
    const grid = [];

    // Переключатель, который поможет определить
    // является ли следующий уровень в пройденым
    for (let i = 0; i < levels.length; i++) {
      const level = levels[i].toJSON();
      if (null === current) {
        grid.push({
          ...level,
          price: level.price > amount ? level.price - amount : level.price,
          is_current: false,
          is_pass: false,
          is_buy: true
        });
      } else {
        grid.push({
          ...level,
          price: level.price > amount ? level.price - amount : level.price,
          is_current: current.id === level.id,
          is_pass: parseFloat(current.price) >= parseFloat(level.price),
          is_buy: parseFloat(current.price) < parseFloat(level.price)
        });
      }
    }

    return grid;
  },

  /**
   * Выборка общей суммы сборов по текущему пользователю.
   * Операции is_withdraw не учитываются.
   *
   * @param user
   * @returns {Promise<Number|*|number>}
   */
  async getUserAmount(user) {
    return await models.Transaction.sum('amount', {
      where: {
        account_type: {
          [Op.in]: [
            consts.ACCOUNT_TYPE_PARTNERSHIP,
            consts.ACCOUNT_TYPE_STRATEGY
          ]
        },
        user_id: user.id
      }
    }) || 0;
  },

  /**
   * Выборка общей суммы сборов по всем реферальным пользователям
   * текущего пользователя.
   *
   * @param user
   * @returns {Promise<Promise<Number>|*|number>}
   */
  async getTeamAmount(user) {
    const users = await models.User.findAll({
      where: {
        parents: {
          [Op.contains]: [user.id]
        },
        level: {
          [Op.between]: [user.level, user.level + 3]
        }
      }
    });

    if (users.length === 0) {
      return 0;
    }

    return await models.Transaction.sum('amount', {
      where: {
        account_type: {
          [Op.in]: [
            consts.ACCOUNT_TYPE_STRATEGY,
            consts.ACCOUNT_TYPE_PARTNERSHIP
          ]
        },
        user_id: { [Op.in]: users.map(u => u.id) }
      }
    }) || 0;
  },

  /**
   * Вспомогательный метод для упрощения тестов
   *
   * @param id
   * @returns {Promise<User>}
   */
  async findUser(id) {
    let user = null;

    if (String(id).indexOf('@') > -1) {
      user = await models.User.findOne({
        where: { email: id }
      });
      if (user) {
        return user;
      }
    }

    user = await models.User.findOne({
      where: { login: String(id) }
    });
    if (user) {
      return user;
    }

    if (Number.isInteger(id)) {
      user = await models.User.findById(id);
      if (user) {
        return user;
      }
    }

    return null;
  },

  /*
  TODO https://blockchaincorp.atlassian.net/browse/CL-259
  async findAllChildren(user) { // TODO move to scope
    return await models.User.findAll({
      where: {
        parents: {
          [Op.contains]: [user.id]
        }
      },
      order: [
        ['parents', 'ASC']
      ]
    });
  },

  async moveToNewParent(parentUser, childrUser) {
    const parents = parentUser.parents || [];
    parents.push(parentUser.id);

    const children = await this.findAllChildren(childrUser);
    for (let i = 0; i < children.length; i++) {
      const child = children[i];
      const tempParents = child.parents || [];
      const index = tempParents.findIndex(id => id === childrUser.id);
      await child.update({
        owner_id: parentUser.owner_id || parentUser.id,
        parents: index ? [
          ...parents,
          ...tempParents.slice(index)
        ] : parents
      });
    }

    console.log(parentUser.owner_id, childrUser.owner_id);
    const parameters = {
      owner_id: parentUser.owner_id || parentUser.id,
      parents
    };
    console.log(parameters);

    await childrUser.update(parameters);

    if (!parentUser.owner_id) {
    }
  },
  */

  /**
   * Поиск главного владельца реф. цепочки и выше стоящего
   * реф. пользователя.
   *
   * nested set использовать не стал, так как является избыточным
   * для данного решения
   *
   * adjacency list выборку по дереву вверх штатными средствами
   * субд (sqlite, mysql, pgsql) не реализовать, а вложенность может
   * быть бесконечная
   *
   * Данная реализация с примитивным materialized path для выборки вверх
   * и хранения главного владельца реф. цепочки
   *
   * @param id
   * @returns {Promise<*>}
   */
  async findReferral(id) {
    const owner = await this.findUser(id);
    if (null === owner) {
      return {
        owner_id: null,
        referral_id: null,
        level: 0,
        parents: null
      };
    }

    const parents = [
      ...(owner.parents || [])
    ];
    parents.push(owner.id);

    return {
      owner_id: parents[0],
      referral_id: owner.id,
      level: parents.length,
      parents
    }
  },

  /**
   * Поиск текущего уровня реферала
   *
   * @param user
   * @returns {Promise<*>}
   */
  getCurrentLevel(user) {
    return referralHelper.getCurrentLevel(user);
  }
};
