const fs = require('fs');
const glob = require('glob');
const path = require('path');
const { spawn } = require('child_process');
const sequelize = require('../models').sequelize;
const config = require('../config/app');

module.exports = {
  getPath() {
    return config.backup_path;
  },

  getList(fullPath = false) {
    const pattern = `${this.getPath()}/dump*.sql.gz`;
    return new Promise((resolve, reject) => {
      glob(pattern, {}, (err, files) => {
        if (err) {
          reject(err);
        } else {
          resolve(
            (fullPath ? files : files.map(file => path.basename(file))).reverse()
          );
        }
      });
    });
  },

  getFilePath() {
    const timestamp = Date.now();
    return `${this.getPath()}/dump-${timestamp}.sql.gz`;
  },

  getFile(name) {
    const filePath = `${this.getPath()}/${name}`;

    return fs.existsSync(filePath) ? filePath : null;
  },

  async create() {
    const file = this.getFilePath();

    const {
      dialectName,
      config
    } = sequelize.connectionManager;

    if (dialectName !== 'postgres') {
      throw new Error('Unsupported database type');
    }

    const {
      username,
      password,
      host,
      port,
      database
    } = config;

    await this.spawnProcess(`pg_dump -h ${host} -p ${port} -U ${username} ${database} | gzip > ${file}`, {
      PGPASSWORD: password
    });

    return file;
  },

  spawnProcess(cmd, env = {}) {
    return new Promise((resolve, reject) => {
      const child = spawn(cmd, { shell: true, env });

      process.stdin.pipe(child.stdin);

      child.stdout.on('data', (data) => {
        console.log(`child stdout:\n${data}`);
      });

      child.on('close', code => {
        if (code === 0) {
          resolve();
        } else {
          reject(code);
        }
      });
    });
  }
};
