const models = require('../models');
const coinpayments = require('../util/coinpayments');

module.exports = {
  async createNewWallet(currency) {
    const client = await coinpayments.client();

    return await client.getCallbackAddress({
      currency: currency.code
    });
  },

  async getWallet(user, strategy, currency) {
    const { merchantId } = strategy.coinpayments;

    const wallet = await models.Wallet.findOne({
      where: {
        merchant_id: String(merchantId),
        user_id: user.id,
        strategy_id: strategy.id
      }
    });

    if (wallet) {
      return wallet;
    }

    const coinpaymentsWallet = await this.createNewWallet(currency);

    return await models.Wallet.create({
      merchant_id: String(merchantId),
      user_id: user.id,
      address: coinpaymentsWallet.address,
      strategy_id: strategy.id,
    });
  },

  async getInternalWallet(user, currency) {
    const parameters = await coinpayments.getParameters();

    const wallet = await models.Wallet.findOne({
      where: {
        is_internal: true,
        merchant_id: parameters.merchantId,
        user_id: user.id,
        currency_id: currency.id
      }
    });

    if (wallet) {
      return wallet;
    }

    const coinpaymentsWallet = await this.createNewWallet(currency);

    return await models.Wallet.create({
      is_internal: true,
      merchant_id: parameters.merchantId,
      user_id: user.id,
      currency_id: currency.id,
      address: coinpaymentsWallet.address,
    });
  }
};
