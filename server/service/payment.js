const models = require('../models');
const consts = require('../consts');
const getRates = require('../util/paymentHelper');

module.exports = {
  /**
   * Метод исключительно для отладки. Не использовать в ipn!
   *
   * @param createdAt
   * @param user
   * @param strategy
   * @param currency
   * @param amount
   * @param transaction_id
   * @returns {Promise<*>}
   */
  async createPayment(user, strategy, currency, amount, transaction_id = 'debug', createdAt = null) {
    return await models.Payment.create({
      transaction_id,
      amount,
      user_id: user.id,
      strategy_id: strategy.id,
      currency_id: currency.id,
      status: consts.PAYMENT_COMPLETE,
      created_at: createdAt || new Date,
      updated_at: createdAt || new Date,
      ...getRates(amount, currency)
    });
  }
};
