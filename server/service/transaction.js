const consts = require('../consts');
const models = require('../models');
const { Op } = require('sequelize');
const referralHelper = require('./referralHelper');
const getRates = require('../util/paymentHelper');

module.exports = {
  createTx(data, entity) {
    if (!entity) {
      throw new Error('Entity is null or undefined');
    }

    return models.Transaction.create({
      ...data,
      ...getRates(data.amount, entity)
    });
  },

  async createPro(franchise) {
    const currency = await franchise.getCurrency();
    const franchiseAmount = franchise.price * currency.to_btc;
    // Выбираем пользователя оставившего заявку на франшизу
    const user = await franchise.getUser();
    if (user.referral_id) {
      const referral = await user.getReferral();
      const level = await referralHelper.getCurrentLevel(referral);
      if (level) {
        const percent = level.pro;
        if (percent > 0) {
          const amount = (franchise.price / 100) * percent;

          await this.createTx({
            franchise_id: franchise.id,
            account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
            amount,
            purchase_amount: franchiseAmount,
            percent,
            owner_id: franchise.user_id,
            user_id: referral.id,
            status: consts.TRANSACTION_PRO_BONUS
          }, await franchise.getCurrency());
        }
      }
    }
  },

  async createPayment(payment) {
    const tx = await this.createTx({
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      user_id: payment.user_id,
      strategy_id: payment.strategy_id,
      payment_id: payment.id,
      amount: payment.amount,
      status: consts.TRANSACTION_FREEZE,
      is_deposit: true,
      is_freeze: true,
      created_at: payment.created_at,
      updated_at: payment.updated_at
    }, payment);

    if (tx.payment_id) {
      const strategy = await tx.getStrategy();

      const { decreaseAmount, transactions } = await this.createReferralTransactions(
        tx.amount,
        await tx.getUser(),
        strategy,
        await strategy.getConvert()
      );
    }

    return tx;
  },

  createDeposit(tx) {
    return tx.update({ is_freeze: false });
  },

  async createWithdraw(user_id, amount, strategy_id = null) {
    // Дополнительной транзакции с фиксированием
    // списания (TRANSACTION_WRITE_OFF) не делаем,
    // так как заявку на вывод не могут отклонить,
    // могут только принять и вернуть средства, вопрос времени.
    // Если не возвращают - тут уже мошенничество
    let currency;
    if (strategy_id) {
      const strategy = await models.Strategy.findById(strategy_id);
      currency = await strategy.getCurrency();
    } else {
      currency = await models.Currency.findOne({
        where: { code: 'BTC' }
      });
    }

    return await this.createTx({
      user_id,
      strategy_id,
      account_type: strategy_id === null
        ? consts.ACCOUNT_TYPE_PARTNERSHIP
        : consts.ACCOUNT_TYPE_STRATEGY,
      amount: -amount,
      status: consts.TRANSACTION_WITHDRAW,
      comment: 'withdraw',
      is_freeze: true
    }, currency);
  },

  isCtPlatform() {
    return process.env.APP_NAME === 'ctplatform';
  },

  /**
   * В случае с ЛК, мы начисляем бонусные токены из СК, фантики.
   * В данном случае мы оперируем реальными деньгами. Таким образом, мы не можем
   * выдать пользователю доход, фонду доход и из воздуха взять вознаграждение
   * реферальным пользователям. Таким образом...
   *
   * Берем доход фонда за базовую цифру, так как не пользователь должен платить за
   * услуги предоставляемые фондом.
   *
   * @param strategy
   * @param amount
   * @param user
   * @returns {Promise<{decreaseAmount: number, transactions: Array}>}
   */
  async createReferral(amount, strategy, user) {
    const transactions = [];

    let decreaseAmount = 0;

    if (this.isCtPlatform()) {
      const teamAmount = await models.Transaction.sum('usd', {
        where: {
          account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
          status: consts.TRANSACTION_PAYMENT
        },
        include: [
          {
            model: models.User,
            as: 'user',
            where: {
              parents: {
                [Op.contains]: [user.id]
              },
              level: user.level + 1
            }
          }
        ],
        group: ['user.id']
      }) || 0;

      let percent = 0;
      if (teamAmount >= 100000) {
        percent = 8;
      } else if (teamAmount >= 50000) {
        percent = 5;
      } else if (teamAmount >= 20000) {
        percent = 3;
      } else if (teamAmount >= 10000) {
        percent = 2;
      }

      decreaseAmount = amount / 100 * percent;

      return { decreaseAmount, transactions };
    } else {
      const currency = await strategy.getConvert();
      return await this.createReferralTransactions(amount, user, strategy, currency);
    }
  },

  async createReferralTransactions(amount, user, strategy, currency) {
    const transactions = [];

    let decreaseAmount = 0;

    // Итерируем реферальных пользователей, если таковые имеются
    const parents = (user.parents || []).reverse();
    for (let t = 0; t < parents.length; t++) {
      const parent = await models.User.findById(parents[t]);

      // Находим уровень в рефералой сетке
      const level = await referralHelper.getCurrentLevel(parent);
      if (level) {
        // Выбираем процент реферального вознаграждения
        const referralPercent = (level.single || [])[t] || 0;
        if (referralPercent > 0) {
          // Расчитываем сумму бонуса реф сетки
          const referralAmount = amount / 100 * referralPercent;

          // Вычитаем сумму вознаграждения рефералу из суммы дохода фонда
          decreaseAmount += referralAmount;

          // Зачисляем транзакцию на счет реферального пользователя
          // https://blockchaincorp.atlassian.net/browse/CL-162
          const tx = await this.createTx({
            account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
            strategy_id: strategy.id,
            comment: `referral level = ${level.name_en}, user level = ${user.level - t}`,
            owner_id: user.id,
            amount: referralAmount,
            user_id: parent.id,
            percent: referralPercent,
            purchase_amount: amount,
            referral_percent: referralPercent,
            is_deposit: false,
            is_freeze: false,
            status: consts.TRANSACTION_ONE_TIME_BONUS
          }, currency);
          transactions.push(tx);
        }
      }
    }

    return { decreaseAmount, transactions };
  },

  async accept(tx) {
    await tx.update({
      status: consts.TRANSACTION_DEPOSIT
    });

    if (tx.payment_id) {
      const strategy = await tx.getStrategy();
      const user = await tx.getUser();
      const currency = await strategy.getConvert();

      await this.createReferralTransactions(tx.amount, user, strategy, currency);
    }
  },

  async createPaymentForStatus(payment, user) {
    const tx = await this.createTx({
      user_id: payment.user_id,
      account_type: consts.ACCOUNT_TYPE_STATUS,
      strategy_id: payment.strategy_id,
      payment_id: payment.id,
      amount: payment.amount,
      status: consts.TRANSACTION_PAY_STATUS,
      is_deposit: false,
      is_freeze: false
    }, payment);

    // Итерируем реферальных пользователей, если таковые имеются
    const parents = (user.parents || []).reverse();
    for (let t = 0; t < parents.length; t++) {
      const parent = await models.User.findById(parents[t]);

      // Находим уровень в рефералой сетке
      const level = await referralHelper.getCurrentLevel(parent);
      if (level) {
        // Выбираем процент реферального вознаграждения
        const referralPercent = (level.active || [])[t] || 0;
        if (referralPercent > 0) {
          // Расчитываем сумму бонуса реф сетки
          const referralAmount = payment.amount / 100 * referralPercent;

          // Зачисляем транзакцию на счет реферального пользователя
          // https://blockchaincorp.atlassian.net/browse/CL-162
          await this.createTx({
            account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
            comment: `referral level = ${level.name_en}, user level = ${user.level - t}`,
            owner_id: user.id,
            amount: referralAmount,
            user_id: parent.id,
            percent: referralPercent,
            purchase_amount: payment.amount,
            referral_percent: referralPercent,
            is_deposit: false,
            is_freeze: false,
            status: consts.TRANSACTION_PAY_STATUS_BONUS
          }, payment);
        }
      }
    }

    return tx;
  }
};
