const settings = require('../util/settings');
const mail = require('../util/mail');

module.exports = {
  async createTransport() {
    const params = await settings.fetch(false);

    return mail.createTransport({
      host: params.smtp_host,
      port: params.smtp_port,
      secure: true,
      auth: {
        user: params.smtp_username,
        pass: params.smtp_password
      }
    });
  },

  hasValue(value) {
    if (typeof value === 'undefined') {
      return false;
    }

    if (value === null) {
      return false;
    }

    return String(value).length > 0;
  },

  async useExternalSmtp(params) {
    return (
      this.hasValue(params.smtp_host) &&
      this.hasValue(params.smtp_port) &&
      this.hasValue(params.smtp_username) &&
      this.hasValue(params.smtp_password)
    );
  },

  async send(to, template, data = {}) {
    const params = await settings.fetch(false);

    const socials = [
      { name: 'bitcoinstalk', link: params.bitcoinstalk_link },
      { name: 'telegram', link: params.telegram_link },
      { name: 'youtube', link: params.youtube_link },
      { name: 'facebook', link: params.facebook_link },
      { name: 'twitter', link: params.twitter_link }
    ];

    const parameters = {
      ...data,
      socials,
      company_name: params.company_name
    };

    return mail.send(
      to,
      template,
      parameters,
      await this.useExternalSmtp(params) ? await this.createTransport() : null,
      params.smtp_from
    );
  }
};
