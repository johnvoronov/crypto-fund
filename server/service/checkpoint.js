const models = require('../models');
const balance = require('./balance');
const referral = require('./referral');
const userCheckpointService = require('./userCheckpoint');
const transactionService = require('./transaction');
const BigNumber = require('bignumber.js');
const balanceService = require('./balance');
const { Op } = require('sequelize');
const consts = require('../consts');
const mail = require('./mail');

module.exports = {
  async checkpoint(strategy_id, percent, end_at = null) {
    // Получаем сумму средств в фонде. Средства в фонде это кол-во
    // криптовалюты вручную одобренное администратором проекта на каждую
    // итерацию времени. Пользователи "передают" свои средства с баланса
    // в определенную стратегию, после чего администратор их одобряет и на
    // следующий интервал времени (после чекпоинта) средства переходят в
    // оборот фонда
    const amount = await balance.getActiveDepositBalance({ strategy_id });

    /*
    // Выбираем последний чекпоинт, если это вновь развернутый проект,
    // то предыдущего чекпоинта может не быть
    const lastCheckpoint = await models.Checkpoint.findOne({
      where: { strategy_id },
      limit: 1,
      offset: 0,
      order: [
        ['id', 'DESC']
      ]
    });
    // Фиксируем процент доходности / убыточности фонда и сумму на момент
    // фиксирования записываем в отдельную таблицу для дальнейшего расчета
    // капитализации процентов и начисления бонусов пользователям
    const sevenDays = 1000 * 60 * 60 * 24 * 7;
    const start_at = lastCheckpoint ? lastCheckpoint.end_at : new Date(new Date().getTime() - sevenDays);
    */

    end_at.setHours(23, 59, 59, 999);

    return await models.Checkpoint.create({
      strategy_id,
      amount,
      percent,
      end_at,
      is_done: false
    });
  },

  sortLimits(a, b) {
    if (a.limit < b.limit) {
      return -1;
    }

    if (a.limit > b.limit) {
      return 1;
    }

    return 0;
  },

  async getUserPercentCtPlatform(strategy, user_id, point) {
    // ctplatform logic
    const amount = await models.Transaction.sum('usd', {
      where: {
        account_type: consts.ACCOUNT_TYPE_STRATEGY,
        strategy_id: strategy.id,
        user_id,
        is_deposit: true,
        is_freeze: false,
        created_at: {
          [Op.lte]: point.end_at
        }
      }
    }) || 0;

    const limits = (strategy.limit || []).sort(this.sortLimits);
    for (let i = limits.length; i > 0; i--) {
      const { limit, percent } = limits[i - 1];
      if (amount >= limit) {
        return percent;
      }
    }

    return 0;
  },

  async getUserPercentBitlab(strategy, user_id, point) {
    // bitlab logic
    const amount = await balanceService.getDepositBalance({
      strategy_id: strategy.id,
      user_id,
      created_at: {
        [Op.lte]: point.end_at
      }
    });

    const limits = (strategy.limit || []).sort(this.sortLimits);
    for (let i = limits.length; i > 0; i--) {
      const { limit, percent } = limits[i - 1];
      if (amount >= limit) {
        return 100 - percent;
      }
    }

    return 50;
  },

  isCtPlatform() {
    return process.env.APP_NAME === 'ctplatform';
  },

  /**
   * Выбираем текущую сумму инвестиций пользователя относительно point.end_at
   *
   * Всегда (автоматически) сверяются данные пополнения пользователя и
   * в какой категории он находится, а всего категорий 4, распределение прибыли
   * согласно категории инвестор/фонд:
   * 0 - 1 BTC         - 50 / 50%
   * 1,01 - 10 BTC     - 60 / 40%
   * 10,01 - 50 BTC    - 70 / 30%
   * 50 BTC и более    - 80 / 20%
   *
   * @param strategy
   * @param user_id
   * @param point
   * @returns {Promise<int>}
   */
  getUserPercent(strategy, user_id, point) {
    return this.isCtPlatform()
      ? this.getUserPercentCtPlatform(strategy, user_id, point)
      : this.getUserPercentBitlab(strategy, user_id, point);
  },

  /**
   * Выбираем процентное соотношение дохоности фонда и пользователя и расчитываем сумму
   * дохода.
   *
   * @param strategy
   * @param user_id
   * @param point
   * @returns {Promise<{userPercent: number, fundPercent: number}>}
   */
  async getIncomePercent(strategy, user_id, point) {
    const userPercent = await this.getUserPercent(strategy, user_id, point);

    return {
      userPercent,
      fundPercent: 100 - userPercent
    };
  },

  /**
   * Выбираем процентное соотношение дохоности фонда и пользователя и расчитываем сумму
   * дохода.
   *
   * @param userPercent
   * @param fundPercent
   * @param incomingAmount
   * @param point
   * @returns {Promise<{userAmount: number, fundAmount: number}>}
   */
  getIncomeAmount(userPercent, fundPercent, incomingAmount, point) {
    return {
      userAmount: (this.bigNumberify(incomingAmount)).multipliedBy(userPercent / 100),
      fundAmount: (this.bigNumberify(incomingAmount)).multipliedBy(fundPercent / 100)
    };
  },

  /**
   * В случае с ЛК, мы начисляем бонусные токены из СК, фантики.
   * В данном случае мы оперируем реальными деньгами. Таким образом, мы не можем
   * выдать пользователю доход, фонду доход и из воздуха взять вознаграждение
   * реферальным пользователям. Таким образом...
   *
   * Берем доход фонда за базовую цифру, так как не пользователь должен платить за
   * услуги предоставляемые фондом.
   *
   * @param point
   * @param strategy_id
   * @param amount
   * @param user
   * @returns {Promise<{decreaseAmount: number, transactions: Array}>}
   */
  async createReferralTransactions(point, strategy_id, amount, user) {
    const transactions = [];

    const currency = await models.Currency.findOne({
      where: { code: 'BTC' }
    });

    let decreaseAmount = this.bigNumberify(0);

    // Итерируем реферальных пользователей, если таковые имеются
    const parents = (user.parents || []).reverse();
    for (let t = 0; t < parents.length; t++) {
      const parent = await models.User.findById(parents[t]);

      // Находим уровень в рефералой сетке
      const level = await referral.getCurrentLevel(parent);
      if (level) {
        // Выбираем процент реферального вознаграждения
        const referralPercent = referral.getPercent(level, t);
        if (referralPercent > 0) {
          // Расчитываем сумму бонуса реф сетки
          const referralAmount = this.bigNumberify(amount).div(100).multipliedBy(referralPercent);

          // Вычитаем сумму вознаграждения рефералу из суммы дохода фонда
          decreaseAmount = decreaseAmount.plus(referralAmount);

          // Зачисляем транзакцию на счет реферального пользователя
          // https://blockchaincorp.atlassian.net/browse/CL-162
          const tx = await transactionService.createTx({
            strategy_id,
            account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
            comment: `Реф. уровень = ${level.id}, Процент = ${referralPercent}`,
            owner_id: user.id,
            amount: this.getNumber(referralAmount),
            user_id: parent.id,
            fund_amount: this.getNumber(amount),
            referral_percent: referralPercent,
            is_deposit: false,
            is_freeze: false,
            status: consts.TRANSACTION_PARTNER_INCOME,
            created_at: point.end_at,
            updated_at: point.end_at
          }, currency);
          transactions.push(tx);

          mail.send(parent.email, 'bonus', {
            name: parent.first_name
          });
        }
      }
    }

    return { decreaseAmount, transactions };
  },

  /**
   * Завершение интервала времени с 4 чекпоинтами.
   * Каждый из чекпоинтов - это 1 календарная неделя, но так как
   * фонд не автоматизирован, то может быть ситуация с человеческим фактором,
   * когда владелец фонда забудет / не создаст чекпоинт.
   *
   * 1 - идеальная ситуация когда человеческий фактор не участвует
   * если владелец фонда забудет / не создаст чекпоинт и пройдет 3 или
   * 5 или 7 чекпоинтов, то все не завершенные ранее будут "захвачены" в данном
   * методе для расчета доходности / убыточности пользователям.
   *
   * @returns {Promise<void>}
   */
  async completeIteration(strategy_id) {
    const finish = await models.Finish.create({ strategy_id });

    const currency = await models.Currency.findOne({
      where: { code: 'BTC' }
    });

    const checkpoints = await models.Checkpoint.findAll({
      where: { is_done: false, strategy_id },
      order: [
        ['end_at', 'ASC'] // Сортировка в end_at ASC обязательна, так как мы учитываем капитализацию
      ]
    });
    if (checkpoints.length === 0) {
      return;
    }

    for (let i = 0; i < checkpoints.length; i++) {
      await checkpoints[i].update({ finish_id: finish.id });
      await this.completeIterationForCheckpoint(checkpoints[i], currency);
    }

    await referral.updateUsers();
  },

  /**
   * Выбираем не транзакции, а всех пользователей которые имеют актив
   * в конкретной стратегии, сами транзакции нас не интересуют, а интересует
   * только сумма инвестиций за интервал времени до point.end_at. См ниже.
   *
   * @param point
   * @returns {Promise<Array<Model>>}
   */
  async findAllUsersWhoHaveTransactions(point) {
    return await models.User.findAll({
      include: [
        {
          model: models.Transaction,
          as: 'transactions',
          where: {
            account_type: consts.ACCOUNT_TYPE_STRATEGY,
            strategy_id: point.strategy_id,
            is_deposit: true,
            is_freeze: false,
            created_at: {
              [Op.lte]: point.end_at
            }
          }
        }
      ]
    });
  },

  /**
   * Нахождение суммы (баланса) пользователя в обороте фонда
   * между интервалами времени в чекпоинте.
   * https://blockchaincorp.atlassian.net/browse/CL-163
   *
   * @param user
   * @param point
   * @returns {Promise<*>}
   */
  async findUserBalanceLowerThanCheckpointEndAt(user, point) {
    const where = {
      is_deposit: true,
      is_freeze: false,
      strategy_id: point.strategy_id,
      user_id: user.id,
      created_at: {
        [Op.lte]: point.end_at
      }
    };
    return await models.Transaction.sum('amount', { where }) || 0;
  },

  async completeProfitCheckpoint(currentUserBalance, newUserBalance, userPercent, userAmount, fundAmount, baseBalance, user, point, strategy, currency) {
    const finishCount = await models.Finish.count({ where: { strategy_id: strategy.id } }) || 0;
    console.log({ method: 'completeProfitCheckpoint', baseBalance, finishCount });
    let transactions = [];

    const strategyFee = this.bigNumberify(userAmount).div(100).multipliedBy(strategy.fee);
    transactions.push(await transactionService.createTx({
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      strategy_id: point.strategy_id,
      user_id: user.id,
      is_deposit: true,
      // Не замораживаем сумму для активации
      // вручную перед следующим чекпоинтом
      is_freeze: false,
      status: consts.TRANSACTION_INCOME,
      comment: `Доход c инвестиций в стратегию. userAmount = ${userAmount}, userPercent = ${userPercent}, strategyFee = ${this.getNumber(strategyFee)}`,
      created_at: point.end_at,
      updated_at: point.end_at,

      amount: this.getNumber(this.bigNumberify(userAmount).minus(strategyFee)),
      fund_amount: this.getNumber(this.bigNumberify(fundAmount))
    }, currency));

    const refData = await this.createReferralTransactions(point, strategy.id, fundAmount, user);
    for (let i = 0; i < refData.transactions.length; i++) {
      transactions.push(refData.transactions[i]);
    }

    transactions.push(await transactionService.createTx({
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      strategy_id: point.strategy_id,
      is_income: true,
      status: consts.TRANSACTION_FUND_INCOME,
      comment: 'Доход фонда',
      created_at: point.end_at,
      updated_at: point.end_at,

      amount: this.getNumber(this.bigNumberify(fundAmount).minus(refData.decreaseAmount)),
      fee: this.getNumber(this.bigNumberify(refData.decreaseAmount))
    }, currency));

    if (strategy.fee > 0) {
      transactions.push(await transactionService.createTx({
        account_type: consts.ACCOUNT_TYPE_STRATEGY,
        strategy_id: point.strategy_id,
        fee: 0,
        is_income: true,
        status: consts.TRANSACTION_FUND_INCOME_FEE,
        comment: 'Доход фонда (комиссия)',
        created_at: point.end_at,
        updated_at: point.end_at,

        amount: this.getNumber(strategyFee)
      }, currency));
    }

    const userCheckpoint = await models.UserCheckpoint.create({
      before: this.getNumber(currentUserBalance),
      strategy_id: point.strategy_id,
      user_id: user.id,
      checkpoint_id: point.id,
      percent: point.percent,
      base: this.getNumber(baseBalance),

      // (userAmount - strategyFee) - currentUserBalance
      diff: this.getNumber(this.bigNumberify(userAmount).minus(strategyFee)),
      fee: this.getNumber(this.bigNumberify(fundAmount)),
      // usd: (userAmount - strategyFee) * currency.to_usd,
      usd: this.getNumber(this.bigNumberify(userAmount).minus(strategyFee).multipliedBy(currency.to_usd)),
      // after: userAmount - strategyFee,
      after: this.getNumber(this.bigNumberify(currentUserBalance).plus(userAmount).minus(strategyFee))
    });

    this.assignTransactionsToUserCheckpoint(userCheckpoint, point, transactions);
  },

  async completeNonProfitCheckpoint(currentUserBalance, newUserBalance, incomingAmount, baseBalance, user, point, strategy, currency) {
    console.log({ method: 'completeNonProfitCheckpoint', baseBalance });
    const transactions = [];

    transactions.push(await transactionService.createTx({
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      strategy_id: point.strategy_id,
      amount: this.getNumber(incomingAmount),
      user_id: user.id,
      fund_amount: 0,
      is_deposit: true,
      // Не замораживаем сумму для активации
      // вручную перед следующим чекпоинтом
      is_freeze: false,
      status: consts.TRANSACTION_INCOME,
      comment: `Возврат средств после убыточности = ${this.getNumber(incomingAmount)}`,
      created_at: point.end_at,
      updated_at: point.end_at
    }, currency));

    const userCheckpoint = await models.UserCheckpoint.create({
      before: currentUserBalance,
      strategy_id: point.strategy_id,
      user_id: user.id,
      checkpoint_id: point.id,
      fee: 0,
      is_loss: true,
      percent: point.percent,
      base: this.getNumber(baseBalance),

      diff: this.getNumber(this.bigNumberify(newUserBalance).minus(currentUserBalance)),
      usd: this.getNumber(newUserBalance) * currency.to_usd,
      after: this.getNumber(newUserBalance)
    });

    this.assignTransactionsToUserCheckpoint(userCheckpoint, point, transactions);
  },

  bigNumberify(value) {
    return new BigNumber(value);
  },

  getNumber(value) {
    if (value instanceof BigNumber) {
      return value.toNumber();
    }

    return value;
  },

  async completeLossCheckpoint(currentUserBalance, newUserBalance, incomingAmount, baseBalance, user, point, strategy, currency) {
    console.log({ method: 'completeLossCheckpoint', baseBalance });
    const transactions = [];

    // Фонд сработал в плюс относительно базового платежа пользователя с учетом возможных
    // предыдущих убытков пользователя. Можно начислять бонусы фонду и рефералам.
    transactions.push(await transactionService.createTx({
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      strategy_id: point.strategy_id,
      user_id: user.id,
      is_deposit: true,
      // Не замораживаем сумму для активации
      // вручную перед следующим чекпоинтом
      is_freeze: false,
      comment: `Фонд сработал в минус = ${-this.getNumber(incomingAmount)}`,
      status: consts.TRANSACTION_INCOME,
      created_at: point.end_at,
      updated_at: point.end_at,

      amount: -this.getNumber(this.bigNumberify(incomingAmount))
    }, currency));

    const userCheckpoint = await models.UserCheckpoint.create({
      strategy_id: point.strategy_id,
      user_id: user.id,
      checkpoint_id: point.id,
      fee: 0,
      percent: point.percent,
      is_loss: true,
      base: this.getNumber(baseBalance),

      before: this.getNumber(this.bigNumberify(currentUserBalance)),
      diff: -this.getNumber(incomingAmount),
      usd: this.getNumber(this.bigNumberify(incomingAmount).multipliedBy(currency.to_usd)),
      after: this.getNumber(this.bigNumberify(newUserBalance))
    });

    this.assignTransactionsToUserCheckpoint(userCheckpoint, point, transactions);
  },

  async completeProfitAndNonProfitCheckpoint(
    userPercent,
    fundPercent,
    profitAmount,
    currentUserBalance,
    newUserBalance,
    incomingAmount,
    baseBalance,
    user,
    point,
    strategy,
    currency
  ) {
    console.log({ method: 'completeProfitAndNonProfitCheckpoint', baseBalance });
    const transactions = [];

    transactions.push(await transactionService.createTx({
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      strategy_id: point.strategy_id,
      user_id: user.id,
      fund_amount: 0,
      is_deposit: true,
      // Не замораживаем сумму для активации
      // вручную перед следующим чекпоинтом
      is_freeze: false,
      status: consts.TRANSACTION_INCOME,
      comment: `Возврат средств после убыточности = ${this.getNumber(incomingAmount.minus(profitAmount))}`,
      created_at: point.end_at,
      updated_at: point.end_at,

      amount: this.getNumber(incomingAmount.minus(profitAmount))
    }, currency));

    const newAmount = await this.getIncomeAmount(userPercent, fundPercent, profitAmount, point);
    const strategyFee = this.bigNumberify(newAmount.userAmount).div(100).multipliedBy(strategy.fee);

    transactions.push(await transactionService.createTx({
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      strategy_id: point.strategy_id,
      user_id: user.id,
      is_deposit: true,
      // Не замораживаем сумму для активации
      // вручную перед следующим чекпоинтом
      is_freeze: false,
      status: consts.TRANSACTION_INCOME,
      comment: `Доход c инвестиций в стратегию. userAmount = ${this.getNumber(this.bigNumberify(newAmount.userAmount))}, userPercent = ${userPercent}, strategyFee = ${this.getNumber(strategyFee)}`,
      created_at: point.end_at,
      updated_at: point.end_at,

      amount: this.getNumber(this.bigNumberify(newAmount.userAmount).minus(strategyFee)),
      fund_amount: this.getNumber(this.bigNumberify(newAmount.fundAmount))
    }, currency));

    const refData = await this.createReferralTransactions(point, strategy.id, newAmount.fundAmount, user);
    for (let i = 0; i < refData.transactions.length; i++) {
      transactions.push(refData.transactions[i]);
    }

    transactions.push(await transactionService.createTx({
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      strategy_id: point.strategy_id,
      is_income: true,
      status: consts.TRANSACTION_FUND_INCOME,
      comment: 'Доход фонда',
      created_at: point.end_at,
      updated_at: point.end_at,

      amount: this.getNumber(this.bigNumberify(newAmount.fundAmount).minus(refData.decreaseAmount)),
      fee: this.getNumber(refData.decreaseAmount)
    }, currency));

    if (strategy.fee > 0) {
      transactions.push(await transactionService.createTx({
        account_type: consts.ACCOUNT_TYPE_STRATEGY,
        strategy_id: point.strategy_id,
        fee: 0,
        is_income: true,
        status: consts.TRANSACTION_FUND_INCOME_FEE,
        comment: 'Доход фонда (комиссия)',
        created_at: point.end_at,
        updated_at: point.end_at,

        amount: this.getNumber(strategyFee)
      }, currency));
    }

    const userCheckpoint = await models.UserCheckpoint.create({
      strategy_id: point.strategy_id,
      user_id: user.id,
      checkpoint_id: point.id,
      percent: point.percent,
      base: this.getNumber(baseBalance),

      before: this.getNumber(currentUserBalance),
      diff: this.getNumber(this.bigNumberify(newUserBalance).minus(this.bigNumberify(newAmount.fundAmount)).minus(strategyFee).minus(currentUserBalance)),
      fee: this.getNumber(this.bigNumberify(newAmount.fundAmount)),
      usd: this.getNumber(this.bigNumberify(newUserBalance).minus(strategyFee).multipliedBy(currency.to_usd)),
      after: this.getNumber(this.bigNumberify(newUserBalance).minus(newAmount.fundAmount).minus(strategyFee))
    });

    this.assignTransactionsToUserCheckpoint(userCheckpoint, point, transactions);
  },

  async completeIterationForUser(user, point, currency, currentUserBalance) {
    const strategy = await point.getStrategy();

    const incomingAmount = (this.bigNumberify(currentUserBalance)).div(100).multipliedBy(Math.abs(point.percent));
    const newUserBalance = point.percent > 0
      ? (this.bigNumberify(currentUserBalance)).plus(incomingAmount)
      : (this.bigNumberify(currentUserBalance)).minus(incomingAmount);

    // Отдельно получаем сумму бонуса по данному платежу, так как часть дохода, если это
    // конечно доход, а не убыток переходит в фонд.
    const { userPercent, fundPercent } = await this.getIncomePercent(strategy, user.id, point);
    const { userAmount, fundAmount } = await this.getIncomeAmount(userPercent, fundPercent, incomingAmount, point);

    const countCheckpoints = await models.UserCheckpoint.count({
      where: { strategy_id: point.strategy_id, user_id: user.id }
    }) || 0;
    const isFirstCheckpoint = countCheckpoints === 0;
    // Расчет убыточности / доходности относительно предыдущих 2 месяцев.
    // https://blockchaincorp.atlassian.net/wiki/spaces/CL/pages/157220865
    const history = await userCheckpointService.getHistory(user.id, point.strategy_id);
    const isPrevLoss = history.length > 0 ? history[history.length - 1].is_loss : false;
    const baseBalance = isFirstCheckpoint ? currentUserBalance : this.bigNumberify(await userCheckpointService.findBaseBalanceFromCheckpoint(point, user.id, point.strategy_id));

    /**
     * Кейсы к обработке:
     * 1) Пользователь работает в плюс и все хорошо и у фонда и у пользователя
     * 2) Пользователь работал в плюс, потом ушел в минус и возмещает следующими чекпоинтами убытки
     * 3) Пользователь несет убытки
     *
     * С кейсов 1 и 2 рефам начисляются бонусы и идет доход фонду. С кейсом 3 если пользователь не возместил убытки
     * или дальше работает в убыток, то бонусы и доход фонду не зачисляется.
     */

    // Если пользователь получает прибыль на этот чекпоинт и его базовый баланс (за 2 месяца = 8 чекпоинтов) не ушел
    // в минус, то начисляем прибыль фонду и бонусы рефералам.
    if (point.percent > 0) {
      if (newUserBalance > baseBalance) {
        const profitAmount = (this.bigNumberify(newUserBalance)).minus(this.bigNumberify(baseBalance));
        if (false === isFirstCheckpoint && isPrevLoss && currentUserBalance < baseBalance && profitAmount.toNumber() > 0) {
          console.log('completeProfitAndNonProfitCheckpoint');
          // Пользователь получил возмещение убытков и доход.
          // К примеру: У пользователя было на старте 1 BTC и имел на старте с первого чекпоинта
          // убыточность в -50%, потом доходность в 10%, 40%, 40%. Это срок одного месяца, базовый
          // баланс по прежнему 1 BTC. По итогу у него получается сумма после всех чекпоинтов 1.078.
          // Тоесть он отбил убыток в 0.5 BTC и сверху получил 0.78 BTC. 0.78 BTC это уже доход сверх
          // вложенной суммы, следовательно с нее мы обязаны зачислить бонусы рефералам и доход фонду
          await this.completeProfitAndNonProfitCheckpoint(
            userPercent,
            fundPercent,
            profitAmount,
            currentUserBalance,
            newUserBalance,
            incomingAmount,
            baseBalance,
            user,
            point,
            strategy,
            currency
          );
        } else {
          console.log('completeProfitCheckpoint');
          // Иначе просто начисляем доход пользователю, так как ранее не было убыточности
          await this.completeProfitCheckpoint(
            currentUserBalance,
            newUserBalance,
            userPercent,
            userAmount,
            fundAmount,
            baseBalance,
            user,
            point,
            strategy,
            currency
          );
        }
      } else {
        console.log('completeNonProfitCheckpoint');
        // Ни реферальные пользователи, ни фонд не получает дохода. Только
        // пользователь возмещает свои убытки.
        await this.completeNonProfitCheckpoint(
          currentUserBalance,
          newUserBalance,
          incomingAmount,
          baseBalance,
          user,
          point,
          strategy,
          currency
        );
      }
    } else {
      // (incomingAmount, point, strategy, currency)
      console.log('completeLossCheckpoint');
      await this.completeLossCheckpoint(
        currentUserBalance,
        newUserBalance,
        incomingAmount,
        baseBalance,
        user,
        point,
        strategy,
        currency
      );
    }

    // await mail.send(user.email, 'user_month_report', { user }); TODO uncomment me
  },

  async assignTransactionsToUserCheckpoint(userCheckpoint, point, transactions) {
    // Сохраняем связь по каждой транзакции реферальной системы до
    // пользовательского чекпоинта, чтобы в разделе /report/income в таблице
    // "Доход фонда" отобразить сколько средств перешло в фонд с суммы инвестиций
    // Подробнее в CL-191
    //
    // Сноска:
    // Твой реферал вложил 10 битков
    // фонд заработал 5 битков
    // 2.5 отдал рефералу
    // оставшиеся 2.5 и есть доход фонда с этого реферала
    return await models.Transaction.update({
      user_checkpoint_id: userCheckpoint.id,
      checkpoint_id: point.id
    }, {
      where: {
        id: { [Op.in]: transactions.map(tx => tx.id) }
      }
    });
  },

  /**
   * Расчет не общий, а по каждому из пользователей.
   * По каждому пользователю выбираем транзакции, расчитываем
   * капитализацию, далее начисляем доходность / убыточность.
   *
   * @param point
   * @param currency
   * @returns {Promise<void>}
   */
  async completeIterationForCheckpoint(point, currency) {
    const users = await this.findAllUsersWhoHaveTransactions(point);
    if (process.env.NODE_ENV !== 'production') {
      console.log('======================');
    }
    for (let z = 0; z < users.length; z++) {
      const currentUserBalance = await this.findUserBalanceLowerThanCheckpointEndAt(users[z], point);
      // Фонд не сможет пользователей загнать в минус, так как на момент создания
      // чекпоинта указываются проценты, а не сумма. Следовательно берется процент
      // от текущей суммы пользователя которая находится в распоряжении фонда.
      // Если пользователя увели в полнейший ноль, то процент от нуля - ноль.
      // Данная проверка "выше уровнем" для того, чтобы не создавать лишние транзакции с
      // нулевыми показателями доходности / убыточности.
      if (currentUserBalance > 0) {
        await this.completeIterationForUser(users[z], point, currency, currentUserBalance);
      }
    }
    await point.update({ is_done: true });
  }
};
