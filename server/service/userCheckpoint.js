const { Op } = require('sequelize');
const models = require('../models');

module.exports = {
  /**
   * Находим последние 2 чекпоинта и вычисляем разницу между
   * базовым балансом пользователя.
   *
   * @param user_id
   * @param strategy_id
   * @param count int
   * @returns {Promise<Array<Model>>}
   */
  async getHistory(user_id, strategy_id, count = 8) {
    return await models.UserCheckpoint.findAll({
      raw: true,
      where: { user_id, strategy_id },
      limit: count,
      offset: 0,
      order: [
        ['id', 'ASC']
      ]
    });
  },

  /**
   * Находим базовый баланс пользователя по чекпоинтам.
   * Базовым балансом является первый по списку чекпоинт
   * за прошлые 2 месяца.
   * @param lastSums array
   * @returns float
   */
  findBaseBalanceByCheckpoints(lastSums = []) {
    if (lastSums.length === 0) {
      return 0;
    }

    if (lastSums.length < 8) {
      return lastSums[0].before;
    }

    // Если прошло меньше 8 чекпоинтов (по 4 в каждом месяце с лимитом в 2 месяца),
    // то выбираем фактический баланс пользователя на начало первого чекпоинта
    // Иначе выбираем баланс по факту отсечки в 2 месяца
    return lastSums[lastSums.length - 1].before;
  },

  async findValidPreviousFinish(finish_id, strategy_id) {
    const finishCount = await models.Finish.count({
      where: { strategy_id }
    }) || 0;
    if (finishCount > 2) {
      /*
      select *
      from checkpoint
      where id < :id
      order by id desc
      limit 1
      offset 1
      */
      return await models.Finish.findOne({
        raw: true,
        offset: finishCount % 2 ? 0 : 1,
        where: {
          strategy_id,
          id: { [Op.lt]: finish_id }
        },
        order: [['id', 'DESC']]
      });
    }

    return null;
  },

  async findFirstCheckpointInFinish(user_id, strategy_id, finish_id) {
    const checkpoints = await models.Checkpoint.findAll({
      raw: true,
      where: { strategy_id, finish_id }
    });

    if (checkpoints.length > 0) {
      const userCheckpoint = await models.UserCheckpoint.findOne({
        raw: true,
        where: {
          user_id,
          strategy_id,
          checkpoint_id: {
            [Op.in]: checkpoints.map(c => c.id)
          }
        },
        order: [['id', 'DESC']]
      });
      return userCheckpoint ? userCheckpoint.after : this.findFirstBaseBalance(strategy_id, user_id);
    } else {
      return this.findFirstBaseBalance(strategy_id, user_id);
    }
  },

  async findFirstBaseBalance(strategy_id, user_id) {
    const userCheckpoint = await models.UserCheckpoint.findOne({
      raw: true,
      where: { strategy_id, user_id },
      order: [['id', 'ASC']]
    });

    return userCheckpoint.before;
  },

  /**
   * Находим базовый баланс пользователя по чекпоинтам.
   * Базовым балансом является первый по списку чекпоинт
   * за прошлые 2 месяца.
   * @param point
   * @param user_id
   * @param strategy_id
   * @returns float
   */
  async findBaseBalanceFromCheckpoint(point, user_id, strategy_id) {
    const previousFinish = await this.findValidPreviousFinish(point.finish_id, strategy_id);
    if (previousFinish) {
      return this.findFirstCheckpointInFinish(user_id, strategy_id, previousFinish.id);
    } else {
      return this.findFirstBaseBalance(strategy_id, user_id);
    }
  }
};
