const models = require('../models');
const consts = require('../consts');
const sequelize = models.sequelize;

module.exports = {
  rawQuery(sql, replacements = {}) {
    return sequelize.query(sql, {
      type: sequelize.QueryTypes.SELECT,
      replacements
    });
  },

  /**
   * Получение данных для графика доходности стратегий (по чекпоинтам)
   *
   * @param user_id
   * @param strategy_id
   * @returns {Promise<void>}
   */
  async getStrategyChartData(user_id, strategy_id) {
    const genericChartsql = `
      select to_char(d, 'YYYY-MM') as date, COALESCE(sum("b"."percent"), 0) as value
      from generate_series(date('now') - interval '1 year', date('now'), interval '1 day') d
             left join lateral (
          select "b"."percent"
          from "user_checkpoint" "b"
          where
            date("b"."created_at") = d
            and "b"."user_id" = :user_id
            and "b"."strategy_id" = :strategy_id
          group by "b"."checkpoint_id", "b"."percent"
          ) b ON true
      group by date
      order by date;
    `;

    return await this.rawQuery(genericChartsql, {
      user_id,
      strategy_id
    });
  },

  /**
   * Получить данные для графика с переданным статусом
   *
   * @param status
   * @param period
   * @param interval (default: month)
   * @param userId
   * @returns {Promise<void>}
   */
  async getChartDataByStatus(status, period = 'year', interval = null, userId = null) {
    const format = this.getFormat(interval);

    let params = {
      status,
      format
    };

    let conditionByUser = '';

    if (userId) {
      params.userId = userId;
      conditionByUser = 'and "transaction"."user_id" = :userId'
    }

    const genericChartsql = `
      select
        to_char(d, :format) as date,
        COALESCE(sum("transaction"."amount"), 0) as value
      from
        generate_series(date('now') - interval '1 ${period}', date('now'), interval '1 day') d
        left join "transaction"
          on date("transaction"."created_at") = d
            and "transaction"."status" = :status
            ${conditionByUser}
      group by date
      order by date;
    `;

    return await this.rawQuery(genericChartsql, params);
  },

  /**
   * Получить данные для графика с переданным статусом
   *
   * @param period
   * @param interval (default: month)
   * @param userId
   * @returns {Promise<void>}
   */
  async getPartnershipChartData(period = 'year', interval = null, userId = null) {
    const format = this.getFormat(interval);

    let params = {
      account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
      format
    };

    let conditionByUser = '';

    if (userId) {
      params.userId = userId;
      conditionByUser = 'and "transaction"."user_id" = :userId'
    }

    const genericChartsql = `
      select
        to_char(d, :format) as date,
        COALESCE(sum("transaction"."amount"), 0) as value
      from
        generate_series(date('now') - interval '1 ${period}', date('now'), interval '1 day') d
        left join "transaction"
          on date("transaction"."created_at") = d
            and "transaction"."account_type" = :account_type
            ${conditionByUser}
      group by date
      order by date;
    `;

    return await this.rawQuery(genericChartsql, params);
  },

  /**
   * @param interval
   * @returns {Promise<void>}
   */
  async checkpoint(interval = null) {
    const genericChartsql = `
      select
        to_char(d, :format)                      as date,
        COALESCE(sum("checkpoint"."percent"), 0) as value
      from
        generate_series(date('now') - interval '1 year', date('now'), interval '1 day') d
          left join "checkpoint"
                    on date("checkpoint"."end_at") = d
      group by date
      order by date;
    `;

    return await this.rawQuery(genericChartsql, {
      format: this.getFormat(interval)
    });
  },

  /**
   * @param interval
   * @returns {Promise<void>}
   */
  async users(interval = null) {
    const genericChartsql = `
      select
        to_char(d, :format)             as date,
        COALESCE(count("user"."id"), 0) as value
      from
        generate_series(date('now') - interval '1 year', date('now'), interval '1 day') d
          left join "user"
                    on date("user"."created_at") = d
      group by date
      order by date;
    `;

    return await this.rawQuery(genericChartsql, {
      format: this.getFormat(interval)
    });
  },

  /**
   * Получить данные для графика количество BTC в каждой отдельной стратегии
   *
   * @returns {Promise<void>}
   */
  async getStrategiesPieChartData() {
    const genericChartsql = `
      select
        "strategy"."name",
        COALESCE(sum("user_checkpoint"."after"), 0) as value
      from
        "strategy"
          left join "user_checkpoint"
                    on "user_checkpoint"."strategy_id" = "strategy"."id"
      group by name;
    `;

    return await this.rawQuery(genericChartsql);
  },

  getFormat(interval) {
    let format;

    switch (interval) {
      case 'day':
        format = 'YYYY-MM-DD';
        break;
      case 'week':
        format = 'YYYY-MM-W';
        break;
      default:
        format = 'YYYY-MM';
    }

    return format;
  }
};
