const xl = require('excel4node');
const models = require('../models');
const jwt = require('../util/jwt');
const balance = require('./balance');
const consts = require('../consts');
const referral = require('./referral');
const { Op } = require('sequelize');

module.exports = {
  async getReportFor(user, strategy, from, to) {
    const where = {
      user_id: user.id,
      strategy_id: strategy.id,
      created_at: {
        [Op.between]: [from, to]
      },
      is_freeze: false
    };

    return {
      strategy,
      currency: await strategy.getCurrency(),
      status: await balance.getInactiveDepositBalance(where) === 0,
      deposit: await balance.getDepositBalance(where),
      percent: await models.Checkpoint.sum('percent', {
        where: {
          strategy_id: strategy.id,
          created_at: {
            [Op.between]: [from, to]
          }
        }
      }) || 0,
      fee: await models.Transaction.count({
        where: {
          user_id: user.id,
          strategy_id: strategy.id,
          status: consts.TRANSACTION_FUND_INCOME,
          created_at: {
            [Op.between]: [from, to]
          }
        }
      }) || 0,
      income_count: await models.Transaction.count({
        where: {
          user_id: user.id,
          strategy_id: strategy.id,
          status: consts.TRANSACTION_INCOME,
          created_at: {
            [Op.between]: [from, to]
          }
        }
      }) || 0,
      income: await balance.getIncomeBalance({
        user_id: user.id,
        strategy_id: strategy.id,
        created_at: {
          [Op.between]: [from, to]
        }
      })
    };
  },

  async getMonthReport(user, date = null) {
    // Продукт	Статус	Сумма	Процент	Доход	Комисиия	Итого
    const strategies = await models.Strategy.findAll();

    date = date || new Date();
    const from = new Date(date.getFullYear(), date.getMonth(), 1);
    const to = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    const result = [];
    for (let i = 0; i < strategies.length; i++) {
      result.push(await this.getReportFor(user, strategies[i], from, to));
    }

    return result;
  },

  async getYearReport(user, date = null) {
    // Продукт	Статус	Сумма	Процент	Доход	Комисиия	Итого
    const strategies = await models.Strategy.findAll();

    date = date || new Date();
    const from = new Date(date.getFullYear(), 1, 1);
    const to = new Date(date.getFullYear(), 12, 0);

    const result = [];
    for (let i = 0; i < strategies.length; i++) {
      for (let t = 1; t <= 12; t++) {
        const firstDayOfMonth = new Date(from.getFullYear(), t, 1);
        const lastDayOfMonth = new Date(to.getFullYear(), t + 1, 0);
        result.push({
          date: [
            firstDayOfMonth.getMonth(),
            firstDayOfMonth.getFullYear()
          ].join('.'),
          report: await this.getReportFor(
            user,
            strategies[i],
            firstDayOfMonth,
            lastDayOfMonth
          )
        });
      }
    }

    return result;
  },

  async downloadYearReport(user, year = null) {
    const wb = new xl.Workbook({
      defaultFont: { size: 14 }
    });

    const date = new Date();
    const ws = wb.addWorksheet(year || date.getFullYear());

    let line = 1;

    ws.cell(1, 1).string('Дата');
    ws.cell(1, 2).string('Продукт');
    ws.cell(1, 3).string('Статус');
    ws.cell(1, 4).string('Депозит');
    ws.cell(1, 5).string('Процент');
    ws.cell(1, 6).string('Доход');

    line++;

    const report = await this.getYearReport(user, year);
    for (let i = 0; i < report.length; i++) {
      const item = report[i];

      ws.cell(line, 1).string(item.date);
      ws.cell(line, 2).string(item.report.strategy.name);
      ws.cell(line, 3).string(item.report.status ? '+' : '');
      ws.cell(line, 4).number(item.report.deposit);
      ws.cell(line, 5).number(item.report.percent);
      ws.cell(line, 6).number(item.report.income);
      line++;
    }

    return wb;
  },

  async getMy(user_id) {
    const user = await models.User.findById(user_id);
    const date = new Date();
    const from = new Date(date.getFullYear(), date.getMonth(), 1);
    const to = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    return {
      detail: {
        month: {
          user: await models.Transaction.sum('amount', {
            where: {
              account_type: consts.ACCOUNT_TYPE_STRATEGY,
              user_id: user_id,
              created_at: {
                [Op.between]: [from, to]
              }
            }
          }) || 0,
          level_2: await models.Transaction.sum('amount', {
            where: {
              account_type: consts.ACCOUNT_TYPE_STRATEGY,
              created_at: {
                [Op.between]: [from, to]
              }
            },
            include: [
              {
                model: models.User,
                as: 'user',
                attributes: jwt.getPublicAttributes(),
                where: {
                  owner_id: user_id,
                  level: user.level + 1
                }
              }
            ],
            group: ['user.id']
          }) || 0,
          level_3: await models.Transaction.sum('amount', {
            where: {
              account_type: consts.ACCOUNT_TYPE_STRATEGY,
              created_at: {
                [Op.between]: [from, to]
              }
            },
            include: [
              {
                model: models.User,
                as: 'user',
                attributes: jwt.getPublicAttributes(),
                where: {
                  owner_id: user_id,
                  level: user.level + 2
                }
              }
            ],
            group: ['user.id']
          }) || 0
        },
        total: {
          user: await models.Transaction.sum('amount', {
            where: {
              account_type: consts.ACCOUNT_TYPE_STRATEGY,
              user_id: user_id
            }
          }) || 0,
          level_2: await models.Transaction.sum('amount', {
            where: {
              account_type: consts.ACCOUNT_TYPE_STRATEGY
            },
            include: [
              {
                model: models.User,
                as: 'user',
                attributes: jwt.getPublicAttributes(),
                where: {
                  parents: {
                    [Op.contains]: [user_id]
                  },
                  level: user.level + 1
                }
              }
            ],
            group: ['user.id']
          }) || 0,
          level_3: await models.Transaction.sum('amount', {
            where: {
              account_type: consts.ACCOUNT_TYPE_STRATEGY
            },
            include: [
              {
                model: models.User,
                as: 'user',
                attributes: jwt.getPublicAttributes(),
                where: {
                  owner_id: user_id,
                  level: user.level + 2
                }
              }
            ],
            group: ['user.id']
          }) || 0
        }
      },
      checkpoints: await models.UserCheckpoint.findAll({
        where: {
          user_id: user_id
        },
        include: [
          {
            model: models.Strategy,
            as: 'strategy',
            include: [
              {
                model: models.Currency,
                as: 'currency'
              }
            ]
          },
          {
            model: models.User,
            as: 'user',
            attributes: jwt.getPublicAttributes(),
          }
        ]
      }),
      transactions: await models.Transaction.findAll({
        where: {
          account_type: consts.ACCOUNT_TYPE_STRATEGY,
          user_id: user_id
        },
        include: [
          {
            model: models.Strategy,
            as: 'strategy',
            include: [
              {
                model: models.Currency,
                as: 'currency'
              }
            ]
          },
          {
            model: models.User,
            as: 'user',
            attributes: jwt.getPublicAttributes(),
          }
        ],
        order: [
          ['created_at', 'ASC']
        ]
      })
    };
  },

  async getTeam(user_id) {
    const date = new Date();
    const from = new Date(date.getFullYear(), date.getMonth(), 1);
    const to = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    return {
      detail: {
        month: {
          level_1: await this._getForLevel(1, user_id, {
            account_type: {
              [Op.in]: [
                consts.ACCOUNT_TYPE_STRATEGY,
                consts.ACCOUNT_TYPE_PARTNERSHIP
              ]
            },
            created_at: { [Op.between]: [from, to] }
          }),
          level_2: await this._getForLevel(2, user_id, {
            account_type: {
              [Op.in]: [
                consts.ACCOUNT_TYPE_STRATEGY,
                consts.ACCOUNT_TYPE_PARTNERSHIP
              ]
            },
            created_at: { [Op.between]: [from, to] }
          }),
          level_3: await this._getForLevel(3, user_id, {
            account_type: {
              [Op.in]: [
                consts.ACCOUNT_TYPE_STRATEGY,
                consts.ACCOUNT_TYPE_PARTNERSHIP
              ]
            },
            created_at: { [Op.between]: [from, to] }
          })
        },
        total: {
          level_1: await this._getForLevel(1, user_id, {
            account_type: {
              [Op.in]: [
                consts.ACCOUNT_TYPE_STRATEGY,
                consts.ACCOUNT_TYPE_PARTNERSHIP
              ]
            },
          }),
          level_2: await this._getForLevel(2, user_id, {
            account_type: {
              [Op.in]: [
                consts.ACCOUNT_TYPE_STRATEGY,
                consts.ACCOUNT_TYPE_PARTNERSHIP
              ]
            },
          }),
          level_3: await this._getForLevel(3, user_id, {
            account_type: {
              [Op.in]: [
                consts.ACCOUNT_TYPE_STRATEGY,
                consts.ACCOUNT_TYPE_PARTNERSHIP
              ]
            },
          })
        }
      },
      transactions: await models.Transaction.findAll({
        where: {
          account_type: {
            [Op.in]: [
              consts.ACCOUNT_TYPE_STRATEGY,
              consts.ACCOUNT_TYPE_PARTNERSHIP
            ]
          },
        },
        include: [
          {
            model: models.Strategy,
            as: 'strategy',
            include: [
              {
                model: models.Currency,
                as: 'currency'
              },
              {
                model: models.Currency,
                as: 'convert'
              }
            ]
          },
          {
            model: models.User,
            as: 'user',
            attributes: jwt.getPublicAttributes(),
            where: {
              parents: {
                [Op.contains]: [user_id]
              }
            }
          }
        ],
        order: [
          ['created_at', 'ASC']
        ]
      })
    };
  },

  async _fetchOneTime(user, level, where = {}) {
    return await models.Transaction.sum('amount', {
      where: {
        account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
        status: consts.TRANSACTION_ONE_TIME_BONUS,
        user_id: user.id,
        ...where
      },
      include: [
        {
          model: models.User,
          as: 'owner',
          attributes: jwt.getPublicAttributes(),
          where: {
            level: user.level + level
          }
        }
      ],
      group: ['owner.id']
    }) || 0;
  },

  async getOneTime(user_id) {
    const date = new Date();
    const from = new Date(date.getFullYear(), date.getMonth(), 1);
    const to = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    const user = await models.User.findById(user_id);

    return {
      detail: {
        month: {
          level_1: await this._fetchOneTime(user, 1, {
            created_at: { [Op.between]: [from, to] }
          }),
          level_2: await this._fetchOneTime(user, 2, {
            created_at: { [Op.between]: [from, to] }
          }),
          level_3: await this._fetchOneTime(user, 3, {
            created_at: { [Op.between]: [from, to] }
          })
        },
        total: {
          level_1: await this._fetchOneTime(user, 1),
          level_2: await this._fetchOneTime(user, 2),
          level_3: await this._fetchOneTime(user, 3)
        }
      },
      transactions: await models.Transaction.findAll({
        where: {
          account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
          status: consts.TRANSACTION_ONE_TIME_BONUS,
          user_id
        },
        include: [
          {
            model: models.Strategy,
            as: 'strategy',
            include: [
              { model: models.Currency, as: 'currency' }
            ]
          },
          {
            model: models.User,
            attributes: jwt.getPublicAttributes(),
            as: 'owner'
          }
        ],
        order: [
          ['created_at', 'DESC']
        ]
      })
    };
  },

  async getActivity(user_id) {
    const date = new Date();
    const from = new Date(date.getFullYear(), date.getMonth(), 1);
    const to = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    return {
      detail: {
        month: {
          level_1: await models.Transaction.sum('amount', {
            where: {
              status: consts.TRANSACTION_PAY_STATUS_BONUS,
              user_id: user_id,
              created_at: {
                [Op.between]: [from, to]
              }
            }
          }) || 0,
          level_2: 0,
          level_3: 0
        },
        total: {
          level_1: await models.Transaction.sum('amount', {
            where: {
              status: consts.TRANSACTION_PAY_STATUS_BONUS,
              user_id: user_id
            }
          }) || 0,
          level_2: 0,
          level_3: 0
        }
      },
      transactions: await models.Transaction.findAll({
        where: {
          status: consts.TRANSACTION_PAY_STATUS_BONUS,
          user_id: user_id
        },
        include: [
          {
            model: models.Strategy,
            as: 'strategy',
            include: [
              {
                model: models.Currency,
                as: 'currency'
              }
            ]
          },
          {
            model: models.User,
            as: 'user',
            attributes: jwt.getPublicAttributes(),
          },
          {
            model: models.User,
            as: 'owner',
            attributes: jwt.getPublicAttributes(),
          }
        ],
        order: [
          ['created_at', 'DESC']
        ]
      })
    };
  },

  async getActivityPro(user_id) {
    const date = new Date();
    const from = new Date(date.getFullYear(), date.getMonth(), 1);
    const to = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    return {
      detail: {
        month: {
          user: await models.Transaction.sum('amount', {
            where: {
              account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
              status: consts.TRANSACTION_PRO_BONUS,
              user_id,
              created_at: {
                [Op.between]: [from, to]
              }
            }
          }) || 0,
          level_2: 0,
          level_3: 0
        },
        total: {
          user: await models.Transaction.sum('amount', {
            where: {
              account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
              status: consts.TRANSACTION_PRO_BONUS,
              user_id
            }
          }) || 0,
          level_2: 0,
          level_3: 0
        }
      },
      transactions: await models.Transaction.findAll({
        where: {
          user_id,
          account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
          status: consts.TRANSACTION_PRO_BONUS
        },
        include: [
          {
            model: models.Franchise,
            as: 'franchise',
            include: [
              {
                model: models.Currency,
                as: 'currency'
              },
              {
                model: models.User,
                as: 'user',
                attributes: jwt.getPublicAttributes()
              }
            ]
          }
        ],
        order: [
          ['created_at', 'DESC']
        ]
      })
    };
  },

  async _getForLevel(level, user_id, where = {}) {
    const user = await models.User.findById(user_id);
    const users = await models.User.findAll({
      where: {
        parents: {
          [Op.contains]: [user_id]
        },
        level: user.level + level
      }
    });

    if (users.length === 0) {
      return 0;
    }

    return await models.Transaction.sum('amount', {
      where: {
        ...where,
        user_id: { [Op.in]: users.map(u => u.id) }
      }
    }) || 0
  },

  async _getIncome(level, user_id, where = {}) {
    const user = await models.User.findById(user_id);
    const users = await models.User.findAll({
      where: {
        parents: {
          [Op.contains]: [user_id]
        },
        level: user.level + level
      }
    });
    const ids = users.map(u => u.id);
    if (ids.length === 0) {
      return 0;
    }

    return await models.Transaction.sum('amount', {
      where: {
        ...where,
        user_id,
        owner_id: { [Op.in]: ids }
      }
    }) || 0
  },

  async getIncome(user_id) {
    const date = new Date();
    const from = new Date(date.getFullYear(), date.getMonth(), 1);
    const to = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    const transactions = await models.Transaction.findAll({
      where: {
        user_id: user_id,
        status: consts.TRANSACTION_PARTNER_INCOME
      },
      include: [
        {
          model: models.Strategy,
          as: 'strategy',
          include: [
            {
              model: models.Currency,
              as: 'currency'
            }
          ]
        },
        {
          model: models.User,
          as: 'user',
          attributes: jwt.getPublicAttributes(),
        },
        {
          model: models.UserCheckpoint,
          as: 'user_checkpoint'
        },
        {
          model: models.User,
          as: 'owner',
          attributes: jwt.getPublicAttributes(),
        }
      ],
      order: [
        ['created_at', 'DESC']
      ]
    });

    return {
      detail: {
        month: {
          level_1: await this._getIncome(1, user_id, {
            status: consts.TRANSACTION_PARTNER_INCOME,
            created_at: { [Op.between]: [from, to] }
          }),
          level_2: await this._getIncome(2, user_id, {
            status: consts.TRANSACTION_PARTNER_INCOME,
            created_at: { [Op.between]: [from, to] }
          }),
          level_3: await this._getIncome(3, user_id, {
            status: consts.TRANSACTION_PARTNER_INCOME,
            created_at: { [Op.between]: [from, to] }
          })
        },
        total: {
          level_1: await this._getIncome(1, user_id, {
            status: consts.TRANSACTION_PARTNER_INCOME
          }),
          level_2: await this._getIncome(2, user_id, {
            status: consts.TRANSACTION_PARTNER_INCOME
          }),
          level_3: await this._getIncome(3, user_id, {
            status: consts.TRANSACTION_PARTNER_INCOME
          })
        }
      },
      transactions
    };
  },

  _countUsersForLevel(where) {
    return models.User.count({ where }) || 0
  },

  async getForPartnership(user_id) {
    const user = await models.User.findById(user_id);
    const date = new Date();
    const from = new Date(date.getFullYear(), date.getMonth(), 1);
    const to = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    const referralUsers = await models.User.findAll({
      where: {
        parents: {
          [Op.contains]: [user_id]
        },
        level: {
          [Op.and]: [
            { [Op.lte]: user.level + 3 },
            { [Op.gte]: user.level }
          ]
        }
      },
      order: [['created_at', 'DESC']]
    });

    const users = [];
    for (let i = 0; i < referralUsers.length; i++) {
      const currentUser = referralUsers[i].toJSON();
      delete currentUser.password;
      delete currentUser.refresh_token;

      users.push({
        ...currentUser,
        referral_level: await referral.getCurrentLevel(currentUser)
      });
    }

    return {
      detail: {
        month: {
          total: await this._countUsersForLevel({
            parents: {
              [Op.contains]: [user_id]
            },
            created_at: {
              [Op.between]: [from, to]
            },
            level: {
              [Op.and]: [
                { [Op.lte]: user.level + 3 },
                { [Op.gte]: user.level }
              ]
            }
          }),
          level_1: await this._countUsersForLevel({
            parents: {
              [Op.contains]: [user_id]
            },
            created_at: {
              [Op.between]: [from, to]
            },
            level: user.level + 1
          }),
          level_2: await this._countUsersForLevel({
            parents: {
              [Op.contains]: [user_id]
            },
            created_at: {
              [Op.between]: [from, to]
            },
            level: user.level + 2
          }),
          level_3: await this._countUsersForLevel({
            parents: {
              [Op.contains]: [user_id]
            },
            created_at: {
              [Op.between]: [from, to]
            },
            level: user.level + 3
          })
        },
        total: {
          total: await this._countUsersForLevel({
            parents: {
              [Op.contains]: [user_id]
            },
            level: {
              [Op.and]: [
                { [Op.lte]: user.level + 3 },
                { [Op.gte]: user.level }
              ]
            }
          }),
          level_1: await this._countUsersForLevel({
            parents: {
              [Op.contains]: [user_id]
            },
            level: user.level + 1
          }),
          level_2: await this._countUsersForLevel({
            parents: {
              [Op.contains]: [user_id]
            },
            level: user.level + 2
          }),
          level_3: await this._countUsersForLevel({
            parents: {
              [Op.contains]: [user_id]
            },
            level: user.level + 3
          })
        }
      },
      users
    };
  }
};
