const { Op } = require('sequelize');
const models = require('../models');
const balance = require('./balance');
const consts = require('../consts');

module.exports = {
  /**
   * https://blockchaincorp.atlassian.net/browse/CL-134
   *
   *     Активность              Текущий месяц      Всего
   *     Мой объем средств        0.5 BTC            2.5 BTC
   *     Групповой объем средств  1 BTC              15 BTC
   *     Новые партнеры          2                  12
   *     Доход                    0.01 BTC          28 BTC
   *     Бонус за актив          0.1 BTC            3 BTC
   *     Выведено                0.5 BTC            18 BTC
   *
   * @param user_id
   * @param where
   * @returns {Promise<{amount: Number|*|number, referral: Number|*|number, partners: *, income: *, withdraw: *}>}
   */
  async getReport(user_id, where = {}) {
    return {
      user: await this.getUser(user_id, where),
      team: await this.getTeam(user_id, where),
      partners: await this.getPartners(user_id, where),
      income: await this.getIncome(user_id, where),
      onetime: await this.getOneTime(user_id, where),
      activity: await this.getActivity(user_id, where),
      activity_pro: await this.getActivityPro(user_id, where),
      withdraw: await this.getWithdraw(user_id, where)
    };
  },

  async getOneTime(user_id, where = {}) {
    return await models.Transaction.sum('amount', {
      where: {
        account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
        status: consts.TRANSACTION_ONE_TIME_BONUS,
        user_id,
        ...where
      }
    }) || 0;
  },

  async getUser(user_id, where = {}) {
    // Мой объем средств 0.5 BTC 2.5 BTC
    return await models.Transaction.sum('amount', {
      where: {
        account_type: consts.ACCOUNT_TYPE_STRATEGY,
        user_id,
        ...where
      }
    }) || 0;
  },

  async getTeam(user_id, where = {}) {
    const user = await models.User.findById(user_id);

    const users = await models.User.findAll({
      where: {
        parents: {
          [Op.contains]: [user_id]
        },
        level: {
          [Op.between]: [user.level, user.level + 3]
        }
      }
    });

    if (users.length === 0) {
      return 0;
    }

    // Групповой объем средств 1 BTC 15 BTC
    return await models.Transaction.sum('amount', {
      where: {
        ...where,
        account_type: {
          [Op.in]: [
            consts.ACCOUNT_TYPE_STRATEGY,
            consts.ACCOUNT_TYPE_PARTNERSHIP
          ]
        },
        user_id: { [Op.in]: users.map(u => u.id) }
      }
    }) || 0;
  },

  async getPartners(user_id, where = {}) {
    const user = await models.User.findById(user_id);

    // Новые партнеры	2	12
    return await models.User.count({
      where: {
        ...where,
        parents: { [Op.contains]: [user_id] },
        level: {
          [Op.and]: [
            { [Op.lte]: user.level + 3 },
            { [Op.gte]: user.level }
          ]
        }
      }
    });
  },

  async getIncome(user_id, where = {}) {
    // Доход 0.01 BTC	28 BTC
    const user = await models.User.findById(user_id);
    const users = await models.User.findAll({
      where: {
        parents: {
          [Op.contains]: [user_id]
        },
        level: {
          [Op.between]: [user.level + 1, user.level + 3]
        }
      }
    });
    const ids = users.map(u => u.id);
    if (ids.length === 0) {
      return 0;
    }

    return await models.Transaction.sum('amount', {
      where: {
        ...where,
        status: consts.TRANSACTION_PARTNER_INCOME,
        user_id,
        owner_id: { [Op.in]: ids }
      }
    }) || 0;
  },

  async getActivity(user_id, where = {}) {
    // Доход 0.01 BTC	28 BTC
    return await models.Transaction.sum('amount', {
      where: {
        ...where,
        user_id,
        account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
        status: consts.TRANSACTION_PAY_STATUS_BONUS
      }
    }) || 0;
  },

  /**
   * @param user_id
   * @param where
   * @returns {Promise<float>}
   */
  async getWithdraw(user_id, where) {
    // Выведено	0.5 BTC	18 BTC
    return await balance.getWithdrawBalance({ user_id, ...where });
  },

  async getActivityPro(user_id, where = {}) {
    return await models.Transaction.sum('amount', {
      where: {
        ...where,
        user_id,
        account_type: consts.ACCOUNT_TYPE_PARTNERSHIP,
        status: consts.TRANSACTION_PRO_BONUS
      }
    }) || 0;
  },

  async getMonthReport(user_id, month = null) {
    const date = new Date();
    const monthStart = new Date(date.getFullYear(), month || date.getMonth(), 1);
    const monthEnd = new Date(date.getFullYear(), month || date.getMonth() + 1, 0);

    return await this.getReport(user_id, {
      created_at: {
        [Op.between]: [monthStart, monthEnd]
      }
    });
  },

  async getTotalReport(user_id) {
    return await this.getReport(user_id);
  }
};
