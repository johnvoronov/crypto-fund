const { Op } = require('sequelize');
const models = require('../models');

module.exports = {
  /**
   * Поиск текущего уровня реферала
   *
   * @param user
   * @returns {Promise<*>}
   */
  async getCurrentLevel(user) {
    let ids = [
      user.buy_referral_level_id,
      user.minimal_referral_level_id,
      user.referral_level_id
    ];
    ids = ids.filter(value => value);
    if (ids.length === 0) {
      return null;
    }

    const levels = await models.Referral.findAll({
      where: { id: { [Op.in]: ids } }
    });

    return levels.reduce((prev, current) => prev.price > current.price ? prev : current);
  }
};
