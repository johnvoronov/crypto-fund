const speakeasy = require('speakeasy');

module.exports = {
  generate() {
    return speakeasy.generateSecret();
  },

  verify(user, token) {
    return speakeasy.totp.verify({ secret: user.two_factor_secret, encoding: 'base32', token });
  }
};
