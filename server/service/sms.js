const qs = require('qs');
const rp = require('request-promise');
const config = require('../config/app');

module.exports = {
  send(to, msg) {
    const query = qs.stringify({
      api_id: config.sms_key,
      json: 1,
      partner_id: 153212,
      to,
      msg,
    });

    return rp({
      uri: `https://sms.ru/sms/send?${query}`,
      json: true
    });
  }
};
