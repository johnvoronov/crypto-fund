const { parsePhoneNumber } = require('libphonenumber-js');

module.exports = {
  clean(phone) {
    return String(phone).replace(/([ ()\-])/gi, '');
  },

  validate(phone, defaultCountry = 'RU') {
    if (!phone) {
      return false;
    }

    phone = this.clean(phone);

    if (phone.length === 10) {
      phone = `7${phone}`;
    }

    if (phone.charAt(0) !== '+') {
      phone = `+${phone}`;
    }

    try {
      const phoneNumber = parsePhoneNumber(phone, defaultCountry);
      if (phoneNumber.isValid()) {
        return phoneNumber.number;
      }

      phone = this.clean(phone);
      console.log(phone);

      return false;
    } catch (e) {
      console.log(e);
      return false;
    }
  }
};
