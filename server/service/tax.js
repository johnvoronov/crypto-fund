const consts = require('../consts');
const transactionService = require('./transaction');
const models = require('../models');

module.exports = {
  async isTax(user_id, strategy_id, minimal = 50) {
    const user = await models.User.findById(user_id);

    if (user === null) {
      return false;
    }

    // Если пользователь не выплатил "таксу" в 50$
    const taxCount = await models.StrategyTax.count({
      where: { strategy_id, user_id }
    });
    if (taxCount === 0) {
      if (await this.shouldUnfreezeUser(user_id, strategy_id, minimal)) {
        await this.createFundTransaction(user_id, strategy_id, minimal);

        await models.StrategyTax.create({ strategy_id, user_id });
      } else {
        return false;
      }
    }

    const newTaxCount = await models.StrategyTax.count({
      where: { strategy_id, user_id }
    });
    if (newTaxCount > 0) {
      await this.updateTransactions(user_id, strategy_id);
    }

    return true;
  },

  async createFundTransaction(user_id, strategy_id, minimal) {
    const btcUserAmount = await models.Transaction.sum('amount', {
      where: {
        account_type: consts.ACCOUNT_TYPE_STRATEGY,
        user_id,
        strategy_id,
        status: consts.TRANSACTION_FREEZE
      }
    }) || 0;

    const btc = await models.Currency.findOne({
      where: { code: 'BTC' }
    });

    ///////////////// фиксируем вычет у пользователя и доход фонда

    const btcAmount = minimal * (1 / btc.to_usd);
    await transactionService.createTx({
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      amount: -btcAmount,
      user_id,
      strategy_id,
      status: consts.TRANSACTION_WRITE_OFF
    }, btc);
    await transactionService.createTx({
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      amount: btcAmount,
      user_id,
      strategy_id,
      status: consts.TRANSACTION_FUND_INCOME
    }, btc);

    /////////////////

    const txs = await models.Transaction.findAll({
      where: {
        account_type: consts.ACCOUNT_TYPE_STRATEGY,
        user_id,
        strategy_id,
        status: consts.TRANSACTION_FREEZE
      }
    });
    for (let i = 0; i < txs.length; i++) {
      await txs[i].destroy();
    }

    /////////////////

    const userAmount = btcUserAmount - btcAmount;
    await transactionService.createTx({
      account_type: consts.ACCOUNT_TYPE_STRATEGY,
      amount: userAmount,
      user_id,
      strategy_id,
      is_freeze: true,
      is_deposit: false,
      status: consts.TRANSACTION_PAYMENT
    }, btc);
  },

  async shouldUnfreezeUser(user_id, strategy_id, minimal = 50) {
    const amount = await models.Transaction.sum('usd', {
      where: {
        account_type: consts.ACCOUNT_TYPE_STRATEGY,
        user_id,
        strategy_id,
        status: consts.TRANSACTION_FREEZE
      }
    }) || 0;

    return amount >= minimal;
  },

  async updateTransactions(user_id, strategy_id) {
    const txs = await models.Transaction.findAll({
      where: {
        account_type: consts.ACCOUNT_TYPE_STRATEGY,
        user_id,
        strategy_id,
        status: consts.TRANSACTION_PAYMENT
      }
    });
    for (let i = 0; i < txs.length; i++) {
      await transactionService.createDeposit(txs[i]);
    }
  }
};
