const tableName = 'currency';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(tableName, null, {});
    await queryInterface.bulkInsert(tableName, [
      {
        id: 1,
        code: 'BTC',
        name: 'Bitcoin',
        to_usd: 6559.48,
        to_btc: 1,
        created_at: new Date,
        updated_at: new Date
      },
      {
        id: 2,
        code: 'USD',
        name: 'Dollar',
        to_usd: 1,
        to_btc: 1 / 6559.48,
        created_at: new Date,
        updated_at: new Date
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(tableName, null, {});
  }
};
