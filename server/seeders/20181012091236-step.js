const tableName = 'step';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(tableName, null, {});
    await queryInterface.bulkInsert(tableName, [
      {
        amount: 10,
        percent: 10,
        color: '#fff',
        name: 'test',
        strategy_id: 1
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(tableName, null, {});
  }
};
