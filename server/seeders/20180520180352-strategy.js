const models = require('../models');
const tableName = 'strategy';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(tableName, null, {});
    const btc = await models.Currency.findOne({ where: { code: 'BTC' } });
    const usd = await models.Currency.findOne({ where: { code: 'USD' } });
    await queryInterface.bulkInsert(tableName, [
      {
        id: 1,
        name: 'BitBTC',
        currency_id: btc.id,
        convert_id: btc.id,
        icon: 'fab fa-btc fa-fw',
        hero_icon: 'fab fa-bitcoin text-warning',
        position: 0,
        description_short: 'Базовая валюта - Bitcoin (BTC). Цель - увеличение количества криптовалюты биткоин. При значительном росте самой криптовалюты',
        description: 'Базовая валюта - Bitcoin (BTC). Цель - увеличение количества криптовалюты биткоин. При значительном росте самой криптовалюты, доход относительно доллара растет геометрически.',
        limit: JSON.stringify([
          { "limit": 50.01, "percent": 20 },
          { "limit": 10.01, "percent": 30 },
          { "limit": 1.01, "percent": 40 }
        ]),
        coinpayments: JSON.stringify({
          "publicKey": "a62d46686265514302b7c2cf680f8eed717367c01eadccd6160f783a4692010f",
          "secretKey": "6F67b95bee3b3308377e53a62E0D2e20c2ffcc202c64e1C859b24e39de81821d",
          "ipnSecret": "JtL2k2ADEwdSZ9P9",
          "merchantId": "faa7faa573b41774de29b5e58e60f390"
        }),
        features: ['Срок инвестиций от 1 месяца', 'Минимальная сумма 0.025 BTC', 'Неограниченный вывод'],
        caption: 'От 10% / мес.',
        items: JSON.stringify([{
          "label": "Арбитражная стратегия",
          "description": "Основная задача данной стратегии - планомерное и безрисковое увеличение депозита"
        }, {
          "label": "Стратегия \"Альткоины\"",
          "description": "Среднесрочная покупка альткоинов с целью получения прибыли за счет их стоимости относительно BTC"
        }, {
          "label": "Перспективные ICO",
          "description": "Анализ и покупка перспективных монет до того, как их можно будет приобрести на бирже "
        }]),
        created_at: new Date,
        updated_at: new Date
      },
      {
        id: 2,
        name: 'BitUSD',
        currency_id: usd.id,
        convert_id: btc.id,
        icon: 'fal fa-chess fa-fw',
        hero_icon: 'fal fa-chess text-success',
        description_short: 'Базовая валюта - Доллар США (USD). Доходность измеряется базовой валютой - доллар США. Цель - увеличение объема средств в долларах США, не зависимо от стоимости криптовалют.',
        description: 'Базовая валюта - Доллар США (USD). Доходность измеряется базовой валютой - доллар США. Цель - увеличение объема средств в долларах США, не зависимо от стоимости криптовалют.',
        position: 1,
        items: JSON.stringify([{
          "label": "Спекулятивная стратегия",
          "description": "Торговля на основе технического анализа позволяет зарабатывать и на растущем, и на падающем рынке"
        }, {
          "label": "Мат. и тех. анализ",
          "description": "Определение точек внутридневной волатильности по которым собирается прибыль"
        }, {
          "label": "Перспкетивные IPO",
          "description": "Анализ и покупка перспективных акций до того, как их можно будет приобрести на бирже"
        }]),
        features: ['Срок инвестиций от 1 месяца', 'Минимальная сумма $200', 'Неограниченный вывод'],
        caption: 'От 12% / мес.',
        coinpayments: JSON.stringify({
          "publicKey": "a62d46686265514302b7c2cf680f8eed717367c01eadccd6160f783a4692010f",
          "secretKey": "6F67b95bee3b3308377e53a62E0D2e20c2ffcc202c64e1C859b24e39de81821d",
          "ipnSecret": "JtL2k2ADEwdSZ9P9",
          "merchantId": "faa7faa573b41774de29b5e58e60f390"
        }),
        limit: JSON.stringify([
          { "limit": 50.01, "percent": 20 },
          { "limit": 10.01, "percent": 30 },
          { "limit": 1.01, "percent": 40 }
        ]),
        created_at: new Date,
        updated_at: new Date
      },
      {
        id: 3,
        name: 'Депозит',
        currency_id: usd.id,
        convert_id: btc.id,
        icon: 'fal fa-shield-alt fa-fw',
        hero_icon: 'fal fa-shield-alt text-danger',
        description_short: 'Базовая валюта - Доллар США (USD). Продукт с фиксированной ставкой в долларах США. Заморозка тела депозита на год с возможностью снимать % ежемесячно или реинвестировать в другие продукты.',
        description: 'Базовая валюта - Доллар США (USD). Продукт с фиксированной ставкой в долларах США. Заморозка тела депозита на год с возможностью снимать % ежемесячно или реинвестировать в другие продукты.',
        position: 2,
        features: ['Срок инвестиций от 1 года', 'Минимальная сумма $1000', 'Ежемесячный вывод %'],
        caption: 'От 24% / год',
        items: JSON.stringify([
          { "label": "Надежность", "description": "Не зависит от сложившейся на рынке ситуации" },
          { "label": "Фикс. ставка", "description": "Возможность вывода прибыли ежемесячно" },
          { "label": "Гибкость", "description": "Возможность реинвестировать прибыль в другие продукты" }
        ]),
        coinpayments: JSON.stringify({
          "publicKey": "a62d46686265514302b7c2cf680f8eed717367c01eadccd6160f783a4692010f",
          "secretKey": "6F67b95bee3b3308377e53a62E0D2e20c2ffcc202c64e1C859b24e39de81821d",
          "ipnSecret": "JtL2k2ADEwdSZ9P9",
          "merchantId": "faa7faa573b41774de29b5e58e60f390"
        }),
        limit: JSON.stringify([
          { "limit": 50.01, "percent": 20 },
          { "limit": 10.01, "percent": 30 },
          { "limit": 1.01, "percent": 40 }
        ]),
        created_at: new Date,
        updated_at: new Date
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(tableName, null, {});
  }
};
