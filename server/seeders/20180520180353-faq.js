const tableName = 'faq';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(tableName, null, {});
    await queryInterface.bulkInsert(tableName, [
      {
        question_ru: 'В каких странах действует фонд?',
        question_en: 'In which countries does the fund operate?',
        answer_ru: '«КриптоФонд» не имеет границ так как работает с применением блокчен технологий. Вы можете использовать платформу находясь в любой точке планеты и рекомендовать ее своим друзьям и знакомым из любых стран мира.',
        answer_en: '“CryptoFund” has no boundaries as it works with the use of block technology. You can use the platform from anywhere in the world and recommend it to your friends and acquaintances from all over the world.',
        created_at: new Date,
        updated_at: new Date
      },
      {
        question_ru: 'Что нужно для регистрации?',
        question_en: 'What is needed for registration?',
        answer_ru: 'Вам потребуется логин или электронная почта пригласившего вас человека и собственная электронная почта. Дополнительно, вам нужно будет придумать свой логин и указать страну и город в которых вы находитесь. После заполнения всех полей и поставив галочку напротив условий, вы можете зарегистрироваться. После того, как вы нажмете кнопку «Зарегистрироваться», вам на указанную почту будет отправлено активационное письмо. Перейдите по указанной в нем ссылке. Регистрация будет завершена, а вы получите на почту еще одно письма, с подтверждением.',
        answer_en: 'You will need a login or email of the person who invited you and your own email. Additionally, you will need to come up with your login and indicate the country and city in which you are located. After filling in all the fields and ticking the conditions, you can register. After you click the "Register" button, an activation letter will be sent to your specified email. Follow the link in it. Registration will be completed, and you will receive another email with confirmation.',
        created_at: new Date,
        updated_at: new Date
      },
      {
        question_ru: 'Как пополнить баланс?',
        question_en: 'How to replenish the balance?',
        answer_ru: 'Для пополнения баланса, перейдите в своем личном кабинете на страницу «Пополнение» и введите желаемую для пополнения сумму. Обратите внимание, вы можете пополнить баланс на любую сумму по своему усмотрению. При этом, в связи с колебаниями курса, на ваш счет будет зачислена сумма в USD по курсу BTC/USD (blockchain.info) на момент поступления средств на счет «Блокчейн Фонд». Учитывая загруженность сети Биткоин, подтверждение платежа может занимать от 10 минут до нескольких часов. Для того чтобы нивелировать колебания курса и получить на счет желаемую сумму вне зависимости от рынка, мы рекомендуем отправлять на 5% больше. Мы не берем никаких дополнительных комиссий, но вам следует учесть комиссию используемого процессингового сервиса Bitaps, равную 0.002 BTC с каждой обработанной транзакции, вне зависимости от объема. После ввода желаемой для пополнения суммы, вам будет предоставлена ссылка BTC кошелька, для пополнения вашего баланса. Переведите на него желаемую сумму, с учетом комиссии. После поступления средств на счет «Блокчейн Фонд», вы увидите их на своем балансе вашего личного кабинета, в разделе «Кошелек».',
        answer_en: 'To replenish the balance, go to the “Refill” page in your personal account and enter the amount you want to refill. Please note, you can replenish the balance by any amount at your discretion. At the same time, due to rate fluctuations, an amount in USD will be credited to your account at the rate of BTC / USD (blockchain.info) at the time of receipt of funds into the Blockchain Fund account. Given the workload of the Bitcoin network, payment confirmation can take from 10 minutes to several hours. In order to level the rate fluctuations and get into the account the desired amount regardless of the market, we recommend sending 5% more. We do not charge any additional fees, but you should consider the commission of the used Bitaps processing service, equal to 0.002 BTC for each processed transaction, regardless of the amount. After entering the desired amount to recharge, you will be given a BTC wallet link, to recharge your balance. Transfer to it the desired amount, taking into account the commission. After the funds are received in the Blockchain Fund account, you will see them on your balance in your personal account in the “Wallet” section.',
        created_at: new Date,
        updated_at: new Date
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(tableName, null, {});
  }
};
