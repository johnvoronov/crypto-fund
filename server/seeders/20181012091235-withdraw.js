const tableName = 'withdraw';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(tableName, null, {});
    await queryInterface.bulkInsert(tableName, [
      {
        amount: 10,
        transaction_hash: '9a2c425ab033f42d420e97fdc9a32efbf2663a13288d8092538c4ef6d4a1121b',
        transaction_id: null,
        user_id: 1,
        btc_address: '1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX',
        created_at: new Date,
        updated_at: new Date
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(tableName, null, {});
  }
};
