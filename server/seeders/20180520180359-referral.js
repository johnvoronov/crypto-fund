module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('referral', null, {});
    await queryInterface.bulkInsert('referral', [
      {
        name_en: 'Start',
        name_ru: 'Старт',
        team: 0,
        user: 0,
        price: 0,
        levels: [10],
        single: [1],
        pro: 0,
        user_count: 5,
        color: '#28a745'
      },
      {
        name_en: 'Standart',
        name_ru: 'Стандарт',
        team: 5,
        user: 0.5,
        levels: [15],
        user_count: 10,
        active: [10],
        price: 500,
        pro: 10,
        single: [2],
        opportunities_ru: '<h3>Возможности получаемые вместе со статусом</h3><ol><li>Посещение обучающих тренингов в офисе своего города</li><li>Посещение обучающих вебинаров</li><li>Доступ в закрытый чат</li></ol>',
        opportunities_en: '<h3>Opportunities obtained with the status</h3><ol><li>Attending training sessions in your city’s office</li><li>Attending training webinars</li><li>Access to private chat</li></ol>',
        description_ru: 'Данный статус позволяет получать повышенное и расширенное партнерское вознаграждение от лично приглашенных партнеров',
        description_en: 'This status allows you to receive increased and expanded partner reward from personally invited partners.',
        caption_ru: 'Начало пути партнера',
        caption_en: 'Start path partner',
        color: '#dc3545'
      },
      {
        name_en: 'Silver',
        name_ru: 'Серебряный',
        team: 15,
        user: 1,
        levels: [20, 5],
        user_count: 10,
        active: [20],
        price: 1500,
        pro: 20,
        single: [2, 0.5],
        opportunities_ru: '<h3>Возможности получаемые вместе со статусом</h3><ol><li>Посещение обучающих тренингов в офисе своего города</li><li>Посещение обучающих вебинаров</li><li>Доступ в закрытый чат</li><li>Приглашения на закрытые мероприятия</li><li>Нетворкинг с клиентами и партнёрами фонда в приватном формате</li></ol>',
        opportunities_en: '<h3>Opportunities obtained with the status</h3><ol><li>Attending training sessions in your city’s office</li><li>Attending training webinars</li><li>Access to private chat</li><li>Invitations to private events</li><li>Networking with clients and partners of the fund in a private format</li></ol>',
        description_ru: 'Данный статус позволяет получать повышенное и расширенное партнерское вознаграждение от лично приглашенных партнеров и партнеров второго поколения',
        description_en: 'This status allows you to receive increased and expanded partnership reward from personally invited partners and partners of the second generation',
        color: '#6c757d',
        caption_ru: 'Опытный партнер',
        caption_en: 'Experienced partner',
      },
      {
        name_en: 'Gold',
        name_ru: 'Золотой',
        team: 50,
        user: 2,
        levels: [25, 7, 3],
        user_count: 25,
        active: [25],
        price: 3000,
        single: [2, 1, 0.5],
        opportunities_ru: '<h3>Возможности получаемые вместе со статусом</h3><ol><li>Посещение обучающих тренингов в офисе своего города</li><li>Посещение обучающих вебинаров</li><li>Доступ в закрытый чат</li><li>Приглашения на закрытые мероприятия</li><li>Нетворкинг с клиентами и партнёрами фонда в приватном формате</li><li>Персональное наставничество опытного партнера</li><li>Приоритетное информирование о новых сервисах и возможностях фонда</li></ol>',
        opportunities_en: '<h3>Opportunities obtained with the status</h3><ol><li>Attending training sessions in your city’s office</li><li>Attending training webinars</li><li>Access to private chat</li><li>Invitations to private events</li><li>Networking with clients and partners of the fund in a private format</li><li>An experienced partner\'s personal coaching</li><li>Priority information on new services and opportunities of the fund</li></ol>',
        description_ru: 'Данный статус позволяет получать повышенное и расширенное партнерское вознаграждение от лично приглашенных партнеров, партнеров второго поколения и парнеров третьего поколения',
        description_en: 'This status allows you to receive an increased and expanded partnership reward from personally invited partners, second generation partners and third generation partners.',
        caption_ru: 'Профессионал своего дела',
        caption_en: 'Professional of his craft',
        pro: 25,
        color: '#f27e1b'
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('referral', null, {});
  }
};
