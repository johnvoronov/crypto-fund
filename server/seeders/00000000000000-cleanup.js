const models = require('../models');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    for (let name in models) {
      if (['sequelize', 'Setting'].indexOf(name) > -1) {
        continue;
      }

      const model = models[name];
      await queryInterface.bulkDelete(model.getTableName(), null, {});
    }
  },

  down: async (queryInterface, Sequelize) => {
  }
};
