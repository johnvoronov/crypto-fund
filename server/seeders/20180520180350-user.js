const password = require('../util/password');
const referral = require('../service/referral');
const tableName = 'user';

async function createUser(id, referral_id) {
  const extra = await referral.findReferral(referral_id);

  return {
    id,
    login: `user${id}`,
    email: `user${id}@demo.com`,
    password: await password.hash(`user${id}@demo.com`),
    two_factor_secret: 'GN3T4T22EZGWS2ZSJRNVC3J2HJ4FQXTE',
    is_active: true,
    is_admin: false,
    is_superuser: false,
    created_at: new Date,
    updated_at: new Date,
    ...extra
  };
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(tableName, null, {});
    await queryInterface.bulkInsert(tableName, [
      {
        id: 1,
        login: 'user',
        email: 'user@demo.com',
        password: await password.hash('user@demo.com'),
        two_factor_secret: 'GN3T4T22EZGWS2ZSJRNVC3J2HJ4FQXTE',
        is_active: true,
        is_admin: true,
        is_superuser: false,
        created_at: new Date,
        updated_at: new Date,
        level: 0,

        // extra part
        city: 'Киров',
        country: 'Россия',
        first_name: 'Олег',
        last_name: 'Газманов',
        phone: '88005553535',
        btc_address: '1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX'
      }
    ], {});
    await queryInterface.bulkInsert(tableName, [
      await createUser(2, 1)
    ], {});
    await queryInterface.bulkInsert(tableName, [
      await createUser(3, 2)
    ], {});
    await queryInterface.bulkInsert(tableName, [
      await createUser(4, 3)
    ], {});
    await queryInterface.bulkInsert(tableName, [
      await createUser(5, 2)
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(tableName, null, {});
  }
};
