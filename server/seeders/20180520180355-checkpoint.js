const tableName = 'checkpoint';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(tableName, null, {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(tableName, null, {});
  }
};
