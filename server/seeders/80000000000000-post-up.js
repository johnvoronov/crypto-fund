const models = require('../models');
const referralService = require('../service/referral');
const currencyUtil = require('../util/currency');
const sequelize = models.sequelize;

module.exports = {
  up: async (queryInterface, Sequelize) => {
    for (let name in models) {
      if (['sequelize', 'Setting'].indexOf(name) > -1) {
        continue;
      }

      const model = models[name];
      const tableName = model.getTableName();
      if (tableName === 'strategy_tax') {
        continue;
      }
      const sql = `SELECT setval('${tableName}_id_seq', COALESCE((SELECT MAX("${tableName}"."id")+1 FROM "${tableName}" AS "${tableName}"), 1), false);`;
      await sequelize.query(sql, {
        type: sequelize.QueryTypes.SELECT
      });
    }
    await currencyUtil.update();
    await referralService.updateUsers();
  },

  down: async (queryInterface, Sequelize) => {
  }
};
