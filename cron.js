const cron = require('node-cron');
const logger = require('./logger');
const currencyUtil = require('./server/util/currency');
const referralService = require('./server/service/referral');

cron.schedule('0 */3 * * *', async () => {
  try {
    logger.info('currency: update start');
    await currencyUtil.update();
    logger.info('currency: update success');
  } catch (error) {
    logger.info('currency: update error');
    logger.error(error);
  }
});


cron.schedule('0 0 * * *', async () => {
  try {
    logger.info('referral: findExpiredStatuses start');
    await referralService.findExpiredStatuses();
    logger.info('referral: findExpiredStatuses success');
  } catch (error) {
    logger.info('referral: findExpiredStatuses error');
    logger.error(error);
  }
});
