{{ 'mail.payment_success.body'|trans }}

{{ 'mail.payment_success.hash'|trans }}: {{ hash }}

{{ 'mail.payment_success.link'|trans }}: https://etherscan.io/tx/{{ hash }}
