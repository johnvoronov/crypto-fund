{{ 'mail.faq_question_subject'|trans }}: {{ question.subject }}
{{ 'mail.faq_question_question'|trans }}: {{ question.question }}
{{ 'mail.faq_question_user'|trans }}: {{ question.user.email }}
