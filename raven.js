const Raven = require('raven');
const {
    dsn,
    release,
    environment
} = require('./server/config/sentry');

if (dsn) {
    Raven
        .config(dsn, { release, environment })
        .install();
}
