require('./bootstrap');
const referralService = require('./server/service/referral');

async function main() {
  await referralService.updateUsers();
  process.exit(0);
}

main();
