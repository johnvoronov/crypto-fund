server:
	npm run start
.PHONY: server

migrate:
	npm run sequelize db:migrate
.PHONY: migrate

undo:
	npm run sequelize db:migrate:undo
.PHONY: undo

seed:
	npm run sequelize db:seed:all
.PHONY: demo

demo: migrate seed
.PHONY: demo
